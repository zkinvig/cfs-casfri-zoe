# Setup a server or VM for use with CASFRI

This folder contains the following Ansible playbook:

 - setup.yml

It, by default, will operate on the localhost to install PostgreSQL, GDAL,
and other requirements needed for running the CASFRI translation process.

Note that it currently expects to be operating on an Ubuntu 18.04 installation.

## Requirements

 - ansible (> 2.10)
 - ansible roles as listed in requirements.yml
 - ansible collections as listed in requirements.yml

# Before running

 Install Ansible (and update all packages) as follows:

  1. `sudo apt update`
  2. `sudo apt upgrade`
  3. `sudo apt install python3-pip` 
  4. You may need to logout/login to reset the paths
  5. `sudo pip3 install ansible==2.10.*`

 Install the required Ansible roles & collections as follows:

  1. `ansible-galaxy install -r requirements.yml`

## How to Run

 1) If desired, adjust the variables declared at the top of "setup.yml"

 2) Run `ansible-playbook setup.yml`

## After running setup

This will install the "casfri" user.  Subsequently login with the
"casfri" user for accessing the database or running scripts.

