
/*
This is a working script to evaluate and validate CASFRI translation of various FRI. Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: January 10, 2022 by Kangakola
Last Modified Date: April 13, 2022 by Kangakola
Last Modified Date: May 30, 2022 by Kangakola

*/

/*
=============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate  the layer 1 and layer 2 and Multiple attributes of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of this attribute between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Update: Integration of crownclosurecompare, heightcompare and siteclasscompare code. Also, add code for validation of dst, eco and nfl tables.	  
	*** Output: Multiple  table and views of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for qc04 for both layer 1 & 2. Relevant table are assessed visually to report any the relationship 
	     between this attributes for layer 1 and layer 2. Also doing some testing and problem solving as needed in the validation processes.
	*** Software: PgAdmin 4.6
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
 ================================================================================================================================================================

*/

/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/


/* LYR INVESTIGATION  */

---------- Sub-setting layer 1 --------------------------
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.test_rawfri_cas_l1_qc04;
CREATE TABLE casfri50_test.test_rawfri_cas_l1_qc04 AS
SELECT 
REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') AS coord1,
REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') AS coord2,
 c.cas_id,
 c.crown_closure_lower,
 c.crown_closure_upper,
 c.height_lower,
 c.height_upper,
 c.layer,
 c.layer_rank,
 c.origin_lower,
 c.origin_upper,
 c.productivity,
 c.productivity_type,
 c.site_class,
 c.site_index,
 c.soil_moist_reg,
 c.species_1,
 c.species_10,
 c.species_2,
 c.species_3,
 c.species_4,
 c.species_5,
 c.species_6,
 c.species_7,
 c.species_8,
 c.species_9,
 c.species_per_1,
 c.species_per_10,
 c.species_per_2,
 c.species_per_3,
 c.species_per_4,
 c.species_per_5,
 c.species_per_6,
 c.species_per_7,
 c.species_per_8,
 c.species_per_9,
 c.structure_per,
 c.structure_range,
 n.an_origine,
 n.an_perturb,
 n.an_pro_ori,
 n.an_pro_sou,
 n.an_saisie,
 n.cl_age,
 n.cl_dens,
 n.cl_drai,
 n.cl_haut,
 n.cl_pent,
 n.co_ter,
 n.correction,
 n.couv_gaule,
 n.dep_sur,
 n.dt_correct,
 n.et_domi,
 n.etagement,
 n.exercice,
 n.geoc_maj,
 n.geoc_maj_11_20,
 n.geoc_maj_1_10,
 n.geocode,
 n.gr_ess,
 n.in_essence,
 n.in_etage,
 n.in_maj,
 n.inventory_id,
 n.latitude,
 n.longitude,
 n.met_at_str,
 n.met_ori,
 n.met_prod,
 n.meta_no_prg,
 n.meta_ver_prg,
 n.no_inf_sur,
 n.no_prg,
 n.no_sec_int,
 n.nopert_pee,
 n.ogc_fid,
 n.or_cl_pent,
 n.origine,
 n.part_str,
 n.perturb,
 n.pro_ori,
 n.pro_sou,
 n.reb_ess1,
 n.reb_ess2,
 n.reb_ess3,
 n.resolution,
 n.shape_area,
 n.shape_length,
 n.source_maj,
 n.src_filename,
 n.statut_acq,
 n.statut_maj,
 n.strate,
 n.superficie,
 n.territoire,
 n.toponyme,
 n.ty_correct,
 n.type_couv,
 n.type_eco,
 n.type_ter,
 n.ver_prg
 --n.wkb_geometry

FROM rawfri.qc04 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	REPLACE(RTRIM(substring(cas_id, 22, 10), '+'), 'x', '') map_id,
	REPLACE(RTRIM(substring(cas_id, 33, 10), '+'), 'x', '') poly_id,
	--ltrim(split_part(cas_id, '-', 3), '+') AS map_id,	
	--ltrim(split_part(cas_id, '-', 4), '+') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'qc04%' and layer =1
) c
ON REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') = c.map_id::text AND REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') = c.poly_id::text;
--ON n.coord1 = c.map_id::text AND n.coord2 = c.poly_id::text;

-- Yeah that could work. Though geoc_maj has a comma in the second number, so it will probably be simpler to use the geoc_maj_1_10, 
-- and geoc_maj_11_20 columns as they have the comma replaced with a period as in the cas_Id

---------- Sub-setting layer 2 --------------------------
-----------------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.test_rawfri_cas_l2_qc04;
CREATE TABLE casfri50_test.test_rawfri_cas_l2_qc04 AS
SELECT 
REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') AS coord1,
REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') AS coord2,
c.cas_id,
 c.crown_closure_lower,
 c.crown_closure_upper,
 c.height_lower,
 c.height_upper,
 c.layer,
 c.layer_rank,
 c.origin_lower,
 c.origin_upper,
 c.productivity,
 c.productivity_type,
 c.site_class,
 c.site_index,
 c.soil_moist_reg,
 c.species_1,
 c.species_10,
 c.species_2,
 c.species_3,
 c.species_4,
 c.species_5,
 c.species_6,
 c.species_7,
 c.species_8,
 c.species_9,
 c.species_per_1,
 c.species_per_10,
 c.species_per_2,
 c.species_per_3,
 c.species_per_4,
 c.species_per_5,
 c.species_per_6,
 c.species_per_7,
 c.species_per_8,
 c.species_per_9,
 c.structure_per,
 c.structure_range,
 n.an_origine,
 n.an_perturb,
 n.an_pro_ori,
 n.an_pro_sou,
 n.an_saisie,
 n.cl_age,
 n.cl_dens,
 n.cl_drai,
 n.cl_haut,
 n.cl_pent,
 n.co_ter,
 n.correction,
 n.couv_gaule,
 n.dep_sur,
 n.dt_correct,
 n.et_domi,
 n.etagement,
 n.exercice,
 n.geoc_maj,
 n.geoc_maj_11_20,
 n.geoc_maj_1_10,
 n.geocode,
 n.gr_ess,
 n.in_essence,
 n.in_etage,
 n.in_maj,
 n.inventory_id,
 n.latitude,
 n.longitude,
 n.met_at_str,
 n.met_ori,
 n.met_prod,
 n.meta_no_prg,
 n.meta_ver_prg,
 n.no_inf_sur,
 n.no_prg,
 n.no_sec_int,
 n.nopert_pee,
 n.ogc_fid,
 n.or_cl_pent,
 n.origine,
 n.part_str,
 n.perturb,
 n.pro_ori,
 n.pro_sou,
 n.reb_ess1,
 n.reb_ess2,
 n.reb_ess3,
 n.resolution,
 n.shape_area,
 n.shape_length,
 n.source_maj,
 n.src_filename,
 n.statut_acq,
 n.statut_maj,
 n.strate,
 n.superficie,
 n.territoire,
 n.toponyme,
 n.ty_correct,
 n.type_couv,
 n.type_eco,
 n.type_ter,
 n.ver_prg
 --n.wkb_geometry

FROM rawfri.qc04 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	REPLACE(RTRIM(substring(cas_id, 22, 10), '+'), 'x', '') map_id,
	REPLACE(RTRIM(substring(cas_id, 33, 10), '+'), 'x', '') poly_id,
	--ltrim(split_part(cas_id, '-', 3), '+') AS map_id,	
	--ltrim(split_part(cas_id, '-', 4), '+') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'qc04%' and layer =2
) c
ON REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') = c.map_id::text AND REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') = c.poly_id::text;
*/
---- SELECT Count(*) from lyr_all where cas_id ILIKE 'qc04%' AND layer = 2; -----241


-------------- ----------------     INVESTIGATION  -------------------------------------------------------------------------------------


-- 1. 	CHECK FOR DUPLICATE CAS_ID


--- test unique cas_id
DROP VIEW IF EXISTS qc04_duplicated_cas_id;
CREATE VIEW casfri50_test.qc04_duplicated_cas_id AS
SELECT cas_id, COUNT(*)
FROM casfri50.cas_all 
WHERE cas_id ILIKE 'qc04%'
GROUP BY cas_id
HAVING COUNT(*) > 1;


SELECT cas_id, COUNT(*)
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'qc04%'
GROUP BY cas_id
HAVING COUNT(*) > 1;

SELECT cas_id, COUNT(*)
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'qc04%'
GROUP BY cas_id
HAVING COUNT(*) > 1 ORDER BY COUNT DESC;


-- CHECK IF duplicated value of geoc_maj for qc04
SELECT geoc_maj, COUNT(*)
FROM rawfri.qc04
GROUP BY geoc_maj
HAVING COUNT(*) > 1 ORDER BY COUNT DESC;

SELECT geocode, COUNT(*)
FROM rawfri.qc04
GROUP BY geocode
HAVING COUNT(*) > 1 ORDER BY COUNT DESC; --TOO MANY DUPLICATED ENTRY.



-- 2. CHECK SPECIES TRANSLATED AS PER QC SPECIES LOOKUP TABLE.

DROP VIEW IF EXISTS  casfri50_test.view_qc04_species_traslation_l1;
CREATE VIEW casfri50_test.view_qc04_species_traslation_l1 AS
SELECT  cas_id,species_1,species_per_1,species_2,species_per_2,species_3,species_per_3,gr_ess
FROM casfri50_test.test_rawfri_cas_l1_qc04;

DROP VIEW IF EXISTS  casfri50_test.view_qc04_species_traslation_l2;
CREATE VIEW casfri50_test.view_qc04_species_traslation_l2 AS
SELECT  cas_id,species_1,species_per_1,species_2,species_per_2,species_3,species_per_3,gr_ess
FROM casfri50_test.test_rawfri_cas_l2_qc04;


------------------------- SPECIES TRANSLATION ERRORS ----------------------------


DROP VIEW IF EXISTS  casfri50_test.view_qc04_species_traslation_errors_l1;
CREATE VIEW casfri50_test.view_qc04_species_traslation_errors AS
SELECT  cas_id,species_1,species_per_1,species_2,species_per_2,species_3,species_per_3,gr_ess
FROM casfri50_test.test_rawfri_cas_l1_qc04
WHERE species_1 = 'NULL_VALUE' OR species_1 = 'TRANSLATION_ERROR';


DROP VIEW IF EXISTS  casfri50_test.view_qc04_species_traslation_errors_l2;
CREATE VIEW casfri50_test.view_qc04_species_traslation_errors_l2 AS
SELECT  cas_id,species_1,species_per_1,species_2,species_per_2,species_3,species_per_3,gr_ess
FROM casfri50_test.test_rawfri_cas_l2_qc04
WHERE species_1 = 'NULL_VALUE' OR species_1 = 'TRANSLATION_ERROR';


--SELECT COUNT(*) FROM lyr_all where height_upper<height_lower; --- 0 rows

--SELECT COUNT(*) FROM nfl_all where height_upper<height_lower; --- 0 rows

---- height
DROP VIEW IF EXISTS casfri50_test.view_qc04_height_error_l1;
CREATE VIEW  casfri50_test.view_qc04_height_error_l1 AS
SELECT DISTINCT left(cas_id, 4) inv, string_agg(layer::text, ', ' ORDER BY layer) layers, 
                string_agg(cl_haut::text, ', ') height, string_agg(height_lower::text, ', ') height_lower,
				string_agg(height_upper::text, ', ') height_upper 
FROM casfri50_test.test_rawfri_cas_l1_qc04 
GROUP BY cas_id ORDER BY height ASC;


DROP VIEW IF EXISTS casfri50_test.view_qc04_height_error_l2;
CREATE VIEW  casfri50_test.view_qc04_height_error_l2 AS
SELECT DISTINCT left(cas_id, 4) inv, string_agg(layer::text, ', ' ORDER BY layer) layers, 
                string_agg(cl_haut::text, ', ') height, string_agg(height_lower::text, ', ') height_lower,
				string_agg(height_upper::text, ', ') height_upper 
FROM casfri50_test.test_rawfri_cas_l2_qc04 
GROUP BY cas_id ORDER BY height ASC;


---- crown closure
DROP VIEW IF EXISTS casfri50_test.view_qc04_crownClosure_error_l1;
CREATE VIEW casfri50_test.view_qc04_crownClosure_error_l1 AS
SELECT DISTINCT left(cas_id, 4) inv, string_agg(layer::text, ', ' ORDER BY layer) layers, 
                string_agg(cl_dens::text, ', ') CrownClosure,
				string_agg(cl_age::text, ', ') Age,
				string_agg(crown_closure_lower::text, ', ') crown_closure_lower,
				string_agg(crown_closure_upper::text, ', ') crown_closure_upper 
FROM casfri50_test.test_rawfri_cas_l1_qc04 
GROUP BY cas_id ORDER BY CrownClosure ASC;

DROP VIEW IF EXISTS casfri50_test.view_qc04_crownClosure_error_l2;
CREATE VIEW casfri50_test.view_qc04_crownClosure_error_l2 AS
SELECT DISTINCT left(cas_id, 4) inv, string_agg(layer::text, ', ' ORDER BY layer) layers, 
                string_agg(cl_dens::text, ', ') CrownClosure,
				string_agg(cl_age::text, ', ') Age,
				string_agg(crown_closure_lower::text, ', ') crown_closure_lower,
				string_agg(crown_closure_upper::text, ', ') crown_closure_upper 
FROM casfri50_test.test_rawfri_cas_l2_qc04 
GROUP BY cas_id ORDER BY CrownClosure ASC;


---- crown closure
DROP VIEW IF EXISTS casfri50_test.view_qc04_crownClosure_photo_error_l1;
CREATE VIEW casfri50_test.view_qc04_crownClosure_photo_error_l1 AS
SELECT DISTINCT left(cas_id, 4) inv, string_agg(layer::text, ', ' ORDER BY layer) layers, 
                string_agg(an_saisie::text, ', ') photoyear,
				string_agg(cl_age::text, ', ') Age,				
				string_agg(origin_lower::text, ', ') origin_lower,
				string_agg(origin_upper::text, ', ') Origin_upper
				
FROM casfri50_test.test_rawfri_cas_l1_qc04 
GROUP BY cas_id ORDER BY photoyear ASC;

DROP VIEW IF EXISTS casfri50_test.view_qc04_crownClosure_photo_error_l2;
CREATE VIEW casfri50_test.view_qc04_crownClosure_photo_error_l2 AS
SELECT DISTINCT left(cas_id, 4) inv, string_agg(layer::text, ', ' ORDER BY layer) layers, 
                string_agg(an_saisie::text, ', ') photoyear,
				string_agg(cl_age::text, ', ') Age,				
				string_agg(origin_lower::text, ', ') origin_lower,
				string_agg(origin_upper::text, ', ') Origin_upper
				
FROM casfri50_test.test_rawfri_cas_l2_qc04 
GROUP BY cas_id ORDER BY photoyear ASC;
*

---- Investigate rows not joined
SELECT COUNT(*)  FROM casfri50.lyr_all WHERE cas_id ILIKE 'qc04%' and layer =1; 
SELECT COUNT(*)  FROM casfri50.lyr_all WHERE cas_id ILIKE 'qc04%' and layer =2; 
SELECT COUNT(*)  FROM casfri50_test.test_rawfri_cas_l1_qc04; 
SELECT COUNT(*)  FROM casfri50_test.test_rawfri_cas_l2_qc04; 

---- number of rows in each layer for FRI and translated data not matching as layer is dropped or order changed
---- at the moment it is validated using spot check but can be automated applying layer dropping or layer ordering constraint

--- FIND DATA MISSING IN TRANSLATED CASFRI DATA*/

SELECT COUNT(*)  FROM casfri50.lyr_all WHERE cas_id ILIKE 'qc04%' and layer =1 AND cas_id NOT IN (SELECT cas_id FROM casfri50_test.test_rawfri_cas_l1_qc04); ---31465

SELECT *  FROM lyr_all WHERE cas_id ILIKE 'qc04%' and layer =1 AND cas_id NOT IN (SELECT cas_id FROM casfri50_test.test_rawfri_cas_l1_qc04);


DROP VIEW IF EXISTS casfri50_test.qc04_l1_from_fri_cas_missing_VIEW;
CREATE VIEW casfri50_test.qc04_l1_from_fri_cas_missing_VIEW AS
SELECT *  FROM casfri50.lyr_all WHERE cas_id ILIKE 'qc04%' and layer =1 AND cas_id NOT IN (SELECT cas_id FROM casfri50_test.test_rawfri_cas_l1_qc04);

DROP VIEW IF EXISTS casfri50_test.qc04_l2_from_fri_cas_missing_VIEW;
CREATE VIEW casfri50_test.qc04_l2_from_fri_cas_missing_VIEW AS
SELECT *  FROM casfri50.lyr_all WHERE cas_id ILIKE 'qc04%' and layer =2 AND cas_id NOT IN (SELECT cas_id FROM casfri50_test.test_rawfri_cas_l2_qc04);

DROP VIEW IF EXISTS casfri50_test.view_qc04_l1_from_fri_cas_missing;
CREATE VIEW casfri50_test.view_qc04_l1_from_fri_cas_missing AS
SELECT *
FROM casfri50_test.qc04_l1_from_fri_cas_missing;

DROP VIEW IF EXISTS casfri50_test.view_qc04_l2_from_fri_cas_missing;
CREATE VIEW casfri50_test.view_qc04_l2_from_fri_cas_missing AS
SELECT *
FROM casfri50_test.qc04_l2_from_fri_cas_missing;



/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/


/* DST INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.disturbance_rawfri_cas_qc04;
CREATE TABLE casfri50_test.disturbance_rawfri_cas_qc04 AS
SELECT 
REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') AS coord1,
REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') AS coord2,
 c.cas_id,
 c.dist_type_1,
 c.dist_year_1,
 c.dist_ext_upper_1,
 c.dist_ext_lower_1,
 n.origine,
 n.an_origine,
 n.perturb,
 n.an_perturb,
 --n.geoc_maj,
 n.geoc_maj_11_20,
 n.geoc_maj_1_10
 
FROM rawfri.qc04 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	REPLACE(RTRIM(substring(cas_id, 22, 10), '+'), 'x', '') map_id,
	REPLACE(RTRIM(substring(cas_id, 33, 10), '+'), 'x', '') poly_id,
	--ltrim(split_part(cas_id, '-', 3), '+') AS map_id,	
	--ltrim(split_part(cas_id, '-', 4), '+') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.dst_all WHERE cas_id ILIKE 'qc04%' 
) c
ON REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') = c.map_id::text AND REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') = c.poly_id::text;


/*   ================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/

/* ECO INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.eco_rawfri_cas_qc04;
CREATE TABLE casfri50_test.eco_rawfri_cas_qc04 AS
SELECT 
REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') AS coord1,
REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') AS coord2,
 c.cas_id,
 c.wetland_type,
 c.wet_veg_cover,
 c.wet_landform_mod,
 c.wet_local_mod,
 c.eco_site,
 c.layer,
 n.co_ter,
 n.type_eco,
 n.type_ter,
 --n.cl_drain,
 n.gr_ess,
 n.cl_dens,
 n.cl_haut
 --n.ver_prg
 --n.wkb_geometry

FROM rawfri.qc04 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	REPLACE(RTRIM(substring(cas_id, 22, 10), '+'), 'x', '') map_id,
	REPLACE(RTRIM(substring(cas_id, 33, 10), '+'), 'x', '') poly_id,
	--ltrim(split_part(cas_id, '-', 3), '+') AS map_id,	
	--ltrim(split_part(cas_id, '-', 4), '+') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.eco_all WHERE cas_id ILIKE 'qc04%' 
) c
ON REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') = c.map_id::text AND REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') = c.poly_id::text;
--ON n.coord1 = c.map_id::text AND n.coord2 = c.poly_id::text;


/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/




/* NFL INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.nfl_rawfri_cas_l1_qc04;
CREATE TABLE casfri50_test.nfl_rawfri_cas_l1_qc04 AS
SELECT 
REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') AS coord1,
REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') AS coord2,
 c.cas_id,
 c.crown_closure_lower,
 c.crown_closure_upper,
 c.height_lower,
 c.height_upper,
 c.layer,
 c.nat_non_veg,
 c.non_for_anth,
 c.non_for_veg,
 n.co_ter,
 --n.ty_correct,
 --n.type_couv,
 --n.type_eco,
 n.type_ter
 --n.ver_prg
 --n.wkb_geometry

FROM rawfri.qc04 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	REPLACE(RTRIM(substring(cas_id, 22, 10), '+'), 'x', '') map_id,
	REPLACE(RTRIM(substring(cas_id, 33, 10), '+'), 'x', '') poly_id,
	--ltrim(split_part(cas_id, '-', 3), '+') AS map_id,	
	--ltrim(split_part(cas_id, '-', 4), '+') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.nfl_all WHERE cas_id ILIKE 'qc04%' 
) c
ON REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') = c.map_id::text AND REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') = c.poly_id::text;
--ON n.coord1 = c.map_id::text AND n.coord2 = c.poly_id::text;


/*   ==================================================================================

     5.    STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 

    ==================================================================================
*/


/* STAND STRUCTURE INVESTIGATION */


DROP TABLE IF EXISTS casfri50_test.stand_structure_qc04;
CREATE TABLE casfri50_test.stand_structure_qc04 AS
SELECT stand_structure, count(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'QC04%' 
GROUP BY stand_structure;



/* NFL LAYER COUNT  INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.nfl_layer_count_qc04;
CREATE TABLE casfri50_test.nfl_layer_count_qc04 AS
SELECT layer, count(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'QC04%' 
GROUP BY layer;



