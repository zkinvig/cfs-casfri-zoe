/*
This is a working script to evaluate and validate CASFRI translation of various FRI. Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: April 05, 2022 by Kangakola
Last Modified Date: May 30, 2022 by Kangakola

*/

/*
=============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate  the layer 1 and layer 2 and several attributes (Species, Height, Crown closure, Origin & Productivity) of layer 1 and layer 2 in CASFRI translated data. 
	   Then assess the accuracy of these attributes between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for YT03 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between these attributes for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
	
 ================================================================================================================================================================

*/

/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/


/* LYR INVESTIGATION  */
---------- Sub-setting layer 1 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 were used then they are common infromation to both layers 
-----------------------------------------------------------------

DROP TABLE  IF EXISTS casfri50_test.yt03_rawfri_cas_l1;
CREATE TABLE casfri50_test.yt03_rawfri_cas_l1 AS
SELECT
c.cas_id,
--c.soil_moist_reg,
--c.structure_per,
--c.structure_range,
c.layer,
c.layer_rank,
--c.crown_closure_upper,
--c.crown_closure_lower,
--c.height_upper,
--c.height_lower,
--c.productivity,
--c.productivity_type,
c.species_1,
c.species_per_1,
c.species_2,
c.species_per_2,
c.species_3,
c.species_per_3,
c.species_4,
c.species_per_4,
--c.species_5,
--c.species_per_5,
--c.species_6,
--c.species_per_6,
--c.species_7,
--c.species_per_7,
--c.species_8,
--c.species_per_8,
--c.species_9,
--c.species_per_9,
--c.species_10,
--c.species_per_10,
--c.origin_upper,
--c.origin_lower,
--c.site_class,
--c.site_index,
--c.ogc_fid,
--n.anthropogenic,
--n.anthropogenic_cover_percent,
--n.cover_type,
--n.cover_type_class,
--n.data_source,
--n.data_source_year,
--n.disturbance1,
--n.disturbance1_code,
--n.disturbance1_extent,
--n.disturbance1_extent_percent
--n.disturbance1_layer,
--n.disturbance1_year,
--n.disturbance2,
--n.disturbance2_code,
--n.disturbance2_extent,
--n.disturbance2_extent_percent,
--n.disturbance2_layer,
--n.disturbance2_year,
--n.disturbance3,
--n.disturbance3_code,
--n.disturbance3_extent,
--n.disturbance3_extent_percent,
--n.disturbance3_layer,
--n.disturbance3_year,
--n.disturbance4,
--n.disturbance4_code,
--n.disturbance4_extent,
--n.disturbance4_extent_percent,
--n.disturbance4_layer,
--n.disturbance4_year,
--n.exposed_land,
--n.exposed_land_cover_percent,
--n.field_plot,
--n.inventory_id,
--n.inventory_year,
--n.landscape_position,
--n.layer1_age_yrs,
--n.layer1_average_height_metres,
--n.layer1_cover_pattern,
--n.layer1_crown_closure_percent,
--n.layer1_density_stems_per_ha,
--n.layer1_est_site_index,
--n.layer1_est_site_index_source,
--n.layer1_est_site_index_species,
--n.layer1_maximum_height_metres,
--n.layer1_minimum_height_metres,
n.layer1_species1,
n.layer1_species1_code,
n.layer1_species1_percent,
n.layer1_species2,
n.layer1_species2_code,
n.layer1_species2_percent,
n.layer1_species3,
n.layer1_species3_code,
n.layer1_species3_percent,
n.layer1_species4,
n.layer1_species4_code,
n.layer1_species4_percent,
--n.layer1_vertical_complexity,
--n.layer2_age_yrs,
--n.layer2_average_height_metres,
--n.layer2_cover_pattern,
--n.layer2_crown_closure_percent,
--n.layer2_density_stems_per_ha,
--n.layer2_est_site_index,
--n.layer2_est_site_index_source,
--n.layer2_est_site_index_species,
--n.layer2_maximum_height_metres,
--n.layer2_minimum_height_metres,
--n.layer2_species1,
--n.layer2_species1_code,
--n.layer2_species1_percent,
--n.layer2_species2,
--n.layer2_species2_code,
--n.layer2_species2_percent,
--n.layer2_species3,
--n.layer2_species3_code,
--n.layer2_species3_percent,
--n.layer2_species4,
--n.layer2_species4_code,
--n.layer2_species4_percent,
--n.layer2_vertical_complexity,
--n.non_shrub_cover_pattern,
--n.non_shrub_cover_percent,
--n.non_shrub_type,
--n.nts_map50k,
n.ogc_fid,
--n.photo_year,
--n.shape_area,
--n.shape_length,
--n.shrub_cover_pattern,
--n.shrub_cover_percent,
--n.shrub_type,
--n.site_class,
--n.site_index,
--n.site_index_decimal,
--n.slope_position,
--n.snow_ice,
--n.snow_ice_cover_percent,
--n.soil_moisture_regime,
--n.soil_nutrient_regime,
--n.src_filename,
--n.stand_structure,
--n.stratum,
n.unique_id
--n.water,
--n.water_cover_percent,
--n.wetland_class,
--n.wetland_landform_modifie,r
--n.wetland_local_modifier,
--n.wetland_vegetation_modifier

FROM rawfri.yt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS nts_map50k,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS unique_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'yt03%' AND layer = 1
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.unique_id:: TEXT = c.unique_id AND n.ogc_fid:: TEXT = c.ogc_fid;


---------- Sub-setting layer 2 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 used in both layers 
-----------------------------------------------------------------

DROP TABLE  IF EXISTS casfri50_test.yt03_rawfri_cas_l2;
CREATE TABLE casfri50_test.yt03_rawfri_cas_l2 AS
SELECT
c.cas_id,
--c.soil_moist_reg,
--c.structure_per,
--c.structure_range,
c.layer,
c.layer_rank,
--c.crown_closure_upper,
--c.crown_closure_lower,
--c.height_upper,
--c.height_lower,
--c.productivity,
--c.productivity_type,
c.species_1,
c.species_per_1,
c.species_2,
c.species_per_2,
c.species_3,
c.species_per_3,
c.species_4,
c.species_per_4,
--c.species_5,
--c.species_per_5,
--c.species_6,
--c.species_per_6,
--c.species_7,
--c.species_per_7,
--c.species_8,
--c.species_per_8,
--c.species_9,
--c.species_per_9,
--c.species_10,
--c.species_per_10,
--c.origin_upper,
--c.origin_lower,
--c.site_class,
--c.site_index,
--c.cas_id,
--c.crown_closure_lower,
--c.crown_closure_upper,
--c.height_lower,
--c.height_upper,
--c.layer,
--c.layer_rank,
--c.origin_lower,
--c.origin_upper,
--c.productivity,
--c.productivity_type,
--c.site_class,
--c.site_index,
--c.soil_moist_reg,
--c.species_1,
--c.species_10,
--c.species_2,
--c.species_3,
--c.species_4,
--c.species_5,
--c.species_6,
--c.species_7,
--c.species_8,
--c.species_9,
--c.species_per_1,
--c.species_per_10,
--c.species_per_2,
--c.species_per_3,
--c.species_per_4,
--c.species_per_5,
--c.species_per_6,
--c.species_per_7,
--c.species_per_8,
--c.species_per_9,
--c.structure_per,
--c.structure_range,
 --c.poly_num,
 --c.ogc_fid,
--n.anthropogenic,
--n.anthropogenic_cover_percent,
--n.cover_type,
--n.cover_type_class,
--n.data_source,
--n.data_source_year,
--n.disturbance1,
--n.disturbance1_code,
--n.disturbance1_extent,
--n.disturbance1_extent_percent
--n.disturbance1_layer,
--n.disturbance1_year,
--n.disturbance2,
--n.disturbance2_code,
--n.disturbance2_extent,
--n.disturbance2_extent_percent,
--n.disturbance2_layer,
--n.disturbance2_year,
--n.disturbance3,
--n.disturbance3_code,
--n.disturbance3_extent,
--n.disturbance3_extent_percent,
--n.disturbance3_layer,
--n.disturbance3_year,
--n.disturbance4,
--n.disturbance4_code,
--n.disturbance4_extent,
--n.disturbance4_extent_percent,
--n.disturbance4_layer,
--n.disturbance4_year,
--n.exposed_land,
--n.exposed_land_cover_percent,
--n.field_plot,
--n.inventory_id,
--n.inventory_year,
--n.landscape_position,
--n.layer1_age_yrs,
--n.layer1_average_height_metres,
--n.layer1_cover_pattern,
--n.layer1_crown_closure_percent,
--n.layer1_density_stems_per_ha,
--n.layer1_est_site_index,
--n.layer1_est_site_index_source,
--n.layer1_est_site_index_species,
--n.layer1_maximum_height_metres,
--n.layer1_minimum_height_metres,
--n.layer1_species1,
--n.layer1_species1_code,
--n.layer1_species1_percent,
--n.layer1_species2,
--n.layer1_species2_code,
--n.layer1_species2_percent,
--n.layer1_species3,
--n.layer1_species3_code,
--n.layer1_species3_percent,
--n.layer1_species4,
--n.layer1_species4_code,
--n.layer1_species4_percent,
--n.layer1_vertical_complexity,
--n.layer2_age_yrs,
--n.layer2_average_height_metres,
--n.layer2_cover_pattern,
--n.layer2_crown_closure_percent,
--n.layer2_density_stems_per_ha,
--n.layer2_est_site_index,
--n.layer2_est_site_index_source,
--n.layer2_est_site_index_species,
--n.layer2_maximum_height_metres,
--n.layer2_minimum_height_metres,
n.layer2_species1,
n.layer2_species1_code,
n.layer2_species1_percent,
n.layer2_species2,
n.layer2_species2_code,
n.layer2_species2_percent,
n.layer2_species3,
n.layer2_species3_code,
n.layer2_species3_percent,
n.layer2_species4,
n.layer2_species4_code,
n.layer2_species4_percent,
--n.layer2_vertical_complexity,
--n.non_shrub_cover_pattern,
--n.non_shrub_cover_percent,
--n.non_shrub_type,
--n.nts_map50k,
n.ogc_fid,
--n.photo_year,
--n.shape_area,
--n.shape_length,
--n.shrub_cover_pattern,
--n.shrub_cover_percent,
--n.shrub_type,
--n.site_class,
--n.site_index,
--n.site_index_decimal,
--n.slope_position,
--n.snow_ice,
--n.snow_ice_cover_percent,
--n.soil_moisture_regime,
--n.soil_nutrient_regime,
--n.src_filename,
--n.stand_structure,
--n.stratum,
n.unique_id
--n.water,
--n.water_cover_percent,
--n.wetland_class,
--n.wetland_landform_modifie,r
--n.wetland_local_modifier,
--n.wetland_vegetation_modifier

FROM rawfri.yt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS nts_map50k,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS unique_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'yt03%' AND layer = 2
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.unique_id:: TEXT = c.unique_id  AND n.ogc_fid:: TEXT = c.ogc_fid;

--===============================================================================

--   SPECIES COMPARE

----- LAYER 1 -------------------------------

DROP TABLE  IF EXISTS casfri50_test.yt03_species_compare_l1;
CREATE TABLE casfri50_test.yt03_species_compare_l1 AS
SELECT
cas_id,
species_1,
species_per_1,
species_2,
species_per_2,
layer1_species1,
layer1_species1_code,
layer1_species1_percent,
layer1_species2,
layer1_species2_code,
layer1_species2_percent
FROM casfri50_test.yt03_rawfri_cas_l1;


-----LAYER 2

DROP TABLE  IF EXISTS casfri50_test.yt03_species_compare_l2;
CREATE TABLE casfri50_test.yt03_species_compare_l2 AS
SELECT
cas_id,
species_1,
species_per_1,
species_2,
species_per_2,
layer2_species1,
layer2_species1_code,
layer2_species1_percent,
layer2_species2,
layer2_species2_code,
layer2_species2_percent
FROM casfri50_test.yt03_rawfri_cas_l2;


-- HEIGHT COMPARISON
-------------- Investigation ---------------
--  Ensures all height_upper (L1) is greater than height_uper (l2) in Lyr_all table
-- comparing height_upper for l1 and l2

DROP TABLE IF EXISTS casfri50_test.yt03_Height_upper_compare_L1_l2;
CREATE TABLE casfri50_test.yt03_Height_upper_compare_L1_l2 AS
WITH a AS(SELECT cas_id, height_upper, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'yt03%' AND layer = 1),
b AS(SELECT cas_id, height_upper, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'yt03%' AND layer = 2)
SELECT a.cas_id, a.height_upper l1_height_upper, b.height_upper l2_height_upper FROM a
LEFT JOIN b
ON a.cas_id = b.cas_id
WHERE a.height_upper < b.height_upper;  -- top layer height is gt that low layer


-- comparing height_lower for l1 and l2

DROP TABLE IF EXISTS casfri50_test.yt03_Height_lower_compare_L1_l2;
CREATE TABLE casfri50_test.yt03_Height_lower_compare_L1_l2 AS
WITH a AS(SELECT cas_id, height_lower, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'yt03%' AND layer = 1),
b AS(SELECT cas_id, height_lower, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'yt03%' AND layer = 2)
SELECT a.cas_id, a.height_lower l1_height_lower, b.height_lower l2_height_lower FROM a
LEFT JOIN b
ON a.cas_id = b.cas_id
WHERE a.height_lower < b.height_lower;  -- top layer height lower is gt that l2 height lower

------- Crown closure -------------------

--- This code which is working in earlier release but not working now. This is becasue CASFRI layer is now reordered based on 
-- the height and does not corresponds to original inventories layer number. Once the layer in the original inventory can be 
-- implemented same contraint as implemented in the height_bc function of CASFRI then the code will work for this comparison.
----

DROP TABLE IF EXISTS casfri50_test.yt03_crown_closure_upper_compare_L1_l2;
CREATE TABLE casfri50_test.yt03_crown_closure_upper_compare_L1_l2 AS
WITH a AS(SELECT cas_id, crown_closure_upper, layer FROM casfri50.lyr_all WHERE cas_id = 'yt03%' AND layer = 1),
b AS(SELECT cas_id, crown_closure_upper, layer FROM casfri50.lyr_all WHERE cas_id = 'yt03%' AND layer = 2)
SELECT a.cas_id, a.crown_closure_upper l1_crown_closure_upper, b.crown_closure_upper l2_crown_closure_upper FROM a
LEFT JOIN b
ON a.cas_id = b.cas_id
WHERE a.crown_closure_upper < b.crown_closure_upper;  -- top layer height is gt that low layer count : 0


-- comparing crown_closure_lower for l1 and l2
DROP TABLE IF EXISTS casfri50_test.yt03_crown_closure_lower_compare_L1_l2;
CREATE TABLE casfri50_test.yt03_crown_closure_lower_compare_L1_l2 AS
WITH a AS(SELECT cas_id, crown_closure_lower, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'yt03%' AND layer = 1),
b AS(SELECT cas_id, crown_closure_lower, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'yt03%' AND layer = 2)
SELECT a.cas_id, a.crown_closure_lower l1_crown_closure_lower, b.crown_closure_lower l2_crown_closure_lower FROM a
LEFT JOIN b
ON a.cas_id = b.cas_id
WHERE a.crown_closure_lower < b.crown_closure_lower;  -- top layer crown_closure lower is gt that l2 crown_closure lower


/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/




/* DST INVESTIGATION */

DROP TABLE  IF EXISTS casfri50_test.disturbance_yt03_rawfri_cas;
CREATE TABLE casfri50_test.disturbance_yt03_rawfri_cas AS
SELECT
c.cas_id,
c.dist_type_1,
c.dist_type_2,
--c.dist_type_3,
--c.dist_type_4,
n.disturbance1,
n.disturbance1_code,
--n.disturbance1_extent,
--n.disturbance1_extent_percent
--n.disturbance1_layer,
--n.disturbance1_year,
n.disturbance2,
n.disturbance2_code,
--n.disturbance2_extent,
--n.disturbance2_extent_percent,
--n.disturbance2_layer,
--n.disturbance2_year,
--n.disturbance3,
--n.disturbance3_code,
--n.disturbance3_extent,
--n.disturbance3_extent_percent,
--n.disturbance3_layer,
--n.disturbance3_year,
--n.disturbance4,
--n.disturbance4_code,
--n.disturbance4_extent,
--n.disturbance4_extent_percent,
--n.disturbance4_layer,
--n.disturbance4_year,
n.ogc_fid,
n.unique_id


FROM rawfri.yt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS nts_map50k,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS unique_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.dst_all WHERE cas_id ILIKE 'yt03%' 
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.unique_id:: TEXT = c.unique_id  AND n.ogc_fid:: TEXT = c.ogc_fid;


/*   ================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/




/* ECO INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.eco_rawfri_cas_qc05;
CREATE TABLE casfri50_test.eco_rawfri_cas_qc05 AS
SELECT 
REPLACE(substring(n.geoc_maj, 1, 10), ',', '.') AS coord1,
REPLACE(substring(n.geoc_maj, 11, 10), ',', '.') AS coord2,
 c.cas_id,
 c.wetland_type,
 c.wet_veg_cover,
 c.wet_landform_mod,
 c.wet_local_mod,
 c.eco_site,
 c.layer,
n.water,
n.water_cover_percent,
n.wetland_class,
n.wetland_landform_modifier,
n.wetland_local_modifier,
n.wetland_vegetation_modifier

FROM rawfri.yt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS nts_map50k,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS unique_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.nfl_all WHERE cas_id ILIKE 'yt03%' 
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.unique_id:: TEXT = c.unique_id  AND n.ogc_fid:: TEXT = c.ogc_fid;


/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/


/* NFL INVESTIGATION */


DROP TABLE  IF EXISTS casfri50_test.nfl_yt03_rawfri_cas;
CREATE TABLE casfri50_test.nfl_yt03_rawfri_cas AS
SELECT
c.cas_id,
--c.soil_moist_reg,
--c.structure_per,
--c.structure_range,
c.layer,
c.layer_rank,
c.crown_closure_upper,
c.crown_closure_lower,
c.height_upper,
c.height_lower,
c.nat_non_veg,
c.non_for_anth,
c.non_for_veg,
n.anthropogenic,
n.anthropogenic_cover_percent,
n.cover_type,
n.exposed_land,
n.exposed_land_cover_percent,
n.layer2_vertical_complexity,
n.non_shrub_cover_pattern,
n.non_shrub_cover_percent,
n.non_shrub_type,
n.nts_map50k,
n.ogc_fid,
n.shrub_cover_pattern,
n.shrub_cover_percent,
n.shrub_type,
n.unique_id,
n.water,
n.water_cover_percent,
n.wetland_class,
--n.wetland_landform_modifie,
n.wetland_local_modifier,
n.wetland_vegetation_modifier

FROM rawfri.yt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS nts_map50k,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS unique_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.nfl_all WHERE cas_id ILIKE 'yt03%' 
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.unique_id:: TEXT = c.unique_id  AND n.ogc_fid:: TEXT = c.ogc_fid;


/*   ==================================================================================

     5.    STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 

    ==================================================================================
*/


/* STAND STRUCTURE INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.stand_structure_yt03;
CREATE TABLE casfri50_test.stand_structure_yt03 AS
SELECT stand_structure, count(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'YT03%' 
GROUP BY stand_structure;


/* NFL LAYER COUNT INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.nfl_layer_count_yt03;
CREATE TABLE casfri50_test.nfl_layer_count_yt03 AS
SELECT layer, count(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'YT03%' 
GROUP BY layer;


