-- getSpeciesCutoff(inventory, attribute_dependencies_location)
-- Given an inventory, gives the species index of the first species that isn't recorded by the raw inventory
-- Input:
	-- inventory: the inventory id that records species
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
-- Output: INTEGER index of first non recorded species
	-- Returns 0 if all 10 species are recorded
CREATE OR REPLACE FUNCTION getSpeciesCutoff(
inventory TEXT,
attribute_dependencies_location TEXT)
RETURNS INTEGER AS
	$func$
	DECLARE 
		cutoff INTEGER;
	BEGIN
		EXECUTE format('SELECT (CASE WHEN species_10 != '''' THEN 0
						WHEN species_1 = '''' THEN 1
						WHEN species_1 != '''' AND species_2 = '''' THEN 2
						WHEN species_2 != '''' AND species_3 = '''' THEN 3
						WHEN species_3 != '''' AND species_4 = '''' THEN 4
						WHEN species_4 != '''' AND species_5 = '''' THEN 5
						WHEN species_5 != '''' AND species_6 = '''' THEN 6
						WHEN species_6 != '''' AND species_7 = '''' THEN 7
						WHEN species_7 != '''' AND species_8 = '''' THEN 8
						WHEN species_8 != '''' AND species_9 = '''' THEN 9
						WHEN species_9 != '''' AND species_10 = '''' THEN 10 
						ELSE 0 END) AS cutoff
						FROM %s
						WHERE inventory_id = ''%s'' AND layer = ''1''', attribute_dependencies_location, inventory) INTO cutoff;	
		RETURN cutoff;
	END;
	$func$
LANGUAGE 'plpgsql';


-- singleSpeciesRowValidity(inventory, species_num, species, species_per, species_string, source_layer)
-- Given a species in a particular row, determines if that species row was translated correctly or not, and what type of error it is
-- Does not check that actual species mapping, just that the inventory was actually recording that species index
-- Only for inventories where there is a single species string that is translated into species_1 through species_10
-- Input:
	-- inventory: the inventory that records species
	-- species_num: the index of the species to check
	-- species: the value of species at species_num index
	-- species_per: the value of species_per at species_num index
	-- species_string: the raw string representation of the species composition
	-- source_layer: the layer the row was translated from
-- Output: TEXT description of the validity of the species
	-- returns either VALID, FALSE_NULL, FALSE_MAPPED, or UNKNOWN
	-- VALID indicates that the species at this index was translated correctly
	-- FALSE_NULL indicates that the species was mapped to a empty error code when it should have been mapped to a species
	-- FALSE_MAPPED indicates that the species was mapped to a species when it should have been mapped to am empty error code
	-- An UNKNOWN value may indicate that the function does not contain an applicable rule for this special case and the fucntion needs to be updated, or that a true translation error occurred
CREATE OR REPLACE FUNCTION singleSpeciesRowValidity( 
inventory TEXT,
species_num INTEGER,
species TEXT,
species_per TEXT,
species_string TEXT,
source_layer TEXT)
RETURNS TEXT AS
	$func$
	DECLARE 
		species_count INTEGER;
		len INTEGER;
		i INTEGER;
		temp_result TEXT;
		bool_result BOOLEAN;
	BEGIN
		IF left(inventory, 2) = 'QC'
		THEN
			len = length(trim(species_string));
			IF species_string IS NULL OR len = 0
			THEN 
				species_count := 0;
			ELSIF inventory IN ('QC05', 'QC10')
			THEN
				FOR i IN 1..10
				LOOP
					bool_result := tt_matchtable(tt_qc_prg5_species(species_string, i::text), 'translation', 'species_code_mapping', 'qc_species_codes');
					IF bool_result = FALSE
					THEN
						species_count := i - 1;
						EXIT;
					ELSIF i = 10
					THEN
						species_count := 10;
						EXIT;
					END IF;
				END LOOP;
			ELSIF inventory IN ('QC03', 'QC08')
			THEN
				IF inventory = 'QC08' AND source_layer = '2'
				THEN 
					species_count := 0;
				ELSE
					SELECT num_of_species::integer INTO species_count FROM "translation".qc_ini03_species_lookup WHERE source_val = species_string;
				END IF;
			-- QC04 and QC09 has a very complicated species mapping, must use helper function to determine the number of species in a species string
			ELSIF inventory IN ('QC04', 'QC09')
			THEN
				IF inventory = 'QC09' AND source_layer = '2'
				THEN 
					species_count := 0;
				ELSIF TT_qc_prg4_lengthMatchList(species_string, '{2, 4, 6}') = FALSE
				THEN
					species_count := -9997;
				ELSE
					FOR i IN 1..10
					LOOP
						temp_result := "translation".TT_qc_prg4_species(species_string, i::text);
						IF temp_result IS NULL
						THEN
							species_count := i - 1;
							EXIT;
						ELSIF i = 10
						THEN
							species_count := 10;
							EXIT;
						END IF;
					END LOOP;
				END IF;
			ELSE
				species_count := -9997;
			END IF;
		ELSE
			species_string := regexp_replace(species_string, '[[:digit:]]', '', 'g');
			species_string := trim(replace(species_string, ' ', ''));
			IF inventory = 'ON02' 
			THEN
				species_string = replace(species_string, 'Red', 'Re');
			END IF;
			len = length(species_string);
			IF species_string IS NULL OR len = 0
			THEN 
				species_count := 0;
			ELSIF mod(len, 2) = 0 AND len >= 2
			THEN 
				species_count := (len/2)::integer;
			ELSE
				species_count := -9997;
			END IF;
		END IF;
		IF species_count IS NOT NULL 
		THEN
			IF (species_count != -9997 AND species_num <= species_count) 
			AND (isError(species) = FALSE AND isError(species_per) = FALSE)
			THEN
				RETURN 'VALID';
			ELSIF (species_count != -9997 AND species_num <= species_count) 
			AND (isError(species) = TRUE OR isError(species_per) = TRUE)
			THEN 
				RETURN 'FALSE_NULL';
			ELSIF (species_count = -9997 OR species_num > species_count) 
			AND (isError(species) = FALSE OR isError(species_per) = FALSE)
			THEN 
				RETURN 'FALSE_MAPPED';
			ELSIF (species_count = -9997 OR species_num > species_count) 
			AND (isError(species) = TRUE AND isError(species_per) = TRUE)
			THEN 
				RETURN 'VALID';
			END IF;
		END IF;
		RETURN 'UNKNOWN';		
	END;
	$func$
LANGUAGE 'plpgsql';


-- runSpeciesValidationOnInventory(dest_table_name, inventory, creation_schema, attribute_dependencies_location)
-- Given a specific inventory, runs species validations on the inventory and stores flagged errors in the dest_table
-- Collects any generic validation errors, species that are mapped past the maximum possible species number for that inventory, 
-- and species that were either mapped when a raw value doesn't exist, or mapped to an error code when a raw value does exists
-- Input:
	-- dest_table_name: the name of the table to store flagged errors in
	-- inventory: the inventory that records species
	-- creation_schema: the schema that the dest_table and raw_joined tables are in
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
-- Output: VOID 
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runSpeciesValidationOnInventory(
dest_table_name TEXT,
inventory TEXT,
creation_schema TEXT, 
attribute_dependencies_location TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		dynamic_sql TEXT;
		raw_merged_table TEXT;
		species_cutoff INTEGER;
		loop_end INTEGER;
		i INTEGER; 
		lyr INTEGER;
		spec_dependency TEXT;
		spec_per_dependency TEXT;
		spec_1_length INTEGER;
		spec_length INTEGER;
		lyr_end INTEGER;
	BEGIN
		raw_merged_table := inventory || '_raw_lyr_all_joined';
		RAISE NOTICE 'Run Species Validation- (%) Finding species null errors for inventory %', LEFT(clock_timestamp()::TEXT, 19), inventory;
		EXECUTE format('SELECT * FROM getSpeciesCutoff(''%s'', ''%s'')', inventory, attribute_dependencies_location) INTO species_cutoff;
		IF species_cutoff = 0 
		THEN 
		-- Species in CASFRI go up to 10
			loop_end = 10;
		ELSE 
			loop_end = species_cutoff-1;
		END IF;
		IF left(inventory,2) = 'NS' OR inventory IN ('QC03', 'QC04')
		THEN
			-- NS has a possibly incorrect mapping for layer 2, only checking layer 1 for now
			-- QC03 and QC04 don't map species for layer 2, only check layer 1
			lyr_end := 1;
		ELSE
			-- Layers in CASFRI go up to 9
			lyr_end := 9;
		END IF;
		-- These inventories have a simple separated species mapping so every column can be checked individually
		IF inventory IN ('AB29','NB02','NB03','NB04','MB06','MB07','MB10','MB12') OR left(inventory, 2) IN ('BC', 'YT', 'PE', 'NS')
		THEN 
			RAISE NOTICE 'Run Species Validation- (%) 	Inventory % uses separated species mappings', LEFT(clock_timestamp()::TEXT, 19), inventory;
			FOR i IN 1..loop_end
			LOOP
				RAISE NOTICE 'Run Species Validation- (%) 		Checking species_%', LEFT(clock_timestamp()::TEXT, 19), i;
				FOR lyr IN 1..lyr_end
				LOOP
					EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''species_%s'', ''%s'', %s, ''lyr_all'')', inventory, i, attribute_dependencies_location, lyr) INTO spec_dependency;
					EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''species_per_%s'', ''%s'', %s, ''lyr_all'')', inventory, i, attribute_dependencies_location, lyr) INTO spec_per_dependency;
					EXIT WHEN spec_dependency IS NULL OR spec_per_dependency IS NULL;
					IF left(inventory, 2) = 'NT' AND i = 1 AND trim(spec_dependency) != ''
					THEN 
						-- NT species_1 has a redundant extra term in the mapping
						spec_dependency := trim(split_part(spec_dependency, 1));
						spec_per_dependency := trim(split_part(spec_per_dependency, 1));
					END IF;
					IF trim(spec_dependency) = ''
					THEN
						-- Catch falsely mapped species when a dependency doesn't exist
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''lyr_all'' AS table_name, ''SPECIES_ERROR'' AS error_type, ''species_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(species_%s::text) = FALSE AND source_layer::text = ''%s'')', 
										creation_schema, dest_table_name, i, creation_schema, raw_merged_table, i, lyr);
					ELSE
						-- Catch falsely null species when a species could have been mapped
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''lyr_all'' AS table_name, ''SPECIES_ERROR'' AS error_type, ''species_%s was null when it could have been mapped from raw'' AS error_desc, source_layer
										FROM %s.%s
										WHERE (species_%s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'')) AND trim(coalesce(raw_%s::text, ''NULL'')) NOT IN ('''', ''NULL'', ''0'') AND source_layer::text = ''%s'')', 
										creation_schema, dest_table_name, i, creation_schema, raw_merged_table, i, spec_dependency, lyr);
						-- Catch falsely mapped species when the raw value is null
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''lyr_all'' AS table_name, ''SPECIES_ERROR'' AS error_type, ''species_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(species_%s::text) = FALSE AND trim(coalesce(raw_%s::text, ''NULL'')) IN ('''', ''NULL'', ''0'') AND source_layer::text = ''%s'')', 
										creation_schema, dest_table_name, i, creation_schema, raw_merged_table, i, spec_dependency, lyr);
					
					END IF;
					IF trim(spec_per_dependency) = ''
					THEN
						-- Catch falsely mapped species_per when a dependency doesn't exist
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''lyr_all'' AS table_name, ''SPECIES_ERROR'' AS error_type, ''species_per_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(species_per_%s::text) = FALSE AND source_layer::text = ''%s'')', 
										creation_schema, dest_table_name, i, creation_schema, raw_merged_table, i, lyr);
					ELSE
						-- Catch falsely null species_per when a species_per could have been mapped
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''lyr_all'' AS table_name, ''SPECIES_ERROR'' AS error_type, ''species_per_%s was null when it could have been mapped from raw'' AS error_desc, source_layer
										FROM %s.%s
										WHERE (species_per_%s::text IN (''-8888'', ''-9999'', ''-8886'')) AND trim(coalesce(raw_%s::text, ''NULL'')) NOT IN ('''', ''NULL'', ''0'') AND source_layer::text = ''%s'')', 
										creation_schema, dest_table_name, i, creation_schema, raw_merged_table, i, spec_per_dependency, lyr);
						-- Catch falsely mapped species_per when the raw value is null
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''lyr_all'' AS table_name, ''SPECIES_ERROR'' AS error_type, ''species_per_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(species_per_%s::text) = FALSE AND trim(coalesce(raw_%s::text, ''NULL'')) IN ('''', ''NULL'', ''0'') AND source_layer::text = ''%s'')', 
										creation_schema, dest_table_name, i, creation_schema, raw_merged_table, i, spec_per_dependency, lyr);
					END IF;
				END LOOP;
			END LOOP;
		-- These inventories use a single string to represent the species composition therefore must use a helper function to determine validity of a mapping
		ELSIF inventory IN ('MB03','MB11','MB13') OR left(inventory, 2) IN ('QC', 'ON')
		THEN 
			RAISE NOTICE 'Run Species Validation- (%) 	Inventory % uses a single species mapping', LEFT(clock_timestamp()::TEXT, 19), inventory;
			FOR i IN 1..loop_end
			LOOP
				RAISE NOTICE 'Run Species Validation- (%) 		Checking species_%', LEFT(clock_timestamp()::TEXT, 19), i;
				FOR lyr IN 1..lyr_end
				LOOP
					EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''species_%s'', ''%s'', %s, ''lyr_all'')', inventory, i, attribute_dependencies_location, lyr) INTO spec_dependency;
					EXIT WHEN spec_dependency IS NULL;
					IF trim(spec_dependency) = ''
					THEN
						-- Catch falsely mapped species when a dependency doesn't exist
						EXECUTE format('INSERT INTO %s.%s
						SELECT cas_id, ''lyr_all'' AS table_name,  ''SPECIES_ERROR'' AS error_type, ''species_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer 
						FROM %s.%s
						WHERE source_layer = ''%s'' AND 
						(isError(species_%s::text) = FALSE OR 
						isError(species_per_%s::text) = FALSE)', 
						creation_schema, dest_table_name, i, creation_schema, raw_merged_table, lyr, i, i);
					ELSE
						-- Run the single species validity function on each row and collect all error rows
						EXECUTE format('INSERT INTO %s.%s
								(WITH species_validity AS (
									SELECT cas_id, singleSpeciesRowValidity(left(cas_id, 4), %s, species_%s::text, species_per_%s::text, raw_%s::text, source_layer) as validity, source_layer
									FROM %s.%s
									WHERE source_layer = ''%s'' AND isTranslationError(species_%s::text) = FALSE AND isTranslationError(species_per_%s::text) = FALSE
								) SELECT cas_id, ''lyr_all'' AS table_name, (CASE WHEN validity = ''UNKNOWN'' THEN ''VALIDATION_ERROR'' ELSE ''SPECIES_ERROR'' END) as error_type,
								(CASE WHEN validity = ''FALSE_NULL'' THEN ''species_%s was null when it could have been mapped from raw''
									WHEN validity = ''FALSE_MAPPED'' THEN ''species_%s was mapped but a raw value doesnt exist''
									WHEN validity = ''UNKNOWN'' THEN ''number of species in raw species string cant be resolved'' END) AS error_desc, source_layer
								FROM species_validity
								WHERE validity != ''VALID'')', creation_schema, dest_table_name, i, i, i, spec_dependency, creation_schema, raw_merged_table, lyr, i, i, i, i);
					END IF;
				END LOOP;
			END LOOP;
		ELSE 
			-- Any inventories left behind will raise a notice. Function must be updated to include missed inventories
			RAISE NOTICE 'Run Species Validation- (%) ALERT: Inventory % does not have rules set for species validation!', LEFT(clock_timestamp()::TEXT, 19), inventory;
		END IF;
		-- Find true translation errors- these should all count as true errors
		RAISE NOTICE 'Run Species Validation- (%) Finding species translation errors in %', LEFT(clock_timestamp()::TEXT, 19), inventory;
		FOR i IN 1..10
		LOOP
			RAISE NOTICE 'Run Species Validation- (%) 	Checking species_%', LEFT(clock_timestamp()::TEXT, 19), i;
			EXECUTE format('INSERT INTO %s.%s
						SELECT cas_id, ''lyr_all'' AS table_name,  ''TRANSLATION_ERROR'' AS error_type, ''species_%s has a translation error'' AS error_desc, source_layer 
						FROM %s.%s
						WHERE isTranslationError(species_%s::text) = TRUE OR isTranslationError(species_per_%s::text) = TRUE', 
						creation_schema, dest_table_name, i, creation_schema, raw_merged_table, i, i);
		END LOOP;
		-- Find species that are mapped past the maximum possible number of species in an inventory	
		RAISE NOTICE 'Run Species Validation- (%) Finding species mapped past maximum possible in %', LEFT(clock_timestamp()::TEXT, 19), inventory;
		IF species_cutoff > 0
		THEN
			dynamic_sql := format('INSERT INTO %s.%s
							SELECT cas_id, ''lyr_all'' as table_name, ''SPECIES_ERROR'' AS error_type, ''A species was mapped when it should have been non-applicable (%s-10)'' AS error_desc, source_layer
							FROM %s.%s
							WHERE (', creation_schema, dest_table_name, species_cutoff, creation_schema, raw_merged_table);
			FOR i IN species_cutoff..10
			LOOP
				dynamic_sql := dynamic_sql || 'isError(species_' || i || '::text) = FALSE OR isError(species_per_' || i || '::text) = FALSE OR ';
			END LOOP;
			dynamic_sql := trim(TRAILING ' OR ' FROM dynamic_sql);
			dynamic_sql := dynamic_sql || ')';
			EXECUTE dynamic_sql;
		END IF;
		RAISE NOTICE 'Run Species Validation- (%) Function complete', LEFT(clock_timestamp()::TEXT, 19);
	END;
	$func$
LANGUAGE 'plpgsql';


