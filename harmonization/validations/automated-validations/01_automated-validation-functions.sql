-- This file contains the primary fucntions responsible for conducting CASFRI validations
-- These functions could be considered the automated validation engine

-- getMergedColumnNames(inventory, casfri_table_name, inventory_schema, casfri_table_schema, inventory_alias, casfri_table_name_alias, inventory_prefix)
-- Given two tables, returns a string list of all column names in the tables, where the second table's columns are prefixed by inventory_prefix
-- Skips wkb_geometry since it takes up too much space
-- Input:
	-- inventory: the first table to search in. ex: 'AB29'
	-- casfri_table_name: the second table to search in. ex: 'cas_all'
	-- inventory_schema: the name of the schema inventory is in, as text. ex: 'rawfri'
	-- casfri_table_schema: the name of the schema casfri_table_name is in, as text. ex: 'casfri50'
	-- inventory_alias: the alias for inventory to be referred by. ex: 'r'
	-- casfri_table_name_alias: the alias for the cas table to be referred by. ex: 'l'
	-- inventory_prefix: the prefix for the inventory column's aliases. ex: 'raw'
-- Output: TEXT string of the merged column names
-- ex: 
-- casfri50.cas_all has columns 'cas_id', 'stand_structure', 'species_1'
-- rawfri.AB29 has columns 'ogc_fid', 'stand_structure', 'species'
-- getMergedColumnNames('AB29', 'cas_all', 'rawfri', 'casfri50', 'r', 'l', 'raw') =
-- 'l.cas_id, l.stand_structure, l.species_1, r.ogc_fid AS raw_ogc_fid, r.stand_structure AS raw_stand_structure, r.species AS raw_species'
CREATE OR REPLACE FUNCTION getMergedColumnNames(
inventory TEXT, 
casfri_table_name TEXT, 
inventory_schema TEXT, 
casfri_table_schema TEXT, 
inventory_alias TEXT, 
casfri_table_name_alias TEXT,
inventory_prefix TEXT)
RETURNS TEXT AS
	$func$
	DECLARE 
		column_record RECORD;
		result_string TEXT;
	BEGIN
		result_string := '';
		FOR column_record IN 
			EXECUTE format('SELECT column_name FROM getColumnNames(''%s'', ''all'', ''%s'')', casfri_table_name, casfri_table_schema)
		LOOP
			IF column_record.column_name != 'wkb_geometry'
			THEN
				result_string := result_string || casfri_table_name_alias || '.' || column_record.column_name || ', ';
			END IF;
		END LOOP;
		FOR column_record IN 
			EXECUTE format('SELECT column_name FROM getColumnNames(''%s'', ''all'', ''%s'')', inventory, inventory_schema)
		LOOP
			IF column_record.column_name != 'wkb_geometry'
			THEN
				result_string := result_string || inventory_alias || '.' || column_record.column_name || ' AS ' || inventory_prefix || '_' || column_record.column_name || ', ';
			END IF;
		END LOOP;
	RETURN trim(TRAILING ', ' FROM result_string);
	END;
	$func$
LANGUAGE 'plpgsql';


-- getAttributeDependency(inventory, attribute_name, attribute_dependencies_location, layer)
-- Given a inventory, attribute, layer, and a location to search in, gives that attributes dependency
-- This does NOT neccesarily give the value in the specific cell of attribute dependencies, may change some values to give a accurate dependency
-- Input:
	-- inventory: the inventory to search for. ex: 'AB29'
	-- attribute_name: the attribute to search for. ex: 'stand_structure'
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
	-- layer: the layer to search for
	-- casfri_table_name: the name of the table the attribute is found in
-- Output: TEXT in specific cell
-- Will reformat some nfl attributes to use the correct column, and changes some attributes to give the most accurate dependency to be used by validation
CREATE OR REPLACE FUNCTION getAttributeDependency(
inventory TEXT, 
attribute_name TEXT, 
attribute_dependencies_location TEXT, 
layer INTEGER,
casfri_table_name TEXT)
RETURNS TEXT
LANGUAGE 'plpgsql' AS
$func$
	DECLARE
		result_text TEXT;
	BEGIN
		-- Special cases for some inventories
		IF left(inventory, 2) = 'BC' AND attribute_name = 'eco_site' AND layer = 1
		THEN 
			RETURN 'bec_zone_code';
		END IF;
		IF left(inventory, 2) = 'BC' AND attribute_name = 'productivity_type' AND layer IN (1,2)
		THEN
			RETURN 'for_mgmt_land_base_ind';
		END IF;
		IF inventory = 'YT04' AND casfri_table_name = 'lyr_all' AND attribute_name = 'soil_moist_reg' AND layer = 1
		THEN 
			RETURN 'soil_moist';
		END IF;
		IF inventory IN ('QC03', 'QC04', 'QC08', 'QC09') AND attribute_name IN ('crown_closure_upper', 'crown_closure_lower') AND casfri_table_name = 'lyr_all' AND layer IN (1,2)
		THEN 
			RETURN 'cl_dens';
		END IF;
		IF inventory IN ('QC03', 'QC04', 'QC08', 'QC09') AND attribute_name IN ('crown_closure_upper', 'crown_closure_lower') AND casfri_table_name = 'nfl_all' AND layer = 3
		THEN 
			RETURN 'NULL';
		END IF;
		IF inventory IN ('QC03', 'QC04', 'QC08', 'QC09') AND attribute_name IN ('height_upper', 'height_lower') AND casfri_table_name = 'lyr_all' AND layer = 1
		THEN 
			RETURN 'cl_haut';
		END IF;
		IF inventory IN ('QC03', 'QC04', 'QC08', 'QC09') AND attribute_name IN ('height_upper', 'height_lower') AND ((casfri_table_name = 'nfl_all' AND layer = 3) OR (casfri_table_name = 'nfl_all' AND layer = 2))
		THEN 
			RETURN 'NULL';
		END IF;
		IF inventory = 'AB29' AND casfri_table_name = 'lyr_all' AND attribute_name IN ('height_upper', 'height_lower')
		THEN 
			IF layer = 1
			THEN 
				RETURN 'height';
			ELSIF layer = 2
			THEN
				RETURN 'uheight';
			END IF;
		END IF;

		-- Species cases for column names in NFL tables
		IF casfri_table_name = 'nfl_all' AND attribute_name IN ('height_upper', 'height_lower') THEN
			attribute_name := 'nfl_height';
		ELSIF casfri_table_name = 'nfl_all' AND attribute_name IN ('crown_closure_upper', 'crown_closure_lower') THEN
			attribute_name := 'nfl_crown_closure';
		END IF;
		EXECUTE format('SELECT %s FROM %s WHERE inventory_id = ''%s'' AND layer = ''%s''', attribute_name, attribute_dependencies_location, inventory, layer) INTO result_text;
		RETURN result_text;
	END;
$func$;


-- sourceLayerMappingHelper(map_type, var)
-- Given a mapping type and a variable to map from, maps the variable to the corresponding value
-- Input:
	-- map_type: the inventory and mapping category to search for
	-- var: the variable text value to map
-- Output: TEXT mapping of the variable value in the given category. If no mapping or category exists, returns 'NOT_MAPPED'
CREATE OR REPLACE FUNCTION sourceLayerMappingHelper(
map_type TEXT, 
var TEXT
)
RETURNS TEXT
LANGUAGE 'plpgsql' AS
$func$
	BEGIN
		IF map_type = 'yt03_nat_non_veg' AND "translation".TT_matchList(var, '{''Lake'',''River'',''Beach'',''Bedrock'',''Cut Bank'',''Exposed Soil'',''Pond/Lake Sediment'',''River Sediment'',''Rubble'',''Sand'',''Talus'',''Snow/Ice'',''Exposed Land''}', 'FALSE', 'FALSE', 'TRUE', 'FALSE') = TRUE
		THEN
			RETURN "translation".TT_mapText(var, '{''Lake'',''River'',''Beach'',''Bedrock'',''Cut Bank'',''Exposed Soil'',''Pond/Lake Sediment'',''River Sediment'',''Rubble'',''Sand'',''Talus'',''Snow/Ice'',''Exposed Land''}', '{''LAKE'',''RIVER'',''BEACH'',''ROCK_RUBBLE'',''SLIDE'',''EXPOSED_LAND'',''WATER_SEDIMENT'',''WATER_SEDIMENT'',''ROCK_RUBBLE'',''SAND'',''ROCK_RUBBLE'',''SNOW_ICE'',''EXPOSED_LAND''}', 'FALSE', 'FALSE');
		ELSIF map_type = 'yt03_non_for_anth' AND "translation".TT_matchList(var, '{''Airport'',''Anthropogenic Other'',''Cultivated'',''Generic Clearing'',''Gravel Pit'',''Industrial Corridor'',''Industrial Site'',''Mine Site'',''Mine Tailings'',''Railway'',''Road'',''Rural Residential'',''Seismic'',''Tower Site'', ''Urban/Settlement''}', 'FALSE', 'FALSE', 'TRUE', 'FALSE') = TRUE
		THEN
			RETURN "translation".TT_mapText(var, '{''Airport'',''Anthropogenic Other'',''Cultivated'',''Generic Clearing'',''Gravel Pit'',''Industrial Corridor'',''Industrial Site'',''Mine Site'',''Mine Tailings'',''Railway'',''Road'',''Rural Residential'',''Seismic'',''Tower Site'', ''Urban/Settlement''}', '{''FACILITY_INFRASTRUCTURE'',''OTHER'',''CULTIVATED'',''OTHER'',''INDUSTRIAL'',''INDUSTRIAL'',''INDUSTRIAL'',''INDUSTRIAL'',''INDUSTRIAL'',''FACILITY_INFRASTRUCTURE'',''FACILITY_INFRASTRUCTURE'',''SETTLEMENT'',''INDUSTRIAL'',''FACILITY_INFRASTRUCTURE'', ''SETTLEMENT''}', 'FALSE', 'FALSE');
		ELSIF map_type = 'yt03_non_for_veg' AND "translation".TT_matchList(var, '{''Low Shrub'',''Tall Shrub'',''Bryoid'',''Forb'',''Graminoid'',''Herb'',''Lichen'',''Moss''}', 'FALSE', 'FALSE', 'TRUE', 'FALSE') = TRUE
		THEN 
			RETURN "translation".TT_mapText(var, '{''Low Shrub'',''Tall Shrub'',''Bryoid'',''Forb'',''Graminoid'',''Herb'',''Lichen'',''Moss''}', '{''LOW_SHRUB'',''TALL_SHRUB'',''BRYOID'',''FORBS'',''GRAMINOIDS'',''HERBS'',''BRYOID'',''BRYOID''}', 'FALSE', 'FALSE');
		ELSEIF map_type = 'nb01_dist' AND "translation".TT_matchList(var,'{''BB'',''CC'',''CL'',''CT'',''CV'',''FP'',''FT'',''FW'',''IT'',''PA'',''PB'',''PC'',''PL'',''PT'',''RC'',''RR'',''SA'',''SC'',''SH'',''SR'',''ST'',''TI'',''TP''}') = TRUE
		THEN
			RETURN "translation".TT_mapText(var, '{''BB'',''CC'',''CL'',''CT'',''CV'',''FP'',''FT'',''FW'',''IT'',''PA'',''PB'',''PC'',''PL'',''PT'',''RC'',''RR'',''SA'',''SC'',''SH'',''SR'',''ST'',''TI'',''TP''}', '{''BURN'',''CUT'',''SILVICULTURE_TREATMENT'',''PARTIAL_CUT'',''PARTIAL_CUT'',''SILVICULTURE_TREATMENT'',''OTHER'',''PARTIAL_CUT'',''PARTIAL_CUT'',''PARTIAL_CUT'',''BURN'',''PARTIAL_CUT'',''SILVICULTURE_TREATMENT'',''OTHER'',''PARTIAL_CUT'',''OTHER'',''PARTIAL_CUT'',''PARTIAL_CUT'',''PARTIAL_CUT'',''PARTIAL_CUT'',''PARTIAL_CUT'',''SILVICULTURE_TREATMENT'',''PARTIAL_CUT''}');
		ELSE	
			RETURN 'NOT_MAPPED';
		END IF;
	END;
$func$;


-- getSourceLayer(raw_merged_table, creation_schema, attribute_dependencies_location, cas_id, layer, layer_rank)
-- Given various information about a certain row in a raw_merged table, returns the source layer of the rows transaltion
-- A source layer is the layer number that the row was originally translated as. After translation, layer numbers are often changed due to ordering of stand heights 
-- Input:
	-- raw_merged_table: the name of the table that we are searching in
	-- creation_schema: the schema that the table was found in
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
	-- layer, layer_rank, nat_non_veg... : various attributes that may or may not exist in the table. 
		-- These attributes are needed for specific inventories to find the source layer and must be passed in to the function. Otherwise, they can be passed as NULL and will be ignored
-- Output: INTEGER representation of the source layer
CREATE OR REPLACE FUNCTION getSourceLayer(
raw_merged_table TEXT, 
creation_schema TEXT,
attribute_dependencies_location TEXT,
cas_id TEXT, 
layer TEXT DEFAULT NULL, 
layer_rank TEXT DEFAULT NULL, 
nat_non_veg TEXT DEFAULT NULL, 
non_for_anth TEXT DEFAULT NULL, 
non_for_veg TEXT DEFAULT NULL, 
l1_for_cover_rank TEXT DEFAULT NULL, 
l2_for_cover_rank TEXT DEFAULT NULL, 
canrank TEXT DEFAULT NULL, 
us2canrank TEXT DEFAULT NULL, 
us3canrank TEXT DEFAULT NULL, 
us4canrank TEXT DEFAULT NULL, 
us5canrank TEXT DEFAULT NULL,
height_upper TEXT DEFAULT NULL,
ht TEXT DEFAULT NULL,
us2ht TEXT DEFAULT NULL,
us3ht TEXT DEFAULT NULL,
us4ht TEXT DEFAULT NULL,
us5ht TEXT DEFAULT NULL,
water TEXT DEFAULT NULL, 
exposed_land TEXT DEFAULT NULL, 
snow_ice TEXT DEFAULT NULL, 
anthropogenic TEXT DEFAULT NULL, 
shrub_type TEXT DEFAULT NULL, 
non_shrub_type TEXT DEFAULT NULL,
dist_type_1 TEXT DEFAULT NULL,
l1trt TEXT DEFAULT NULL,
l2trt TEXT DEFAULT NULL,
sup_hauteur TEXT DEFAULT NULL,
inf_hauteur TEXT DEFAULT NULL,
species_1 TEXT DEFAULT NULL,
bc_height_upper TEXT DEFAULT NULL,
l1_height_1 TEXT DEFAULT NULL,
l1_height_2 TEXT DEFAULT NULL,
l2_height_1 TEXT DEFAULT NULL,
l2_height_2 TEXT DEFAULT NULL,
l1_species_pct_1 TEXT DEFAULT NULL,
l1_species_pct_2 TEXT DEFAULT NULL,
l2_species_pct_1 TEXT DEFAULT NULL,
l2_species_pct_2 TEXT DEFAULT NULL,
crown_closure_upper TEXT DEFAULT NULL,
l1_crown_closure TEXT DEFAULT NULL,
l2_crown_closure TEXT DEFAULT NULL
)
RETURNS INTEGER
LANGUAGE 'plpgsql' AS
$func$
	DECLARE
		inventory TEXT;
		min_nfl INTEGER;
		lyr_count INTEGER;
	BEGIN
		inventory := left(cas_id, 4);
		-- cas, eco, and dst all use layer 1 mappings
		IF raw_merged_table LIKE '%cas%' OR raw_merged_table LIKE '%eco%' OR raw_merged_table LIKE '%dst%'
		THEN 
			RETURN 1;
		-- AB30 and AB33 only have one row in attribute_dependencies
		ELSIF inventory IN ('AB30', 'AB33')
			THEN
				RETURN 1;
		-- AB and NT inventories should always have layer_rank as the correct source layer
		-- However, for NFL rows, it will be the layer ranking relative to the first NFL row in attribute_dependencies
		ELSIF LEFT(inventory, 2) IN ('AB', 'NT')
		THEN
			IF layer_rank IS NOT NULL AND isError(layer_rank::text) = FALSE 
			THEN 
				layer := layer_rank::integer;
				IF raw_merged_table LIKE '%nfl%' THEN
					EXECUTE format('SELECT min(layer::integer) FROM %s WHERE inventory_id = ''%s'' AND "table" = ''''''NFL''''''', attribute_dependencies_location, inventory) INTO min_nfl;
					layer := (layer::integer + min_nfl::integer - 1)::text;
				END IF;
				RETURN layer;
			END IF;
		-- These inventories have exactly one NFL row in layer 3 position, and otherwise have layer_rank as a source layer mapping
		ELSIF LEFT(inventory, 2) IN ('NB', 'ON', 'NS') OR inventory = 'SK01'
		THEN 
			IF raw_merged_table LIKE '%nfl%'
			THEN
				RETURN 3;
			ELSIF layer_rank IS NOT NULL AND isError(layer_rank::text) = FALSE 
			THEN 
				RETURN layer_rank::integer;
			END IF;
		-- YT03 uses layer_rank for lyr rows, and otherwise must use the NFL type mappings to determine which layer the row was mapped from
		ELSIF inventory = 'YT03'
		THEN
			IF raw_merged_table NOT LIKE '%nfl%'
			THEN
				RETURN layer_rank::integer;
			ELSIF nat_non_veg = sourceLayerMappingHelper('yt03_nat_non_veg', water)
			THEN 
				RETURN 3;
			ELSIF nat_non_veg = sourceLayerMappingHelper('yt03_nat_non_veg', exposed_land)
			THEN 
				RETURN 4;
			ELSIF nat_non_veg = sourceLayerMappingHelper('yt03_nat_non_veg', snow_ice)
			THEN 
				RETURN 5;
			ELSIF non_for_anth = sourceLayerMappingHelper('yt03_non_for_anth', anthropogenic)
			THEN 
				RETURN 6;
			ELSIF non_for_veg = sourceLayerMappingHelper('yt03_non_for_veg', shrub_type)
			THEN 
				RETURN 7;
			ELSIF non_for_veg = sourceLayerMappingHelper('yt03_non_for_veg', non_shrub_type)
			THEN 
				RETURN 8;
			END IF;
		-- QC uses layer 3 for NFL mappings and its layer 1 and 2 are basically the same, height can be used to distinguish lyr rows
		ELSIF left(inventory, 2) = 'QC'
		THEN
			IF raw_merged_table LIKE '%nfl%'
			THEN 
				RETURN 3;
			ELSIF inventory NOT IN ('QC03', 'QC04', 'QC05', 'QC10') AND raw_merged_table LIKE '%lyr%' AND isError(layer) =  FALSE
			THEN 
				RETURN layer::integer;
			-- These inventories always map all species in layer 2 to UNKNOWN_VALUE
			ELSIF inventory IN ('QC03', 'QC04') AND raw_merged_table LIKE '%lyr%'
			THEN 
				IF species_1 = 'UNKNOWN_VALUE'
				THEN 
					RETURN 2;
				ELSE
					RETURN 1;
				END IF;
			ELSIF inventory IN ('QC05', 'QC10') AND raw_merged_table LIKE '%lyr%' AND height_upper::TEXT = sup_hauteur::TEXT 
			THEN 	
				RETURN 1;
			ELSIF inventory IN ('QC05', 'QC10') AND raw_merged_table LIKE '%lyr%' AND height_upper::TEXT = inf_hauteur::TEXT 
			THEN 	
				RETURN 2;
			END IF;
		-- BC uses a for_cover_rank of either NULL or 1. Must match the mapping to layer rank to determine source layer
		-- For NFL rows, only one NFL type is mapped per attribute_dependencies row
		ELSIF left(inventory, 2) = 'BC'
		THEN 
			IF raw_merged_table NOT LIKE '%nfl%'
			THEN
				IF (layer_rank = '-8888' AND l1_for_cover_rank IS NULL AND l2_for_cover_rank IS NOT NULL)
				OR (layer_rank = '1' AND l1_for_cover_rank IS NOT NULL AND l2_for_cover_rank IS NULL )
				THEN 
					RETURN 1;
				ELSIF (layer_rank = '-8888' AND l2_for_cover_rank IS NULL AND l1_for_cover_rank IS NOT NULL)
				OR (layer_rank = '1' AND l2_for_cover_rank IS NOT NULL AND l1_for_cover_rank IS NULL)
				THEN 
					RETURN 2;
				ELSIF COALESCE(l1_height_1::TEXT, 'NULL') != COALESCE(l2_height_1::TEXT, 'NULL') OR COALESCE(l1_height_2::TEXT, 'NULL') != COALESCE(l1_height_2::TEXT, 'NULL')
				THEN
					IF bc_height_upper::text = "translation".TT_bc_height(l1_height_1, l1_height_2, l1_species_pct_1, l1_species_pct_2)::text
					THEN
						RETURN 1;
					ELSIF bc_height_upper::text = "translation".TT_bc_height(l2_height_1, l2_height_2, l2_species_pct_1, l2_species_pct_2)::text
					THEN
						RETURN 2;
					END IF;
				ELSIF COALESCE(l1_crown_closure::TEXT, 'NULL') != COALESCE(l2_crown_closure::TEXT, 'NULL')
				THEN
					IF crown_closure_upper = COALESCE(l1_crown_closure::TEXT, 'NULL')
					THEN
						RETURN 1;
					ELSIF crown_closure_upper = COALESCE(l2_crown_closure::TEXT, 'NULL')
					THEN
						RETURN 2;
					END IF;
				ELSE
					-- There are only a handful of rows like this, assume correct layer
					IF isError(layer::text) = FALSE
					THEN
						RETURN layer::integer;
					END IF;
				END IF;				
			ELSE
				IF isError(nat_non_veg) = FALSE
				THEN
					RETURN 4;
				ELSIF isError(non_for_anth) = FALSE
				THEN
					RETURN 5;
				ELSIF isError(non_for_veg) = FALSE
				THEN
					RETURN 3;
				END IF;
			END IF;
		-- These MB inventories map canrank to layer_rank, must match which one was used
		-- Sometimes there is no layer_rank, in this case, forest heights should work to fix edge cases
		ELSIF inventory IN ('MB06', 'MB02', 'MB04', 'MB07', 'MB10', 'MB12')
		THEN
			IF raw_merged_table NOT LIKE '%nfl%'
			THEN
				IF isError(layer_rank::text) = FALSE
				THEN
					IF layer_rank = canrank
					THEN 	
						RETURN 1;
					ELSIF layer_rank = us2canrank
					THEN 	
						RETURN 2;
					ELSIF layer_rank = us3canrank
					THEN 	
						RETURN 3;
					ELSIF layer_rank = us4canrank
					THEN 	
						RETURN 4;
					ELSIF layer_rank = us5canrank
					THEN 	
						RETURN 5;
					END IF;
				ELSIF isError(height_upper::text) = FALSE AND raw_merged_table LIKE '%lyr%'
				THEN
					IF height_upper = ht
					THEN 
						RETURN 1;
					ELSIF height_upper = us2ht
					THEN 
						RETURN 2;
					ELSIF height_upper = us3ht
					THEN 
						RETURN 3;
					ELSIF height_upper = us4ht
					THEN 
						RETURN 4;
					ELSIF height_upper = us5ht
					THEN 
						RETURN 5;
					END IF;
				END IF;
			ELSE
				RETURN 6;
			END IF;
		-- These inventories  have only two rows in attribute_dependencies, one for lyr and one for nfl
		ELSIF LEFT(inventory, 2) = 'PE' OR inventory IN ('MB05', 'MB01', 'MB03', 'MB11', 'MB13', 'YT01', 'YT02', 'YT04', 'NL01')
		THEN
			IF raw_merged_table NOT LIKE '%nfl%'
			THEN 
				RETURN 1;
			ELSE
				RETURN 2;
			END IF;
		END IF;
		-- For anything that slips by, if there is only one layer in lyr_all, just return 1
		-- This should catch most edge cases
		IF raw_merged_table LIKE '%lyr%'
		THEN
			EXECUTE format('SELECT count(*)::integer FROM %s.%s WHERE cas_id = ''%s''',
								creation_schema, raw_merged_table, cas_id) INTO lyr_count;
			IF lyr_count = 1
			THEN
				RETURN 1;
			END IF;
		END IF;
		-- This table holds any rows that had a source layer not mapped to flag bugs in this function
		INSERT INTO layersnotmapped VALUES (cas_id, raw_merged_table, layer::text);
		RETURN -8886;
	END;
$func$;



-- updateSourceLayers(casfri_table_name, inventory, attribute_dependencies_location, creation_schema)
-- Given a casfri table and inventory, updates the raw merged table to set all null/error source layers to their correct source layer
-- Input:
	-- casfri_table_name: the name of the casfri table it is updating layers for
	-- inventory: the inventory it is updating layers for
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
	-- creation_schema: the schema that the merged table is found in
-- Output: VOID
CREATE OR REPLACE FUNCTION updateSourceLayers(
casfri_table_name TEXT, 
inventory TEXT, 
attribute_dependencies_location TEXT, 
creation_schema TEXT)
RETURNS VOID AS
	$func$
	DECLARE 
		dynamic_sql TEXT;
	BEGIN
		RAISE NOTICE 'Update Source Layers- (%) Updating source layers for %_raw_%_joined', LEFT(clock_timestamp()::TEXT, 19), inventory, casfri_table_name;
		dynamic_sql := format('UPDATE %s.%s_raw_%s_joined
							    SET source_layer = getSourceLayer(
								raw_merged_table => ''%s_raw_%s_joined'', 
								creation_schema => ''%s'', 
								attribute_dependencies_location => ''%s'', 
								cas_id => cas_id, ', creation_schema, inventory, casfri_table_name, inventory, casfri_table_name, creation_schema, attribute_dependencies_location);
		-- Assign special case variables for specific inventories and tables
		IF casfri_table_name  IN ('dst_all', 'eco_all')
		THEN
			dynamic_sql := dynamic_sql || 'layer => layer::text, ';
		ELSIF casfri_table_name  IN ('lyr_all', 'nfl_all')
		THEN
			dynamic_sql := dynamic_sql || 'layer => layer::text, layer_rank => layer_rank::text, ';
		END IF;crown_closure_lower
		IF casfri_table_name = 'nfl_all'
		THEN
			dynamic_sql := dynamic_sql || 'nat_non_veg => nat_non_veg::text, non_for_anth => non_for_anth::text, non_for_veg => non_for_veg::text, ';
		END IF;
		IF left(inventory, 2) = 'BC'
		THEN
			dynamic_sql := dynamic_sql || 'l1_for_cover_rank => raw_l1_for_cover_rank_cd::text, l2_for_cover_rank => raw_l2_for_cover_rank_cd::text, ';
		END IF;
		IF inventory IN ('MB06', 'MB02', 'MB04', 'MB07', 'MB10', 'MB12')
		THEN
			dynamic_sql := dynamic_sql || 'canrank => raw_canrank::text, us2canrank => raw_us2canrank::text, us3canrank => raw_us3canrank::text, us4canrank => raw_us4canrank::text, us5canrank => raw_us5canrank::text, ';
		END IF;
		IF inventory IN ('MB06', 'MB02', 'MB04', 'MB07', 'MB10', 'MB12') AND casfri_table_name = 'lyr_all'
		THEN
			dynamic_sql := dynamic_sql || 'height_upper => height_upper::text, ht => raw_ht::text, us2ht => raw_us2ht::text, us3ht => raw_us3ht::text, us4ht => raw_us4ht::text, us5ht => raw_us5ht::text, ';
		ELSEIF inventory IN ('QC05', 'QC10') AND casfri_table_name = 'lyr_all'
		THEN
			dynamic_sql := dynamic_sql || 'height_upper => height_upper::text, ';
		END IF;
		IF inventory IN ('QC05', 'QC10') AND casfri_table_name = 'lyr_all'
		THEN 
			dynamic_sql := dynamic_sql || 'sup_hauteur => raw_sup_hauteur::text, inf_hauteur => raw_inf_hauteur::text, ';
		END IF;
		IF inventory = 'YT03'
		THEN
			dynamic_sql := dynamic_sql || 'water => raw_water::text, exposed_land => raw_exposed_land::text, snow_ice => raw_snow_ice::text, anthropogenic => raw_anthropogenic::text, shrub_type => raw_shrub_type::text, non_shrub_type => raw_non_shrub_type::text, ';
		END IF;
		IF inventory = 'NB01' AND casfri_table_name = 'dst_all'
		THEN 
			dynamic_sql := dynamic_sql || 'dist_type_1 => dist_type_1::text, l1trt => raw_l1trt::text, l2trt => raw_l2trt::text, ';
		END IF;	
		IF inventory IN ('QC03', 'QC04') AND casfri_table_name = 'lyr_all'
		THEN 
			dynamic_sql := dynamic_sql || 'species_1 => species_1::text, ';
		END IF;	
		IF left(inventory, 2) = 'BC' AND casfri_table_name = 'lyr_all'
		THEN 
			dynamic_sql := dynamic_sql || 'bc_height_upper => height_upper::text, l1_height_1 => raw_l1_proj_height_1::text, l1_height_2 => raw_l1_proj_height_2::text, l2_height_1 => raw_l2_proj_height_1::text, l2_height_2 => raw_l2_proj_height_2::text, l1_species_pct_1 => raw_l1_species_pct_1::text, l1_species_pct_2 => raw_l1_species_pct_2::text, l2_species_pct_1 => raw_l2_species_pct_1::text, l2_species_pct_2 => raw_l2_species_pct_2::text, crown_closure_upper => crown_closure_upper::text, l1_crown_closure => raw_l1_crown_closure::text, l2_crown_closure => raw_l2_crown_closure::text, ';
		END IF;
		dynamic_sql := trim(TRAILING ', ' FROM dynamic_sql);
		dynamic_sql := dynamic_sql || ') WHERE source_layer IS NULL OR isError(source_layer::text) = TRUE';
		EXECUTE dynamic_sql;
	END;
	$func$
LANGUAGE 'plpgsql';



-- createRawJoinedTable(casfri_table_name, casfri_table_schema, inventory, inventory_schema, attribute_dependencies_location, creation_schema)
-- Given a casfri table and inventory, creates a new table as a join of the raw inventory with the casfri table
-- Will find source layers for each row, and create a btree index on the cas_id
-- Input:
	-- casfri_table_name: the name of the casfri table to join
	-- casfri_table_schema: the schema the casfri table is found in
	-- inventory: the raw inventory to join
	-- inventory_schema: the schema the raw inventory is found in
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
	-- creation_schema: the schema that the merged table should be created in
-- Output: VOID
-- Creates a table with name (inventory)_raw_(casfri_table)_joined in creation schema
-- New tables contains all the column names of the casfri table, plus all the column names of the raw inventory prefixed by 'raw_' (except wkb_geometry)
-- Tbale will also contain a source_layer row with the layer number that the row was originally mapped from
CREATE OR REPLACE FUNCTION createRawJoinedTable(
casfri_table_name TEXT, 
casfri_table_schema TEXT, 
inventory TEXT, 
inventory_schema TEXT, 
attribute_dependencies_location TEXT, 
creation_schema TEXT)
RETURNS VOID AS
	$func$
	DECLARE 
		dependency TEXT;
		param TEXT[5];
		i INTEGER;
	BEGIN
		-- Uses cas_id mapping to create the join condition
		EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''cas_id'', ''%s'', 1, ''%s'')', inventory, attribute_dependencies_location, casfri_table_name) INTO dependency;
		FOR i IN 1..5
		LOOP
			IF TRIM(SPLIT_PART(dependency, ',', i)) IN ('''''', '', '''') THEN
				param[i] := '''''''''';
			ELSE
				param[i] = 'r.'||trim(SPLIT_PART(dependency, ',', i));
			END IF;
		END LOOP;
		-- Creates table
		RAISE NOTICE 'Create Raw Joined Table- (%) Creating %.%_raw_%_joined', LEFT(clock_timestamp()::TEXT, 19), creation_schema, inventory, casfri_table_name;
		EXECUTE format('CREATE TABLE IF NOT EXISTS %s.%s_raw_%s_joined AS
		SELECT %s, NULL AS source_layer 
		FROM %s.%s r
		INNER JOIN %s.%s l ON l.cas_id = translation.tt_padConcat(
										''{''||%s||'', ''||%s||'', ''||%s||'', ''||(CASE WHEN %s::text = '''' THEN '''''''' ELSE %s::text END)||'', ''||%s||''}'', 
										''{''''4'''',''''15'''',''''10'''',''''10'''',''''7''''}'', 
										''{''''x'''',''''x'''',''''x'''',''''x'''',''''x''''}'', ''-'', ''TRUE'', ''TRUE'')'
		, creation_schema, inventory, casfri_table_name, getMergedColumnNames(inventory, casfri_table_name, inventory_schema, casfri_table_schema, 'r', 'l', 'raw')
		, inventory_schema, inventory, casfri_table_schema, casfri_table_name,
		param[1], param[2], param[3], param[4], param[4], param[5]);
		EXECUTE format('CREATE INDEX IF NOT EXISTS %s_raw_%s_joined_cas_id_idx ON %s.%s_raw_%s_joined USING btree(cas_id);', inventory, casfri_table_name, creation_schema, inventory, casfri_table_name);
		-- Updates source layers
		EXECUTE FORMAT('SELECT * FROM updateSourceLayers(''%s'', ''%s'', ''%s'',''%s'')', casfri_table_name, inventory, attribute_dependencies_location, creation_schema);	
	END;
	$func$
LANGUAGE 'plpgsql';



-- dropRawJoinedTables(creation_schema, inventory_source)
-- Given a list of inventories, drops all raw_joined tables for each casfri table in each inventory
-- Intended as a maintenance function to optimize dropping tables when needed
-- Input:
	-- creation_schema: the schema that the merged tables were created in
	-- inventory_source: the source of the list of inventories. Must include a inventory column
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'getListInvs()'
-- Output: VOID
CREATE OR REPLACE FUNCTION dropRawJoinedTables(
creation_schema TEXT, 
inventory_source TEXT)
RETURNS VOID AS
	$func$
	DECLARE 
		inventory_record RECORD;
		table_name TEXT;
	BEGIN
		RAISE NOTICE 'Drop Raw Joined Tables- (%) Function started ----------', LEFT(clock_timestamp()::TEXT, 19);
		FOR inventory_record IN 
			EXECUTE format('SELECT * FROM %s', inventory_source)
		LOOP
			FOR table_name IN (VALUES ('lyr_all'), ('eco_all'), ('nfl_all'), ('dst_all'), ('cas_all'))
			LOOP
				RAISE NOTICE 'Drop Raw Joined Tables- (%) Dropping %_raw_%_joined', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, table_name;
				EXECUTE format('DROP TABLE IF EXISTS %s.%s_raw_%s_joined', creation_schema, inventory_record.inventory, table_name);
			END LOOP;
		END LOOP;
	END;
	$func$
LANGUAGE 'plpgsql';


-- estimateRawValidationOperations(casfri_table_schema, inventory_source, excluded_columns_location)
-- Given a list of inventories, estimates how many operations is required to complete the createRawValidationTableRunner() function
-- Input:
	-- creation_schema: the schema that the merged tables were created in
	-- inventory_source: the source of the list of inventories. Must include a inventory column
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'getListInvs()'
	-- excluded_columns_location: the source of the list of excluded columns, similar format to inventory_source
		-- ex: 'casfri_validation.excluded_validation_columns'
-- Output: INTEGER total number of operations
CREATE OR REPLACE FUNCTION estimateRawValidationOperations( 
casfri_table_schema TEXT, 
inventory_source TEXT,
excluded_columns_location TEXT)
RETURNS INTEGER AS
	$func$
	DECLARE 
		total_operations INTEGER;
		inventory_record RECORD;
		attribute_record RECORD;
		table_name TEXT;
	BEGIN
		total_operations := 1; -- updating error types
		FOR inventory_record IN 
			EXECUTE format('SELECT * FROM %s', inventory_source)
		LOOP
			-- Species validation, disturbance validation, NFL validation, other validation, and refreshing aggregated stats per inventory
			total_operations := total_operations + 5 + 3 + 3 + 1 + 1;
			FOR table_name IN (VALUES ('lyr_all'), ('eco_all'), ('nfl_all'), ('cas_all'))
			LOOP
				-- Creating raw joined view per table
				total_operations := total_operations + 1;
				FOR attribute_record IN
					EXECUTE format('SELECT * FROM getColumnNames(''%s'', ''all'', ''%s'') WHERE column_name NOT IN (SELECT column_name FROM %s)', 
					table_name, casfri_table_schema, excluded_columns_location)
				LOOP
					-- Each attribute per table in validation
					total_operations := total_operations + 1;
				END LOOP;
			END LOOP;
		END LOOP;
		RETURN total_operations;
	END;
	$func$
LANGUAGE 'plpgsql';


-- runRawValidationOnTable(dest_table_name, creation_schema, casfri_table_name,casfri_table_schema,inventory,total_operations,start_time,operation_count,excluded_columns_location,attribute_dependencies_location)
-- Given a specific inventory, runs general validations on the inventory and stores flagged errors in the dest_table
-- Collects any generic validation errors and attributes that were either mapped when a raw value doesn't exist, or mapped to an error code when a raw value does exists
-- Will call specified helper functions in complex_mappings_location to validate special case attributes
-- Input:
	-- dest_table_name: the name of the table to store flagged errors in
	-- creation_schema: the schema that the dest_table and raw_joined tables are in
	-- casfri_table_name: the name of the casfri table to validate (('lyr_all'), ('eco_all'), ('nfl_all'), ('cas_all')) dst_all technically also works, but generally all attributes are ignored
	-- casfri_table_schema: the schema that the casfri tables are found in
	-- inventory: the id of the inventory to validate
	-- total_operations: total number of operations to be performed to be used in progress messages
	-- start_time: the start time that the progress messages should use
	-- operation_count: the current number of operations that have been performed
	-- excluded_columns_locations: the source of the excluded columns list. Must include a column_name column
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'casfri_validation.excluded_validation_columns'
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
	-- complex_mappings_location: the source of the complex mapping attribute list. Must include a inventory, table, attribute_name, and function_call
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'casfri_validation.complexMappingAttributes'
-- Output: VOID 
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runRawValidationOnTable(
dest_table_name TEXT, 
creation_schema TEXT, 
casfri_table_name TEXT,
casfri_table_schema TEXT,
inventory TEXT,
total_operations INTEGER,
start_time TIMESTAMPTZ,
operation_count INTEGER,
excluded_columns_location TEXT,
attribute_dependencies_location TEXT,
complex_mappings_location TEXT)
RETURNS INTEGER AS
	$func$
	DECLARE 
		attribute_record RECORD;
		dependency TEXT;
		raw_merged_table TEXT;
		lyr INTEGER;
	    msg TEXT;
	   	function_call TEXT;
	BEGIN
		raw_merged_table := inventory || '_raw_' || casfri_table_name || '_joined';
		-- Iterate through each attribute in the given table
		FOR attribute_record IN
			EXECUTE format('SELECT * FROM getColumnNames(''%s'', ''all'', ''%s'') WHERE column_name NOT IN (SELECT column_name FROM %s)', casfri_table_name, casfri_table_schema, excluded_columns_location)
		LOOP
			RAISE NOTICE 'Create Raw Validation- (%) 		Processing attribute: %', LEFT(clock_timestamp()::TEXT, 19), attribute_record.column_name;
			-- Iterate through each layer
			FOR lyr IN 1..9 LOOP
				EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''%s'', ''%s'', %s, ''%s'')', inventory, attribute_record.column_name, attribute_dependencies_location, lyr, casfri_table_name) INTO dependency;
				-- End loop when dependency is null because this likely means that the layer has surpassed the maximum layer for that table
				EXIT WHEN dependency IS NULL;
				-- Attribute has no mapping, confirm that no values were mapped
				IF dependency = '' OR upper(trim(dependency)) = 'NULL'
				THEN
					RAISE NOTICE 'Create Raw Validation- (%) 			Attribute does not exist in raw inventory for layer %, finding errors', LEFT(clock_timestamp()::TEXT, 19), lyr;
					EXECUTE format('SELECT * FROM runNoMappingValidation(''%s'',''%s'',''%s'',''%s'',''%s'')',
					dest_table_name, creation_schema, raw_merged_table, attribute_record.column_name, lyr);		
				-- Attribute has a direct numeric mapping, can check equality
				ELSIF dependency ~ '^[0-9]+$'
				THEN
					RAISE NOTICE 'Create Raw Validation- (%) 			Attribute has a static numeric mapping for layer %, finding errors', LEFT(clock_timestamp()::TEXT, 19), lyr;
					EXECUTE format('SELECT * FROM runNumericDependencyValidation(''%s'',''%s'',''%s'',''%s'',''%s'',''%s'')',
					dest_table_name, creation_schema, raw_merged_table, attribute_record.column_name, dependency, lyr);		
				-- wbk_geometry always exists in raw, but it isn't in the raw joined views to save space so needs special case
				ELSIF dependency = 'wkb_geometry'
				THEN
					RAISE NOTICE 'Create Raw Validation- (%) 			Attribute exists with geometry mapping for layer %, finding errors', LEFT(clock_timestamp()::TEXT, 19), lyr;
					EXECUTE format('SELECT * FROM runGeoDependencyValidation(''%s'',''%s'',''%s'',''%s'',''%s'')',
					dest_table_name, creation_schema, raw_merged_table, attribute_record.column_name, lyr);	
				ELSE
					EXECUTE format('SELECT function_call FROM %s WHERE inventory = ''%s'' AND "table" = ''%s'' AND attribute_name = ''%s''',
					complex_mappings_location, inventory, casfri_table_name, attribute_record.column_name) INTO function_call;
					IF function_call IS NOT NULL 
					THEN
						RAISE NOTICE 'Create Raw Validation- (%) 			Attribute has a complex mapping', LEFT(clock_timestamp()::TEXT, 19);
						EXECUTE format('SELECT * FROM ' || function_call, dest_table_name, creation_schema);
						-- Ensure this is the last iteration since these functions handle ALL layers
						EXECUTE format('SELECT * FROM runGenericTranslationErrorValidation(''%s'',''%s'',''%s'',''%s'',''%s'')',
						dest_table_name, creation_schema, raw_merged_table, attribute_record.column_name, lyr);	
						EXIT;
					-- Attribute has exactly one simple mapping, check that NULL values are treated correctly
					ELSIF countInstances(dependency, ',') = 0
					THEN
						RAISE NOTICE 'Create Raw Validation- (%) 			Attribute exists with single mapping for layer %, finding errors', LEFT(clock_timestamp()::TEXT, 19), lyr;
						EXECUTE format('SELECT * FROM runBasicNullValidation(''%s'',''%s'',''%s'',''%s'',''%s'',''%s'')',
						dest_table_name, creation_schema, raw_merged_table, attribute_record.column_name, dependency, lyr);	
					ELSE
						-- Attribute does not have a function specified to handle it
						RAISE NOTICE 'Create Raw Validation- (%) 			Attribute cannot be handled for layer %. Skipping', LEFT(clock_timestamp()::TEXT, 19), lyr;
					END IF;
				END IF;
				-- Get generic translation errors
				EXECUTE format('SELECT * FROM runGenericTranslationErrorValidation(''%s'',''%s'',''%s'',''%s'',''%s'')',
				dest_table_name, creation_schema, raw_merged_table, attribute_record.column_name, lyr);	
			END LOOP;
			operation_count := operation_count + 1;
			EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
			RAISE NOTICE 'Create Raw Validation- (%) 		Processed attribute ''%'' - %', LEFT(clock_timestamp()::TEXT, 19), attribute_record.column_name, msg;
		END LOOP;
	RETURN operation_count;
	END;
	$func$
LANGUAGE 'plpgsql';



-- createRawValidationTableRunner(dest_table_name TEXT, creation_schema, casfri_table_schema,inventory_source, inventory_schema,attribute_dependencies_location,excluded_columns_location, complex_mappings_location)
-- Given a list of inventories, runs all validations on each inventory for each casfri table. This included general null error validations, translation errors,
-- species errors, disturbance errors, and nfl errors validation.
-- Will also delete any prexisting errors in the given inventories to avoid duplicates
-- Input:
	-- dest_table_name: the name of the table to store flagged errors in
	-- creation_schema: the schema that the dest_table and raw_joined tables are in
	-- casfri_table_schema: the schema that the casfri tables are found in
	-- inventory_source: the source of the inventory list to valdidate. Must include a inventory column
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'getListInvs()'
	-- inventory_schema: the schema that the raw inventories are found in
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
	-- excluded_columns_locations: the source of the excluded columns list. Must include a column_name column
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'casfri_validation.excluded_validation_columns'
	-- complex_mappings_location: the source of the complex mapping attribute list. Must include a inventory, table, attribute_name, and function_call
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'casfri_validation.complexMappingAttributes'
-- Output: VOID 
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION createRawValidationTableRunner(
dest_table_name TEXT, 
creation_schema TEXT, 
casfri_table_schema TEXT,
inventory_source TEXT, 
inventory_schema TEXT,
attribute_dependencies_location TEXT,
excluded_columns_location TEXT,
complex_mappings_location TEXT)
RETURNS VOID AS
	$func$
	DECLARE 
		inventory_record RECORD;
		attribute_record RECORD;
		row_record RECORD;
		att_val TEXT;
		dep_val TEXT;
		table_name TEXT;
		raw_merged_table TEXT;
		total_operations INTEGER;
	    start_time TIMESTAMPTZ;
	   	operation_count INTEGER;
	    msg TEXT;
	BEGIN
		RAISE NOTICE 'Create Raw Validation- (%) Function started ----------', LEFT(clock_timestamp()::TEXT, 19);
		-- Initialize variables for progress messages
		start_time := now();
		operation_count := 0;
		total_operations := estimateRawValidationOperations(casfri_table_schema, inventory_source, excluded_columns_location);
		-- Create result tables
		EXECUTE format('CREATE TABLE IF NOT EXISTS %s.%s (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT)', creation_schema, dest_table_name);
		EXECUTE format('CREATE TABLE IF NOT EXISTS %s.layersnotmapped (cas_id TEXT, table_name TEXT, layer TEXT)', creation_schema);
		-- Iterate through each inventory
		FOR inventory_record IN 
			EXECUTE format('SELECT * FROM %s', inventory_source)
		LOOP
			RAISE NOTICE 'Create Raw Validation- (%) Processing inventory: %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory;
			RAISE NOTICE 'Create Raw Validation- (%) Deleting existing error entries', LEFT(clock_timestamp()::TEXT, 19);
			EXECUTE format('DELETE FROM %s.%s WHERE left(cas_id,4) = ''%s''', creation_schema, dest_table_name, inventory_record.inventory);
			-- Iterate through each casfri table (other than cas_all and geo_all)
			FOR table_name IN (VALUES ('lyr_all'), ('eco_all'), ('nfl_all'), ('cas_all'))
			LOOP
				RAISE NOTICE 'Create Raw Validation- (%) 	Processing table: %', LEFT(clock_timestamp()::TEXT, 19), table_name;
				-- Create raw joined table if it doesn't exist
				EXECUTE format('SELECT * FROM createRawJoinedTable(''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'')', 
				table_name, casfri_table_schema, inventory_record.inventory, inventory_schema, attribute_dependencies_location, creation_schema);
				operation_count := operation_count + 1;
				EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
				RAISE NOTICE 'Create Raw Validation- (%) 		Created %_raw_%_joined table - %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, table_name, msg;
				-- Run basic validation on the raw joined table
				EXECUTE format('SELECT runRawValidationOnTable::integer FROM runRawValidationOnTable(''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'', TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''), ''%s'', ''%s'', ''%s'', ''%s'')',
				dest_table_name, creation_schema, table_name, casfri_table_schema, inventory_record.inventory, total_operations, start_time, operation_count, excluded_columns_location, attribute_dependencies_location, complex_mappings_location) INTO operation_count;
				EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
				RAISE NOTICE 'Create Raw Validation- (%) 		Ran null validation on %_raw_%_joined- %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, table_name, msg;
				
			END LOOP;
			-- Run species validation
			EXECUTE format('SELECT * FROM runSpeciesValidationOnInventory(''%s'', ''%s'', ''%s'', ''%s'')',
			dest_table_name, inventory_record.inventory, creation_schema, attribute_dependencies_location);
			operation_count := operation_count + 5;
			EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
			RAISE NOTICE 'Create Raw Validation- (%) 	Ran species validation on % - %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, msg;
			-- Run disturbance validation
			-- MB13 doesn't contain any disturbance data
			IF inventory_record.inventory != 'MB13'
			THEN
				EXECUTE format('SELECT * FROM createRawJoinedTable(''dst_all'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'')', 
				casfri_table_schema, inventory_record.inventory, inventory_schema, attribute_dependencies_location, creation_schema);
				EXECUTE format('SELECT * FROM runDisturbanceValidationOnInventory(''%s'', ''%s'', ''%s'', ''%s'')',
				dest_table_name, inventory_record.inventory, creation_schema, attribute_dependencies_location);
			END IF;
			operation_count := operation_count + 3;
			EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
			RAISE NOTICE 'Create Raw Validation- (%) 	Ran disturbance validation on % - %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, msg;
			-- Run NFL validation
			EXECUTE format('SELECT * FROM runNFLValidationOnInventory(''%s'', ''%s'', ''%s'', ''%s'')',
			dest_table_name, inventory_record.inventory, creation_schema, attribute_dependencies_location);
			operation_count := operation_count + 3;
			EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
			RAISE NOTICE 'Create Raw Validation- (%) 	Ran NFL validation on % - %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, msg;	
			-- Collect other errors
			RAISE NOTICE 'Create Raw Validation- (%) 	Collecting other errors on % - %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, msg;	
			EXECUTE format('SELECT * FROM runNumOfLayersValidationOnInventory(''%s'',''%s'',''%s'',''%s'')', inventory_record.inventory, dest_table_name, creation_schema, casfri_table_schema);
			operation_count := operation_count + 1;
			-- Refresh aggregated view
			EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
			RAISE NOTICE 'Create Raw Validation- (%) 	Refreshing aggregated stats for % - %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, msg;		
			EXECUTE format('SELECT * FROM refreshValidationSummaryForInventory(''%s'')', inventory_record.inventory);
			operation_count := operation_count + 1;
		END LOOP;
		-- Update error codes
		EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
		RAISE NOTICE 'Create Raw Validation- (%) 	Updating error types - %', LEFT(clock_timestamp()::TEXT, 19), msg;		
		EXECUTE format('SELECT * FROM updateValidationErrorCodes(''%s'',''%s'')', dest_table_name, creation_schema);
		operation_count := operation_count + 1;
		RAISE NOTICE 'Create Raw Validation- (%) Function completed ----------', LEFT(clock_timestamp()::TEXT, 19);
	END;
	$func$
LANGUAGE 'plpgsql';


