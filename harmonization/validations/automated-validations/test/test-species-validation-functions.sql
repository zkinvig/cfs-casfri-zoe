SET search_path = casfri_validation, casfri_metrics, casfri50, casfri_web, casfri_post_processing, rawfri, "translation", public;

-- Tests all functions in species_validation_functions.sql to ensure that the correct result is returned.
-- Run as script to see results of each test (you can also comment out the drop table at the end of the file to persist results)
-- Ensure that your search path is set with casfri_validation first. Most table creations should default to casfri_validation, but things may break if a different schema is used for this test

DROP TABLE IF EXISTS casfri_validation.validations_test_results;
CREATE TABLE IF NOT EXISTS casfri_validation.validations_test_results (test TEXT, result TEXT);
----------------------------------------------------------------------------------

-- Test for getSpeciesCutoff(inventory, attribute_dependencies_location)
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	"table" TEXT,
	species_1 TEXT,
	species_2 TEXT,
	species_3 TEXT,
	species_4 TEXT,
	species_5 TEXT,
	species_6 TEXT,
	species_7 TEXT,
	species_8 TEXT,
	species_9 TEXT,
	species_10 TEXT
);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
('TE00', '1', '''LYR''', '', '', '', '', '', '', '', '', '', ''),
('TE01', '1', '''LYR''', 'sp1', '', '', '', '', '', '', '', '', ''),
('TE02', '1', '''LYR''', 'sp', 'sp', '', '', '', '', '', '', '', ''),
('TE03', '1', '''LYR''', 'species', 'species', 'species', '', '', '', '', '', '', ''),
('TE04', '1', '''LYR''', 'sp1', 'sp2', 'sp3', 'sp4', '', '', '', '', '', ''),
('TE05', '1', '''LYR''', 'a', 'b', 'c', 'd', 'e', '', '', '', '', ''),
('TE06', '1', '''LYR''', 'sp1', 'sp2', 'sp3', 'sp4', 'sp5', 'sp6', '', '', '', ''),
('TE07', '1', '''LYR''', 'sp1', 'sp2', 'sp3', 'sp4', 'sp5', 'sp6', 'sp7', '', '', ''),
('TE08', '1', '''LYR''', 'sp1', 'sp2', 'sp3', 'sp4', 'sp5', 'sp6', 'sp7', 'sp8', '', ''),
('TE09', '1', '''LYR''', 'sp1', 'sp2', 'sp3', 'sp4', 'sp5', 'sp6', 'sp7', 'sp8', 'sp9', ''),
('TE10', '1', '''LYR''', 'sp1', 'sp2', 'sp3', 'sp4', 'sp5', 'sp6', 'sp7', 'sp8', 'sp9', 'sp10');

WITH row_validity AS (
	SELECT getSpeciesCutoff('TE00', 'casfri_validation.attribute_dependencies_test') = 1 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE01', 'casfri_validation.attribute_dependencies_test') = 2 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE02', 'casfri_validation.attribute_dependencies_test') = 3 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE03', 'casfri_validation.attribute_dependencies_test') = 4 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE04', 'casfri_validation.attribute_dependencies_test') = 5 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE05', 'casfri_validation.attribute_dependencies_test') = 6 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE06', 'casfri_validation.attribute_dependencies_test') = 7 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE07', 'casfri_validation.attribute_dependencies_test') = 8 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE08', 'casfri_validation.attribute_dependencies_test') = 9 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE09', 'casfri_validation.attribute_dependencies_test') = 10 AS value
	UNION ALL
	SELECT getSpeciesCutoff('TE10', 'casfri_validation.attribute_dependencies_test') = 0 AS value
)
INSERT INTO validations_test_results SELECT 'getSpeciesCutoff' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;
----------------------------------------------------------------------------------

-- Test for singleSpeciesRowValidity(inventory, species_num, species, species_per, species_string, source_layer)
WITH row_validity AS (
	SELECT singleSpeciesRowValidity('MB03', 1, 'UNKNOWN_VALUE', '-8887', '', '1') = 'VALID' AS value
	UNION ALL
	SELECT singleSpeciesRowValidity('MB03', 7, 'PICE_MAR_###', '100', 'PM10', '1') = 'FALSE_MAPPED' AS value
	UNION ALL
	SELECT singleSpeciesRowValidity('MB03', 2, 'PICE_MAR_###', '50', 'PE7RE3', '1') = 'VALID' AS value
	UNION ALL
	SELECT singleSpeciesRowValidity('ON02', 2, 'EMPTY_STRING', '-8888', 'PE 100', '1') = 'VALID' AS value
	UNION ALL
	SELECT singleSpeciesRowValidity('ON02', 1, 'PICE_MAR_###', '-8888', 'PE 50RE 50', '1') = 'FALSE_NULL' AS value
	UNION ALL
	SELECT singleSpeciesRowValidity('QC08', 1, 'PICE_MAR_###', '100', 'PE7RE3', '2') = 'FALSE_MAPPED' AS value
	UNION ALL
	SELECT singleSpeciesRowValidity('QC03', 2, 'GENH_SPP_###', '75', 'ALF', '1') = 'VALID' AS value
	UNION ALL
	SELECT singleSpeciesRowValidity('QC04', 2, 'NULL_VALUE', '75', 'ALF', '1') = 'FALSE_MAPPED' AS value
	UNION ALL
	SELECT singleSpeciesRowValidity('QC05', 4, 'GENH_SPP_###', '75', 'AL40BL10FR10GR10PR10', '1') = 'FALSE_MAPPED' AS value
	UNION ALL
	SELECT singleSpeciesRowValidity('QC05', 1, 'GENH_SPP_###', '75', 'ABCDEF', '1') = 'FALSE_MAPPED' AS value
)
INSERT INTO validations_test_results SELECT 'singleSpeciesRowValidity' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

----------------------------------------------------------------------------------

-- Test for runSpeciesValidationOnInventory(dest_table_name,inventory,creation_schema, attribute_dependencies_location)
-- NOTE: This is NOT a comprehensive test since there are too many inventories to distinctly test for, this just goes through one possible case
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	"table" TEXT,
	species_1 TEXT,
	species_per_1 TEXT,
	species_2 TEXT,
	species_per_2 TEXT,
	species_3 TEXT,
	species_per_3 TEXT,
	species_4 TEXT,
	species_per_4 TEXT,
	species_5 TEXT,
	species_per_5 TEXT,
	species_6 TEXT,
	species_per_6 TEXT,
	species_7 TEXT,
	species_per_7 TEXT,
	species_8 TEXT,
	species_per_8 TEXT,
	species_9 TEXT,
	species_per_9 TEXT,
	species_10 TEXT,
	species_per_10 TEXT
);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
-- Leave out l1_species_pct_2 to have a missing dependency test
('BC80', '1', '''LYR''', 'l1_species_cd_1', 'l1_species_pct_1', 'l1_species_cd_2', '', 'l1_species_cd_3', 'l1_species_pct_3', 'l1_species_cd_4', 'l1_species_pct_4', 'l1_species_cd_5', 'l1_species_pct_5', 'l1_species_cd_6', 'l1_species_pct_6', '', '', '', '', '', '', '', ''),
('BC80', '2', '''LYR''', 'l2_species_cd_1', 'l2_species_pct_1', 'l2_species_cd_2', 'l2_species_pct_2', 'l2_species_cd_3', 'l2_species_pct_3', 'l2_species_cd_4', 'l2_species_pct_4', 'l2_species_cd_5', 'l2_species_pct_5', 'l2_species_cd_6', 'l2_species_pct_6', '', '', '', '', '', '', '', '');

CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_lyr_all_joined (
	cas_id TEXT,
	source_layer TEXT,
	species_1 TEXT,
	species_per_1 TEXT,
	species_2 TEXT,
	species_per_2 TEXT,
	species_3 TEXT,
	species_per_3 TEXT,
	species_4 TEXT,
	species_per_4 TEXT,
	species_5 TEXT,
	species_per_5 TEXT,
	species_6 TEXT,
	species_per_6 TEXT,
	species_7 TEXT,
	species_per_7 TEXT,
	species_8 TEXT,
	species_per_8 TEXT,
	species_9 TEXT,
	species_per_9 TEXT,
	species_10 TEXT,
	species_per_10 TEXT,
	raw_l1_species_cd_1 TEXT,
	raw_l1_species_pct_1 TEXT,
	raw_l1_species_cd_2 TEXT,
	raw_l1_species_pct_2 TEXT,
	raw_l1_species_cd_3 TEXT,
	raw_l1_species_pct_3 TEXT,
	raw_l1_species_cd_4 TEXT,
	raw_l1_species_pct_4 TEXT,
	raw_l1_species_cd_5 TEXT,
	raw_l1_species_pct_5 TEXT,
	raw_l1_species_cd_6 TEXT,
	raw_l1_species_pct_6 TEXT,
	raw_l2_species_cd_1 TEXT,
	raw_l2_species_pct_1 TEXT,
	raw_l2_species_cd_2 TEXT,
	raw_l2_species_pct_2 TEXT,
	raw_l2_species_cd_3 TEXT,
	raw_l2_species_pct_3 TEXT,
	raw_l2_species_cd_4 TEXT,
	raw_l2_species_pct_4 TEXT,
	raw_l2_species_cd_5 TEXT,
	raw_l2_species_pct_5 TEXT,
	raw_l2_species_cd_6 TEXT,
	raw_l2_species_pct_6 TEXT
);

INSERT INTO casfri_validation.bc80_raw_lyr_all_joined VALUES 
-- FALSE_MAPPED due to no dependency
('BC80-TEST-1', '1', 'PICE_MAR_####', '50', 'ABIE_BAL_####', '50', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888',
'PM', '50', 'AB', '50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
NULL, '100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
-- FALSE_MAPPED due to null value
('BC80-TEST-1', '2', 'ABIE_LAS_####', '100', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888',
'PM', '50', 'AB', '50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
NULL, '100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
-- FALSE_NULLs and translation error
('BC80-TEST-2', '1', 'PICE_MAR_####', '-8888', 'NULL_VALUE', '50', 'INVALID_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888',
'PM', '50', 'AB', '50', 'I', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
-- Mapped past maximum possible
('BC80-TEST-2', '2', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'ABIE_BAL_####', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888',
'PM', '50', 'AB', '50', 'I', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);

SELECT * FROM runSpeciesValidationOnInventory('validation_error_rows_test', 'BC80', 'casfri_validation', 'casfri_validation.attribute_dependencies_test');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-TEST-1' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'species_per_2 was mapped but a raw value doesnt exist' AND source_layer = '1')
		OR   (cas_id = 'BC80-TEST-1' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'species_1 was mapped but a raw value doesnt exist' AND source_layer = '2')
		OR   (cas_id = 'BC80-TEST-2' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'species_per_1 was null when it could have been mapped from raw' AND source_layer = '1')
		OR   (cas_id = 'BC80-TEST-2' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'species_2 was null when it could have been mapped from raw' AND source_layer = '1')
		OR   (cas_id = 'BC80-TEST-2' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'species_per_2 was mapped but a raw value doesnt exist' AND source_layer = '1')
		OR   (cas_id = 'BC80-TEST-2' AND table_name = 'lyr_all' AND error_type = 'TRANSLATION_ERROR' AND error_desc = 'species_3 has a translation error' AND source_layer = '1')
		OR   (cas_id = 'BC80-TEST-2' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'A species was mapped when it should have been non-applicable (7-10)' AND source_layer = '2')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runSpeciesValidationOnInventory' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are 6 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 6
	-- Check that there are exactly 7 rows in the result
	AND (SELECT count(*) FROM test_result) = 7
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;


DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;
DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;

----------------------------------------------------------------------------------

-- Display results
SELECT * FROM casfri_validation.validations_test_results;
DROP TABLE IF EXISTS casfri_validation.validations_test_results;
