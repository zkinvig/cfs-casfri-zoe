SET search_path = casfri_validation, casfri_metrics, casfri50, casfri_web, casfri_post_processing, rawfri, "translation", public;


-- Tests all functions in grafana_validation_helper_functions.sql to ensure that the correct result is returned.
-- Run as script to see results of each test (you can also comment out the drop table at the end of the file to persist results)
-- Ensure that your search path is set with casfri_validation first. Most table creations should default to casfri_validation, but things may break if a different schema is used for this test

DROP TABLE IF EXISTS casfri_validation.validations_test_results;
CREATE TABLE IF NOT EXISTS casfri_validation.validations_test_results (test TEXT, result TEXT);
----------------------------------------------------------------------------------

-- Test for getTranslationTable(inventory TEXT, casfri_table TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.hdr_all_test (inventory_id TEXT, standard_id TEXT);
INSERT INTO casfri_validation.hdr_all_test VALUES
('BC10', 'VRI01'),
('YT04', 'YVI02'),
('AB29', 'AVI01');

WITH row_validity AS (
	SELECT getTranslationTable('BC10', 'lyr_all', 'casfri_validation.hdr_all_test') = 'BC_VRI01_lyr' AS value
	UNION ALL
	SELECT getTranslationTable('BC10', 'geo_all', 'casfri_validation.hdr_all_test') = 'BC_VRI01_geo' AS value
	UNION ALL
	SELECT getTranslationTable('YT04', 'cas_all', 'casfri_validation.hdr_all_test') = 'YT_YVI01_cas' AS value
	UNION ALL
	SELECT getTranslationTable('YT04', 'nfl_all', 'casfri_validation.hdr_all_test') = 'YT_YVI03_nfl' AS value
	UNION ALL
	SELECT getTranslationTable('AB29', 'lyr_all', 'casfri_validation.hdr_all_test') = 'AB_AVI01_lyr' AS value	
)
INSERT INTO validations_test_results  SELECT 'getTranslationTable' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.hdr_all_test;
----------------------------------------------------------------------------------

-- Test for getTranslationRow(inventory TEXT, casfri_table TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.hdr_all_test (inventory_id TEXT, standard_id TEXT);
INSERT INTO casfri_validation.hdr_all_test VALUES
('BC80', 'VRI01');

CREATE TABLE IF NOT EXISTS casfri_validation.bc_vri01_lyr (rule_id TEXT, target_attribute TEXT, target_attribute_type TEXT, validation_rules TEXT, translation_rules TEXT, description TEXT);
INSERT INTO casfri_validation.bc_vri01_lyr VALUES
('1', 'origin_upper', 'INT', 'isInt()', 'copyInt()', 'Test desc'),
('2', 'origin_lower', 'TEXT', 'isString()', 'copyString()', 'Test desc2');

WITH test_result AS (
	SELECT *
	FROM getTranslationRow('BC80', 'lyr_all', 'origin_upper', 'casfri_validation.hdr_all_test', 'casfri_validation')	
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (rule_id = '1' AND target_attribute = 'origin_upper' AND target_attribute_type = 'INT' AND validation_rules = 'isInt()' AND translation_rules = 'copyInt()' AND description = 'Test desc')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'getTranslationRow' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct rule id
	AND (SELECT count(DISTINCT rule_id) FROM test_result) = 1
	-- Check that there are exactly 1 row in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.hdr_all_test;
DROP TABLE IF EXISTS casfri_validation.bc_vri01_lyr;
----------------------------------------------------------------------------------

-- Test for getAttributeDependencies(inventory TEXT, casfri_table TEXT, attribute_name TEXT, attribute_dependencies_location TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (ogc_fid INT, inventory_id TEXT, layer TEXT, "table" TEXT, ttable_exists TEXT, crown_closure_upper TEXT);
INSERT INTO casfri_validation.attribute_dependencies_test VALUES
(1, 'BC',   '1', '', 'yes', 'crown_closure_upper'),
(2, 'BC80', '1', 'LYR', 'no', 'cc1'),
(3, 'BC80', '2', 'LYR', 'no', 'cc2'),
(4, 'BC81', '1', 'LYR', 'no', 'ccu1'),
(5, 'BC81', '2', 'LYR', 'no', 'ccu2');

WITH test_result AS (
	SELECT *
	FROM getAttributeDependencies('BC81', 'lyr_all', 'crown_closure_upper', 'casfri_validation.attribute_dependencies_test')	
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory_id = 'BC' AND layer = '1' AND "table" = '' AND dependency = 'crown_closure_upper')
		OR (inventory_id = 'BC81' AND layer = '1' AND "table" = 'LYR' AND dependency = 'ccu1')
		OR (inventory_id = 'BC81' AND layer = '2' AND "table" = 'LYR' AND dependency = 'ccu2')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'getAttributeDependencies' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 3 distinct dependencies
	AND (SELECT count(DISTINCT dependency) FROM test_result) = 3
	-- Check that there are exactly 3 rows in the result
	AND (SELECT count(*) FROM test_result) = 3
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;
----------------------------------------------------------------------------------

-- Test for appendCommaSeparated(arr TEXT[],string TEXT,prefix TEXT)
WITH row_validity AS (
	SELECT appendCommaSeparated('{}', 'test, test1, test2', 'raw_') = '{raw_test,raw_test1,raw_test2}' AS value
	UNION ALL
	SELECT appendCommaSeparated('{test}', 'test1, test2', '') = '{test,test1,test2}' AS value
	UNION ALL
	SELECT appendCommaSeparated('{test}', 'test, test, test2, test, test3', 'raw_') = '{test,raw_test,"raw_test AS raw_test_2",raw_test2,"raw_test AS raw_test_3",raw_test3}' AS value
	UNION ALL
	SELECT appendCommaSeparated('{test}', 'test, ''NO_DEPENDENCY'' AS dep, test', 'a_') = '{test,a_test,"''NO_DEPENDENCY'' AS dep","a_test AS a_test_2"}' AS value
	UNION ALL
	SELECT appendCommaSeparated('{test, b_val}', 'val, val', 'b_') = '{test,b_val,"b_val AS b_val_2","b_val AS b_val_3"}' AS value
	UNION ALL
	SELECT appendCommaSeparated('{}', 'val', 'test_') = '{test_val}' AS value
	UNION ALL
	SELECT appendCommaSeparated('{}', '', 'test_') = '{}' AS value
)
INSERT INTO validations_test_results  SELECT 'appendCommaSeparated' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

----------------------------------------------------------------------------------

-- Test for getInvestigateValidationRowQuery(table_name,error_desc,source_layer,cas_id,attribute_dependencies_location)
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	"table" TEXT,
	-- species validations
	species_1 TEXT,
	species_per_1 TEXT,
	species_2 TEXT,
	species_per_2 TEXT,
	species_3 TEXT,
	species_per_3 TEXT,
	species_4 TEXT,
	species_per_4 TEXT,
	species_5 TEXT,
	species_per_5 TEXT,
	species_6 TEXT,
	species_per_6 TEXT,
	species_7 TEXT,
	species_per_7 TEXT,
	species_8 TEXT,
	species_per_8 TEXT,
	species_9 TEXT,
	species_per_9 TEXT,
	species_10 TEXT,
	species_per_10 TEXT,
	-- disturbance validations
	dist_type_1 TEXT,
	dist_year_1 TEXT,
	dist_ext_upper_1 TEXT,
	dist_ext_lower_1 TEXT,
	dist_type_2 TEXT,
	dist_year_2 TEXT,
	dist_ext_upper_2 TEXT,
	dist_ext_lower_2 TEXT,
	dist_type_3 TEXT,
	dist_year_3 TEXT,
	dist_ext_upper_3 TEXT,
	dist_ext_lower_3 TEXT,
	-- nfl valdiations
	nat_non_veg TEXT,
	non_for_anth TEXT,
	non_for_veg TEXT,
	-- other valdiation
	num_of_layers TEXT
);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
('BC80', '1', '''LYR''',
'sp1', 'per1', 'sp2', 'per2', 'sp3', 'per3', 'sp4', 'per4', 'sp5', 'per5', 'sp6', 'per6', '', '', '', '', '', '', '', '',
'd1', 'dy1', 'deu1', 'del1', 'd2', 'dy2', 'deu2', 'del2', '', '', '', '',
'', '', '',
'layers, lyrs2'),
('BC80', '2', '''NFL''',
'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
'', '', '', '', '', '', '', '', '', '', '', '',
'nat_veg', 'non_anth', 'non_veg',
'layers, lyrs2'),
('BC80', '3', '''LYR''',
'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
'', '', '', '', '', '', '', '', '', '', '', '',
'', '', '',
'wkb_geometry, other, wkb_geometry');


WITH row_validity AS (
	SELECT getInvestigateValidationRowQuery('lyr_all','species_1 was null when it could have been mapped from raw','1','BC80-1','casfri_validation.attribute_dependencies_test') 
	= 'SELECT cas_id, source_layer, species_1, raw_sp1, species_per_1, raw_per1, species_2, raw_sp2, species_per_2, raw_per2, species_3, raw_sp3, species_per_3, raw_per3, species_4, raw_sp4, species_per_4, raw_per4, species_5, raw_sp5, species_per_5, raw_per5, species_6, raw_sp6, species_per_6, raw_per6, species_7, ''NO_DEPENDENCY'' AS dep_species_7, species_per_7, ''NO_DEPENDENCY'' AS dep_species_per_7, species_8, ''NO_DEPENDENCY'' AS dep_species_8, species_per_8, ''NO_DEPENDENCY'' AS dep_species_per_8, species_9, ''NO_DEPENDENCY'' AS dep_species_9, species_per_9, ''NO_DEPENDENCY'' AS dep_species_per_9, species_10, ''NO_DEPENDENCY'' AS dep_species_10, species_per_10, ''NO_DEPENDENCY'' AS dep_species_per_10 FROM casfri_validation.BC80_raw_lyr_all_joined where cas_id = ''BC80-1'' and source_layer::integer = 1;' AS value
	UNION ALL
	SELECT getInvestigateValidationRowQuery('dst_all','dist_year_1 was mapped but a raw value doesnt exist','1','BC80-2','casfri_validation.attribute_dependencies_test') 
	= 'SELECT cas_id, source_layer, dist_type_1, raw_d1, dist_year_1, raw_dy1, dist_ext_upper_1, raw_deu1, dist_ext_lower_1, raw_del1, dist_type_2, raw_d2, dist_year_2, raw_dy2, dist_ext_upper_2, raw_deu2, dist_ext_lower_2, raw_del2, dist_type_3, ''NO_DEPENDENCY'' AS dep_dist_type_3, dist_year_3, ''NO_DEPENDENCY'' AS dep_dist_year_3, dist_ext_upper_3, ''NO_DEPENDENCY'' AS dep_dist_ext_upper_3, dist_ext_lower_3, ''NO_DEPENDENCY'' AS dep_dist_ext_lower_3 FROM casfri_validation.BC80_raw_dst_all_joined where cas_id = ''BC80-2'' and source_layer::integer = 1;' AS value
	UNION ALL
	SELECT getInvestigateValidationRowQuery('nfl_all','nat_non_veg had a translation error','2','BC80-3','casfri_validation.attribute_dependencies_test') 
	= 'SELECT cas_id, source_layer, nat_non_veg, raw_nat_veg, non_for_anth, raw_non_anth, non_for_veg, raw_non_veg FROM casfri_validation.BC80_raw_nfl_all_joined where cas_id = ''BC80-3'' and source_layer::integer = 2;' AS value
	UNION ALL
	SELECT getInvestigateValidationRowQuery('cas_all','num_of_layers was 1 but lyr_count is 2 and nfl_count is 0','1','BC80-4','casfri_validation.attribute_dependencies_test') 
	= 'SELECT cas_id, source_layer, num_of_layers, raw_layers, raw_lyrs2 FROM casfri_validation.BC80_raw_cas_all_joined where cas_id = ''BC80-4'' and source_layer::integer = 1;' AS value	
	UNION ALL
	SELECT getInvestigateValidationRowQuery('lyr_all','num_of_layers was null when it could have been mapped from raw','3','BC80-1','casfri_validation.attribute_dependencies_test')
	= 'SELECT cas_id, source_layer, num_of_layers, ''wkb_geometry'' AS wkb_geometry, raw_other, ''wkb_geometry'' AS wkb_geometry_2 FROM casfri_validation.BC80_raw_lyr_all_joined where cas_id = ''BC80-1'' and source_layer::integer = 3;' AS value	
)
INSERT INTO validations_test_results  SELECT 'getInvestigateValidationRowQuery' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;
----------------------------------------------------------------------------------

SELECT * FROM casfri_validation.validations_test_results;
DROP TABLE IF EXISTS casfri_validation.validations_test_results;

