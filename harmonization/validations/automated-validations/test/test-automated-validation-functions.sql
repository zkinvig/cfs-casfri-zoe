SET search_path = casfri_validation, casfri_metrics, casfri50, casfri_web, casfri_post_processing, rawfri, "translation", public;

-- Tests all functions in automated-validation-functions.sql to ensure that the correct result is returned.
-- Run as script to see results of each test (you can also comment out the drop table at the end of the file to persist results)
-- Ensure that your search path is set with casfri_validation first. Most table creations should default to casfri_validation, but things may break if a different schema is used for this test

DROP TABLE IF EXISTS casfri_validation.validations_test_results;
CREATE TABLE IF NOT EXISTS casfri_validation.validations_test_results (test TEXT, result TEXT);
----------------------------------------------------------------------------------

-- Test for getMergedColumnNames(inventory, cas_table_name, inventory_schema, cas_table_schema, inventory_alias, cas_table_name_alias, inventory_prefix)
CREATE TABLE IF NOT EXISTS casfri_validation.getMergedColumnNames_casfri (
	col1 TEXT,
	col2 TEXT);

CREATE TABLE IF NOT EXISTS casfri_validation.getMergedColumnNames_raw (
	col2 TEXT,
	col3 TEXT,
	wkb_geometry TEXT);

WITH row_validity AS (
	SELECT getMergedColumnNames('getMergedColumnNames_raw', 'getMergedColumnNames_casfri', 'casfri_validation', 'casfri_validation', 'r', 'l', 'raw') = 
	'l.col1, l.col2, r.col2 AS raw_col2, r.col3 AS raw_col3' AS value
	UNION ALL
	SELECT getMergedColumnNames('getMergedColumnNames_casfri', 'getMergedColumnNames_raw', 'casfri_validation', 'casfri_validation', 'a', 'b', 'test') = 
	'b.col2, b.col3, a.col1 AS test_col1, a.col2 AS test_col2' AS value
)
INSERT INTO validations_test_results SELECT 'getMergedColumnNames' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.getMergedColumnNames_raw;
DROP TABLE IF EXISTS casfri_validation.getMergedColumnNames_casfri;

----------------------------------------------------------------------------------

-- Test for getAttributeDependency(inventory, attribute_name, attribute_dependencies_location, layer)
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	col1 TEXT,
	col2 TEXT,
	nfl_height TEXT);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
('BC10', '1', 'test', 'test, testy', 'ht'),
('BC10', '2', 'test1', 'test2', 'ht2'),
('BC11', '1', 'something', '', 'ht3');


WITH row_validity AS (
	SELECT getAttributeDependency('BC10', 'col1', 'casfri_validation.attribute_dependencies_test', 1, 'tbl') =  'test' AS value
	UNION ALL
	SELECT getAttributeDependency('BC10', 'col2', 'casfri_validation.attribute_dependencies_test', 1, 'tbl') =  'test, testy' AS value
	UNION ALL
	SELECT getAttributeDependency('BC10', 'col1', 'casfri_validation.attribute_dependencies_test', 2, 'tbl') =  'test1' AS value
	UNION ALL
	SELECT getAttributeDependency('BC10', 'col2', 'casfri_validation.attribute_dependencies_test', 2, 'tbl') =  'test2' AS value
	UNION ALL
	SELECT getAttributeDependency('BC11', 'col1', 'casfri_validation.attribute_dependencies_test', 1, 'tbl') =  'something' AS value
	UNION ALL
	SELECT getAttributeDependency('BC11', 'col2', 'casfri_validation.attribute_dependencies_test', 1, 'tbl') =  '' AS value
	UNION ALL
	SELECT getAttributeDependency('BC10', 'height_upper', 'casfri_validation.attribute_dependencies_test', 1, 'nfl_all') =  'ht' AS value
)
INSERT INTO validations_test_results SELECT 'getAttributeDependency' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;

----------------------------------------------------------------------------------

-- Test for sourceLayerMappingHelper(map_type, var)
WITH row_validity AS (
	SELECT sourceLayerMappingHelper('yt03_nat_non_veg', 'Lake') = 'LAKE' AS value
	UNION ALL
	SELECT sourceLayerMappingHelper('yt03_non_for_anth', 'Airport') = 'FACILITY_INFRASTRUCTURE' AS value
	UNION ALL
	SELECT sourceLayerMappingHelper('yt03_non_for_veg', 'Low Shrub') = 'LOW_SHRUB' AS value
	UNION ALL
	SELECT sourceLayerMappingHelper('nb01_dist', 'BB') = 'BURN' AS value
	UNION ALL
	SELECT sourceLayerMappingHelper('unknown', 'Lake') = 'NOT_MAPPED' AS value
	UNION ALL
	SELECT sourceLayerMappingHelper('yt03_nat_non_veg', 'Lake2') = 'NOT_MAPPED' AS value
)
INSERT INTO validations_test_results SELECT 'sourceLayerMappingHelper' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

----------------------------------------------------------------------------------

-- Test for getSourceLayer(raw_merged_table, creation_schema, attribute_dependencies_location, cas_id, ... ) 
WITH row_validity AS (
	SELECT getSourceLayer(raw_merged_table => 'bc08_raw_cas_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'BC08-1234') = 1 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'ab30_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'AB30-1234') = 1 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'ab29_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'AB29-1234', layer_rank => '2') = 2 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'nb03_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'NB03-1234', layer_rank => '2') = 2 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'nb03_raw_nfl_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'NB03-1234', layer_rank => '2') = 3 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'yt03_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'YT03-1234', layer_rank => '2') = 2 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'yt03_raw_nfl_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'YT03-1234', nat_non_veg => 'LAKE', water => 'Lake') = 3 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'qc09_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'QC09-1234', layer => '3') = 3 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'qc03_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'QC03-1234', species_1 => 'UNKNOWN_VALUE') = 2 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'qc05_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'QC05-1234', height_upper => '12', sup_hauteur => '12', inf_hauteur => '13') = 1 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'bc08_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'BC08-1234', layer_rank => '-8888', l1_for_cover_rank => NULL, l2_for_cover_rank => '1') = 1 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'bc08_raw_nfl_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'BC08-1234', nat_non_veg => 'INVALID_VALUE', non_for_anth => 'NULL_VALUE', non_for_veg => 'TEST') = 3 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'mb06_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'MB06-1234', layer_rank => '2', canrank => '1', us2canrank => '2') = 2 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'mb06_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'MB06-1234', layer_rank => '-8888', height_upper => '12', ht => '12') = 1 AS value
	UNION ALL
	SELECT getSourceLayer(raw_merged_table => 'mb05_raw_lyr_all_joined', creation_schema => 'casfri_validation', attribute_dependencies_location => 'translation.attribute_dependencies',
						  cas_id => 'MB05-1234') = 1 AS value
)
INSERT INTO validations_test_results SELECT 'getSourceLayer' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

----------------------------------------------------------------------------------

-- Test for updateSourceLayers(casfri_table_name, inventory, attribute_dependencies_location, creation_schema)
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_lyr_all_joined (
	cas_id TEXT,
	layer INTEGER,
	layer_rank INTEGER,
	raw_l1_for_cover_rank_cd INTEGER,
	raw_l2_for_cover_rank_cd INTEGER,
	source_layer TEXT,
	height_upper INTEGER,
	crown_closure_upper INTEGER,
	raw_l1_proj_height_1 INTEGER,
	raw_l1_proj_height_2 INTEGER,
	raw_l2_proj_height_1 INTEGER,
	raw_l2_proj_height_2 INTEGER,
	raw_l1_species_pct_1 INTEGER,
	raw_l1_species_pct_2 INTEGER,
	raw_l2_species_pct_1 INTEGER,
	raw_l2_species_pct_2 INTEGER,
	raw_l1_crown_closure INTEGER,
	raw_l2_crown_closure INTEGER
);

INSERT INTO casfri_validation.bc80_raw_lyr_all_joined VALUES 
('BC80-1234-TEST_VAL', 1,    1,   NULL,   1,   NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('BC80-1235-TEST_VAL', 1,    1,    1,   NULL,  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('BC80-1236-TEST_VAL', 1, -8888,  NULL,   1,   NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('BC80-1237-TEST_VAL', 1, -8888,   1,   NULL,  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

SELECT * FROM casfri_validation.updateSourceLayers('lyr_all', 'BC80', NULL, 'casfri_validation');

WITH test_result AS (
	SELECT * 
	FROM bc80_raw_lyr_all_joined
	ORDER BY cas_id
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-1234-TEST_VAL' AND source_layer = '2')
		OR   (cas_id = 'BC80-1235-TEST_VAL' AND source_layer = '1')
		OR   (cas_id = 'BC80-1236-TEST_VAL' AND source_layer = '1')
		OR   (cas_id = 'BC80-1237-TEST_VAL' AND source_layer = '2')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'updateSourceLayers' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are four distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 4
	-- Check that there are exactly 4 rows in the result
	AND (SELECT count(*) FROM test_result) = 4
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;
DELETE FROM layersnotmapped WHERE right(cas_id, 8) = 'TEST_VAL';

----------------------------------------------------------------------------------

-- Test for createRawJoinedTable(casfri_table_name, casfri_table_schema, inventory, inventory_schema, attribute_dependencies_location, creation_schema)
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	"table" TEXT,
	cas_id TEXT
);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
('BC80', '1', '''LYR''', 'inventory_id, src_filename, map_id, feature_id, ogc_fid');

CREATE TABLE IF NOT EXISTS casfri_validation.bc80 (
	inventory_id TEXT,
	src_filename TEXT,
	map_id TEXT,
	feature_id TEXT,
	ogc_fid TEXT,
	l1_for_cover_rank_cd INTEGER,
	l2_for_cover_rank_cd INTEGER,
	l1_proj_height_1 INTEGER,
	l1_proj_height_2 INTEGER,
	l2_proj_height_1 INTEGER,
	l2_proj_height_2 INTEGER,
	l1_species_pct_1 INTEGER,
	l1_species_pct_2 INTEGER,
	l2_species_pct_1 INTEGER,
	l2_species_pct_2 INTEGER,
	l1_crown_closure INTEGER,
	l2_crown_closure INTEGER
);

INSERT INTO casfri_validation.bc80 VALUES 
('BC80', 'FILENAME', '1234', '345', '1', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('BC80', 'FILENAME2', '1234', '345', '2', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


CREATE TABLE IF NOT EXISTS casfri_validation.lyr_all (
	cas_id TEXT,
	layer INTEGER,
	layer_rank INTEGER,
	soil_moist_reg TEXT,
	height_upper INTEGER,
	crown_closure_upper INTEGER
);

INSERT INTO casfri_validation.lyr_all VALUES
('BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1', 1, 1, 'UNKNOWN', 13, NULL),
('BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1', 2, -8888, 'WET', 12, NULL),
('BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2', 1, -8888, 'DRY', 15, NULL),
('AB29-xxxxxxFILENAME4-xxxxx45678-xxxxxxx347-xxxxxx4', 1, 1, 'MOIST', 18, NULL);

SELECT * FROM casfri_validation.createRawJoinedTable('lyr_all', 'casfri_validation', 'BC80', 'casfri_validation', 'casfri_validation.attribute_dependencies_test', 'casfri_validation');

WITH test_result AS (
	SELECT * 
	FROM bc80_raw_lyr_all_joined
	ORDER BY cas_id
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1' AND layer = 1 AND height_upper = 13 AND raw_ogc_fid = '1' AND source_layer = '1')
		OR   (cas_id = 'BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1' AND layer = 2 AND height_upper = 12 AND raw_ogc_fid = '1' AND source_layer = '2')
		OR   (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND layer = 1 AND height_upper = 15 AND raw_ogc_fid = '2' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'createRawJoinedTable' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are three distinct height_uppers
	AND (SELECT count(DISTINCT height_upper) FROM test_result) = 3
	-- Check that there are exactly 3 rows in the result
	AND (SELECT count(*) FROM test_result) = 3
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.bc80;
DROP TABLE IF EXISTS casfri_validation.lyr_all;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;
DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;

----------------------------------------------------------------------------------

-- Test for dropRawJoinedTables(creation_schema, inventory_source)
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_lyr_all_joined (inventory_id TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_cas_all_joined (inventory_id TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_eco_all_joined (inventory_id TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_nfl_all_joined (inventory_id TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_dst_all_joined (inventory_id TEXT);

CREATE TABLE IF NOT EXISTS casfri_validation.bc81_raw_lyr_all_joined (inventory_id TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc81_raw_cas_all_joined (inventory_id TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc81_raw_eco_all_joined (inventory_id TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc81_raw_nfl_all_joined (inventory_id TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc81_raw_dst_all_joined (inventory_id TEXT);

SELECT * FROM dropRawJoinedTables('casfri_validation', '(VALUES (''BC80''), (''BC81''))  AS foo(inventory)');

WITH test_result AS (
	SELECT table_name 
	FROM information_schema.COLUMNS
	WHERE table_schema = 'casfri_validation' AND table_name IN ('bc80_raw_lyr_all_joined', 'bc80_raw_cas_all_joined', 'bc80_raw_eco_all_joined', 'bc80_raw_dst_all_joined', 'bc80_raw_nfl_all_joined',
																'bc81_raw_lyr_all_joined', 'bc81_raw_cas_all_joined', 'bc81_raw_eco_all_joined', 'bc81_raw_dst_all_joined', 'bc81_raw_nfl_all_joined')
)
INSERT INTO validations_test_results  SELECT 'dropRawJoinedTables' AS test,
(SELECT CASE
	WHEN (SELECT * FROM test_result) IS NULL
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM test_result
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_cas_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_eco_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_nfl_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_dst_all_joined;

DROP TABLE IF EXISTS casfri_validation.bc81_raw_lyr_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc81_raw_cas_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc81_raw_eco_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc81_raw_nfl_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc81_raw_dst_all_joined;

----------------------------------------------------------------------------------

-- Test for estimateRawValidationOperations(casfri_table_schema, inventory_source, excluded_columns_location)
CREATE TABLE IF NOT EXISTS casfri_validation.lyr_all (cas_id TEXT, col1 TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.eco_all (cas_id TEXT, col1 TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.nfl_all (cas_id TEXT, col1 TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.cas_all (cas_id TEXT, col2 TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.excluded_columns_test (column_name TEXT);

INSERT INTO casfri_validation.excluded_columns_test VALUES
('col2');


WITH row_validity AS (
	SELECT estimateRawValidationOperations('casfri_validation', '(VALUES (''BC80'')) as foo(inventory)', 'casfri_validation.excluded_columns_test') = 14 AS value
	UNION ALL
	SELECT estimateRawValidationOperations('casfri_validation', '(VALUES (''BC80''), (''BC81'')) as foo(inventory)', 'casfri_validation.excluded_columns_test') = 28 AS value
)
INSERT INTO validations_test_results SELECT 'estimateRawValidationOperations' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.lyr_all;
DROP TABLE IF EXISTS casfri_validation.eco_all;
DROP TABLE IF EXISTS casfri_validation.nfl_all;
DROP TABLE IF EXISTS casfri_validation.cas_all;
DROP TABLE IF EXISTS casfri_validation.excluded_columns_test;

----------------------------------------------------------------------------------

-- Test for runRawValidationOnTable()
CREATE TABLE IF NOT EXISTS casfri_validation.complex_attributes_empty_test (inventory TEXT, "table" TEXT, attribute_name TEXT, function_call TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	"table" TEXT,
	water TEXT,
	land TEXT,
	tree TEXT,
	per TEXT,
	poly_area TEXT,
	dist TEXT
);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
('BC80', '1', '''LYR''', 'lake',  'island, mainland', '', '100', 'wkb_geometry', 'SUPER_COMPLICATED_MAPPING'),
('BC80', '2', '''LYR''', 'lake2', 'island, mainland', '', '50',  'wkb_geometry', 'SUPER_COMPLICATED_MAPPING');

CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_lyr_all_joined (
	cas_id TEXT,
	water TEXT,
	land TEXT,
	tree TEXT,
	per INTEGER,
	poly_area FLOAT,
	dist TEXT,
	raw_lake TEXT,
	raw_lake2 TEXT,
	raw_island TEXT,
	raw_mainland TEXT,
	source_layer TEXT	
);

INSERT INTO casfri_validation.bc80_raw_lyr_all_joined VALUES 
('BC80-1', 'wet',               'land', 'SPRUCE_TREE', 100, 100.2, '1990',    'wet_lake', NULL,       'la', 'nd', '1'),
('BC80-1', 'watery',            'land', 'NULL_VALUE',   80, -8888, '1980',    'wet_lake', NULL,       'la', 'nd', '2'),
('BC80-2', 'TRANSLATION_ERROR', 'land', 'NOT_APPLICABLE', 100, 100.2, '1970', 'dry_lake', 'wet_lake', 'la', 'nd', '1');

CREATE TABLE IF NOT EXISTS casfri_validation.lyr_all (cas_id TEXT, water TEXT, land TEXT, tree TEXT, per INTEGER, poly_area FLOAT);

CREATE TABLE IF NOT EXISTS casfri_validation.excluded_columns_test (column_name TEXT);

INSERT INTO casfri_validation.excluded_columns_test VALUES
('dist'),
('cas_id');

CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);

SELECT * FROM runRawValidationOnTable('validation_error_rows_test', 'casfri_validation', 
'lyr_all', 'casfri_validation', 'BC80', 100, now(), 0, 
'casfri_validation.excluded_columns_test', 'casfri_validation.attribute_dependencies_test', 'casfri_validation.complex_attributes_empty_test');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
	ORDER BY cas_id
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-1' AND table_name = 'lyr_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'water is not an error code but raw value is empty' AND source_layer = '2')
		OR (cas_id = 'BC80-1' AND table_name = 'lyr_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'tree is not an error code but attribute does not exist in raw inventory' AND source_layer = '1')
		OR (cas_id = 'BC80-1' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'per does not match its numeric mapping' AND source_layer = '2')
		OR (cas_id = 'BC80-1' AND table_name = 'lyr_all' AND error_type = '-8888' AND error_desc = 'poly_area is empty but raw value exists' AND source_layer = '2')
		OR (cas_id = 'BC80-2' AND table_name = 'lyr_all' AND error_type = 'TRANSLATION_ERROR' AND error_desc = 'water has a translation error' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runRawValidationOnTable' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are five distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 5
	-- Check that there are exactly 5 rows in the result
	AND (SELECT count(*) FROM test_result) = 5
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;
DROP TABLE IF EXISTS casfri_validation.lyr_all;
DROP TABLE IF EXISTS casfri_validation.excluded_columns_test;
DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.complex_attributes_empty_test;

----------------------------------------------------------------------------------

-- Test for createRawValidationTableRunner()
-- NOTE: This is NOT a comprehensive test since there are too many inventories to distinctly test for, this just goes through one possible case
CREATE TABLE IF NOT EXISTS casfri_validation.complex_attributes_empty_test (inventory TEXT, "table" TEXT, attribute_name TEXT, function_call TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	"table" TEXT,
	cas_id TEXT,
	crown_closure_upper TEXT,
	-- lyr null validations
	water TEXT,
	land TEXT,
	tree TEXT,
	per TEXT,
	poly_area TEXT,
	-- species validations
	species_1 TEXT,
	species_per_1 TEXT,
	species_2 TEXT,
	species_per_2 TEXT,
	species_3 TEXT,
	species_per_3 TEXT,
	species_4 TEXT,
	species_per_4 TEXT,
	species_5 TEXT,
	species_per_5 TEXT,
	species_6 TEXT,
	species_per_6 TEXT,
	species_7 TEXT,
	species_per_7 TEXT,
	species_8 TEXT,
	species_per_8 TEXT,
	species_9 TEXT,
	species_per_9 TEXT,
	species_10 TEXT,
	species_per_10 TEXT,
	-- disturbance valiadtions
	dist_type_1 TEXT,
	dist_year_1 TEXT,
	dist_ext_upper_1 TEXT,
	dist_ext_lower_1 TEXT,
	dist_type_2 TEXT,
	dist_year_2 TEXT,
	dist_ext_upper_2 TEXT,
	dist_ext_lower_2 TEXT,
	dist_type_3 TEXT,
	dist_year_3 TEXT,
	dist_ext_upper_3 TEXT,
	dist_ext_lower_3 TEXT,
	-- other table validations
	stand_structure TEXT,
	wetland_type TEXT,
	nfl_soil_moist_reg TEXT,
	-- nfl valdiations
	nat_non_veg TEXT,
	non_for_anth TEXT,
	non_for_veg TEXT
);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
-- lyr1: create raw joined view
('BC80', '1', '''LYR''', 'inventory_id, src_filename, map_id, feature_id, ogc_fid', 'l1_crown_closure',
-- lyr1: lyr null validations
'lake',  'island, mainland', '', '100', 'wkb_geometry',
-- lyr1: species validations
'l1_species_cd_1', 'l1_species_pct_1', 'l1_species_cd_2', 'l1_species_pct_1', 'l1_species_cd_3', 'l1_species_pct_3', 'l1_species_cd_4', 'l1_species_pct_4', 'l1_species_cd_5', 'l1_species_pct_5', 'l1_species_cd_6', 'l1_species_pct_6', '', '', '', '', '', '', '', '',
-- lyr1: dist validations
'd1', 'dy1', 'deu1', 'del1', 'd2', 'dy2', 'deu2', 'del2', '', '', '', '',
'std_struc', 'wetland', '', '', '', ''),
-- lyr2: create raw joined view
('BC80', '2', '''LYR''', 'inventory_id, src_filename, map_id, feature_id, ogc_fid', 'l2_crown_closure',
-- lyr2: lyr null validations
'lake2', 'island, mainland', '', '50',  'wkb_geometry',
-- lyr2: species validations
'l2_species_cd_1', 'l2_species_pct_1', 'l2_species_cd_2', 'l2_species_pct_2', 'l2_species_cd_3', 'l2_species_pct_3', 'l2_species_cd_4', 'l2_species_pct_4', 'l2_species_cd_5', 'l2_species_pct_5', 'l2_species_cd_6', 'l2_species_pct_6', '', '', '', '', '', '', '', '',
-- lyr2: dist valdiations (no mappings for second layer)
'', '', '', '','', '', '', '','', '', '', '',
'', '', '', '', '', ''),
('BC80', '4', '''NFL''', '', '', 
'', '', '', '',  '',
'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
'', '', '', '','', '', '', '','', '', '', '',
'', '', '', 'nat_non', 'non_anth', 'non_veg');


CREATE TABLE IF NOT EXISTS casfri_validation.bc80 (
	inventory_id TEXT,
	src_filename TEXT,
	map_id TEXT,
	feature_id TEXT,
	ogc_fid TEXT,
	l1_for_cover_rank_cd INTEGER,
	l2_for_cover_rank_cd INTEGER,
	l1_proj_height_1 INTEGER,
	l1_proj_height_2 INTEGER,
	l2_proj_height_1 INTEGER,
	l2_proj_height_2 INTEGER,
	l1_crown_closure INTEGER,
	l2_crown_closure INTEGER,
	-- lyr null validations
	lake TEXT,
	lake2 TEXT,
	island TEXT,
	mainland TEXT,
	wkb_geometry TEXT,
	-- species validations
	l1_species_cd_1 TEXT,
	l1_species_pct_1 TEXT,
	l1_species_cd_2 TEXT,
	l1_species_pct_2 TEXT,
	l1_species_cd_3 TEXT,
	l1_species_pct_3 TEXT,
	l1_species_cd_4 TEXT,
	l1_species_pct_4 TEXT,
	l1_species_cd_5 TEXT,
	l1_species_pct_5 TEXT,
	l1_species_cd_6 TEXT,
	l1_species_pct_6 TEXT,
	l2_species_cd_1 TEXT,
	l2_species_pct_1 TEXT,
	l2_species_cd_2 TEXT,
	l2_species_pct_2 TEXT,
	l2_species_cd_3 TEXT,
	l2_species_pct_3 TEXT,
	l2_species_cd_4 TEXT,
	l2_species_pct_4 TEXT,
	l2_species_cd_5 TEXT,
	l2_species_pct_5 TEXT,
	l2_species_cd_6 TEXT,
	l2_species_pct_6 TEXT,
	-- disturbance validations
	d1 TEXT,
	dy1 TEXT,
	deu1 TEXT,
	del1 TEXT,
	d2 TEXT,
	dy2 TEXT,
	deu2 TEXT,
	del2 TEXT,
	-- other table validations
	std_struc TEXT,
	wetland TEXT,
	-- nfl valdiations
	inventory_standard_cd TEXT, 
	land_cover_class_cd_1 TEXT, 
	bclcs_level_4 TEXT, 
	non_productive_descriptor_cd TEXT, 
	non_veg_cover_type_1 TEXT
	
);
INSERT INTO casfri_validation.bc80 VALUES 
('BC80', 'FILENAME', '1234', '345',  '1', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
'wet_lake', NULL,       'la', 'nd', NULL,
'PM', '50', 'AB', '50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
NULL, '100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
'B', '2000', '50', '25', 'C', '1990', '25', '12',
'SL', 'WET', 'V', 'LA', NULL, NULL, 'LA'),
('BC80', 'FILENAME2', '1234', '345', '2', 1, NULL,   NULL, NULL, NULL, NULL, NULL, NULL,
'dry_lake', 'wet_lake', 'la', 'nd', NULL,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
'PM', '50', 'AB', '50', 'I', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
'B', '2010', '75', '50', '', '1980', '30', '10',
'ML', '', 'V', 'SL', 'SL', NULL, 'BR');


CREATE TABLE IF NOT EXISTS casfri_validation.lyr_all (
	cas_id TEXT,
	layer INTEGER,
	layer_rank INTEGER,
	soil_moist_reg TEXT,
	height_upper INTEGER,
	crown_closure_upper INTEGER,
	water TEXT,
	land TEXT,
	tree TEXT,
	per INTEGER,
	poly_area FLOAT,
	species_1 TEXT,
	species_per_1 TEXT,
	species_2 TEXT,
	species_per_2 TEXT,
	species_3 TEXT,
	species_per_3 TEXT,
	species_4 TEXT,
	species_per_4 TEXT,
	species_5 TEXT,
	species_per_5 TEXT,
	species_6 TEXT,
	species_per_6 TEXT,
	species_7 TEXT,
	species_per_7 TEXT,
	species_8 TEXT,
	species_per_8 TEXT,
	species_9 TEXT,
	species_per_9 TEXT,
	species_10 TEXT,
	species_per_10 TEXT
);
INSERT INTO casfri_validation.lyr_all VALUES
('BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1', 1, 1,     'UNKNOWN', 13, NULL, 'wet',               'land', 'SPRUCE_TREE',    100, 100.2, 
'PICE_MAR_####', '50',    'ABIE_BAL_####', '50',    'NULL_VALUE',    '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE',    '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888'),
('BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1', 2, -8888, 'UNKNOWN', 13, NULL, 'watery',            'land', 'NOT_APPLICABLE', 50, 100.2,  
'NULL_VALUE',  '100',   'NULL_VALUE',    '-8888', 'NULL_VALUE',    '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE',    '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888'),
('BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2', 1, 1,     'WET',     12, NULL, 'wet',               'land', 'NULL_VALUE',    80,   400,   
'ABIE_BAL_###',    '-8888', 'NULL_VALUE',    '-8888', 'NULL_VALUE',    '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'PICE_MAR_###', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888'),
('BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2', 2, -8888, 'WET',     12, NULL, 'TRANSLATION_ERROR', 'land', 'NULL_VALUE',    100,  -8888, 
'PICE_MAR_####', '-8888', 'NULL_VALUE',    '50',    'INVALID_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE',    '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888', 'NULL_VALUE', '-8888');


CREATE TABLE IF NOT EXISTS casfri_validation.dst_all (
	cas_id TEXT,
	layer INTEGER,
	dist_type_1 TEXT,
	dist_year_1 TEXT,
	dist_ext_upper_1 TEXT,
	dist_ext_lower_1 TEXT,
	dist_type_2 TEXT,
	dist_year_2 TEXT,
	dist_ext_upper_2 TEXT,
	dist_ext_lower_2 TEXT,
	dist_type_3 TEXT,
	dist_year_3 TEXT,
	dist_ext_upper_3 TEXT,
	dist_ext_lower_3 TEXT
);

INSERT INTO casfri_validation.dst_all VALUES 
('BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1', -8886, 'BURN', '2000', '50', '25', 'CUT', '1990', '25', '12', 'NULL_VALUE', '100', '-8888', '-8888'),
('BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2', -8886, 'EMPTY_STRING', '2010', '75', '50', 'INSECT', '1980', '30', '10', 'NULL_VALUE', '-8888', '-8888', '-8888');


CREATE TABLE IF NOT EXISTS casfri_validation.cas_all (
	cas_id TEXT,
	stand_structure TEXT,
	num_of_layers INT
);

INSERT INTO casfri_validation.cas_all VALUES 
('BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1', 'NULL_VALUE', 3),
('BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2', 'SINGLE_LAYERED', 3);

CREATE TABLE IF NOT EXISTS casfri_validation.eco_all (
	cas_id TEXT,
	layer INTEGER,
	wetland_type TEXT
);

INSERT INTO casfri_validation.eco_all VALUES 
('BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1', -8886, 'WET'),
('BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2', -8886, 'DRY');

CREATE TABLE IF NOT EXISTS casfri_validation.nfl_all (
	cas_id TEXT,
	layer INTEGER,
	layer_rank INTEGER,
	nat_non_veg TEXT,
	non_for_anth TEXT,
	non_for_veg TEXT,
	nfl_soil_moist_reg TEXT
);

INSERT INTO casfri_validation.nfl_all VALUES 
('BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1', -8886, -8886, 'VEG', 'OCEAN', 'NOT_APPLICABLE', 'LAKE'),
('BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2', -8886, -8886, 'VEG', 'INVALID_VALUE', 'NOT_APPLICABLE', 'LOW_SHRUB');


CREATE TABLE IF NOT EXISTS casfri_validation.excluded_columns_test (column_name TEXT);
INSERT INTO casfri_validation.excluded_columns_test VALUES
('cas_id'),
('layer'),
('num_of_layers'),
('layer_rank'),
('soil_moist_reg'),
('height_upper'),
('crown_closure_upper'),
('nat_non_veg'),
('non_for_anth'),
('non_for_veg'),
('species_1'),
('species_per_1'),
('species_2'),
('species_per_2'),
('species_3'),
('species_per_3'),
('species_4'),
('species_per_4'),
('species_5'),
('species_per_5'),
('species_6'),
('species_per_6'),
('species_7'),
('species_per_7'),
('species_8'),
('species_per_8'),
('species_9'),
('species_per_9'),
('species_10'),
('species_per_10');


SELECT * FROM createRawValidationTableRunner('validation_error_rows_test', 'casfri_validation', 
'casfri_validation', '(VALUES (''BC80''))  AS foo(inventory)', 'casfri_validation',
'casfri_validation.attribute_dependencies_test', 'casfri_validation.excluded_columns_test', 'casfri_validation.complex_attributes_empty_test');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
	ORDER BY cas_id
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1' AND table_name = 'dst_all' AND error_type = 'DISTURBANCE_ERROR' AND error_desc = 'A disturbance was mapped when it should have been non-applicable (3-3)' AND source_layer = '1')
		OR (cas_id = 'BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1' AND table_name = 'lyr_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'tree is not an error code but attribute does not exist in raw inventory' AND source_layer = '1')
		OR (cas_id = 'BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1' AND table_name = 'cas_all' AND error_type = 'NULL_VALUE' AND error_desc = 'stand_structure is empty but raw value exists' AND source_layer = '1')
		OR (cas_id = 'BC80-xxxxxxxFILENAME-xxxxxx1234-xxxxxxx345-xxxxxx1' AND table_name = 'lyr_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'water is not an error code but raw value is empty' AND source_layer = '2')
		OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'dst_all' AND error_type = 'DISTURBANCE_ERROR' AND error_desc = 'dist_type_1 was null when it could have been mapped from raw' AND source_layer = '1')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'dst_all' AND error_type = 'DISTURBANCE_ERROR' AND error_desc = 'dist_type_2 was mapped but a raw value doesnt exist' AND source_layer = '1')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'per does not match its numeric mapping' AND source_layer = '1')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'eco_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'wetland_type is not an error code but raw value is empty' AND source_layer = '1')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'species_1 was mapped but a raw value doesnt exist' AND source_layer = '1')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'A species was mapped when it should have been non-applicable (7-10)' AND source_layer = '1')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'lyr_all' AND error_type = 'NULL_VALUE' AND error_desc = 'poly_area is empty but raw value exists' AND source_layer = '2')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'per does not match its numeric mapping' AND source_layer = '2')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'species_per_1 was null when it could have been mapped from raw' AND source_layer = '2')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'lyr_all' AND error_type = 'SPECIES_ERROR' AND error_desc = 'species_2 was null when it could have been mapped from raw' AND source_layer = '2')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'lyr_all' AND error_type = 'TRANSLATION_ERROR' AND error_desc = 'water has a translation error' AND source_layer = '2')
        OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'lyr_all' AND error_type = 'TRANSLATION_ERROR' AND error_desc = 'species_3 has a translation error' AND source_layer = '2')
		OR (cas_id = 'BC80-xxxxxxFILENAME2-xxxxxx1234-xxxxxxx345-xxxxxx2' AND table_name = 'nfl_all' AND error_type = 'INVALID_VALUE' AND error_desc = 'non_for_anth has a translation error' AND source_layer = '4')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'createRawValidationTableRunner' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are 2 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 2
	-- Check that there are 5 distinct table_names
	AND (SELECT count(DISTINCT table_name) FROM test_result) = 5
	-- Check that there are 8 distinct error_types
	AND (SELECT count(DISTINCT error_type) FROM test_result) = 7
	-- Check that there are 16 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 16
	-- Check that there are 3 distinct source layers
	AND (SELECT count(DISTINCT source_layer) FROM test_result) = 3
	-- Check that there are exactly 17 rows in the result
	AND (SELECT count(*) FROM test_result) = 17
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_dst_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_cas_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_eco_all_joined;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_nfl_all_joined;
DROP TABLE IF EXISTS casfri_validation.lyr_all;
DROP TABLE IF EXISTS casfri_validation.dst_all;
DROP TABLE IF EXISTS casfri_validation.cas_all;
DROP TABLE IF EXISTS casfri_validation.eco_all;
DROP TABLE IF EXISTS casfri_validation.nfl_all;
DROP TABLE IF EXISTS casfri_validation.bc80;
DROP TABLE IF EXISTS casfri_validation.excluded_columns_test;
DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.complex_attributes_empty_test;
----------------------------------------------------------------------------------


SELECT * FROM casfri_validation.validations_test_results;
DROP TABLE IF EXISTS casfri_validation.validations_test_results;

