SET search_path = casfri_validation, casfri_metrics, casfri50, casfri_web, casfri_post_processing, rawfri, "translation", public;

-- Tests all functions in nfl-validation_functions.sql to ensure that the correct result is returned.
-- Run as script to see results of each test (you can also comment out the drop table at the end of the file to persist results)
-- Ensure that your search path is set with casfri_validation first. Most table creations should default to casfri_validation, but things may break if a different schema is used for this test

DROP TABLE IF EXISTS casfri_validation.validations_test_results;
CREATE TABLE IF NOT EXISTS casfri_validation.validations_test_results (test TEXT, result TEXT);
----------------------------------------------------------------------------------

-- Test for getNFLMatchList(inventory, nfl_col, source_layer)
WITH row_validity AS (
	SELECT getNFLMatchList('AB29', 'nat_non_veg', '1') = '{''NMB'',''NWF'',''NWL'',''NWI'',''NWR'',''NMC'',''NMR'',''NMS'',''MB'',''MC'',''MS'',''WF'',''WL'',''WR'',''NMG'',''NMM''}' AS value
	UNION ALL
	SELECT getNFLMatchList('MB02', 'non_for_veg', '1') = '{''SO'',''SO1'',''SO2'',''SO3'',''SO4'',''SO5'',''SO6'',''SO7'',''SO8'',''SO9'',''AL'',''SC'',''SC1'',''SC2'',''SC3'',''SC4'',''SC5'',''SC6'',''SC7'',''SC8'',''SC9'',''CC'',''HG'',''CS'',''HF'',''AS'',''HU'',''VI'',''BR'',''RA'',''CL'',''DL'',''AU''}' AS value
	UNION ALL
	SELECT getNFLMatchList('MB03', 'non_for_anth', '1') = '{810,811,812,813,815,816,841,842,843,844,845,846,847,849,851}' AS value
	UNION ALL
	SELECT getNFLMatchList('NT03', 'non_for_anth', '1') = '{''AP'',''BP'',''EL'',''GP'',''TS'',''RD'',''SH'',''SU'',''PM''}' AS value
	UNION ALL
	SELECT getNFLMatchList('QC03', 'non_for_anth', '1') = '{''A'',''AEP'',''AER'',''AF'',''ANT'',''BAS'',''CFO'',''CU'',''DEF'',''DEP'',''GR'',''HAB'',''LTE'',''MI'',''NF'',''RO'',''US'',''VIL'',''BHE'',''BLE'',''CAM'',''CAR'',''CEX'',''CHE'',''CIM'',''CNE'',''CS'',''CV'',''DEM'',''GOL'',''PIC'',''PPN'',''QUA'',''SC'',''TOE'',''VRG'',''OBS'',''PAI''}' AS value
	UNION ALL
	SELECT getNFLMatchList('NS04', 'not_a_type', '1') IS NULL AS value
	UNION ALL
	SELECT getNFLMatchList('TE01', 'nat_non_veg', '1') IS NULL AS value
)
INSERT INTO validations_test_results SELECT 'getNFLMatchList' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

----------------------------------------------------------------------------------

-- Test for runNFLValidationOnInventory(dest_table_name, inventory, creation_schema, attribute_dependencies_location)
-- NOTE: This is NOT a comprehensive test since there are too many inventories to distinctly test for, this just goes through one possible case
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	"table" TEXT,
	nat_non_veg TEXT,
	non_for_anth TEXT,
	non_for_veg TEXT
);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
('NT80', '1', '''LYR''', '', '', ''),
('NT80', '2', '''LYR''', 'l1_nat_veg', 'l1_non_anth', 'l1_non_veg'),
('NT80', '3', '''NFL''', 'l1_nat_veg', 'l1_non_anth', 'l1_non_veg'),
('NT80', '4', '''NFL''', 'l2_nat_veg', 'l2_non_anth', 'l2_non_veg'),
('NT80', '5', '''NFL''', 'NULL', 'NULL', 'NULL');

CREATE TABLE IF NOT EXISTS casfri_validation.nt80_raw_nfl_all_joined (
	cas_id TEXT,
	source_layer TEXT,
	nat_non_veg TEXT,
	non_for_anth TEXT,
	non_for_veg TEXT,
	raw_l1_nat_veg TEXT,
	raw_l1_non_anth TEXT,
	raw_l1_non_veg TEXT,
	raw_l2_nat_veg TEXT,
	raw_l2_non_anth TEXT,
	raw_l2_non_veg TEXT
);

INSERT INTO casfri_validation.nt80_raw_nfl_all_joined VALUES 
('NT80-TEST-1', '3', 'INVALID_VALUE', 'ANTH_SITE', 'NULL_VALUE', 'BE', 'NA', 'BY', '', NULL, 'SL'),
('NT80-TEST-1', '4', 'NOT_APPLICABLE', 'AIRPORT', 'LAKE', 'BE', 'NA', 'BY', '', NULL, 'SL'),
('NT80-TEST-1', '5', 'NULL_VALUE', 'NULL_VALUE', 'BOG', 'BE', 'NA', 'BY', '', NULL, 'SL');


CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);

SELECT * FROM runNFLValidationOnInventory('validation_error_rows_test', 'NT80', 'casfri_validation', 'casfri_validation.attribute_dependencies_test');

SELECT * FROM validation_error_rows_test;

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'NT80-TEST-1' AND table_name = 'nfl_all' AND error_type = 'NFL_ERROR' AND error_desc = 'non_for_anth was mapped but a raw value doesnt exist' AND source_layer = '3')
		OR (cas_id = 'NT80-TEST-1' AND table_name = 'nfl_all' AND error_type = 'NFL_ERROR' AND error_desc = 'non_for_veg was null when it could have been mapped from raw' AND source_layer = '3')
		OR (cas_id = 'NT80-TEST-1' AND table_name = 'nfl_all' AND error_type = 'INVALID_VALUE' AND error_desc = 'nat_non_veg has a translation error' AND source_layer = '3')
		OR (cas_id = 'NT80-TEST-1' AND table_name = 'nfl_all' AND error_type = 'NFL_ERROR' AND error_desc = 'non_for_anth was mapped but a raw value doesnt exist' AND source_layer = '4')
		OR (cas_id = 'NT80-TEST-1' AND table_name = 'nfl_all' AND error_type = 'NFL_ERROR' AND error_desc = 'non_for_veg was mapped but a raw value doesnt exist' AND source_layer = '5')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runNFLValidationOnInventory' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are 4 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 4
	-- Check that there are 3 distinct source_layers
	AND (SELECT count(DISTINCT source_layer) FROM test_result) = 3
	-- Check that there are exactly 5 rows in the result
	AND (SELECT count(*) FROM test_result) = 5
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;


DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;
DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.nt80_raw_nfl_all_joined;

----------------------------------------------------------------------------------

-- Display results
SELECT * FROM casfri_validation.validations_test_results;
DROP TABLE IF EXISTS casfri_validation.validations_test_results;
