#!/bin/bash -x

# Batch file for loading validation tables into PostgreSQL

# Input tables are format csv

# User provides the path to a folder, all csv files in the folder are loaded 

# If overwrite=True any existing tables will be replaced
# If overwrite=False existing tables will not be replaced, loop will fail for any tables already loaded

#################################### Set variables ######################################

source common.sh

#####################################################################################################################################################################

# make schema if it doesn't exist
"$gdalFolder/ogrinfo" "$pg_connection_string" -sql "CREATE SCHEMA IF NOT EXISTS $targetValidationSchema";


for i in *.csv
do
  x=${i##*/} # gets file name with .csv
  tab_name=${x%%.csv} # removes .csv

  # load using ogr
  echo "loading..."$tab_name
  "$gdalFolder/ogr2ogr" \
  -f "PostgreSQL" "$pg_connection_string" $i \
  -nln $targetValidationSchema.$tab_name \
  $overwrite_tab
done
