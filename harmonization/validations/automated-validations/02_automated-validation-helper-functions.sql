-- This file contains helper functions to validate attributes in each inventory
-- This includes general validation functions, and special case function for complex mappings

-- isTranslationError(value TEXT)
-- Given a text value, returns true if the value is a translation error, and false otherwise
-- Input:
	-- value: text value that may be a casfri error code
-- Output: BOOLEAN
CREATE OR REPLACE FUNCTION isTranslationError(
value TEXT
)
RETURNS BOOLEAN AS
	$func$
	BEGIN
		IF value IS NULL 
		THEN 
			RETURN FALSE;
		END IF;
		RETURN value IN ('WRONG_TYPE', 'NOT_IN_SET', 'INVALID_GEOMETRY', 'NO_INTERSECT', 'TRANSLATION_ERROR', 'OUT_OF_RANGE', 'INVALID_VALUE', '-9999', '-9998', '-9997', '-9995', '-7779', '-7778', '-3333');
	END;
	$func$
LANGUAGE 'plpgsql';


-- updateValidationErrorCodes(dest_table_name TEXT, creation_schema TEXT)
-- Updates all error_types with numeric codes to their text counterparts
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined tables are found in
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION updateValidationErrorCodes(
dest_table_name TEXT,
creation_schema TEXT
)
RETURNS VOID AS
	$func$
	BEGIN
		EXECUTE format('UPDATE %s.%s 
						SET error_type = TRANSLATION.tt_maptext(error_type, ''{''''-8888'''', ''''-8887'''', ''''-8886'''', ''''-9999'''', ''''-9998'''', ''''-9997'''', ''''-9995'''', ''''-7779'''', ''''-7778'''', ''''-3333''''}'', 
						''{''''NULL_VALUE'''', ''''NOT_APPLICABLE'''', ''''UNKNOWN_VALUE'''', ''''OUT_OF_RANGE'''', ''''NOT_IN_SET'''', ''''INVALID_VALUE'''', ''''WRONG_TYPE'''', ''''INVALID_GEOMETRY'''', ''''NO_INTERSECT'''', ''''TRANSLATION_ERROR''''}'')
						WHERE error_type IN (''-8888'', ''-8887'', ''-8886'', ''-9999'', ''-9998'', ''-9997'', ''-9995'', ''-7779'', ''-7778'', ''-3333'');', creation_schema, dest_table_name);
	END;
	$func$
LANGUAGE 'plpgsql';


-- runBasicNullValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, dependency, source_layer)
-- Given a destination table to insert results into, runs generic null value validations to flag when a value was mapped when it shouldn't have, 
-- or when a value wasnt mapped when it should have
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- raw_merged_table: the raw merged table to validate
	-- column_name: the column to validate
	-- dependency: the raw column to compare to
	-- source_layer: the layer to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runBasicNullValidation(
dest_table_name TEXT,
creation_schema TEXT,
raw_merged_table TEXT,
column_name TEXT,
dependency TEXT,
source_layer TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		casfri_table_name TEXT;
	BEGIN
		casfri_table_name := LEFT(RIGHT(raw_merged_table, 14), 7);
		EXECUTE format('INSERT INTO %s.%s 
						(SELECT cas_id, ''%s'' AS table_name, %s::text AS error_type, ''%s is empty but raw value exists'' AS error_desc, source_layer
						FROM %s.%s
						WHERE %s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8887'', ''-8888'', ''-8886'') AND trim(coalesce(raw_%s::text, ''NULL'')) NOT IN ('''', ''NULL'') AND source_layer::text = ''%s'')
						UNION ALL 
						(SELECT cas_id, ''%s'' AS table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
						FROM %s.%s
						WHERE isError(%s::text) = FALSE AND trim(coalesce(raw_%s::text, ''NULL'')) IN ('''', ''NULL'') AND source_layer::text = ''%s'')', 
						creation_schema, dest_table_name, casfri_table_name, column_name, column_name, creation_schema, raw_merged_table, 
						column_name, dependency, source_layer,
						casfri_table_name, column_name, creation_schema, raw_merged_table, column_name, dependency, source_layer);	
		END;
	$func$
LANGUAGE 'plpgsql';


-- runNoMappingValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, source_layer)
-- Given a destination table to insert results into, runs null value validations to flag when a value was mapped when a raw mapping doesn't exist
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- raw_merged_table: the raw merged table to validate
	-- column_name: the column to validate
	-- source_layer: the layer to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runNoMappingValidation(
dest_table_name TEXT,
creation_schema TEXT,
raw_merged_table TEXT,
column_name TEXT,
source_layer TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		casfri_table_name TEXT;
	BEGIN
		casfri_table_name := LEFT(RIGHT(raw_merged_table, 14), 7);
		EXECUTE format('INSERT INTO %s.%s 
						(SELECT cas_id, ''%s'' AS table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but attribute does not exist in raw inventory'' AS error_desc, source_layer
						FROM %s.%s
						WHERE isError(%s::text) = FALSE AND source_layer::text = ''%s'')', 
						creation_schema, dest_table_name, casfri_table_name, column_name, creation_schema, raw_merged_table, column_name, source_layer);
					
		END;
	$func$
LANGUAGE 'plpgsql';


-- runGeoDependencyValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, source_layer)
-- Given a destination table to insert results into, runs null value validations to flag when a value wasn't mapped when the raw value is assumed to exist
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- raw_merged_table: the raw merged table to validate
	-- column_name: the column to validate
	-- source_layer: the layer to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runGeoDependencyValidation(
dest_table_name TEXT,
creation_schema TEXT,
raw_merged_table TEXT,
column_name TEXT,
source_layer TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		casfri_table_name TEXT;
	BEGIN
		casfri_table_name := LEFT(RIGHT(raw_merged_table, 14), 7);
		EXECUTE format('INSERT INTO %s.%s 
						(SELECT cas_id, ''%s'' AS table_name, %s::text AS error_type, ''%s is empty but raw value exists'' AS error_desc, source_layer
						FROM %s.%s
						WHERE %s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8887'', ''-8888'', ''-8886'') AND source_layer::text = ''%s'')', 
						creation_schema, dest_table_name, casfri_table_name, column_name, column_name, creation_schema, raw_merged_table, column_name, source_layer);
		END;
	$func$
LANGUAGE 'plpgsql';


-- runNumericDependencyValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, dependency, source_layer)
-- Given a destination table to insert results into, runs validations to check if the value matches its direct numeric mapping
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- raw_merged_table: the raw merged table to validate
	-- column_name: the column to validate
	-- dependency: the value to compare to
	-- source_layer: the layer to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runNumericDependencyValidation(
dest_table_name TEXT,
creation_schema TEXT,
raw_merged_table TEXT,
column_name TEXT,
dependency TEXT,
source_layer TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		casfri_table_name TEXT;
	BEGIN
		casfri_table_name := LEFT(RIGHT(raw_merged_table, 14), 7);
		EXECUTE format('INSERT INTO %s.%s 
						(SELECT cas_id, ''%s'' AS table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its numeric mapping'' AS error_desc, source_layer
						FROM %s.%s
						WHERE %s::text != ''%s'' AND source_layer::text = ''%s'' AND isTranslationError(%s::TEXT) = FALSE)', 
						creation_schema, dest_table_name, casfri_table_name, column_name, creation_schema, raw_merged_table, column_name, dependency, source_layer, column_name);	
		END;
	$func$
LANGUAGE 'plpgsql';


-- runGenericTranslationErrorValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, source_layer)
-- Given a destination table to insert results into, collects all rows of the specific column with a translation error code
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- raw_merged_table: the raw merged table to validate
	-- column_name: the column to validate
	-- source_layer: the layer to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runGenericTranslationErrorValidation(
dest_table_name TEXT,
creation_schema TEXT,
raw_merged_table TEXT,
column_name TEXT,
source_layer TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		casfri_table_name TEXT;
	BEGIN
		casfri_table_name := LEFT(RIGHT(raw_merged_table, 14), 7);
		EXECUTE format('INSERT INTO %s.%s 
						(SELECT cas_id, ''%s'' AS table_name, %s::text AS error_type, ''%s has a translation error'' AS error_desc, source_layer
						FROM %s.%s
						WHERE isTranslationError(%s::text) = TRUE 
						AND source_layer::text = ''%s'')', 
						creation_schema, dest_table_name, casfri_table_name, column_name, column_name, creation_schema, raw_merged_table, column_name, source_layer);		
		END;
	$func$
LANGUAGE 'plpgsql';


-- runNumOfLayersValidationOnInventory(inventory TEXT,dest_table_name TEXT,creation_schema TEXT,casfri_schema TEXT)
-- Given an inventory, runs num_of_layers miscount validations on it
-- Input:
	-- inventory: the inventory to validate
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined tables are found in
	-- casfri_schema: the schema the casfri tables are found in
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runNumOfLayersValidationOnInventory(
inventory TEXT,
dest_table_name TEXT,
creation_schema TEXT,
casfri_schema TEXT
)
RETURNS VOID AS
	$func$
	BEGIN
		-- get num_of_layers errors
		EXECUTE format('INSERT INTO %s.%s 
						WITH lyr_lyrs AS (
							SELECT cas_id, count(*)
							FROM %s.lyr_all
							WHERE left(cas_id,4) = ''%s''
							GROUP BY cas_id
						), nfl_lyrs AS (
							SELECT cas_id, count(*)
							FROM %s.nfl_all
							WHERE left(cas_id,4) = ''%s''
							GROUP BY cas_id
						), nums AS (
							SELECT c.cas_id, num_of_layers, 
							(CASE WHEN l.count IS NULL THEN 0 ELSE l.count END) AS lyr_count, 
							(CASE WHEN n.count IS NULL THEN 0 ELSE n.count END) AS nfl_count
							FROM %s.cas_all c
							FULL JOIN lyr_lyrs l ON l.cas_id = c.cas_id 
							FULL JOIN nfl_lyrs n ON n.cas_id = c.cas_id
							WHERE left(c.cas_id,4) = ''%s''
						)
						SELECT cas_id, ''cas_all'' AS table_name, ''ROW_TRANSLATION'' AS error_type, 
						''num_of_layers was '' || num_of_layers || '' but lyr_count is '' || lyr_count || '' and nfl_count is '' || nfl_count AS error_desc,
						''1'' AS source_layer
						FROM nums
						WHERE ((lyr_count + nfl_count != num_of_layers and iserror(num_of_layers) = FALSE) OR (lyr_count + nfl_count != 0 AND iserror(num_of_layers) = TRUE));',
						creation_schema, dest_table_name, casfri_schema, inventory, casfri_schema, inventory, casfri_schema, inventory);
	END;
	$func$
LANGUAGE 'plpgsql';


-- ab_avi01_stand_photo_year_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs stand_photo_year validations on the inventory for the avi01 standard
-- Just checks that AB30 stand_photo_years are 2022, and AB29 ones macth photo_yr
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION ab_avi01_stand_photo_year_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		dep TEXT;
	BEGIN
		IF inventory = 'AB30'
		THEN
			dep := '2022';
		ELSE
			dep := 'raw_photo_yr';
		END IF;
		EXECUTE format('INSERT INTO %s.%s
						(SELECT cas_id, ''cas_all'' AS table_name, ''INCORRECT_MAPPING'' AS error_type, ''stand_photo_year does not match its numeric mapping'' AS error_desc, source_layer
						FROM %s.%s_raw_cas_all_joined
						WHERE stand_photo_year != %s AND isTranslationError(stand_photo_year::text) = FALSE);', 
						creation_schema, dest_table_name, creation_schema, inventory, dep);
	END;
	$func$
LANGUAGE 'plpgsql';



-- nb_nbi01_stand_photo_year_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs stand_photo_year validations on the inventory for the nbi01 standard
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION nb_nbi01_stand_photo_year_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		dep TEXT;
	BEGIN
		IF inventory = 'NB02'
		THEN
			dep := 'raw_datayr';
		ELSE
			dep := 'raw_l1datayr';
		END IF;
		EXECUTE format('INSERT INTO %s.%s
						(WITH vals AS (
							SELECT cas_id, source_layer, stand_photo_year,
							"translation".tt_coalesceint(''{'' || (CASE WHEN %s IS NULL THEN 0 ELSE %s END) || 
							'', '' || (CASE WHEN raw_l1datayr IS NULL THEN 0 ELSE raw_l1datayr END) || 
							'', '' || (CASE WHEN raw_l2datayr IS NULL THEN 0 ELSE raw_l2datayr END) || '', '' || ''-8888'' || ''}'', ''TRUE'') AS validity
							FROM %s.%s_raw_cas_all_joined yrnaj
							WHERE isTranslationError(stand_photo_year::text) = FALSE
						)
						SELECT cas_id, ''cas_all'' as table_name, stand_photo_year::TEXT AS error_type, ''stand_photo_year is empty but a raw value exists'' AS error_desc, source_layer
						FROM vals
						WHERE isError(stand_photo_year::text) = TRUE AND isError(validity::TEXT) = FALSE
						UNION ALL
						SELECT cas_id, ''cas_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''stand_photo_year is not an error code but raw value is empty'' AS error_desc, source_layer
						FROM vals
						WHERE isError(stand_photo_year::text) = FALSE AND isError(validity::TEXT) = TRUE
						UNION ALL
						SELECT cas_id, ''cas_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''stand_photo_year does not match its numeric mapping'' AS error_desc, source_layer
						FROM vals
						WHERE isError(validity::TEXT) = FALSE AND isError(stand_photo_year::TEXT) = FALSE AND validity::TEXT != stand_photo_year::TEXT);', 
 						creation_schema, dest_table_name, dep, dep, creation_schema, inventory);
	END;
	$func$
LANGUAGE 'plpgsql';


-- yt_yvi03_nfl_soil_moist_reg_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs soil_moist_reg validations on the inventory for the yvi03 standard
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION yt_yvi03_nfl_soil_moist_reg_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT
)
RETURNS VOID AS
	$func$
	BEGIN
		EXECUTE format('INSERT INTO %s.%s
						(WITH vals AS (
							SELECT cas_id, source_layer, soil_moist_reg,
							("translation".tt_yvi03_nfl_soil_moisture_validation(raw_land_type, raw_landcov_cl, raw_cov_cl_mod, raw_landsc_pos, raw_cover_type)
							AND "translation".tt_mapText(raw_soil_moist,''{''''Aquatic'''',''''Dry'''',''''Mesic'''',''''Wet''''}'', ''{''''AQUATIC'''',''''DRY'''',''''MESIC'''',''''WET''''}'') IS NOT NULL) AS validity,
							"translation".tt_mapText(raw_soil_moist,''{''''Aquatic'''',''''Dry'''',''''Mesic'''',''''Wet''''}'', ''{''''AQUATIC'''',''''DRY'''',''''MESIC'''',''''WET''''}'') AS value
							FROM %s.%s_raw_nfl_all_joined yrnaj
							WHERE isTranslationError(soil_moist_reg::text) = FALSE
						)
						SELECT cas_id, ''nfl_all'' as table_name, soil_moist_reg::TEXT AS error_type, ''soil_moist_reg is empty but a raw value exists'' AS error_desc, source_layer
						FROM vals
						WHERE isError(soil_moist_reg::text) = TRUE AND validity = TRUE
						UNION ALL
						SELECT cas_id, ''nfl_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''soil_moist_reg is not an error code but raw value is empty'' AS error_desc, source_layer
						FROM vals
						WHERE isError(soil_moist_reg::text) = FALSE AND validity = FALSE
						UNION ALL
						SELECT cas_id, ''nfl_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''soil_moist_reg does not match its direct mapping'' AS error_desc, source_layer
						FROM vals
						WHERE validity = TRUE AND isError(soil_moist_reg::TEXT) = FALSE AND value::TEXT != soil_moist_reg::TEXT);', creation_schema, dest_table_name, creation_schema, inventory);
	END;
	$func$
LANGUAGE 'plpgsql';


-- bc_vri01_height_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs height validations on the inventory for the vri01 standard
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION bc_vri01_height_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT,
col TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		i INTEGER;
	BEGIN
		FOR i IN 1..2
		LOOP
			EXECUTE format('INSERT INTO %s.%s
							(WITH vals AS (
								SELECT cas_id, source_layer, %s, 
								COALESCE("translation".tt_bc_height(raw_l%s_proj_height_1::text, raw_l%s_proj_height_2::text, raw_l%s_species_pct_1::text, raw_l%s_species_pct_2::text)::TEXT, ''0'')  AS validity
								FROM %s_raw_lyr_all_joined brlaj
								WHERE source_layer = ''%s'' AND
								isTranslationError(%s::text) = FALSE
							)
							SELECT cas_id, ''lyr_all'' as table_name, %s::TEXT AS error_type, ''%s is empty but a raw value exists'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = TRUE AND validity != ''0''
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = FALSE AND validity = ''0''
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its numeric mapping'' AS error_desc, source_layer
							FROM vals
							WHERE validity != ''0'' AND isError(%s::TEXT) = FALSE AND validity::TEXT != %s::TEXT);', creation_schema, dest_table_name, col, i, i, i, i, inventory, i, col, col, col, col, col, col, col, col, col);
		END LOOP;
	END;
	$func$
LANGUAGE 'plpgsql';


-- lyr_mapping_validations(dest_table_name TEXT,creation_schema TEXT, inventory TEXT, col TEXT)
-- Given a destination table to insert results into, runs various similarly formatted validations on the specific column
-- Checks that the translation functions produce the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
	-- col: the attribute to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION lyr_mapping_validations(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT,
col TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		function_call TEXT;
	BEGIN
		IF left(inventory, 2) = 'ON' AND col = 'productivity'
		THEN 
			function_call := '"translation".tt_mapTextNotNullIndex(raw_formod, ''{''''PF'''', ''''RP'''', ''''MR''''}'', 
							''{''''PRODUCTIVE_FOREST'''', ''''PRODUCTIVE_FOREST'''', ''''PRODUCTIVE_FOREST''''}'',
							raw_polytype, ''{''''BSH'''',''''TMS''''}'', ''{''''NON_PRODUCTIVE_FOREST'''',''''NON_PRODUCTIVE_FOREST''''}'', ''1'')';
		ELSIF left(inventory, 2) = 'ON' AND col = 'productivity_type'
		THEN 
			function_call := '"translation".tt_mapTextNotNullIndex(raw_formod, ''{''''PF'''', ''''RP'''', ''''MR''''}'', 
							''{''''PROTECTION_FOREST'''', ''''HARVESTABLE'''', ''''PROTECTION_FOREST''''}'',
							raw_polytype, ''{''''BSH'''',''''TMS''''}'', ''{''''ALDER'''',''''TREED_MUSKEG''''}'', ''1'')';
		ELSIF left(inventory, 2) = 'NB' AND col = 'productivity'
		THEN
			function_call := '"translation".tt_mapTextNotNullIndex(raw_fst::text, ''{1,2,3,4,5,6,7}'', 
							''{''''PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST'''',''''NON_PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST'''',''''NON_PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST''''}'',
							raw_wc, ''{''''FW''''}'', ''{''''NON_PRODUCTIVE_FOREST''''}'', ''1'')';
		ELSIF left(inventory, 2) = 'NB' AND col = 'productivity_type'
		THEN
			function_call := '"translation".tt_mapTextNotNullIndex(raw_fst::text, ''{1,2,4,5,7}'', 
							''{''''HARVESTABLE'''',''''HARVESTABLE'''',''''HARVESTABLE'''',''''HARVESTABLE'''',''''HARVESTABLE''''}'',
							raw_wc, ''{''''FW''''}'', ''{''''TREED_MUSKEG''''}'', ''1'')';
		ELSIF inventory IN ('NS03', 'NS04') AND col = 'site_class'
		THEN 
			function_call := '"translation".tt_mapText(''{'' || raw_site_sw::TEXT || '', '' || raw_site_hw::TEXT || ''}'', 
							''{''''00'''', ''''01'''', ''''02'''', ''''03'''', ''''04'''', ''''05'''', ''''10'''', ''''20'''', ''''30'''', ''''40'''', ''''50'''', ''''60'''', ''''70'''', ''''80'''', ''''90'''', ''''100'''', ''''110'''', ''''120'''', ''''130''''}'', 
							''{''''POOR'''', ''''POOR'''', ''''MEDIUM'''', ''''MEDIUM'''', ''''GOOD'''', ''''GOOD'''', ''''POOR'''', ''''POOR'''', ''''POOR'''', ''''POOR'''', ''''MEDIUM'''', ''''MEDIUM'''', ''''MEDIUM'''', ''''MEDIUM'''', ''''MEDIUM'''', ''''GOOD'''', ''''GOOD'''', ''''GOOD'''', ''''GOOD''''}'')';
		ELSIF inventory = 'NT03' AND col = 'productivity_type'
		THEN
			function_call = '"translation".tt_mapText(raw_landuse::TEXT, ''{''''FO'''',''''FI'''',''''FE'''',''''FU'''',''''FN'''',''''FP'''',''''FR'''',''''FW''''}'', 
							''{''''HARVESTABLE'''',''''HARVESTABLE'''',''''HARVESTABLE'''',''''HARVESTABLE'''',''''HARVESTABLE'''',''''HARVESTABLE'''',''''HARVESTABLE'''',''''HARVESTABLE''''}'')';
		ELSIF inventory = 'NT03' AND col = 'productivity'
		THEN
			function_call = '"translation".tt_mapText(raw_landuse::TEXT, ''{''''FO'''',''''FI'''',''''FE'''',''''FU'''',''''FN'''',''''FP'''',''''FR'''',''''FW''''}'', 
							''{''''PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST'''',''''PRODUCTIVE_FOREST''''}'')';
		END IF;
		EXECUTE format('INSERT INTO %s.%s
						(WITH vals AS (
							SELECT cas_id, source_layer, %s, 
							%s AS validity
							FROM %s.%s_raw_lyr_all_joined brlaj
							WHERE isTranslationError(%s::text) = FALSE
						)
						SELECT cas_id, ''lyr_all'' as table_name, %s::TEXT AS error_type, ''%s is empty but a raw value exists'' AS error_desc, source_layer
						FROM vals
						WHERE isError(%s::text) = TRUE AND validity IS NOT NULL
						UNION ALL
						SELECT cas_id, ''lyr_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
						FROM vals
						WHERE isError(%s::text) = FALSE AND validity IS NULL
						UNION ALL
						SELECT cas_id, ''lyr_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its direct mapping'' AS error_desc, source_layer
						FROM vals
						WHERE validity IS NOT NULL AND isError(%s::TEXT) = FALSE AND validity::TEXT != %s::TEXT);', creation_schema, dest_table_name, col, function_call, creation_schema, inventory, col, col, col, col, col, col, col, col, col);
	END;
	$func$
LANGUAGE 'plpgsql';


-- bc_vri01_origin_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs origin validations on the inventory for the vri01 standard
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION bc_vri01_origin_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT,
col TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		i INTEGER;
	BEGIN
		FOR i IN 1..2
		LOOP
			EXECUTE format('INSERT INTO %s.%s
							(WITH vals AS (
								SELECT cas_id, source_layer, %s, 
								"translation".tt_vri01_origin_translation(raw_projected_date::text, raw_l%s_proj_age_1::text) AS validity
								FROM %s.%s_raw_lyr_all_joined brlaj
								WHERE source_layer = ''%s'' AND
								isTranslationError(%s::text) = FALSE
							)
							SELECT cas_id, ''lyr_all'' as table_name, %s::TEXT AS error_type, ''%s is empty but a raw value exists'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = TRUE AND validity IS NOT NULL
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = FALSE AND validity IS NULL
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its numeric mapping'' AS error_desc, source_layer
							FROM vals
							WHERE validity IS NOT NULL AND isError(%s::TEXT) = FALSE AND validity::TEXT != %s::TEXT);', creation_schema, dest_table_name, col, i, creation_schema, inventory, i, col, col, col, col, col, col, col, col, col);
		END LOOP;
	END;
	$func$
LANGUAGE 'plpgsql';


-- qc_origin_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs origin validations on the inventory for QC inventories
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION qc_origin_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT,
col TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		i INTEGER;
		cl_age TEXT;
		stand_photo_year TEXT;
		layer_origin TEXT;
	BEGIN
		FOR i IN 1..2
		LOOP
			IF inventory IN ('QC03', 'QC08')
			THEN
				cl_age := 'raw_cl_age';
				stand_photo_year := 'raw_an_saisie';
				layer_origin := format('l%s_age_origin', i);
			ELSIF inventory IN ('QC04', 'QC09')
			THEN
				cl_age := 'raw_cl_age';
				stand_photo_year := 'raw_an_pro_ori';
				layer_origin := format('l%s_age_origin', i);
			ELSIF inventory IN ('QC05', 'QC10') AND i = 1
			THEN 
				cl_age := 'raw_sup_cl_age_et';
				stand_photo_year := 'raw_an_pro_ori';
				layer_origin := 'l1_age_origin';
			ELSE
				-- Should cover 'QC05' and 'QC10') for i = 2
				cl_age := 'raw_inf_cl_age_et';
				stand_photo_year := 'raw_an_pro_ori';
				layer_origin := 'l1_age_origin';
			END IF;
			EXECUTE format('INSERT INTO %s.%s
							(WITH vals AS (
								SELECT cas_id, source_layer, %s, 
								"translation".tt_XMinusYInt(%s, "translation".tt_lookupText(%s, ''translation'', ''qc_standstructure_lookup'', ''source_val'', ''%s'', ''TRUE'')) AS validity
								FROM %s.%s_raw_lyr_all_joined brlaj
								WHERE source_layer = ''%s'' AND
								isTranslationError(%s::text) = FALSE
							)
							SELECT cas_id, ''lyr_all'' as table_name, %s::TEXT AS error_type, ''%s is empty but a raw value exists'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = TRUE AND validity IS NOT NULL
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = FALSE AND validity IS NULL
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its numeric mapping'' AS error_desc, source_layer
							FROM vals
							WHERE validity IS NOT NULL AND isError(%s::TEXT) = FALSE AND validity::TEXT != %s::TEXT);', creation_schema, dest_table_name, col, stand_photo_year, cl_age, layer_origin, creation_schema, inventory, i, col, col, col, col, col, col, col, col, col);
		END LOOP;
	END;
	$func$
LANGUAGE 'plpgsql';


-- yt_ns_origin_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs origin validations on the inventory for YT and NS inventories
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION yt_ns_origin_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT,
col TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		i INTEGER;
		_age TEXT;
		_year TEXT;
		cutoff TEXT;
		extra TEXT;
	BEGIN
		FOR i IN 1..2
		LOOP
			IF inventory = 'YT03'
			THEN 
				_age := format('raw_layer%s_age_yrs', i);
				_year := 'raw_inventory_year';
				cutoff := '2024';
				extra := '';
			ELSIF inventory = 'YT04'
			THEN 
				_age := 'raw_age';
				_year := 'raw_invent_yr';
				cutoff := '2020';
				extra := '';
			ELSIF inventory = 'NS03' AND i = 1
			THEN
				_age := 'raw_age';
				_year := 'raw_photoyr';
				cutoff := '2024';
				extra := 'AND "translation".tt_notMatchList(raw_age::text, 0::text) AND "translation".tt_notMatchList(raw_age::text, 999::text)';
			ELSIF inventory = 'NS04' AND i = 1
			THEN
				_age := '999';
				_year := 'raw_photoyr';
				cutoff := '2024';
				extra := 'AND "translation".tt_notMatchList(999::text, 0::text) AND "translation".tt_notMatchList(999::text, 999::text)';
			ELSE
				-- Should cover 'NS03' and 'NS04' for i = 2 
				_age := '999';
				_year := '999';
				cutoff := '2024';
				extra := 'AND "translation".tt_notMatchList(999::text, 0::text) AND "translation".tt_notMatchList(999::text, 999::text)';
			END IF;
			EXECUTE format('INSERT INTO %s.%s
							(WITH vals AS (
								SELECT cas_id, source_layer, %s, 
								("translation".tt_isInt(%s::TEXT) 
								AND "translation".tt_isInt(%s::TEXT) 
								AND "translation".tt_isGreaterThan(%s::TEXT, 1::TEXT) 
								AND "translation".tt_isXMinusYBetween(%s::TEXT, %s::TEXT, 1000::TEXT, %s::TEXT)%s) AS validity,
								"translation".tt_xMinusYInt(%s::TEXT, %s::TEXT) AS value
								FROM %s.%s_raw_lyr_all_joined brlaj
								WHERE source_layer = ''%s'' AND
								isTranslationError(%s::text) = FALSE
							)
							SELECT cas_id, ''lyr_all'' as table_name, %s::TEXT AS error_type, ''%s is empty but a raw value exists'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = TRUE AND validity = TRUE
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = FALSE AND validity = FALSE
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its numeric mapping'' AS error_desc, source_layer
							FROM vals
							WHERE validity = TRUE AND isError(%s::TEXT) = FALSE AND value::TEXT != %s::TEXT);', 
							creation_schema, dest_table_name, col, _age, _year, _year, _year, _age, cutoff, extra, _year, _age, creation_schema, inventory, i, col, col, col, col, col, col, col, col, col);
		END LOOP;
	END;
	$func$
LANGUAGE 'plpgsql';


-- bc_vri01_site_index_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs site_index validations on the inventory for the vri01 standard
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION bc_vri01_site_index_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		i INTEGER;
	BEGIN
		FOR i IN 1..2
		LOOP
			EXECUTE format('INSERT INTO %s.%s
							(WITH vals AS (
								SELECT cas_id, source_layer, site_index, 
								"translation".tt_vri01_site_index_translation(raw_l%s_site_index::text, raw_l%s_est_site_index::text) AS validity
								FROM %s.%s_raw_lyr_all_joined brlaj
								WHERE source_layer = ''%s'' AND
								isTranslationError(site_index::text) = FALSE
							)
							SELECT cas_id, ''lyr_all'' as table_name, site_index::TEXT AS error_type, ''site_index is empty but a raw value exists'' AS error_desc, source_layer
							FROM vals
							WHERE isError(site_index::text) = TRUE AND validity IS NOT NULL
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''site_index is not an error code but raw value is empty'' AS error_desc, source_layer
							FROM vals
							WHERE isError(site_index::text) = FALSE AND validity IS NULL
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''site_index does not match its numeric mapping'' AS error_desc, source_layer
							FROM vals
							WHERE validity IS NOT NULL AND isError(site_index::TEXT) = FALSE AND validity::TEXT != site_index::TEXT);', creation_schema, dest_table_name, i, i, creation_schema, inventory, i);
		END LOOP;
	END;
	$func$
LANGUAGE 'plpgsql';


-- ab_nfl_crown_closure_height_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT, col TEXT)
-- Given a destination table to insert results into, runs crown_closure/height validations on the inventory for AB inventories
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
	-- col: either height or crown_closure
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION ab_nfl_crown_closure_height_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT,
col TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		i INTEGER;
		_nfl TEXT;
		_nfl_per TEXT;
		cutoff TEXT;
		function_call TEXT;
		value_mapping TEXT;
	BEGIN
		IF col IN ('height_upper', 'height_lower')
		THEN
			cutoff := '100';
			function_call := 'isNumeric';
		ELSIF col IN ('crown_closure_upper', 'crown_closure_lower')
		THEN
			cutoff := '10';
			function_call := 'isInt';
		END IF;
		FOR i IN 3..4
		LOOP
			IF i = 3
			THEN 
				_nfl := 'raw_nfl';
				IF col IN ('height_upper', 'height_lower')
				THEN
					_nfl_per := 'raw_height';
					value_mapping := format('copyDouble(%s::TEXT)', _nfl_per);
				ELSE
					_nfl_per := 'raw_nfl_per';
					value_mapping := format('multiplyInt(%s::TEXT, 10::TEXT)', _nfl_per);
				END IF;
			ELSIF i = 4
			THEN 
				_nfl := 'raw_unfl';
				IF col IN ('height_upper', 'height_lower')
				THEN
					_nfl_per := 'raw_uheight';
					value_mapping := format('copyDouble(%s::TEXT)', _nfl_per);
				ELSE
					_nfl_per := 'raw_unfl_per';
					value_mapping := format('multiplyInt(%s::TEXT, 10::TEXT)', _nfl_per);
				END IF;
			END IF;
			EXECUTE format('INSERT INTO %s.%s
							(WITH vals AS (
								SELECT cas_id, source_layer, %s, 
								("translation".tt_%s(%s::TEXT) AND 
								"translation".tt_matchList(%s::text, ''{''''SO'''', ''''SC''''}'') AND 
								"translation".tt_isBetween(%s::text, 1::text,%s::text)) AS validity,
								"translation".tt_%s AS value
								FROM %s.%s_raw_nfl_all_joined brlaj
								WHERE isTranslationError(%s::text) = FALSE
								AND source_layer = %s::TEXT
							)
							SELECT cas_id, ''nfl_all'' as table_name, %s::TEXT AS error_type, ''%s is empty but a raw value exists'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = TRUE AND validity = TRUE
							UNION ALL
							SELECT cas_id, ''nfl_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
							FROM vals
							WHERE isError(%s::text) = FALSE AND validity = FALSE
							UNION ALL
							SELECT cas_id, ''nfl_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its numeric mapping'' AS error_desc, source_layer
							FROM vals
							WHERE validity = TRUE AND isError(%s::TEXT) = FALSE AND value::TEXT != %s::TEXT);', 
							creation_schema, dest_table_name, col, function_call, _nfl_per, _nfl, _nfl_per, cutoff, value_mapping, creation_schema, inventory, col, i, col, col, col, col, col, col, col, col);
		END LOOP;
	END;
	$func$
LANGUAGE 'plpgsql';


-- wetland_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT, fucntion_call TEXT)
-- Given a destination table to insert results into, runs wetland type validations on the inventory
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
	-- function_call: the wetland translation function used for that standard
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION wetland_validation(
dest_table_name TEXT,
creation_schema TEXT,
col TEXT,
inventory TEXT,
function_call TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		translation_text TEXT;
	BEGIN
		translation_text := replace(function_call, 'validation', 'translation');
		EXECUTE format('INSERT INTO %s.%s
						(WITH vals AS (
							SELECT cas_id, source_layer, %s, 
							"translation".tt_%s AS validity,
							"translation".tt_%s AS value
							FROM %s.%s_raw_eco_all_joined brlaj
							WHERE isTranslationError(%s::text) = FALSE
						)
						SELECT cas_id, ''eco_all'' as table_name, %s::TEXT AS error_type, ''%s is empty but a raw value exists'' AS error_desc, source_layer
						FROM vals
						WHERE isError(%s::text) = TRUE AND validity = TRUE
						UNION ALL
						SELECT cas_id, ''eco_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
						FROM vals
						WHERE isError(%s::text) = FALSE AND validity = FALSE
						UNION ALL
						SELECT cas_id, ''eco_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its direct mapping'' AS error_desc, source_layer
						FROM vals
						WHERE validity = TRUE AND isError(%s::TEXT) = FALSE AND value::TEXT != %s::TEXT);', creation_schema, dest_table_name, col, function_call, translation_text, creation_schema, inventory, col, col, col, col, col, col, col, col, col);
	END;
	$func$
LANGUAGE 'plpgsql';


-- bc_vri01_productivity_type_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs productivity_type validations on the inventory for the vri01 standard
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION bc_vri01_productivity_type_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT
)
RETURNS VOID AS
	$func$
	BEGIN
		EXECUTE format('INSERT INTO %s.%s
						(WITH vals AS (
							SELECT cas_id, source_layer, productivity_type, 
							"translation".tt_matchListTwice(raw_non_productive_descriptor_cd, raw_for_mgmt_land_base_ind, ''{''''AF''''}'',''{''''Y''''}'') AS validity,
							"translation".tt_mapTextNotNullIndex(raw_non_productive_descriptor_cd, ''{''''AF''''}'', ''{''''ALPINE_FOREST''''}'', raw_for_mgmt_land_base_ind, ''{''''Y''''}'', ''{''''HARVESTABLE''''}'', 1::TEXT) AS value
							FROM %s.%s_raw_lyr_all_joined brlaj
							WHERE isTranslationError(productivity_type::text) = FALSE
						)
						SELECT cas_id, ''lyr_all'' as table_name, productivity_type::TEXT AS error_type, ''productivity_type is empty but a raw value exists'' AS error_desc, source_layer
						FROM vals
						WHERE isError(productivity_type::text) = TRUE AND validity = TRUE
						UNION ALL
						SELECT cas_id, ''lyr_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''productivity_type is not an error code but raw value is empty'' AS error_desc, source_layer
						FROM vals
						WHERE isError(productivity_type::text) = FALSE AND validity = FALSE
						UNION ALL
						SELECT cas_id, ''lyr_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''productivity_type does not match its direct mapping'' AS error_desc, source_layer
						FROM vals
						WHERE validity = TRUE AND isError(productivity_type::TEXT) = FALSE AND value::TEXT != productivity_type::TEXT);', creation_schema, dest_table_name, creation_schema, inventory);
	END;
	$func$
LANGUAGE 'plpgsql';


-- mb_fli01_nfl_crown_closure_height_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT, col TEXT)
-- Given a destination table to insert results into, runs crown_closure/height validations on the inventory for MB FLI01 inventories
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
	-- col: either height or crown_closure
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION mb_fli01_nfl_crown_closure_height_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT,
col TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		extra TEXT;
		value_mapping TEXT;
	BEGIN
		IF col IN ('height_upper', 'height_lower')
		THEN
			extra := format(' AND coalesce(raw_ht::text, ''NULL'') != %s::text ', col);
			value_mapping := 'raw_ht';
		ELSE
			extra := '';
			value_mapping := '"translation".tt_mapInt(raw_nnf_anth, ''{''''SO1'''',''''SO2'''',''''SO3'''',''''SO4'''',''''SO5'''',''''SO6'''',''''SO7'''',''''SO8'''',''''SO9'''',''''SC1'''',''''SC2'''',''''SC3'''',''''SC4'''',''''SC5'''',''''SC6'''',''''SC7'''',''''SC8'''',''''SC9''''}'', ''{20,30,40,50,60,70,80,90,100,20,30,40,50,60,70,80,90,100}'')';
		END IF;
		EXECUTE format('INSERT INTO %s.%s
						(WITH vals AS (
							SELECT cas_id, source_layer, %s,
							("translation".tt_matchList(raw_nnf_anth::TEXT,''{''''SO1'''',''''SO2'''',''''SO3'''',''''SO4'''',''''SO5'''',''''SO6'''',''''SO7'''',''''SO8'''',''''SO9'''',''''SC1'''',''''SC2'''',''''SC3'''',''''SC4'''',''''SC5'''',''''SC6'''',''''SC7'''',''''SC8'''',''''SC9''''}'') 
							%s) AS validity,
							%s::TEXT AS value
							FROM %s.%s_raw_nfl_all_joined brlaj
							WHERE isTranslationError(%s::text) = FALSE
						)
						SELECT cas_id, ''nfl_all'' as table_name, %s::TEXT AS error_type, ''%s is empty but a raw value exists'' AS error_desc, source_layer
						FROM vals
						WHERE isError(%s::text) = TRUE AND validity = TRUE
						UNION ALL
						SELECT cas_id, ''nfl_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
						FROM vals
						WHERE isError(%s::text) = FALSE AND validity = FALSE
						UNION ALL
						SELECT cas_id, ''nfl_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its numeric mapping'' AS error_desc, source_layer
						FROM vals
						WHERE validity = TRUE AND isError(%s::TEXT) = FALSE AND value::TEXT != %s::TEXT);', creation_schema, dest_table_name, col, extra, value_mapping, creation_schema, inventory, col, col, col, col, col, col, col, col, col);	
		
	END;
	$func$
LANGUAGE 'plpgsql';


-- mb_fli01_structure_range_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs structure range validations on the inventory for MB FLI01 inventories
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION mb_fli01_structure_range_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT
)
RETURNS VOID AS
	$func$
	BEGIN
		EXECUTE format('INSERT INTO %s.%s
						(WITH vals AS (
							SELECT cas_id, source_layer, structure_range,
							COALESCE(raw_canlay::text, ''NULL'') = ''C'' AND COALESCE(raw_comht::text, ''NULL'') ~ ''^[0-9]+$'' AS validity,
							(raw_comht::INT * 2)::TEXT AS value
							FROM %s.%s_raw_lyr_all_joined brlaj
							WHERE isTranslationError(structure_range::text) = FALSE
						)
						SELECT cas_id, ''lyr_all'' as table_name, structure_range::TEXT AS error_type, ''structure_range is empty but a raw value exists'' AS error_desc, source_layer
						FROM vals
						WHERE isError(structure_range::text) = TRUE AND validity = TRUE
						UNION ALL
						SELECT cas_id, ''lyr_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''structure_range is not an error code but raw value is empty'' AS error_desc, source_layer
						FROM vals
						WHERE isError(structure_range::text) = FALSE AND validity = FALSE
						UNION ALL
						SELECT cas_id, ''lyr_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''structure_range does not match its numeric mapping'' AS error_desc, source_layer
						FROM vals
						WHERE validity = TRUE AND isError(structure_range::TEXT) = FALSE AND value::TEXT != structure_range::TEXT);', creation_schema, dest_table_name, creation_schema, inventory);	
					END;
	$func$
LANGUAGE 'plpgsql';


-- ns_nfl_height_crown_closure_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT, col TEXT)
-- Given a destination table to insert results into, runs nfl height/crown_closure  validations on the inventory for NS inventories
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
	-- col: either height_upper/lower or crown_closure_upper/lower
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION ns_nfl_height_crown_closure_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT,
col TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		list TEXT;
		value_mapping TEXT;
	BEGIN
		IF col LIKE 'height%'
		THEN
			list := '''5''';
			value_mapping := 'mapDouble(raw_fornon::TEXT, ''{5}'', ''{0.99}'')';
		ELSIF col LIKE 'crown_closure%'
		THEN
			list := '''83'',''88'',''89'',''5''';
			value_mapping := 'mapInt(raw_fornon::TEXT, ''{83,88,89,5}'', ''{99,74,100,24}'')';
		END IF;
		EXECUTE format('INSERT INTO %s.%s
						(WITH vals AS (
							SELECT cas_id, source_layer, %s,
							COALESCE(raw_fornon::text, ''NULL'') IN (%s) AS validity,
							"translation".tt_%s AS value
							FROM %s.%s_raw_nfl_all_joined brlaj
							WHERE isTranslationError(%s::text) = FALSE
						)
						SELECT cas_id, ''nfl_all'' as table_name, %s::TEXT AS error_type, ''%s is empty but a raw value exists'' AS error_desc, source_layer
						FROM vals
						WHERE isError(%s::text) = TRUE AND validity = TRUE
						UNION ALL
						SELECT cas_id, ''nfl_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''%s is not an error code but raw value is empty'' AS error_desc, source_layer
						FROM vals
						WHERE isError(%s::text) = FALSE AND validity = FALSE
						UNION ALL
						SELECT cas_id, ''nfl_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''%s does not match its numeric mapping'' AS error_desc, source_layer
						FROM vals
						WHERE validity = TRUE AND isError(%s::TEXT) = FALSE AND value::TEXT != %s::TEXT);', 
						creation_schema, dest_table_name, col, list, value_mapping, creation_schema, inventory, col, col, col, col, col, col, col, col, col);	
		END;
	$func$
LANGUAGE 'plpgsql';


-- on_site_index_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
-- Given a destination table to insert results into, runs site_index validations on the inventory for ON inventories
-- Checks that the translation function produces the same result as found in casfri
-- Input:
	-- dest_table_name: the name of the table to store validation results in
	-- creation_schema: the schema the dest table and raw joined table sare found in
	-- inventory: the inventory to validate
-- Output: VOID
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION on_site_index_validation(
dest_table_name TEXT,
creation_schema TEXT,
inventory TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		i INTEGER;
		dep TEXT;
	BEGIN
		FOR i IN 1..2
		LOOP 
			IF i = 1
			THEN
				dep := 'raw_osi';
			ELSE
				dep := 'raw_usi';
			END IF;
			EXECUTE format('INSERT INTO %s.%s
							(WITH vals AS (
								SELECT cas_id, source_layer, site_index,
								%s IS NOT NULL AND %s::INT > 0 AND %s::INT < 40 AS validity,
								%s AS value
								FROM %s.%s_raw_lyr_all_joined brlaj
								WHERE isTranslationError(site_index::text) = FALSE
								AND source_layer = ''%s''
							)
							SELECT cas_id, ''lyr_all'' as table_name, site_index::TEXT AS error_type, ''site_index is empty but a raw value exists'' AS error_desc, source_layer
							FROM vals
							WHERE isError(site_index::text) = TRUE AND validity = TRUE
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''MISSING_ERROR'' AS error_type, ''site_index is not an error code but raw value is empty'' AS error_desc, source_layer
							FROM vals
							WHERE isError(site_index::text) = FALSE AND validity = FALSE
							UNION ALL
							SELECT cas_id, ''lyr_all'' as table_name, ''INCORRECT_MAPPING'' AS error_type, ''site_index does not match its numeric mapping'' AS error_desc, source_layer
							FROM vals
							WHERE validity = TRUE AND isError(site_index::TEXT) = FALSE AND value::TEXT != site_index::TEXT);', 
							creation_schema, dest_table_name, dep, dep, dep, dep, creation_schema, inventory, i);	
		END LOOP;
	END;
	$func$
LANGUAGE 'plpgsql';

