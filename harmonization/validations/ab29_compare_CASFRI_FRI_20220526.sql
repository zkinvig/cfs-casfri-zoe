/*
This is a working script to evaluate and validate CASFRI translation of various FRI. 
Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: May 31, 2022 by Kangakola
/* =============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate CASFRI translation of various FRI of Juridiction forest inventory datata. 
	*** This is proceed by comparing the attributes values of CASFRI translate data as per CASFRI SCHEMAS to the attributes of inventory sources data. 
	     To assess if the translation was accurate and the translation rule was followed and the resulted translated data are similar to the original data as per translation rules.
	*** Update: - Add code to process dst, eco, nfl tables	 
	*** Output: Multiples tables and views inside the casfri50_test schemas in the casfri traslation virtual machine.
	*** Software: PgAdmin 4.6
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
													   7. AB30 DATA PROCESSING  (ONLY DST DATA RELATED TO HARVESTING )
 ================================================================================================================================================================

*/


/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/

---------- Sub-setting layer 1 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 were used then they are common infromation to both layers 
-----------------------------------------------------------------

DROP TABLE  IF EXISTS casfri50_test.ab29_rawfri_cas_l1;
CREATE TABLE casfri50_test.ab29_rawfri_cas_l1 AS
SELECT
 c.cas_id,
 c.crown_closure_lower,
 c.crown_closure_upper,
 c.height_lower,
 c.height_upper,
 c.layer,
 c.layer_rank,
 c.origin_lower,
 c.origin_upper,
 c.productivity,
 c.productivity_type,
 c.site_class,
 c.site_index,
 c.soil_moist_reg,
 c.species_1,
 c.species_10,
 c.species_2,
 c.species_3,
 c.species_4,
 c.species_5,
 c.species_6,
 c.species_7,
 c.species_8,
 c.species_9,
 c.species_per_1,
 c.species_per_10,
 c.species_per_2,
 c.species_per_3,
 c.species_per_4,
 c.species_per_5,
 c.species_per_6,
 c.species_per_7,
 c.species_per_8,
 c.species_per_9,
 c.structure_per,
 c.structure_range,
 --c.poly_num,
 --c.ogc_fid,
 n.ogc_fid,
 n.poly_num,
 n.moist_reg,
 n.density,
 n.height,
 n.sp1,
 n.sp1_per,
 n.sp2,
 n.sp2_per,
 n.sp3,
 n.sp3_per,
 n.sp4,
 n.sp4_per,
 n.sp5,
 n.sp5_per,
 n.struc,
 n.struc_val,
 n.origin,
 n.tpr,
 n.initials,
 n.nfl,
 n.nfl_per,
 n.nat_non,
 n.anth_veg,
 n.anth_non,
 n.mod1,
 n.mod1_ext,
 n.mod1_yr,
 n.mod2,
 n.mod2_ext,
 n.mod2_yr,
 n.data,
 n.data_yr,
 n.umoist_reg,
 n.udensity,
 n.uheight,
 n.usp1,
 n.usp1_per,
 n.usp2,
 n.usp2_per,
 n.usp3,
 n.usp3_per,
 n.usp4,
 n.usp4_per,
 n.usp5,
 n.usp5_per,
 n.ustruc,
 n.ustruc_val,
 n.uorigin,
 n.utpr,
 n.uinitials,
 n.unfl,
 n.unfl_per,
 n.unat_non,
 n.uanth_veg,
 n.uanth_non,
 n.umod1,
 n.umod1_ext,
 n.umod1_yr,
 n.umod2,
 n.umod2_ext,
 n.umod2_yr,
 n.udata,
 n.udata_yr,
 n.photo_yr,
 n.aris,
 n.src_filename,
 n.inventory_id
FROM rawfri.ab29 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'ab29%' AND layer = 1
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.poly_num:: INTEGER = c.poly_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;


---------- Sub-setting layer 2 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 used in both layers 
-----------------------------------------------------------------

DROP TABLE  IF EXISTS casfri50_test.ab29_rawfri_cas_l2;
CREATE TABLE casfri50_test.ab29_rawfri_cas_l2 AS
SELECT
 c.cas_id,
 c.crown_closure_lower,
 c.crown_closure_upper,
 c.height_lower,
 c.height_upper,
 c.layer,
 c.layer_rank,
 c.origin_lower,
 c.origin_upper,
 c.productivity,
 c.productivity_type,
 c.site_class,
 c.site_index,
 c.soil_moist_reg,
 c.species_1,
 c.species_10,
 c.species_2,
 c.species_3,
 c.species_4,
 c.species_5,
 c.species_6,
 c.species_7,
 c.species_8,
 c.species_9,
 c.species_per_1,
 c.species_per_10,
 c.species_per_2,
 c.species_per_3,
 c.species_per_4,
 c.species_per_5,
 c.species_per_6,
 c.species_per_7,
 c.species_per_8,
 c.species_per_9,
 c.structure_per,
 c.structure_range,
 --c.poly_num,
 --c.ogc_fid,
 n.ogc_fid,
 n.poly_num,
 n.moist_reg,
 n.density,
 n.height,
 n.sp1,
 n.sp1_per,
 n.sp2,
 n.sp2_per,
 n.sp3,
 n.sp3_per,
 n.sp4,
 n.sp4_per,
 n.sp5,
 n.sp5_per,
 n.struc,
 n.struc_val,
 n.origin,
 n.tpr,
 n.initials,
 n.nfl,
 n.nfl_per,
 n.nat_non,
 n.anth_veg,
 n.anth_non,
 n.mod1,
 n.mod1_ext,
 n.mod1_yr,
 n.mod2,
 n.mod2_ext,
 n.mod2_yr,
 n.data,
 n.data_yr,
 n.umoist_reg,
 n.udensity,
 n.uheight,
 n.usp1,
 n.usp1_per,
 n.usp2,
 n.usp2_per,
 n.usp3,
 n.usp3_per,
 n.usp4,
 n.usp4_per,
 n.usp5,
 n.usp5_per,
 n.ustruc,
 n.ustruc_val,
 n.uorigin,
 n.utpr,
 n.uinitials,
 n.unfl,
 n.unfl_per,
 n.unat_non,
 n.uanth_veg,
 n.uanth_non,
 n.umod1,
 n.umod1_ext,
 n.umod1_yr,
 n.umod2,
 n.umod2_ext,
 n.umod2_yr,
 n.udata,
 n.udata_yr,
 n.photo_yr,
 n.aris,
 n.src_filename,
 n.inventory_id
FROM rawfri.ab29 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'ab29%' AND layer = 2
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.poly_num:: INTEGER = c.poly_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;



--==================================================================
----------------------- SPECIES VERIFICATION -----------------------------------------------------

-- Link CASFRI lyr tabe to FRI data to verify the casfri translation accuracy vs FRI raw data.
--  validation process: visually inspecte the first 100 and the last 100 rows of the combine data set for the key species variables.
--==================================================================

-- LAYER 1
DROP TABLE IF EXISTS casfri50_test.ab29_rawfri_cas_species_l1;
CREATE TABLE casfri50_test.ab29_rawfri_cas_species_l1 AS
SELECT
 cas_id,
 species_1,
 species_per_1,
 sp1,
 sp1_per,
 usp1,
 usp1_per,
 species_2,
 species_per_2,
 sp2,
 sp2_per,
 usp2,
 usp2_per,
 species_3,
 species_per_3,
 sp3,
 sp3_per,
 usp3,
 usp3_per,
 species_4,
 species_per_4,
 sp4,
 sp4_per,
 usp4,
 usp4_per,
 species_5,
 species_per_5
 sp5,
 sp5_per,
 usp5,
 usp5_per
FROM casfri50_test.ab29_rawfri_cas_l1;

-- LAYER 2
DROP TABLE IF EXISTS casfri50_test.ab29_rawfri_cas_species_l2;
CREATE TABLE casfri50_test.ab29_rawfri_cas_species_l2 AS
SELECT
 cas_id,
 species_1,
 species_per_1,
 sp1,
 sp1_per,
 usp1,
 usp1_per,
 species_2,
 species_per_2,
 sp2,
 sp2_per,
 usp2,
 usp2_per,
 species_3,
 species_per_3,
 sp3,
 sp3_per,
 usp3,
 usp3_per,
 species_4,
 species_per_4,
 sp4,
 sp4_per,
 usp4,
 usp4_per,
 species_5,
 species_per_5
 sp5,
 sp5_per,
 usp5,
 usp5_per
FROM casfri50_test.ab29_rawfri_cas_l2;


---         CHECK CROWN CLOSURE FOR BOTH L1 & L2

SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l1 WHERE  crown_closure_upper = crown_closure_lower; ----0
SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l1 WHERE  crown_closure_upper > crown_closure_lower; ----611775
SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l1 WHERE  crown_closure_upper < crown_closure_lower; ----0

SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l2 WHERE  crown_closure_upper > crown_closure_lower; ----328154
SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l2 WHERE  crown_closure_upper = crown_closure_lower; ----0
SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l2 WHERE  crown_closure_upper < crown_closure_lower; ----0

--         CHECK HEIGHT FOR BOTH L1 & L2

SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l1 WHERE  height_upper = height_lower; ----0
SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l1 WHERE  crown_closure_upper > crown_closure_lower; ----611775
SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l1 WHERE  crown_closure_upper < crown_closure_lower; ----0


--SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l2 WHERE  crown_closure_upper > crown_closure_lower; ----328154
--SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l2 WHERE  crown_closure_upper = crown_closure_lower; ----0
--SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l2 WHERE  crown_closure_upper < crown_closure_lower; ----0

--- CHECK FOR POSSIBLE HEIGHT PROBLEM
DROP TABLE IF EXISTS casfri50_test.ab29_possible_height_prob_L1;
CREATE TABLE casfri50_test.ab29_possible_height_prob_L1 AS
SELECT
  cas_id,
  height_upper,
  height_lower,
  height,
  uheight
FROM casfri50_test.ab29_rawfri_cas_l1;

DROP TABLE IF EXISTS casfri50_test.ab29_possible_height_prob_L2;
CREATE TABLE casfri50_test.ab29_possible_height_prob_L2 AS
SELECT
  cas_id,
  height_upper,
  height_lower,
  height,
  uheight
FROM casfri50_test.ab29_rawfri_cas_l2;

-- CHECK FOR DUPLICATED CAS_ID

SELECT cas_id, COUNT(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'AB29%'
GROUP BY cas_id
HAVING COUNT(*) > 1; ---- 0


SELECT cas_id, COUNT(*)
FROM casfri50.lyr_all
WHERE cas_id ILIKE 'AB29%'
GROUP BY cas_id
HAVING COUNT(*) > 1;   ---- 328154


SELECT cas_id, COUNT(*)
FROM casfri50.lyr_all
WHERE cas_id ILIKE 'AB29%'
GROUP BY cas_id
HAVING COUNT(*) > 2; ----0



---------------------------------------------------------------------------------------------------

-- INVESTIGATE L2 NOT IN L1
SELECT cas_id, COUNT(*)
FROM casfri50_test.ab29_rawfri_cas_l2
WHERE cas_id NOT IN casfri50_test.ab29_rawfri_cas_l1;

SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l1 WHERE cas_id NOT IN (SELECT cas_id FROM casfri50_test.ab29_rawfri_cas_l2); --- 283621

SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_l2 WHERE cas_id IN (SELECT cas_id FROM casfri50_test.ab29_rawfri_cas_l1); --- 328154

---------------------------------------------------------------------------------------------------
/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/
---- DISTURBANCE INVESTIGATION ------------
DROP TABLE  IF EXISTS casfri50_test.ab29_rawfri_cas_dist;
CREATE TABLE casfri50_test.ab29_rawfri_cas_dist AS
SELECT
 c.cas_id,
 c.dist_type_1,
 c.dist_year_1,
 c.dist_ext_upper_1,
 c.dist_ext_lower_1,
 c.dist_type_2,
 c.dist_year_2,
 c.dist_ext_upper_2,
 c.dist_ext_lower_2,
 --c.layer,
 --c.layer_rank,
 --c.structure_per,
 --c.soil_moist_reg,
 --c.nat_non_veg,
 --c.non_for_anth,
 --c.non_for_veg,
 --c.origin_lower,
 --c.origin_upper,
 --c.productivity,
 --c.productivity_type,
 --c.site_class,
 --c.site_index,
 --c.soil_moist_reg,
 --c.species_1,
 --c.species_10,
 --c.species_2,
 --c.species_3,
 --c.species_4,
 --c.species_5,
 --c.species_6,
 --c.species_7,
 --c.species_8,
 --c.species_9,
 --c.species_per_1,
 --c.species_per_10,
 --c.species_per_2,
 --c.species_per_3,
 --c.species_per_4,
 --c.species_per_5,
 --c.species_per_6,
 --c.species_per_7,
 --c.species_per_8,
 --c.species_per_9,
 --c.structure_per,
 --c.structure_range,
 --c.poly_num,
 --c.ogc_fid,
 n.ogc_fid,
 n.poly_num,
 --n.moist_reg,
 --n.density,
 --n.height,
 --n.sp1,
 --n.sp1_per,
 --n.sp2,
 --n.sp2_per,
 --n.sp3,
 --n.sp3_per,
 --n.sp4,
 --n.sp4_per,
 --n.sp5,
 --n.sp5_per,
 --n.struc,
 --n.struc_val,
 --n.origin,
 --n.tpr,
 --n.initials,
 --n.nfl,
 --n.nfl_per,
 --n.nat_non,
 --n.anth_veg,
 --n.anth_non,
 n.mod1,
 n.mod1_ext,
 n.mod1_yr,
 n.mod2,
 n.mod2_ext,
 n.mod2_yr,
 --n.data,
 --n.data_yr,
 --n.umoist_reg,
 --n.udensity,
 --n.uheight,
 --n.usp1,
 --n.usp1_per,
 --n.usp2,
 --n.usp2_per,
 --n.usp3,
 --n.usp3_per,
 --n.usp4,
 --n.usp4_per,
 --n.usp5,
-- n.usp5_per,
 --n.ustruc,
 --n.ustruc_val,
 --n.uorigin,
 --n.utpr,
 --n.uinitials,
 --n.unfl,
 --n.unfl_per,
 --n.unat_non,
 --n.uanth_veg,
 --n.uanth_non,
 n.umod1,
 n.umod1_ext,
 n.umod1_yr,
 n.umod2,
 n.umod2_ext,
 n.umod2_yr,
 --n.udata,
 --n.udata_yr,
 --n.photo_yr,
 --n.aris,
 n.src_filename,
 n.inventory_id
FROM rawfri.ab29 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.dst_all WHERE cas_id ILIKE 'ab29%' 
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.poly_num:: INTEGER = c.poly_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;

-- compare dist_type and mod1 umod1

DROP TABLE IF EXISTS casfri50_test.ab29_dist_mod1_dist_type1_2;
CREATE TABLE casfri50_test.ab29_dist_mod1_dist_type1_2 AS
SELECT mod1,dist_type_1, dist_year_1,mod2,dist_type_2, dist_year_2
FROM casfri50_test.ab29_rawfri_cas_dist
GROUP BY mod1, dist_type_1, dist_year_1, mod2, dist_type_2, dist_year_2;

DROP TABLE IF EXISTS casfri50_test.ab29_dist_umod1_dist_type1;
CREATE TABLE casfri50_test.ab29_dist_umod1_dist_type1 AS
SELECT umod1,dist_type_1
FROM casfri50_test.ab29_rawfri_cas_dist
GROUP BY umod1, dist_type_1;

DROP TABLE IF EXISTS casfri50_test.ab29_dist_mod2_dist_type2;
CREATE TABLE casfri50_test.ab29_dist_mod2_dist_type2 AS
SELECT mod2,dist_type_2
FROM casfri50_test.ab29_rawfri_cas_dist
GROUP BY mod2, dist_type_2;

DROP TABLE IF EXISTS casfri50_test.ab29_dist_umod2_dist_type2;
CREATE TABLE casfri50_test.ab29_dist_umod2_dist_type2 AS
SELECT umod2, dist_type_2
FROM casfri50_test.ab29_rawfri_cas_dist
GROUP BY umod2, dist_type_2;

-------------------------------------------------------------------------------------------------------------------

/*   ================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/

DROP TABLE  IF EXISTS casfri50_test.eco_ab29_rawfri_cas;
CREATE TABLE casfri50_test.eco_ab29_rawfri_cas AS
SELECT
 c.cas_id,
   c.wetland_type,
   c.wet_veg_cover,
   c.wet_landform_mod,
   c.wet_local_mod,
   c.eco_site,
 --c.crown_closure_lower,
 --c.crown_closure_upper,
 --c.height_lower,
 --c.height_upper,
   c.layer,
 --c.layer_rank,
 --c.structure_per,
 --c.soil_moist_reg,
 --c.nat_non_veg,
 --c.non_for_anth,
 --c.non_for_veg,
 --c.origin_lower,
 --c.origin_upper,
 --c.productivity,
 --c.productivity_type,
 --c.site_class,
 --c.site_index,
 --c.soil_moist_reg,
 --c.species_1,
 --c.species_10,
 --c.species_2,
 --c.species_3,
 --c.species_4,
 --c.species_5,
 --c.species_6,
 --c.species_7,
 --c.species_8,
 --c.species_9,
 --c.species_per_1,
 --c.species_per_10,
 --c.species_per_2,
 --c.species_per_3,
 --c.species_per_4,
 --c.species_per_5,
 --c.species_per_6,
 --c.species_per_7,
 --c.species_per_8,
 --c.species_per_9,
 --c.structure_per,
 --c.structure_range,
 --c.poly_num,
 --c.ogc_fid,
 n.ogc_fid,
 n.poly_num,
 n.moist_reg,
 --n.nonfor_veg,
 --n.nat_non,
 --n.nat_nonveg,
 n.tpr,
 --n.density,
 --n.height,
   n.sp1,
 --n.sp1_per,
   n.sp2,
 --n.sp2_per,
 --n.sp3,
 --n.sp3_per,
 --n.sp4,
 --n.sp4_per,
 --n.sp5,
 --n.sp5_per,
 --n.struc,
 --n.struc_val,
 --n.origin,
 --n.tpr,
 --n.initials,
 --n.nfl,
 --n.nfl_per,
   n.nat_non,
   n.anth_veg,
   n.anth_non,
 --n.mod1,
 --n.mod1_ext,
 --n.mod1_yr,
 --n.mod2,
 --n.mod2_ext,
 --n.mod2_yr,
 --n.data,
 --n.data_yr,
 n.umoist_reg,
 --n.udensity,
 --n.uheight,
 --n.usp1,
 --n.usp1_per,
 --n.usp2,
 --n.usp2_per,
 --n.usp3,
 --n.usp3_per,
 --n.usp4,
 --n.usp4_per,
 --n.usp5,
-- n.usp5_per,
 --n.ustruc,
 --n.ustruc_val,
 --n.uorigin,
 n.utpr,
 --n.uinitials,
 --n.unfl,
 --n.unfl_per,
 n.unat_non,
 --n.uanth_veg,
 --n.uanth_non,
 --n.umod1,
 --n.umod1_ext,
 --n.umod1_yr,
 --n.umod2,
 --n.umod2_ext,
 --n.umod2_yr,
 --n.udata,
 --n.udata_yr,
 --n.photo_yr,
 --n.aris,
 n.src_filename,
 n.inventory_id
FROM rawfri.ab29 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.eco_all WHERE cas_id ILIKE 'ab29%' 
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.poly_num:: INTEGER = c.poly_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;




/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/

DROP TABLE  IF EXISTS casfri50_test.ab29_rawfri_cas_nfl;
CREATE TABLE casfri50_test.ab29_rawfri_cas_nfl AS
SELECT
 c.cas_id,
 c.crown_closure_lower,
 c.crown_closure_upper,
 c.height_lower,
 c.height_upper,
 c.layer,
 c.layer_rank,
 c.structure_per,
 c.soil_moist_reg,
 c.nat_non_veg,
 c.non_for_anth,
 c.non_for_veg,
 --c.origin_lower,
 --c.origin_upper,
 --c.productivity,
 --c.productivity_type,
 --c.site_class,
 --c.site_index,
 --c.soil_moist_reg,
 --c.species_1,
 --c.species_10,
 --c.species_2,
 --c.species_3,
 --c.species_4,
 --c.species_5,
 --c.species_6,
 --c.species_7,
 --c.species_8,
 --c.species_9,
 --c.species_per_1,
 --c.species_per_10,
 --c.species_per_2,
 --c.species_per_3,
 --c.species_per_4,
 --c.species_per_5,
 --c.species_per_6,
 --c.species_per_7,
 --c.species_per_8,
 --c.species_per_9,
 --c.structure_per,
 --c.structure_range,
 --c.poly_num,
 --c.ogc_fid,
 n.ogc_fid,
 n.poly_num,
 n.moist_reg,
 --n.density,
 n.height,
 --n.sp1,
 --n.sp1_per,
 --n.sp2,
 --n.sp2_per,
 --n.sp3,
 --n.sp3_per,
 --n.sp4,
 --n.sp4_per,
 --n.sp5,
 --n.sp5_per,
 n.struc,
 n.struc_val,
 --n.origin,
 --n.tpr,
 --n.initials,
 n.nfl,
 n.nfl_per,
 n.nat_non,
 n.anth_veg,
 n.anth_non,
 --n.mod1,
 --n.mod1_ext,
 --n.mod1_yr,
 --n.mod2,
 --n.mod2_ext,
 --n.mod2_yr,
 --n.data,
 --n.data_yr,
 n.umoist_reg,
 --n.udensity,
 n.uheight,
 --n.usp1,
 --n.usp1_per,
 --n.usp2,
 --n.usp2_per,
 --n.usp3,
 --n.usp3_per,
 --n.usp4,
 --n.usp4_per,
 --n.usp5,
-- n.usp5_per,
 n.ustruc,
 n.ustruc_val,
 --n.uorigin,
 --n.utpr,
 --n.uinitials,
 --n.unfl,
 --n.unfl_per,
 n.unat_non,
 n.uanth_veg,
 n.uanth_non,
 --n.umod1,
 --n.umod1_ext,
 --n.umod1_yr,
 --n.umod2,
 --n.umod2_ext,
 --n.umod2_yr,
 --n.udata,
 --n.udata_yr,
 --n.photo_yr,
 --n.aris,
 n.src_filename,
 n.inventory_id
FROM rawfri.ab29 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.nfl_all WHERE cas_id ILIKE 'ab29%' 
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.poly_num:: INTEGER = c.poly_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;


/*   ==================================================================================

     5.    STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 

    ==================================================================================
*/



/* STAND STRUCTURE INVESTIGATION*/

DROP TABLE IF EXISTS casfri50_test.stand_structure_ab29;
CREATE TABLE casfri50_test.stand_structure_ab29 AS
SELECT stand_structure, count(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'AB29%' 
GROUP BY stand_structure;

/* LAYER COUNT INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.nfl_layer_count_ab29;
CREATE TABLE casfri50_test.nfl_layer_count_ab29 AS
SELECT layer, count(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'AB29%' 
GROUP BY layer;

----------------------------------------------------------------------------------------------------------------------------------------------------------

/*   ==================================================================================

     6.    MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE 

    ==================================================================================
*/



SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_nfl WHERE cas_id NOT IN (SELECT cas_id FROM casfri50_test.ab29_rawfri_cas_l1); --- 207600

SELECT COUNT(*) FROM casfri50_test.ab29_rawfri_cas_nfl WHERE cas_id IN (SELECT cas_id FROM casfri50_test.ab29_rawfri_cas_l2); --- 0

-- create table

DROP TABLE IF EXISTS casfri50_test.ab29_l1_data_in_NFL;
CREATE TABLE casfri50_test.ab29_l1_data_in_NFL AS
SELECT * FROM casfri50_test.ab29_rawfri_cas_l1 WHERE cas_id IN (SELECT cas_id FROM casfri50_test.ab29_rawfri_cas_nfl); --- 109384


DROP TABLE IF EXISTS casfri50_test.ab29_NFL_data_NOT_in_L1;
CREATE TABLE casfri50_test.ab29_NFL_data_NOT_in_L1 AS
SELECT * FROM casfri50_test.ab29_rawfri_cas_nfl WHERE cas_id NOT IN (SELECT cas_id FROM casfri50_test.ab29_rawfri_cas_l1); --- 207600


-- check for the intersection betwee lyr and nfl TABLE

SELECT COUNT(*) FROM casfri50.lyr_all WHERE cas_id ILIKE 'AB29%' AND cas_id IN (SELECT cas_id FROM casfri50.nfl_all WHERE cas_id ILIKE 'AB29%');--109384



/*  INVESTIGATION LYR AND NFL IN THE RAWFRI */

SELECT COUNT(*) FROM rawfri.ab29 where sp1 = '' AND nfl != ''; -- 101884
SELECT COUNT(*) FROM rawfri.ab29 where sp1 != '' AND nfl != ''; -- 0
SELECT COUNT(*) FROM rawfri.ab29 where sp1 != '' AND nfl = ''; -- 611695
SELECT COUNT(*) FROM ab29_l1_data_in_nfl where sp1 != '' AND unfl != '';
SELECT COUNT(*) FROM ab29_nfl_data_not_in_l1 WHERE uheight > 0;-- 893
SELECT COUNT(*) FROM ab29_nfl_data_not_in_l1 WHERE nfl != '';-- 130857
SELECT COUNT(*) FROM ab29_nfl_data_not_in_l1 WHERE nfl != '' AND layer = 1;-- 101810
SELECT COUNT(*) FROM ab29_l1_data_in_nfl WHERE nfl != '' AND layer = 1;-- 74
SELECT COUNT(*) FROM ab29_rawfri_cas_nfl WHERE nfl != '' AND layer = 1; -- 101810
SELECT COUNT(*) FROM ab29_rawfri_cas_nfl WHERE nfl != '' AND layer = 2; -- 29121
SELECT COUNT(*) FROM ab29_nfl_data_not_in_l1 WHERE nfl!= ''AND nfl_per = 0;--56350
SELECT COUNT(*) FROM ab29_nfl_data_not_in_l1 WHERE nfl!= ''AND struc = '';--72763

/* MULTIPLE INVESTIGATION FOR DUPLICATED CAS_ID */

/SELECT cas_id, COUNT(*)
FROM ab29_nfl_data_not_in_l1
WHERE layer = 1
GROUP BY cas_id
HAVING COUNT(*) > 1; ---- 0

/
SELECT cas_id, COUNT(*)
FROM ab29_rawfri_cas_nfl
WHERE layer = 1
GROUP BY cas_id
HAVING COUNT(*) > 1; ---- 0



SELECT cas_id, COUNT(*)
FROM casfri50.lyr_all
WHERE cas_id ILIKE 'AB29%'
GROUP BY cas_id
HAVING COUNT(*) > 1;   ---- 328154


SELECT cas_id, COUNT(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'AB29%'
GROUP BY cas_id
HAVING COUNT(*) > 1;   ---- no duplicate to table cas_all


SELECT cas_id, COUNT(*)
FROM casfri50.lyr_all
WHERE cas_id ILIKE 'AB29%' AND layer = 1
GROUP BY cas_id
HAVING COUNT(*) > 1;   ---- no duplicate to table lyr_all & layer = 1


/SELECT cas_id, COUNT(*)
FROM casfri50.lyr_all
WHERE cas_id ILIKE 'AB29%' AND layer = 2
GROUP BY cas_id
HAVING COUNT(*) > 1;   ---- no duplicate to table lyr_all & layer = 1

SELECT cas_id, COUNT(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'AB29%' AND layer = 1
GROUP BY cas_id
HAVING COUNT(*) > 1;   ---- no duplicate to table lyr_all & layer = 1


SELECT cas_id, COUNT(*)
FROM casfri50.
WHERE layer = 1
GROUP BY cas_id
HAVING COUNT(*) > 1;   ---- no duplicate to table lyr_all & layer = 1


-- MISCELLANEOUS INVESTIGATION FOR DOCUMENTATION.
SELECT DISTINCT(layer), COUNT(*) 
FROM casfri50_test.ab29_rawfri_cas_nfl
GROUP BY layer;





/*   ==================================================================================

     7.    AB30 DATA PROCESSING 

    ==================================================================================
*/


DROP TABLE  IF EXISTS casfri50_test.ab30_rawfri_cas_dist;
CREATE TABLE casfri50_test.ab30_rawfri_cas_dist AS
SELECT
 c.cas_id,
 --c.ogc_fid,
 --c.poly_id,
 --c.harv_yr,
 n.ogc_fid,
 n.harv_yr,
 n.poly_id
FROM rawfri.ab30 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.dst_all WHERE cas_id ILIKE 'ab30%' 
) c
ON n.poly_id:: INTEGER = c.poly_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;

