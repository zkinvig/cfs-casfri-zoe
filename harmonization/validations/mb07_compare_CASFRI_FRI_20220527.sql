/*
This is a working script to evaluate and validate CASFRI translation of various FRI. Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: April 01, 2022 by Kangakola
Last Modified Date: May 27, 2022 by Kangakola

*/

/*
=============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate  the layer 1 and layer 2 and several attributes (Species, Height, Crown closure, Origin & Productivity) of layer 1 and layer 2 in CASFRI translated data. 
	   Then assess the accuracy of these attributes between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Update: Integration of crownclosurecompare, heightcompare and siteclasscompare code. Also, add code for validation of dst, eco and nfl tables.	    
	*** Output: Multiple  table of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for MB07 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between these attributes for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
 ================================================================================================================================================================

*/


/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/


/*   LYR  INVESTIGATION   */

---------- Sub-setting layer 1 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 were used then they are common infromation to both layers 
-----------------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_all_l1;
CREATE TABLE casfri50_test.mb07_rafri_cas_all_l1 AS
SELECT
c.cas_id,
c.soil_moist_reg,
c.structure_per,
c.structure_range,
--c.layer,
--c.layer_rank,
c.crown_closure_upper,
c.crown_closure_lower,
c.height_upper,
c.height_lower,
c.productivity,
c.productivity_type,
c.species_1,
c.species_per_1,
c.species_2,
c.species_per_2,
--c.species_3,
--c.species_per_3,
--c.species_4,
--c.species_per_4,
--c.species_5,
--c.species_per_5,
--c.species_6,
--c.species_per_6,
--c.species_7,
--c.species_per_7,
--c.species_8,
--c.species_per_8,
--c.species_9,
--c.species_per_9,
--c.species_10,
--c.species_per_10,
c.origin_upper,
c.origin_lower,
c.site_class,
c.site_index,
n.age_2012,
n.agecl_2012,
--n.asp,
--n.canlay,
--n.canpat,
--n.canrank,
n.cc,
n.cc_sum,
--n.comht,
--n.covarr,
--n.covertype,
--n.denagg,
--n.drainpat,
--n.dtype,
--n.ext1,
--n.ext2,
n.ht,
n.ht_sum,
--n.inventory_id,
--n.landmod,
--n.lmodno,
--n.mod1,
--n.mod2,
--n.mr,
--n.nnf_anth,
--n.ogc_fid,
n.orig1,
n.orig2,
n.origin,
n.origin_sum,
n.poly_id,
--n.poly_id_1,
--n.saclass1,
--n.saclass2,
--n.sacov1,
--n.sacov2,
--n.seq,
--shape_area
--shape_leng
--n.si_leading,
--n.sic,
--n.slopepos,
--n.slper,
--n.soiltex,
n.sp1,
n.sp1_sum,
n.sp1per,
n.sp1per_sum,
n.sp2,
n.sp2_sum,
n.sp2per,
n.sp2per_sum,
--n.sp3,
--n.sp3_sum,
--n.sp3per,
--n.sp3per_sum,
--n.sp4,
--n.sp4_sum,
--n.sp4per,
--n.sp4per_sum,
--n.sp5,
--n.sp5_sum,
--n.sp5per,
--n.sp5per_sum,
--n.sp6,
--n.sp6_sum,
--n.sp6per,
--n.sp6per_sum,
--n.sph,
--n.spp_sum,
--n.src_filename,
--n.strata,
--n.topo,
n.treatext,
n.treatmod,
n.trorig,
--n.us2canlay,
--n.us2canpat,
--n.us2canrank,
n.us2cc,
n.us2ht,
--n.us2nnf_ant,
--n.us2origin,
n.us2sp1,
n.us2sp1per,
n.us2sp2,
n.us2sp2per,
--n.us2sp3,
--n.us2sp3per,
--n.us2sp4,
--n.us2sp4per,
--n.us2sp5,
--n.us2sp5per,
--n.us2sp6,
--n.us2sp6per,
--n.us2sph,
--n.us3canlay,
--n.us3canpat,
--n.us3canrank,
--n.us3cc,
--n.us3ht,
--n.us3nnf_ant,
--n.us3origin,
--n.us3sp1,
--n.us3sp1per,
--n.us3sp2,
--n.us3sp2per,
--n.us3sp3,
--n.us3sp3per,
--n.us3sp4,
--n.us3sp4per,
--n.us3sp5,
--n.us3sp5per,
--n.us3sp6,
--n.us3sp6per,
--n.us3sph,
--n.us4canlay,
--n.us4canpat,
--n.us4canrank,
--n.us4cc,
--n.us4ht,
--n.us4nnf_ant,
--n.us4origin,
--n.us4sp1,
--n.us4sp1per,
--n.us4sp2,
--n.us4sp2per,
--n.us4sp3,
--n.us4sp3per,
--n.us4sp4,
--n.us4sp4per,
--n.us4sp5,
--n.us4sp5per,
--n.us4sp6,
--n.us4sp6per,
--n.us4sph,
--n.us5canlay,
--n.us5canpat,
--n.us5canrank,
--n.us5cc,
--n.us5ht,
--n.us5nnf_ant,
--n.us5origin,
--n.us5sp1,
--n.us5sp1per,
--n.us5sp2,
--n.us5sp2per,
--n.us5sp3,
--n.us5sp3per,
--n.us5sp4,
--n.us5sp4per,
--n.us5sp5,
--n.us5sp5per,
--n.us5sp6,
--n.us5sp6per,
--n.us5sph,
--n.vol_key,
--n.weteco1,
--n.weteco2,
--n.wkb_geometry
n.yearphoto

FROM rawfri.mb07 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'mb07%' AND layer = 1
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.poly_id:: INTEGER = c.poly_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;

---------- Sub-setting layer 2 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 used in both layers 
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_all_l2;
CREATE TABLE casfri50_test.mb07_rafri_cas_all_l2 AS
SELECT
c.cas_id,
c.soil_moist_reg,
c.structure_per,
c.structure_range,
--c.layer,
--c.layer_rank,
c.crown_closure_upper,
c.crown_closure_lower,
c.height_upper,
c.height_lower,
c.productivity,
c.productivity_type,
c.species_1,
c.species_per_1,
c.species_2,
c.species_per_2,
--c.species_3,
--c.species_per_3,
--c.species_4,
--c.species_per_4,
--c.species_5,
--c.species_per_5,
--c.species_6,
--c.species_per_6,
--c.species_7,
--c.species_per_7,
--c.species_8,
--c.species_per_8,
--c.species_9,
--c.species_per_9,
--c.species_10,
--c.species_per_10,
c.origin_upper,
c.origin_lower,
c.site_class,
c.site_index,
n.age_2012,
n.agecl_2012,
--n.asp,
--n.canlay,
--n.canpat,
--n.canrank,
n.cc,
n.cc_sum,
--n.comht,
--n.covarr,
--n.covertype,
--n.denagg,
--n.drainpat,
--n.dtype,
--n.ext1,
--n.ext2,
n.ht,
n.ht_sum,
--n.inventory_id,
--n.landmod,
--n.lmodno,
--n.mod1,
--n.mod2,
--n.mr,
--n.nnf_anth,
--n.ogc_fid,
n.orig1,
n.orig2,
n.origin,
n.origin_sum,
n.poly_id,
--n.poly_id_1,
--n.saclass1,
--n.saclass2,
--n.sacov1,
--n.sacov2,
--n.seq,
--shape_area
--shape_leng
--n.si_leading,
--n.sic,
--n.slopepos,
--n.slper,
--n.soiltex,
n.sp1,
n.sp1_sum,
n.sp1per,
n.sp1per_sum,
n.sp2,
n.sp2_sum,
n.sp2per,
n.sp2per_sum,
--n.sp3,
--n.sp3_sum,
--n.sp3per,
--n.sp3per_sum,
--n.sp4,
--n.sp4_sum,
--n.sp4per,
--n.sp4per_sum,
--n.sp5,
--n.sp5_sum,
--n.sp5per,
--n.sp5per_sum,
--n.sp6,
--n.sp6_sum,
--n.sp6per,
--n.sp6per_sum,
--n.sph,
--n.spp_sum,
--n.src_filename,
--n.strata,
--n.topo,
n.treatext,
n.treatmod,
n.trorig,
--n.us2canlay,
--n.us2canpat,
--n.us2canrank,
n.us2cc,
n.us2ht,
--n.us2nnf_ant,
--n.us2origin,
n.us2sp1,
n.us2sp1per,
n.us2sp2,
n.us2sp2per,
--n.us2sp3,
--n.us2sp3per,
--n.us2sp4,
--n.us2sp4per,
--n.us2sp5,
--n.us2sp5per,
--n.us2sp6,
--n.us2sp6per,
--n.us2sph,
--n.us3canlay,
--n.us3canpat,
--n.us3canrank,
--n.us3cc,
--n.us3ht,
--n.us3nnf_ant,
--n.us3origin,
--n.us3sp1,
--n.us3sp1per,
--n.us3sp2,
--n.us3sp2per,
--n.us3sp3,
--n.us3sp3per,
--n.us3sp4,
--n.us3sp4per,
--n.us3sp5,
--n.us3sp5per,
--n.us3sp6,
--n.us3sp6per,
--n.us3sph,
--n.us4canlay,
--n.us4canpat,
--n.us4canrank,
--n.us4cc,
--n.us4ht,
--n.us4nnf_ant,
--n.us4origin,
--n.us4sp1,
--n.us4sp1per,
--n.us4sp2,
--n.us4sp2per,
--n.us4sp3,
--n.us4sp3per,
--n.us4sp4,
--n.us4sp4per,
--n.us4sp5,
--n.us4sp5per,
--n.us4sp6,
--n.us4sp6per,
--n.us4sph,
--n.us5canlay,
--n.us5canpat,
--n.us5canrank,
--n.us5cc,
--n.us5ht,
--n.us5nnf_ant,
--n.us5origin,
--n.us5sp1,
--n.us5sp1per,
--n.us5sp2,
--n.us5sp2per,
--n.us5sp3,
--n.us5sp3per,
--n.us5sp4,
--n.us5sp4per,
--n.us5sp5,
--n.us5sp5per,
--n.us5sp6,
--n.us5sp6per,
--n.us5sph,
--n.vol_key,
--n.weteco1,
--n.weteco2,
--n.wkb_geometry
n.yearphoto

FROM rawfri.mb07 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'mb07%' AND layer = 2
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.poly_id:: INTEGER = c.poly_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;
*/
-----------------------------------------------------------------------------------------

--             SPECIES COMPARISON BETWEEN CAS AND RAWFRI

-----------------------------------------------------------------------------------------
-- LAYER 1

DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_Species_compare_l1;
CREATE TABLE casfri50_test.mb07_rafri_cas_Species_compare_l1 AS
SELECT
cas_id,
species_1,
species_per_1,
species_2,
species_per_2,
sp1,
sp1_sum,
sp1per,
sp1per_sum,
sp2,
sp2_sum,
sp2per,
sp2per_sum,
us2sp1,
us2sp1per,
us2sp2,
us2sp2per
FROM casfri50_test.mb07_rafri_cas_all_l1;

-- LAYER 2
DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_Species_compare_l2;
CREATE TABLE casfri50_test.mb07_rafri_cas_Species_compare_l2 AS
SELECT
cas_id,
species_1,
species_per_1,
species_2,
species_per_2,
sp1,
sp1_sum,
sp1per,
sp1per_sum,
sp2,
sp2_sum,
sp2per,
sp2per_sum,
us2sp1,
us2sp1per,
us2sp2,
us2sp2per
FROM casfri50_test.mb07_rafri_cas_all_l2;

-----------------------------------------------------------------------------------------

--             Height COMPARISON BETWEEN CAS AND RAWFRI

-----------------------------------------------------------------------------------------
-- layer 1

DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_height_compare_l1;
CREATE TABLE casfri50_test.mb07_rafri_cas_height_compare_l1 AS
SELECT
cas_id,
height_upper,
height_lower,
ht,
ht_sum,
us2ht
--us3ht
FROM casfri50_test.mb07_rafri_cas_all_l1;

--LAYER 2
DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_height_compare_l2;
CREATE TABLE casfri50_test.mb07_rafri_cas_height_compare_l2 AS
SELECT
cas_id,
height_upper,
height_lower,
ht,
ht_sum,
us2ht
--us3ht
FROM casfri50_test.mb07_rafri_cas_all_l2;
-----------------------------------------------------------------------------------------

--             CROWN CLOSURE COMPARISON BETWEEN CAS AND RAWFRI

-----------------------------------------------------------------------------------------
-- layer 1

DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_cc_l1;
CREATE TABLE casfri50_test.mb07_rafri_cas_cc_l1 AS
SELECT
cas_id,
crown_closure_upper,
crown_closure_lower,
cc,
cc_sum,
us2cc
FROM casfri50_test.mb07_rafri_cas_all_l1;

-- layer 2


DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_cc_l2;
CREATE TABLE casfri50_test.mb07_rafri_cas_cc_l2 AS
SELECT
cas_id,
crown_closure_upper,
crown_closure_lower,
cc,
cc_sum,
us2cc
FROM casfri50_test.mb07_rafri_cas_all_l2;

-----------------------------------------------------------------------------------------

--             ORIGIN COMPARISON BETWEEN CAS AND RAWFRI

-----------------------------------------------------------------------------------------
-- layer 1

DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_corigin_l1;
CREATE TABLE casfri50_test.mb07_rafri_cas_corigin_l1 AS
SELECT
cas_id,
origin_upper,
origin_lower,
orig1,
orig2,
origin,
origin_sum
FROM casfri50_test.mb07_rafri_cas_all_l1;

-- layer 2

DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_corigin_l2;
CREATE TABLE casfri50_test.mb07_rafri_cas_corigin_l2 AS
SELECT
cas_id,
origin_upper,
origin_lower,
orig1,
orig2,
origin,
origin_sum
FROM casfri50_test.mb07_rafri_cas_all_l2;


-----------------------------------------------------------------------------------------

--             PRODUCTIVITY COMPARISON BETWEEN CAS AND RAWFRI

-----------------------------------------------------------------------------------------
-- layer 1

DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_prodictivity_dist_l1;
CREATE TABLE casfri50_test.mb07_rafri_cas_prodictivity_dist_l1 AS
SELECT
cas_id,
productivity,
productivity_type
--covertype,
--dtype,

FROM casfri50_test.mb07_rafri_cas_all_l1;

-- Layer 2

DROP TABLE IF EXISTS casfri50_test.mb07_rafri_cas_prodictivity_dist_l2;
CREATE TABLE casfri50_test.mb07_rafri_cas_prodictivity_dist_l2 AS
SELECT
cas_id,
productivity,
productivity_type
FROM casfri50_test.mb07_rafri_cas_all_l2;



/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/



/*   DST INVESTIGATION  */


DROP TABLE IF EXISTS casfri50_test.disturbance_MB07_CAS;
CREATE TABLE casfri50_test.disturbance_MBO7_CAS AS
SELECT *
FROM casfri50.dst_all 
WHERE cas_id ILIKE 'MB07%';


DROP TABLE IF EXISTS casfri50_test.disturbance_MB07_CAS_FRI;
CREATE TABLE casfri50_test.disturbance_MBO7_CAS_FRI AS
SELECT
   c.cas_id,
   c.dist_type_1,
   c.dist_year_1,
   n.mod1,
   n.orig1,
   n.ogc_fid,
   n.poly_id
   
FROM rawfri.mb07 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50_test.disturbance_mbo7_cas
) c
ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_id:: INTEGER = c.poly_id:: INTEGER;


/*   ================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/



/* ECO INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.eco_MB07_CAS_FRI;
CREATE TABLE casfri50_test.eco_MB07_CAS_FRI AS
SELECT
   c.cas_id,
   c.wetland_type,
   c.wet_veg_cover,
   c.wet_landform_mod,
   c.wet_local_mod,
   c.eco_site,
   n.landmod,
   n.weteco1,
   n.mr,
   --n.landmod,
   n.ogc_fid,
   n.poly_id
   
FROM rawfri.mb07 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.eco_all WHERE cas_id ILIKE 'mb07%'
) c
ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_id:: INTEGER = c.poly_id:: INTEGER;


/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/


/* NFL INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.nfl_MB07_CAS;
CREATE TABLE casfri50_test.nfl_MB07_CAS AS
SELECT *
FROM casfri50.nfl_all 
WHERE cas_id ILIKE 'MB07%';


DROP TABLE IF EXISTS casfri50_test.nfl_MB07_CAS_FRI;
CREATE TABLE casfri50_test.nfl_MB07_CAS_FRI AS
SELECT
   c.cas_id,
   c.nat_non_veg,
   c.non_for_anth,
   c.non_for_veg,
   c.crown_closure_upper,
   c.crown_closure_lower,
   c.height_upper,
   c.height_lower,
   n.landmod,
   n.weteco1,
   n.mr,
   --n.landmod,
   n.nnf_anth,
   n.sp1,
   n.sp2,
   n.sp1per,
   n.cc,
   n.ht,
   n.ogc_fid,
   n.poly_id
   
FROM rawfri.mb07 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50_test.nfl_mb07_cas
) c
ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_id:: INTEGER = c.poly_id:: INTEGER;


/*   ==================================================================================

     5.    STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 

    ==================================================================================
*/




/* STAND STRUCTURE  INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.stand_structure_mb07;
CREATE TABLE casfri50_test.stand_structure_mb07 AS
SELECT stand_structure, count(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'MB07%' 
GROUP BY stand_structure;


/* NFL LAYER COUNT INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.nfl_layer_count_mb07;
CREATE TABLE casfri50_test.nfl_layer_count_mb07 AS
SELECT layer, count(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'MB07%' 
GROUP BY layer;

