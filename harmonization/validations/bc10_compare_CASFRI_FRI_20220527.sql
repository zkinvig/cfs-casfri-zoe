/*
This is a working script to evaluate and validate CASFRI translation of various FRI. Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: March 31, 2022 by Kangakola
Last Modified Date: May 27, 2022 by Kangakola

*/



/*
=============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate CASFRI translation of various FRI of Juridiction forest inventory datata. 
	*** This is proceed by comparing the attributes values of CASFRI translate data as per CASFRI SCHEMAS to the attributes of inventory sources data. 
	     To assess if the translation was accurate and the translation rule was followed and the resulted translated data are similar to the original data as per translation rules.
	*** Update: Integration of crownclosurecompare, heightcompare and siteclasscompare code. Also, add code for validation of dst, eco and nfl tables.		 
	*** Output: Two table of Join between CASFRI DATA and  RAWFRI for BC10 for layer 1 & 2 that have the relevant combine attributes of CASFRI translated data and raw forest resource inventory data.
	*** Software: PgAdmin 4.6
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
	
 ================================================================================================================================================================

*/


--=============================================================================================================================================================================



/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/


/*    LYR INVESTIGATION     */


---------- Sub-setting layer 1 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 were used then they are common infromation to both layers 
-----------------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.test_rawfri_cas_l1_bc10;
CREATE TABLE casfri50_test.test_rawfri_cas_l1_bc10 AS
SELECT
c.cas_id,
 c.crown_closure_lower,
 c.crown_closure_upper,
 c.height_lower,
 c.height_upper,
 c.layer,
 c.layer_rank,
 c.origin_lower,
 c.origin_upper,
 c.productivity,
 c.productivity_type,
 c.site_class,
 c.site_index,
 c.soil_moist_reg,
 c.species_1,
 c.species_10,
 c.species_2,
 c.species_3,
 c.species_4,
 c.species_5,
 c.species_6,
 c.species_7,
 c.species_8,
 c.species_9,
 c.species_per_1,
 c.species_per_10,
 c.species_per_2,
 c.species_per_3,
 c.species_per_4,
 c.species_per_5,
 c.species_per_6,
 c.species_per_7,
 c.species_per_8,
 c.species_per_9,
 c.structure_per,
 c.structure_range,
 n.adjusted_ind,
 n.alpine_designation,
 n.attribution_base_date,
 n.avail_label_height,
 n.avail_label_width,
 n.bclcs_level_1,
 n.bclcs_level_2,
 n.bclcs_level_3,
 n.bclcs_level_4,
 n.bclcs_level_5,
 n.bec_phase,
 n.bec_subzone,
 n.bec_variant,
 n.bec_zone_code,
 n.bryoid_cover_pct,
 n.coast_interior_cd,
 n.compartment,
 n.compartment_letter,
 n.earliest_nonlogging_dist_date,
 n.earliest_nonlogging_dist_type,
 n.ecosys_class_data_src_cd,
 n.est_coverage_pct_1,
 n.est_coverage_pct_2,
 n.est_coverage_pct_3,
 n.feature_area_sqm,
 n.feature_class_skey,
 n.feature_id,
 n.feature_length_m,
 n.fiz_cd,
 n.for_mgmt_land_base_ind,
 n.free_to_grow_ind,
 n.full_label,
 n.harvest_date,
 n.herb_cover_pattern,
 n.herb_cover_pct,
 n.herb_cover_type,
 n.input_date,
 n.interpretation_date,
 n.interpreter,
 n.inventory_id,
 n.inventory_region,
 n.inventory_standard_cd,
 n.l1_crown_closure,
 n.l1_crown_closure_class_cd,
 n.l1_data_source_age_cd,
 n.l1_data_source_height_cd,
 n.l1_data_src_vri_live_stem_ha_cd,
 n.l1_est_site_index,
 n.l1_est_site_index_source_cd,
 n.l1_est_site_index_species_cd,
 n.l1_for_cover_rank_cd,
 n.l1_geometry_area,
 n.l1_geometry_len,
 n.l1_interpreted_data_src_cd,
 n.l1_layer_id,
 n.l1_non_forest_descriptor,
 n.l1_proj_age_1,
 n.l1_proj_age_2,
 n.l1_proj_age_class_cd_1,
 n.l1_proj_age_class_cd_2,
 n.l1_proj_height_1,
 n.l1_proj_height_2,
 n.l1_proj_height_class_cd_1,
 n.l1_proj_height_class_cd_2,
 n.l1_reference_date,
 n.l1_site_index,
 n.l1_species_cd_1,
 n.l1_species_cd_2,
 n.l1_species_cd_3,
 n.l1_species_cd_4,
 n.l1_species_cd_5,
 n.l1_species_cd_6,
 n.l1_species_pct_1,
 n.l1_species_pct_2,
 n.l1_species_pct_3,
 n.l1_species_pct_4,
 n.l1_species_pct_5,
 n.l1_species_pct_6,
 n.l1_tree_cover_pattern,
 n.l1_vertical_complexity,
 n.l1_vri_dead_stems_per_ha,
 n.l1_vri_live_stems_per_ha, 
 n.label_centre_x,
 n.label_centre_y,
 n.label_height,
 n.label_width,
 n.land_cover_class_cd_1,
 n.land_cover_class_cd_2,
 n.land_cover_class_cd_3,
 n.line_1_opening_number,
 n.line_1_opening_symbol_cd,
 n.line_2_polygon_id,
 n.line_3_tree_species,
 n.line_4_classes_indexes,
 n.line_5_vegetation_cover,
 n.line_6_site_prep_history,
 n.line_7_activity_hist_symbol,
 n.line_7a_stand_tending_history,
 n.line_7b_disturbance_history,
 n.line_8_planting_history,
 n.map_id,
 n.modifying_process,
 n.non_productive_cd,
 n.non_productive_descriptor_cd,
 n.non_veg_cover_pattern_1,
 n.non_veg_cover_pattern_2,
 n.non_veg_cover_pattern_3,
 n.non_veg_cover_pct_1,
 n.non_veg_cover_pct_2,
 n.non_veg_cover_pct_3,
 n.non_veg_cover_type_1,
 n.non_veg_cover_type_2,
 n.non_veg_cover_type_3,
 n.ogc_fid,
 n.opening_id,
 n.opening_ind,
 n.opening_number,
 n.opening_source,
 n.org_unit_code,
 n.org_unit_no,
 n.polygon_area,
 n.polygon_id,
 n.printable_ind,
 n.project,
 n.projected_date,
 n.reference_year,
 n.shrub_cover_pattern,
 n.shrub_crown_closure,
 n.shrub_height,
 n.site_position_meso,
 n.small_label,
 n.soil_moisture_regime_1,
 n.soil_moisture_regime_2,
 n.soil_moisture_regime_3,
 n.soil_nutrient_regime,
 n.special_cruise_number,
 n.special_cruise_number_cd,
 n.src_filename,
 n.stand_percentage_dead,
 n.surface_expression
 
FROM rawfri.bc10 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE left(cas_id, 4) = 'BC10' and layer =1
) c
ON n.map_id = c.map_id AND n.feature_id = c.poly_id:: INTEGER;



---------- Sub-setting layer 2 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 used in both layers 
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.test_rawfri_cas_l2_bc10;
CREATE TABLE casfri50_test.test_rawfri_cas_l2_bc10 AS
SELECT
c.cas_id,
 c.crown_closure_lower,
 c.crown_closure_upper,
 c.height_lower,
 c.height_upper,
 c.layer,
 c.layer_rank,
 c.origin_lower,
 c.origin_upper,
 c.productivity,
 c.productivity_type,
 c.site_class,
 c.site_index,
 c.soil_moist_reg,
 c.species_1,
 c.species_10,
 c.species_2,
 c.species_3,
 c.species_4,
 c.species_5,
 c.species_6,
 c.species_7,
 c.species_8,
 c.species_9,
 c.species_per_1,
 c.species_per_10,
 c.species_per_2,
 c.species_per_3,
 c.species_per_4,
 c.species_per_5,
 c.species_per_6,
 c.species_per_7,
 c.species_per_8,
 c.species_per_9,
 c.structure_per,
 c.structure_range,
 n.adjusted_ind,
 n.alpine_designation,
 n.attribution_base_date,
 n.avail_label_height,
 n.avail_label_width,
 n.bclcs_level_1,
 n.bclcs_level_2,
 n.bclcs_level_3,
 n.bclcs_level_4,
 n.bclcs_level_5,
 n.bec_phase,
 n.bec_subzone,
 n.bec_variant,
 n.bec_zone_code,
 n.bryoid_cover_pct,
 n.coast_interior_cd,
 n.compartment,
 n.compartment_letter,
 n.earliest_nonlogging_dist_date,
 n.earliest_nonlogging_dist_type,
 n.ecosys_class_data_src_cd,
 n.est_coverage_pct_1,
 n.est_coverage_pct_2,
 n.est_coverage_pct_3,
 n.feature_area_sqm,
 n.feature_class_skey,
 n.feature_id,
 n.feature_length_m,
 n.fiz_cd,
 n.for_mgmt_land_base_ind,
 n.free_to_grow_ind,
 n.full_label,
 n.harvest_date,
 n.herb_cover_pattern,
 n.herb_cover_pct,
 n.herb_cover_type,
 n.input_date,
 n.interpretation_date,
 n.interpreter,
 n.inventory_id,
 n.inventory_region,
 n.inventory_standard_cd, 
 n.l2_crown_closure,
 n.l2_crown_closure_class_cd,
 n.l2_data_source_age_cd,
 n.l2_data_source_height_cd,
 n.l2_data_src_vri_live_stem_ha_cd,
 n.l2_est_site_index,
 n.l2_est_site_index_source_cd,
 n.l2_est_site_index_species_cd,
 n.l2_for_cover_rank_cd,
 n.l2_interpreted_data_src_cd,
 n.l2_layer_id,
 n.l2_non_forest_descriptor,
 n.l2_proj_age_1,
 n.l2_proj_age_2,
 n.l2_proj_age_class_cd_1,
 n.l2_proj_age_class_cd_2,
 n.l2_proj_height_1,
 n.l2_proj_height_2,
 n.l2_proj_height_class_cd_1,
 n.l2_proj_height_class_cd_2,
 n.l2_reference_date,
 n.l2_site_index,
 n.l2_species_cd_1,
n.l2_species_cd_2,
 n.l2_species_cd_3,
 n.l2_species_cd_4,
 n.l2_species_cd_5,
 n.l2_species_cd_6,
 n.l2_species_pct_1,
 n.l2_species_pct_2,
 n.l2_species_pct_3,
 n.l2_species_pct_4,
 n.l2_species_pct_5,
 n.l2_species_pct_6,
 n.l2_tree_cover_pattern,
 n.l2_vertical_complexity,
 n.l2_vri_dead_stems_per_ha,
 n.l2_vri_live_stems_per_ha,
 n.label_centre_x,
 n.label_centre_y,
 n.label_height,
 n.label_width,
 n.land_cover_class_cd_1,
 n.land_cover_class_cd_2,
 n.land_cover_class_cd_3,
 n.line_1_opening_number,
 n.line_1_opening_symbol_cd,
 n.line_2_polygon_id,
 n.line_3_tree_species,
 n.line_4_classes_indexes,
 n.line_5_vegetation_cover,
 n.line_6_site_prep_history,
 n.line_7_activity_hist_symbol,
 n.line_7a_stand_tending_history,
 n.line_7b_disturbance_history,
 n.line_8_planting_history,
 n.map_id,
 n.modifying_process,
 n.non_productive_cd,
 n.non_productive_descriptor_cd,
 n.non_veg_cover_pattern_1,
 n.non_veg_cover_pattern_2,
 n.non_veg_cover_pattern_3,
 n.non_veg_cover_pct_1,
 n.non_veg_cover_pct_2,
 n.non_veg_cover_pct_3,
 n.non_veg_cover_type_1,
 n.non_veg_cover_type_2,
 n.non_veg_cover_type_3,
 n.ogc_fid,
 n.opening_id,
 n.opening_ind,
 n.opening_number,
 n.opening_source,
 n.org_unit_code,
 n.org_unit_no,
 n.polygon_area,
 n.polygon_id,
 n.printable_ind,
 n.project,
 n.projected_date,
 n.reference_year,
 n.shrub_cover_pattern,
 n.shrub_crown_closure,
 n.shrub_height,
 n.site_position_meso,
 n.small_label,
 n.soil_moisture_regime_1,
 n.soil_moisture_regime_2,
 n.soil_moisture_regime_3,
 n.soil_nutrient_regime,
 n.special_cruise_number,
 n.special_cruise_number_cd,
 n.src_filename,
 n.stand_percentage_dead,
 n.surface_expression
 
FROM rawfri.bc10 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE left(cas_id, 4) = 'BC10' and layer =2
) c
ON n.map_id = c.map_id AND n.feature_id = c.poly_id:: INTEGER;



/*    CROWN CLOSURE INVESTIGATION. */
/*
=============================================================================================================================================================
 
	*** Purpose: This code evaluate and validate  the Crown Closure of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of Crown closure attributes between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of Crown closure comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for BC10 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between the Crown closure attributes for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
 ================================================================================================================================================================

*/


------ EVALUATING upper and lower height of layer 1 and layer 2 ----------------------------------------
DROP TABLE IF EXISTS casfri50_test.CrownClosure_BC10_TL;
DROP TABLE IF EXISTS casfri50_test.CrownClosure_BC10_LL;
DROP TABLE IF EXISTS casfri50_test.CrownClosure_BC10_CAS;
DROP TABLE IF EXISTS casfri50_test.CrownClosure_BC10_FRI_CAS; 


---------- Top Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.CrownClosure_BC10_TL AS
SELECT 
	cas_id, 
	crown_closure_upper as crown_closure_upper_TL, --- Value to be declared 
	crown_closure_lower as crown_closure_lower_TL --- Value to be declared 
FROM lyr_all 
WHERE cas_id ILIKE 'BC10%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.CrownClosure_BC10_LL AS
SELECT 
	cas_id as cas_id_L2, 
	crown_closure_upper as crown_closure_upper_LL, --- Value to be declared 
	crown_closure_lower as crown_closure_lower_LL  --- Value to be declared 
FROM lyr_all 
WHERE cas_id ILIKE 'BC10%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
CREATE TABLE casfri50_test.CrownClosure_BC10_CAS AS
SELECT U.cas_id, 
	U.crown_closure_upper_TL, 
	U.crown_closure_lower_TL, 
	L.crown_closure_upper_LL,
	L.crown_closure_lower_LL 
FROM casfri50_test.CrownClosure_BC10_TL U 
LEFT JOIN 
(SELECT 
	cas_id_l2, 
	crown_closure_upper_LL, 
	crown_closure_lower_LL 
FROM casfri50_test.CrownClosure_BC10_LL) L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------

CREATE TABLE casfri50_test.CrownClosure_BC10_FRI_CAS AS
SELECT 
	c.cas_id,
	n.map_id,
	n.src_filename,
	-- n.l1_src_filename, 
	-- n.l2_src_filename, 
	n.polygon_id,
	n.ogc_fid,	
	n.feature_id,
	-- n.l1_feature_id,
	-- n.l2_feature_id,
	c.crown_closure_upper_TL, 	 
	c.crown_closure_lower_TL, 
	c.crown_closure_upper_LL, 
	c.crown_closure_lower_LL, 
	n.l1_crown_closure, --- Value to be declared 
	n.l2_crown_closure --- Value to be declared 
FROM rawfri.bc10 n
JOIN 
	(select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid 
FROM casfri50_test.CrownClosure_BC10_CAS ) c

ON n.map_id = c.map_id AND n.feature_id = c.poly_id:: INTEGER;  --- seems its not a poly_id it should be feature_id



/* HEIGHT INVESTIGATION */
/*
=============================================================================================================================================================

	*** Purpose: This code evaluate and validate  the height of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of height attributes between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of Height comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for BC10 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between the heigth attributes for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
 ================================================================================================================================================================

*/

------ EVALUATING upper and lower height of layer 1 and layer 2 ----------------------------------------
DROP TABLE IF EXISTS TEST_Height_L1_bc10;
DROP TABLE IF EXISTS TEST_Height_L2_bc10;
DROP TABLE IF EXISTS TEST_CAS_bc10;
DROP TABLE IF EXISTS TEST_FRI_CAS_bc10;

--- original data excluding wkb_geomtery
-- DROP TABLE TEST_fri_bc10;
-- CREATE TABLE TEST_fri_bc10 AS SELECT * FROM fri.bc10;
 -- ALTER TABLE TEST_fri_bc10 DROP COLUMN wkb_geometry;
 -- SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'TEST_fri_bc10' AND table_schema = 'casfri'  ORDER BY COLUMN_NAME;	
-- SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'TEST_fri_bc10';

---- Check translated inventories
-- SELECT DISTINCT(inventory_id) FROM cas_all; --- PE01, NS03, BC10, ON02, NB02

---------- Top Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.TEST_Height_L1_bc10 AS
SELECT 
	cas_id, 
	height_upper as height_upper_TL, 
	height_lower as height_lower_TL 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'BC10%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.TEST_Height_L2_bc10 AS
SELECT 
	cas_id as cas_id_L2, 
	height_upper as height_upper_LL, 
	height_lower as height_lower_LL 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'BC10%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
CREATE TABLE casfri50_test.TEST_CAS_bc10 AS
SELECT U.cas_id, 
	U.height_upper_TL, 
	U.height_lower_TL, 
	L.height_upper_LL,
	L.height_lower_LL 
FROM  casfri50_test.TEST_Height_L1_bc10 as U 
LEFT JOIN 
(SELECT 
	cas_id_l2, 
	height_upper_LL, 
	height_lower_LL 
FROM casfri50_test.TEST_Height_L2_bc10) as L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------

--- 

CREATE TABLE casfri50_test.TEST_FRI_CAS_bc10 AS
SELECT 
	c.cas_id,
	n.map_id, 	
	n.src_filename, 
	--n.l2_src_filename, 
	n.polygon_id,
	n.ogc_fid,	
	n.feature_id,
	--n.l2_feature_id,
	c.height_upper_TL, 	 
	c.height_lower_TL, 
	c.height_upper_LL, 
	c.height_lower_LL, 
	n.l1_proj_height_1, 
	n.l1_proj_height_2,
	n.l2_proj_height_1, 
	n.l2_proj_height_2
	--n.feature_id
FROM rawfri.bc10 n
JOIN 
	(select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid 
FROM casfri50_test.TEST_CAS_bc10) c

ON n.map_id = c.map_id AND n.feature_id = c.poly_id:: INTEGER;  --- seems its not a poly_id it should be feayure_id


/* SITE CLASS AND INDEX INVESTIGATION */

/*
=============================================================================================================================================================
    
	*** Purpose: This code evaluate and validate  the site class of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of site class attributes between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of site class comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for BC10 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between the site class attributes for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
 ================================================================================================================================================================

*/

------ EVALUATING site index (original and tranlated values) of lower layer 1 and layer 2 ----------------------------------------
DROP TABLE IF EXISTS  casfri50_test.BC10_TEST_SiteIndex_TL;
DROP TABLE IF EXISTS  casfri50_test.BC10_TEST_SiteIndex_LL;
DROP TABLE IF EXISTS  casfri50_test.BC10_TEST_SiteIndex_CAS;
DROP TABLE IF EXISTS  casfri50_test.BC10_TEST_SiteIndex_FRI_CAS;



---------- Top Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.BC10_TEST_SiteIndex_TL AS
SELECT 
	cas_id, 
	site_class as site_class_TL --- Value to be declared 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'BC10%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.BC10_TEST_SiteIndex_LL AS
SELECT 
	cas_id as cas_id_L2, 
	site_class as site_class_LL --- Value to be declared
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'BC10%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
CREATE TABLE casfri50_test.BC10_TEST_SiteIndex_CAS AS
SELECT U.cas_id, 
	U.site_class_TL,
	L.site_class_LL
FROM casfri50_test.BC10_TEST_SiteIndex_TL U 
LEFT JOIN T
(SELECT 
	cas_id_l2, 
	site_class_LL	
FROM casfri50_test.BC10_TEST_SiteIndex_LL) L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------

CREATE TABLE casfri50_test.BC10_TEST_SiteIndex_FRI_CAS AS
SELECT 
	c.cas_id, 
	n.src_filename, 
	n.polygon_id,
	n.ogc_fid,	
	c.site_class_TL,
	c.site_class_LL,
	n.l1_site_index, --- Value to be declared 
	n.l2_site_index --- Value to be declared
FROM rawfri.bc10 n
JOIN 
	(select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid 
FROM casfri50_test.BC10_TEST_SiteIndex_CAS) c

ON n.map_id = c.map_id AND n.feature_id = c.poly_id:: INTEGER;  --- seems its not a poly_id it should be feayure_id



/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/



/*    DST INVESTIGATION     */

DROP TABLE IF EXISTS casfri50_test.disturbance_rawfri_cas_bc10;
CREATE TABLE casfri50_test.disturbance_rawfri_cas_bc10 AS
SELECT
c.cas_id,
 c.dist_type_1,
 c.dist_year_1,
 c.dist_ext_upper_1,
 c.dist_ext_lower_1,
 n.feature_id,
 n.map_id,
 --n.line_7_activity_hist_symbol,
 --n.line_7a_stand_tending_history,
 n.line_7b_disturbance_history,
 --n.line_8_planting_history,
 n.modifying_process
 
FROM rawfri.bc10 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.dst_all WHERE left(cas_id, 4) = 'BC10' 
) c
ON n.map_id = c.map_id AND n.feature_id = c.poly_id:: INTEGER;


/*   ================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/



/*    ECO INVESTIGATION     */

DROP TABLE IF EXISTS casfri50_test.eco_rawfri_cas_bc10;
CREATE TABLE casfri50_test.eco_rawfri_cas_bc10 AS
SELECT
c.cas_id,
 c.layer,
 c.wetland_type,
 c.wet_veg_cover,
 c.wet_landform_mod,
 c.wet_local_mod,
 c.eco_site,
 n.feature_id,
 n.map_id,
 n.land_cover_class_cd_1
  
FROM rawfri.bc10 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.eco_all WHERE left(cas_id, 4) = 'BC10' 
) c
ON n.map_id = c.map_id AND n.feature_id = c.poly_id:: INTEGER;



/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/



/*    NFL  INVESTIGATION     */

DROP TABLE IF EXISTS casfri50_test.nfl_rawfri_cas_bc10;
CREATE TABLE casfri50_test.nfl_rawfri_cas_bc10 AS
SELECT
c.cas_id,
 c.layer,
 c.crown_closure_lower,
 c.crown_closure_upper,
 c.height_lower,
 c.height_upper,
 c.non_for_veg,
 n.feature_id,
 n.map_id,
 n.non_productive_cd,
 n.non_productive_descriptor_cd,
 n.shrub_crown_closure,
 n.shrub_height,
 n.bclcs_level_4,
 n.land_cover_class_cd_1
  
FROM rawfri.bc10 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.nfl_all WHERE left(cas_id, 4) = 'BC10' 
) c
ON n.map_id = c.map_id AND n.feature_id = c.poly_id:: INTEGER;




/*   ==================================================================================

     5.    STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 

    ==================================================================================
*/




/* STAND STRUCTURE INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.stand_structure_bc10;
CREATE TABLE casfri50_test.stand_structure_bc10 AS
SELECT stand_structure, count(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'BC10%' 
GROUP BY stand_structure;


- NFL layer count.

DROP TABLE IF EXISTS casfri50_test.nfl_layer_count_bc10;
CREATE TABLE casfri50_test.nfl_layer_count_bc10 AS
SELECT layer, count(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'BC10%' 
GROUP BY layer;

