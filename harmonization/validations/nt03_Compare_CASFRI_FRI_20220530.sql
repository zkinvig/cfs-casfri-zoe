
/*
This is a working script to evaluate and validate CASFRI translation of various FRI. Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: April 04, 2022 by Kangakola
Last Modified Date: May 30, 2022 by Kangakola

*/

/*
=============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate  the layer 1 and layer 2 and several attributes (Species, Height, Crown closure, Origin & Productivity) of layer 1 and layer 2 in CASFRI translated data. 
	   Then assess the accuracy of these attributes between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Update: Integration of crownclosurecompare, heightcompare and siteclasscompare code. Also, add code for validation of dst, eco and nfl tables.	    
	*** Output: Multiple  table of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for NT03 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between these attributes for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
 ================================================================================================================================================================

*/

/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/



/* LYR INVESTIGATION  */

---------- Sub-setting layer 1 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 were used then they are common infromation to both layers 
-----------------------------------------------------------------

DROP TABLE  IF EXISTS casfri50_test.nt03_rawfri_cas_l1;
CREATE TABLE casfri50_test.nt03_rawfri_cas_l1 AS
SELECT
c.cas_id,
c.soil_moist_reg,
c.structure_per,
c.structure_range,
c.layer,
c.layer_rank,
c.crown_closure_upper,
c.crown_closure_lower,
c.height_upper,
c.height_lower,
c.productivity,
c.productivity_type,
c.species_1,
c.species_per_1,
c.species_2,
c.species_per_2,
c.species_3,
c.species_per_3,
c.species_4,
c.species_per_4,
c.species_5,
c.species_per_5,
c.species_6,
c.species_per_6,
c.species_7,
c.species_per_7,
c.species_8,
c.species_per_8,
c.species_9,
c.species_per_9,
c.species_10,
c.species_per_10,
c.origin_upper,
c.origin_lower,
c.site_class,
c.site_index,
--c.cas_id,
--c.crown_closure_lower,
--c.crown_closure_upper,
--c.height_lower,
--c.height_upper,
--c.layer,
--c.layer_rank,
--c.origin_lower,
--c.origin_upper,
--c.productivity,
--c.productivity_type,
--c.site_class,
--c.site_index,
--c.soil_moist_reg,
--c.species_1,
--c.species_10,
--c.species_2,
--c.species_3,
--c.species_4,
--c.species_5,
--c.species_6,
--c.species_7,
--c.species_8,
--c.species_9,
--c.species_per_1,
--c.species_per_10,
--c.species_per_2,
--c.species_per_3,
--c.species_per_4,
--c.species_per_5,
--c.species_per_6,
--c.species_per_7,
--c.species_per_8,
--c.species_per_9,
--c.structure_per,
--c.structure_range,
 --c.poly_num,
 --c.ogc_fid,
n.ogc_fid,
n.fc_id,
n.landcov,
n.landpos,
n.typeclas,
n.densitycls,
n.moisture,
n.sp1,
n.sp1_per,
n.sp2,
n.sp2per,
n.sp3,
n.sp3per,
n.sp4,
n.sp4per,
n.height,
n.crownclos,
n.origin,
n.siteclass,
n.wetland,
n.ref_year,
n.landuse,
n.dis1code,
n.dis1ext,
n.dis1year,
n.mintypeclas,
n.minmoist,
n.minsp1,
n.minsp1per,
n.minsp2,
n.minsp2per,
n.minsp3,
n.minsp3per,
n.minsp4,
n.minsp4per,
n.minheight,
n.mincrownclos,
n.minorigin,
n.minsiteclass,
n.si_50,
n.src_filename,
n.inventory_id

FROM rawfri.nt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS invproj_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS fc_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'nt03%' AND layer = 1
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.fc_id:: INTEGER = c.fc_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;


---------- Sub-setting layer 2 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 used in both layers 
-----------------------------------------------------------------

DROP TABLE  IF EXISTS casfri50_test.nt03_rawfri_cas_l2;
CREATE TABLE casfri50_test.nt03_rawfri_cas_l2 AS
SELECT
c.cas_id,
c.soil_moist_reg,
c.structure_per,
c.structure_range,
c.layer,
c.layer_rank,
c.crown_closure_upper,
c.crown_closure_lower,
c.height_upper,
c.height_lower,
c.productivity,
c.productivity_type,
c.species_1,
c.species_per_1,
c.species_2,
c.species_per_2,
c.species_3,
c.species_per_3,
c.species_4,
c.species_per_4,
c.species_5,
c.species_per_5,
c.species_6,
c.species_per_6,
c.species_7,
c.species_per_7,
c.species_8,
c.species_per_8,
c.species_9,
c.species_per_9,
c.species_10,
c.species_per_10,
c.origin_upper,
c.origin_lower,
c.site_class,
c.site_index,
--c.cas_id,
--c.crown_closure_lower,
--c.crown_closure_upper,
--c.height_lower,
--c.height_upper,
--c.layer,
--c.layer_rank,
--c.origin_lower,
--c.origin_upper,
--c.productivity,
--c.productivity_type,
--c.site_class,
--c.site_index,
--c.soil_moist_reg,
--c.species_1,
--c.species_10,
--c.species_2,
--c.species_3,
--c.species_4,
--c.species_5,
--c.species_6,
--c.species_7,
--c.species_8,
--c.species_9,
--c.species_per_1,
--c.species_per_10,
--c.species_per_2,
--c.species_per_3,
--c.species_per_4,
--c.species_per_5,
--c.species_per_6,
--c.species_per_7,
--c.species_per_8,
--c.species_per_9,
--c.structure_per,
--c.structure_range,
 --c.poly_num,
 --c.ogc_fid,
n.ogc_fid,
n.fc_id,
n.landcov,
n.landpos,
n.typeclas,
n.densitycls,
n.moisture,
n.sp1,
n.sp1_per,
n.sp2,
n.sp2per,
n.sp3,
n.sp3per,
n.sp4,
n.sp4per,
n.height,
n.crownclos,
n.origin,
n.siteclass,
n.wetland,
n.ref_year,
n.landuse,
n.dis1code,
n.dis1ext,
n.dis1year,
n.mintypeclas,
n.minmoist,
n.minsp1,
n.minsp1per,
n.minsp2,
n.minsp2per,
n.minsp3,
n.minsp3per,
n.minsp4,
n.minsp4per,
n.minheight,
n.mincrownclos,
n.minorigin,
n.minsiteclass,
n.si_50,
n.src_filename,
n.inventory_id

FROM rawfri.nt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS invproj_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS fc_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'nt03%' AND layer = 2
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.fc_id:: INTEGER = c.fc_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;

--===============================================================================

--   SPECIES COMPARE

----- LAYER 1 -------------------------------

DROP TABLE  IF EXISTS casfri50_test.nt03_species_compare_l1;
CREATE TABLE casfri50_test.nt03_species_compare_l1 AS
SELECT
cas_id,
layer,
species_1,
sp1,
minsp1,
species_2,
sp2,
minsp2,
species_3,
sp3,
minsp3,
species_4,
sp4,
minsp4,
species_per_1,
sp1_per,
minsp1per,
species_per_2,
sp2per,
minsp2per,
species_per_3,
sp3per,
minsp3per,
species_per_4,
sp4per,
minsp4per
FROM casfri50_test.nt03_rawfri_cas_l1;


-----LAYER 2

DROP TABLE  IF EXISTS casfri50_test.nt03_species_compare_l2;
CREATE TABLE casfri50_test.nt03_species_compare_l2 AS
SELECT
cas_id,
layer,
species_1,
sp1,
minsp1,
species_2,
sp2,
minsp2,
species_3,
sp3,
minsp3,
species_4,
sp4,
minsp4,
species_per_1,
sp1_per,
minsp1per,
species_per_2,
sp2per,
minsp2per,
species_per_3,
sp3per,
minsp3per,
species_per_4,
sp4per,
minsp4per
FROM casfri50_test.nt03_rawfri_cas_l2;

------ INVESTIGATION ------------------------------------
--- CHECK L1 AND L2 SPECIES

DROP TABLE IF EXISTS casfri50_test.nt03_species_compare_l1_CASE;
CREATE TABLE casfri50_test.nt03_species_compare_l1_CASE AS
SELECT 
	cas_id,
	species_1,
	species_per_1,
	sp1, ------ CHANGE THIS
	sp1_per, ------ CHANGE THIS
	species_2,
	species_per_2,
	sp2, 
	sp2per, 
	CASE WHEN species_per_1 = (sp1_per*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	CASE WHEN species_per_1 = (minsp1per*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_2, ------- CHANGE THIS
	CASE WHEN species_per_2 = (sp2per*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_3, ------- CHANGE THIS   
	CASE WHEN species_per_2 = (minsp2per*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_4 ------- CHANGE THIS
FROM casfri50_test.nt03_species_compare_l1;

--- layer 2

DROP TABLE IF EXISTS casfri50_test.nt03_species_compare_l2_CASE;
CREATE TABLE casfri50_test.nt03_species_compare_l2_CASE AS
SELECT 
	cas_id,
	species_1,
	species_per_1,
	sp1, ------ CHANGE THIS
	sp1_per, ------ CHANGE THIS
	minsp1,
	minsp1per,
	species_2,
	species_per_2,
	sp2, 
	sp2per, 
	minsp2,
	minsp2per,
	CASE WHEN species_per_1 = (sp1_per*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	CASE WHEN species_per_1 = (minsp1per*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_2, ------- CHANGE THIS
	CASE WHEN species_per_2 = (sp2per*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_3, ------- CHANGE THIS   
	CASE WHEN species_per_2 = (minsp2per*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_4 ------- CHANGE THIS
FROM casfri50_test.nt03_species_compare_l2;

--SELECT DISTINCT (species_1),species_per_1,sp1,sp1_per,minsp1,minsp1per FROM casfri50_test.nt03_rawfri_cas_l1;

--SELECT DISTINCT (species_1,species_per_1,sp1,sp1per,minisp1,minsp1per) FROM casfri50_test.nt03_rawfri_cas_l2;


---- HEIGHT COMPARISON

DROP TABLE IF EXISTS casfri50_test.nt03_Height_upper_compare_L1_l2;
CREATE TABLE casfri50_test.nt03_Height_upper_compare_L1_l2 AS
WITH a AS(SELECT cas_id, height_upper, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'nt03%' AND layer = 1),
b AS(SELECT cas_id, height_upper, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'nt03%' AND layer = 2)
SELECT a.cas_id, a.height_upper l1_height_upper, b.height_upper l2_height_upper FROM a
LEFT JOIN b
ON a.cas_id = b.cas_id
WHERE a.height_upper < b.height_upper;  -- top layer height is gt that low layer


-- comparing height_lower for l1 and l2

DROP TABLE IF EXISTS casfri50_test.nt03_Height_lower_compare_L1_l2;
CREATE TABLE casfri50_test.nt03_Height_lower_compare_L1_l2 AS
WITH a AS(SELECT cas_id, height_lower, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'nt03%' AND layer = 1),
b AS(SELECT cas_id, height_lower, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'nt03%' AND layer = 2)
SELECT a.cas_id, a.height_lower l1_height_lower, b.height_lower l2_height_lower FROM a
LEFT JOIN b
ON a.cas_id = b.cas_id
WHERE a.height_lower < b.height_lower;  -- top layer height lower is gt that l2 height lower



-------- CROW CLOSURE COMPARISON


------- Crown closure -------------------

--- This code which is working in earlier release but not working now. This is becasue CASFRI layer is now reordered based on 
-- the height and does not corresponds to original inventories layer number. Once the layer in the original inventory can be 
-- implemented same contraint as implemented in the height_bc function of CASFRI then the code will work for this comparison.
----

DROP TABLE IF EXISTS casfri50_test.nt03_crown_closure_upper_compare_L1_l2;
CREATE TABLE casfri50_test.nt03_crown_closure_upper_compare_L1_l2 AS
WITH a AS(SELECT cas_id, crown_closure_upper, layer FROM casfri50.lyr_all WHERE cas_id = 'nt03%' AND layer = 1),
b AS(SELECT cas_id, crown_closure_upper, layer FROM casfri50.lyr_all WHERE cas_id = 'nt03%' AND layer = 2)
SELECT a.cas_id, a.crown_closure_upper l1_crown_closure_upper, b.crown_closure_upper l2_crown_closure_upper FROM a
LEFT JOIN b
ON a.cas_id = b.cas_id
WHERE a.crown_closure_upper < b.crown_closure_upper;  -- top layer height is gt that low layer count : 0


-- comparing crown_closure_lower for l1 and l2
DROP TABLE IF EXISTS casfri50_test.nt03_crown_closure_lower_compare_L1_l2;
CREATE TABLE casfri50_test.nt03_crown_closure_lower_compare_L1_l2 AS
WITH a AS(SELECT cas_id, crown_closure_lower, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'nt03%' AND layer = 1),
b AS(SELECT cas_id, crown_closure_lower, layer FROM casfri50.lyr_all WHERE cas_id ILIKE 'nt03%' AND layer = 2)
SELECT a.cas_id, a.crown_closure_lower l1_crown_closure_lower, b.crown_closure_lower l2_crown_closure_lower FROM a
LEFT JOIN b
ON a.cas_id = b.cas_id
WHERE a.crown_closure_lower < b.crown_closure_lower;  -- top layer crown_closure lower is gt that l2 crown_closure lower



/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/



/* DST INVESTIGATION  */

DROP TABLE  IF EXISTS casfri50_test.disturbance_nt03_rawfri_cas;
CREATE TABLE casfri50_test.disturbance_nt03_rawfri_cas AS
SELECT
c.cas_id,
c.layer,
c.dist_type_1,
c.dist_year_1,
n.ogc_fid,
n.fc_id,
n.mod1code,
n.mod1ext
--n.dis1year

FROM rawfri.nt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS invproj_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS fc_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.dst_all WHERE cas_id ILIKE 'nt03%' 
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.fc_id:: INTEGER = c.fc_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;



/*   ================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/



/* ECO INVESTIGATION  */

DROP TABLE  IF EXISTS casfri50_test.eco_nt03_rawfri_cas;
CREATE TABLE casfri50_test.eco_nt03_rawfri_cas AS
SELECT
c.cas_id,
c.wetland_type,
c.wet_veg_cover,
c.wet_landform_mod,
c.wet_local_mod,
c.eco_site,
--c.nat_non_veg,
--c.non_for_anth,
--c.non_for_veg,
n.ogc_fid,
n.fc_id,
n.siteclass,
n.wetland,
n.structur,
n.moisture,
n.landcov,
n.landpos,
n.typeclas,
n.mintypeclas

FROM rawfri.nt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS invproj_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS fc_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.eco_all WHERE cas_id ILIKE 'nt03%' 
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.fc_id:: INTEGER = c.fc_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;



/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/



/* NFL INVESTIGATION  */


DROP TABLE  IF EXISTS casfri50_test.nfl_nt03_rawfri_cas;
CREATE TABLE casfri50_test.nfl_nt03_rawfri_cas AS
SELECT
c.cas_id,
c.crown_closure_upper,
c.crown_closure_lower,
c.height_upper,
c.height_lower,
c.nat_non_veg,
c.non_for_anth,
c.non_for_veg,
n.ogc_fid,
n.fc_id,
n.landbase,
n.landpos,
n.nonveg,
n.lveg1,
n.lveg2,
n.lveg3,
n.anth,
n.wetform,
n.wetmod

FROM rawfri.nt03 n
JOIN 
(
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS invproj_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS fc_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.nfl_all WHERE cas_id ILIKE 'nt03%' 
) c
--ON n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER AND n.poly_num:: INTEGER = c.poly_id:: INTEGER;
ON n.fc_id:: INTEGER = c.fc_id:: INTEGER AND n.ogc_fid:: INTEGER = c.ogc_fid:: INTEGER;

----- NFL DEEP INVESTIGATION ---------------
DROP TABLE IF EXISTS casfri50_test.nt03_nfl_cas;
CREATE TABLE casfri50_test.nt03_nfl_cas AS
SELECT *
FROM casfri50.nfl_all WHERE cas_id ILIKE 'nt03%';



/*   ==================================================================================

     5.    STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 

    ==================================================================================
*/



/* STAND STRUCTURE INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.stand_structure_nt03;
CREATE TABLE casfri50_test.stand_structure_nt03 AS
SELECT stand_structure, count(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'NT03%' 
GROUP BY stand_structure;


/*NFL LAYER COUNT */

DROP TABLE IF EXISTS casfri50_test.nfl_layer_count_nt03;
CREATE TABLE casfri50_test.nfl_layer_count_nt03 AS
SELECT layer, count(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'NT03%' 
GROUP BY layer;