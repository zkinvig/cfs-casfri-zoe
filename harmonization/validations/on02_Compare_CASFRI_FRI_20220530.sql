/*
This is a working script to evaluate and validate CASFRI translation of various FRI. Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: April 04, 2022 by Kangakola
Last Modified Date: May 30, 2022 by Kangakola




=============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate  the layer 1 and layer 2 and several attributes (Species, Height, Crown closure, Origin & Productivity) of layer 1 and layer 2 in CASFRI translated data. 
	   Then assess the accuracy of these attributes between CASFRI and  FRI of Juridiction forest inventory datata.
	*** Update: Integration of crownclosurecompare, heightcompare and siteclasscompare code. Also, add code for validation of dst, eco and nfl tables.	  
	*** Output: Multiple  tables and Views  of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for ON02 for both layer 1 & 2. Relevant table are assessed visually to report any the relationship 
	     between this attributes for layer 1 and layer 2. Also doing some testing and problem solving as needed for the validation processes.
	*** Software: PgAdmin 4.6
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
 ================================================================================================================================================================

*/


/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/



/* LYR INVESTIGATION */

--- Species compare ---------------------



------ EVALUATING upper and lower Species composition of layer 1 and layer 2 ----------------------------------------
DROP TABLE IF EXISTS casfri50_test.ON2_sp_TEST_TL;
DROP TABLE IF EXISTS casfri50_test.ON2_sp_TEST_LL;
DROP TABLE IF EXISTS casfri50_test.ON2_sp_TEST_CAS;
DROP TABLE IF EXISTS casfri50_test.ON2_sp_TEST_FRI_CAS; 


---------- Top Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_sp_TEST_TL AS
SELECT 
	cas_id, 
	species_1 as Spps_1_TL, --- Value to be declared 
	species_per_1 as SpPer_1_TL, --- Value to be declared
	species_2 as Spps_2_TL, --- Value to be declared 
	species_per_2 as SpPer_2_TL --- Value to be declared 	
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_sp_TEST_LL AS
SELECT 
	cas_id as cas_id_L2, 
	species_1 as Spps_1_LL, --- Value to be declared 
	species_per_1 as SpPer_1_LL,  --- Value to be declared
	species_2 as Spps_2_LL, --- Value to be declared 
	species_per_2 as SpPer_2_LL  --- Value to be declared 	
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
CREATE TABLE casfri50_test.ON2_sp_TEST_CAS AS
SELECT U.cas_id, 
	U.Spps_1_TL, 
	U.SpPer_1_TL, 
	U.Spps_2_TL, 
	U.SpPer_2_TL,
	L.Spps_1_LL,
	L.SpPer_1_LL,
	L.Spps_2_LL,
	L.SpPer_2_LL
FROM casfri50_test.ON2_sp_TEST_TL U 
LEFT JOIN 
(SELECT 
	cas_id_l2, 
	Spps_1_LL, 
	SpPer_1_LL,
    Spps_2_LL,
	SpPer_2_LL
FROM casfri50_test.ON2_sp_TEST_LL) L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------

CREATE TABLE casfri50_test.ON2_sp_TEST_FRI_CAS AS
SELECT 
	c.cas_id, 
	n.src_filename, 
	n.polyid,
	n.ogc_fid,	
	c.Spps_1_TL, 	 
	c.SpPer_1_TL,
	c.Spps_2_TL, 	 
	c.SpPer_2_TL,	
	c.Spps_1_LL, 
	c.SpPer_1_LL,
	c.Spps_2_LL, 
	c.SpPer_2_LL, 	
	n.ospcomp, --- Value to be declared 
	n.oleadspc --- Value to be declared
FROM casfri50_test.ON2_sp_TEST_CAS c, rawfri.on02 n
WHERE concat(ltrim(substr(c.cas_id, 22, 10), 'x'), ltrim(substr(c.cas_id, 33, 10), 'x')) = n.polyid
AND ltrim(substr(c.cas_id,44, 10), 'x')::INTEGER = n.ogc_fid;

---------- Counting numbers ----------------
--------------------------------------------------------------


SELECT COUNT(DISTINCT(Spps_1_tl)) FROM casfri50_test.On2_sp_test_fri_CAS WHERE oleadspc ='Sb';  --7

SELECT COUNT(*) FROM casfri50_test.ON2_sp_TEST_TL;  ------ 4024412
SELECT COUNT(*) FROM casfri50_test.ON2_sp_TEST_LL;  ------ 284837
SELECT COUNT(*) FROM casfri50_test.ON2_sp_TEST_CAS; ------ 4024412
SELECT COUNT(*) FROM rawfri.on02;  ------ 6760231
SELECT COUNT(*) FROM casfri50_test.ON2_sp_TEST_FRI_CAS;     ------ 4022309
SELECT * FROM casfri50_test.ON2_sp_TEST_FRI_CAS LIMIT 10;


------------ CREATEING VIEWS
DROP VIEW IF EXISTS casfri50_test.ON2_sp_TEST_FRI_CAS_OSPCOMP_EMPTY;
CREATE VIEW casfri50_test.ON2_sp_TEST_FRI_CAS_OSPCOMP_EMPTY AS
SELECT * FROM casfri50_test.ON2_sp_TEST_FRI_CAS WHERE ospcomp=''; --- 8420

--SELECT * FROM casfri50_test.ON2_sp_TEST_FRI_CAS WHERE oleadspc::INTEGER > 0 LIMIT 10;

DROP VIEW IF EXISTS casfri50_test.ON2_sp_TEST_FRI_CAS_OSPCOMP_NULL;
CREATE VIEW casfri50_test.ON2_sp_TEST_FRI_CAS_OSPCOMP_NULL AS
SELECT * FROM casfri50_test.ON2_sp_TEST_FRI_CAS WHERE ospcomp IS NULL;
--SELECT COUNT(*) FROM casfri50_test.ON2_sp_TEST_FRI_CAS WHERE ospcomp IS NULL; ---- 223492


DROP VIEW IF EXISTS casfri50_test.ON2_sp_TEST_FRI_CAS_SPSS_1_TL_LESS_SPSS_1_LL;
CREATE VIEW casfri50_test.ON2_sp_TEST_FRI_CAS_SPSS_1_TL_LESS_SPSS_1_LL AS
SELECT * FROM casfri50_test.ON2_sp_TEST_FRI_CAS WHERE Spps_1_TL<Spps_1_LL;
--SELECT COUNT(*) FROM casfri50_test.ON2_sp_TEST_FRI_CAS WHERE Spps_1_TL<Spps_1_LL; -------- 32428


--- Crown clouse compare



------ EVALUATING crown closure of layer 1 and layer 2 ----------------------------------------
DROP TABLE IF EXISTS casfri50_test.ON2_CcC_TEST_TL;
DROP TABLE IF EXISTS casfri50_test.ON2_CcC_TEST_LL;
DROP TABLE IF EXISTS casfri50_test.ON2_CcC_TEST_CAS;
DROP TABLE IF EXISTS casfri50_test.ON2_CcC_TEST_FRI_CAS; 



---------- Top Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_CcC_TEST_TL AS
SELECT 
	cas_id, 
	crown_closure_upper as CCU_TL, --- Value to be declared 
	crown_closure_lower as CCL_TL --- Value to be declared 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_CcC_TEST_LL AS
SELECT 
	cas_id as cas_id_L2, 
	crown_closure_upper as CCU_LL, --- Value to be declared 
	crown_closure_lower as CCL_LL  --- Value to be declared 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
CREATE TABLE casfri50_test.ON2_CcC_TEST_CAS AS
SELECT U.cas_id, 
	U.CCU_TL, 
	U.CCL_TL, 
	L.CCU_LL,
	L.CCL_LL 
FROM casfri50_test.ON2_CcC_TEST_TL U 
LEFT JOIN 
(SELECT 
	cas_id_l2, 
	CCU_LL, 
	CCL_LL 
FROM casfri50_test.ON2_CcC_TEST_LL) L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------

CREATE TABLE casfri50_test.ON2_CcC_TEST_FRI_CAS AS
SELECT 
	c.cas_id, 
	n.src_filename, 
	n.polyid,
	n.ogc_fid,	
	c.CCU_TL, 	 
	c.CCL_TL, 
	c.CCU_LL, 
	c.CCL_LL, 
	n.occlo, --- Value to be declared 
	n.ucclo --- Value to be declared 
FROM rawfri.on02 n
JOIN 
	(select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename, 
	--ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	--CONCAT(ltrim(split_part(cas_id, '-', 3), 'x'),ltrim(split_part(cas_id, '-', 4), 'x')) AS poly_id,
	ltrim(split_part(cas_id, '-', 3), 'x') As poly_id1,  -- first 10 digit of fri polyid
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id2, -- second 10 digit of fri polyid
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid 
FROM casfri50_test.ON2_CcC_TEST_CAS) c

ON n.src_filename=c.src_filename AND n.polyid_1_10=c.poly_id1 AND n.polyid_11_20=c.poly_id2 AND n.ogc_fid::text=c.ogc_fid;

------------- problem linking cas and fri -------------- case ----
DROP TABLE IF EXISTS casfri50_test.ON2_CcC_TEST_FRI_CAS_solved_f; 

DROP TABLE IF EXISTS casfri50_test.on2_ccc_test_rawrfri_cast_prob_join;
CREATE TABLE casfri50_test.on2_ccc_test_rawrfri_cast_prob_join AS
SELECT c.cas_id, 
	   n.src_filename, 
	   n.polyid,
	   n.ogc_fid,	
	   c.CCU_TL, 	 
	   c.CCL_TL, 
	   c.CCU_LL, 
	   c.CCL_LL, 
	   n.occlo, --- Value to be declared 
	   n.ucclo 
FROM casfri50_test.ON2_CcC_TEST_CAS c, rawfri.on02 n
WHERE concat(ltrim(substr(c.cas_id, 22, 10), 'x'), ltrim(substr(c.cas_id, 33, 10), 'x')) = n.polyid
AND ltrim(substr(c.cas_id,44, 10), 'x')::INTEGER = n.ogc_fid;


--ON n.src_filename=c.src_filename AND n.polyid_1_10::text=c.poly_id_1 AND n.polyid_11_20::text=c.poly_id_2 AND n.ogc_fid::text=c.ogc_fid;
--ON n.polyid=c.poly_id AND n.ogc_fid::text=c.ogc_fid;
--ON n.src_filename=c.src_filename AND n.polyid=c.poly_id; -- 30806
--ON n.polyid=c.poly_id; -- 282683
--ON n.polyid!=c.poly_id; 
ON n.src_filename=c.src_filename AND n.polyid_1_10=c.poly_id_1 AND n.polyid_11_20=c.poly_id_2;


DROP TABLE IF EXISTS casfri50_test.on2_ccc_test_rawrfri_cast_prob_join;
CREATE TABLE casfri50_test.on2_ccc_test_rawrfri_cast_prob_join AS
SELECT *
FROM casfri50_test.ON2_CcC_TEST_CAS test, rawfri.on02 raw
WHERE concat(ltrim(substr(test.cas_id, 22, 10), 'x'), ltrim(substr(test.cas_id, 33, 10), 'x')) = raw.polyid
AND ltrim(substr(test.cas_id,44, 10), 'x')::INTEGER = raw.ogc_fid;  -- 4022309


/*  RESOLVING DIFFERENT PROBLEM ECONTERED IN THE PROCESSING */


-------- PROBLEM SOLVING CAS_ID ------------------------------------

DROP TABLE IF EXISTS casfri50_test.on2_casID_problem;
CREATE TABLE casfri50_test.on2_casID_problem AS
SELECT cas_id, regexp_replace(cas_id,'(--)(.*?)(\1)','\1\2_') AS cas_id_junk, split_part(cas_id, '-', 3) AS poly_id_1, split_part(cas_id, '-', 4) AS poly_id_2
FROM casfri50_test.ON2_CcC_TEST_CAS;

DROP TABLE IF EXISTS casfri50_test.on2_casID_problem_substr_1;
CREATE TABLE casfri50_test.on2_casID_problem_substr_1 AS
SELECT
  cas_id,
  substr(cas_id, 22, 10) AS poly_id_1,
  ltrim(substr(cas_id, 33, 10),'x') AS poly_id_2
  --ltrim(poly_id_2,'x') AS poly_id_3,
  --CONCAT(poly_id_1,poly_id_3) AS poly_id_4
  --CONCAT (poly_id_1, poly_id_2) AS poly_id_concated
FROM casfri50_test.on2_casID_problem_substr;

DROP TABLE IF EXISTS casfri50_test.on2_ccc_test_rawrfri_cast_prob_join;
CREATE TABLE casfri50_test.on2_ccc_test_rawrfri_cast_prob_join AS
SELECT *
FROM casfri50_test.ON2_CcC_TEST_CAS test, rawfri.on02 raw
WHERE concat(ltrim(substr(test.cas_id, 22, 10), 'x'), ltrim(substr(test.cas_id, 33, 10), 'x')) = raw.polyid;


SELECT *
FROM casfri50_test.ON2_CcC_TEST_CAS test, rawfri.on02 raw
WHERE concat(ltrim(substr(test.cas_id, 22, 10), 'x'), ltrim(substr(test.cas_id, 33, 10), 'x')) = raw.polyid
AND ltrim(substr(test.cas_id,44, 10), 'x')::INTEGER = raw.ogc_fid

------------------------ ECO --- LAYER.
DROP TABLE IF EXISTS casfri50_test.on2_eco_test;
CREATE TABLE casfri50_test.on2_eco_test AS
SELECT * FROM casfri50.eco_all WHERE cas_id ILIKE 'ON02%';


-- CHECK FOR DUPLICATED CAS_ID

SELECT cas_id, COUNT(*)
FROM casfri50_test.on2_ccc_test_cas
GROUP BY cas_id
HAVING COUNT(*) > 1;  -- no duplicated 


--- Height compare


------ EVALUATING upper and lower height of layer 1 and layer 2 ----------------------------------------
DROP TABLE IF EXISTS casfri50_test.ON2_HC_TEST_TL;
DROP TABLE IF EXISTS casfri50_test.ON2_HC_TEST_LL;
DROP TABLE IF EXISTS casfri50_test.ON2_HC_CAS;
DROP TABLE IF EXISTS casfri50_test.ON2_HC_FRI_CAS; 


---------- Top Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_HC_TEST_TL AS
SELECT 
	cas_id, 
	height_upper as HU_TL, 
	height_lower as HL_TL 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_HC_TEST_LL AS
SELECT 
	cas_id as cas_id_L2, 
	height_upper as HU_LL, 
	height_lower as HL_LL 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
CREATE TABLE casfri50_test.ON2_HC_CAS AS
SELECT U.cas_id, 
	U.HU_TL, 
	U.HL_TL, 
	L.HU_LL,
	L.HL_LL 
FROM casfri50_test.ON2_HC_TEST_TL U 
LEFT JOIN 
(SELECT 
	cas_id_l2, 
	HU_LL, 
	HL_LL 
FROM casfri50_test.ON2_HC_TEST_LL) L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------
DROP TABLE IF EXISTS casfri50_test.ON2_HC_FRI_CAS;
CREATE TABLE casfri50_test.ON2_HC_FRI_CAS AS
SELECT 
	c.cas_id, 
	n.src_filename, 
	n.polyid,
	n.ogc_fid,	
	c.HU_TL, 	 
	c.HL_TL, 
	c.HU_LL, 
	c.HL_LL, 
	n.oht, 
	n.uht 
FROM casfri50_test.ON2_HC_CAS c, rawfri.on02 n
WHERE concat(ltrim(substr(c.cas_id, 22, 10), 'x'), ltrim(substr(c.cas_id, 33, 10), 'x')) = n.polyid
AND ltrim(substr(c.cas_id,44, 10), 'x')::INTEGER = n.ogc_fid;


---------- Counting numbers ----------------


SELECT COUNT(*) FROM casfri50_test.ON2_HC_TEST_TL;  ------ 3635675 : 4024412
SELECT COUNT(*) FROM casfri50_test.ON2_HC_TEST_LL;  ------ 284837 : 284837
SELECT COUNT(*) FROM casfri50_test.ON2_HC_CAS; ------ 3635675 : 4024412
SELECT COUNT(*) FROM rawfri.on02;  ------ 6760231
SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS;     ------ 3152975 : 4022309

DROP VIEW IF EXISTS casfri50_test.ON2_HC_FRI_CAS_oht_null;
CREATE VIEW casfri50_test.ON2_HC_FRI_CAS_oht_null AS
--SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE oht IS NULL; ---- 98003
SELECT * FROM casfri50_test.ON2_HC_FRI_CAS WHERE oht IS NULL; 

DROP VIEW IF EXISTS casfri50_test.ON2_HC_FRI_CAS_oht_null_not_zero;
CREATE VIEW casfri50_test.ON2_HC_FRI_CAS_oht_null_not_zero AS
--SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE oht IS NULL; ---- 98003
SELECT * FROM casfri50_test.ON2_HC_FRI_CAS WHERE oht = -9999  AND uht != 0;


DROP VIEW IF EXISTS casfri50_test.ON2_HC_FRI_CAS_hu_tl_less_hu_ll;
CREATE VIEW casfri50_test.ON2_HC_FRI_CAS_hu_tl_less_hu_ll AS
SELECT * FROM casfri50_test.ON2_HC_FRI_CAS WHERE HU_TL<HU_LL; -- 0


SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE oht = 0  AND uht != 0; -- 56
SELECT * FROM casfri50_test.ON2_HC_FRI_CAS WHERE oht = 0  AND uht != 0 AND hu_tl = -9999; --9
SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE HU_TL<HU_LL; -------- 262 : 0

SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE HU_TL!=oht; ------ 1111941 : 1430857
SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE HU_TL = oht; ----- 2493449
SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE HU_LL = oht; ---- 1130
SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE HU_TL = uht; -- 12008
  
SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE (HU_TL-oht) < 0.1; ------ 3143422 : 3664392 : 3923971

SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE HU_LL!=uht; ------ 51099 : 51815

SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE HU_LL<uht; -- 43628  : 44316
SELECT COUNT(*) FROM casfri50_test.ON2_HC_FRI_CAS WHERE HU_LL=uht; -- 217721 : 232971
  
------------------------------------------------------------------
-- Create issues tables
--------------------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.ON2_issue462_1a_height;
CREATE TABLE casfri50_test.ON2_issue462_1a_height AS
SELECT * FROM casfri50_test.ON2_HT_FRI_CAS WHERE oht = 0  AND uht != 0;

DROP TABLE IF EXISTS casfri50_test.ON2_issue462_1b_height;
CREATE TABLE casfri50_test.ON2_issue462_1b_height AS
SELECT * FROM casfri50_test.ON2_HT_FRI_CAS WHERE oht = 0  AND uht != 0 AND hu_tl = -9999;


DROP TABLE IF EXISTS casfri50_test.ON2_issue_fri_uht_gt0_less_1;
CREATE TABLE casfri50_test.ON2_issue_fri_uht_gt0_less_1 AS
SELECT * FROM rawfri.on02 WHERE uht > 0  AND uht < 1;   ---- 15111

------------------------------------------------------------------
--------------------------------------------------------------------

SELECT * FROM casfri50_test.ON2_HT_FRI_CAS WHERE HU_TL<HU_LL LIMIT 100; -- 0


---- Origin compare ------------------


------ EVALUATING upper and lower height of layer 1 and layer 2 ----------------------------------------
DROP TABLE IF EXISTS casfri50_test.ON2_OC_TEST_TL;
DROP TABLE IF EXISTS casfri50_test.ON2_OC_TEST_LL;
DROP TABLE IF EXISTS casfri50_test.ON2_OC_TEST_CAS;
DROP TABLE IF EXISTS casfri50_test.ON2_OC_TEST_FRI_CAS; 


---------- Top Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_OC_TEST_TL AS
SELECT 
	cas_id, 
	origin_upper as OU_TL,  --- Value to be declared 
	origin_lower as OL_TL   --- Value to be declared 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_OC_TEST_LL AS
SELECT 
	cas_id as cas_id_L2, 
	origin_upper as OU_LL, --- Value to be declared 
	origin_lower as OL_LL  --- Value to be declared 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
CREATE TABLE casfri50_test.ON2_OC_TEST_CAS AS
SELECT U.cas_id, 
	U.OU_TL, 
	U.OL_TL, 
	L.OU_LL,
	L.OL_LL 
FROM casfri50_test.ON2_OC_TEST_TL U 
LEFT JOIN 
(SELECT 
	cas_id_l2, 
	OU_LL, 
	OL_LL 
FROM casfri50_test.ON2_OC_TEST_LL) L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------

CREATE TABLE casfri50_test.ON2_OC_TEST_FRI_CAS AS
SELECT 
	c.cas_id, 
	n.src_filename, 
	n.polyid,
	n.ogc_fid,	
	c.OU_TL, 	 
	c.OL_TL, 
	c.OU_LL, 
	c.OL_LL, 
	n.oyrorg, --- Value to be declared 
	n.uyrorg --- Value to be declared 
FROM rawfri.on02 n
JOIN 
	(select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename, 
	--ltrim(split_part(cas_id, '-', 4), '0') AS poly_id, ---- to be changed based on jurisdiction
	CONCAT(ltrim(split_part(cas_id, '-', 3), 'x'),ltrim(split_part(cas_id, '-', 4), 'x')) AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid 
FROM casfri50_test.ON2_OC_TEST_CAS) c

ON n.src_filename=c.src_filename AND n.polyid=c.poly_id AND n.ogc_fid::text=c.ogc_fid;

---------- Counting numbers ----------------
--------------------------------------------------------------

--SELECT COUNT(*) FROM casfri50_test.ON2_OC_TEST_TL;  ------ 3635675 : 4024412
--SELECT COUNT(*) FROM casfri50_test.ON2_OC_TEST_LL;  ------ 284837
--SELECT COUNT(*) FROM casfri50_test.ON2_OC_TEST_CAS; ------ 3635675 : 4024412
--SELECT COUNT(*) FROM rawfri.on02;  ------ 6760231
--SELECT COUNT(*) FROM casfri50_test.ON2_OC_TEST_FRI_CAS;     ------ 3152975  : 3762684

--SELECT * FROM casfri50_test.ON2_OC_TEST_FRI_CAS LIMIT 10;
SELECT * FROM casfri50_test.ON2_OC_TEST_FRI_CAS WHERE oyrorg is NULL AND uyrorg IS NOT NULL LIMIT 10;
/*SELECT COUNT(*) FROM casfri50_test.ON2_OC_TEST_FRI_CAS WHERE oyrorg is NULL AND uyrorg IS NOT NULL;  ---- 9522
SELECT DISTINCT (src_filename) FROM casfri50_test.ON2_OC_TEST_FRI_CAS WHERE oyrorg is NULL AND uyrorg IS NOT NULL;  -- FC060, FC140

------- Explore FC060
DROP TABLE IF EXISTS casfri50_test.ON_OC_TEST_TEMP_FC60;
CREATE TABLE casfri50_test.ON_OC_TEST_TEMP_FC60 AS SELECT * FROM rawfri.on02 WHERE src_filename ILIKE '%FC060%';
SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'casfri50_test.ON_OC_TEST_TEMP_FC60' AND table_schema = 'casfri50_test' ORDER BY COLUMN_NAME;
ALTER TABLE casfri50_test.ON_OC_TEST_TEMP_FC60 DROP COLUMN wkb_geometry;
SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC60;
SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC60 WHERE uyrorg>0 LIMIT 10;

SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC60 WHERE oht IS NULL AND uht IS NOT NULL;
SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC60 WHERE oht IS NULL AND uht>0;
SELECT COUNT(*) FROM casfri50_test.ON_OC_TEST_TEMP_FC60 WHERE oht IS NULL AND uht>0; --------- 6016

------- Explore FC140
DROP TABLE casfri50_test.ON_OC_TEST_TEMP_FC140;
CREATE TABLE casfri50_test.ON_OC_TEST_TEMP_FC140 AS SELECT * FROM rawfri.on02 WHERE src_filename ILIKE '%FC140%';
SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'casfri50_test.ON_OC_TEST_TEMP_FC140' AND table_schema = 'casfri50_test' ORDER BY COLUMN_NAME;
ALTER TABLE casfri50_test.ON_OC_TEST_TEMP_FC140 DROP COLUMN wkb_geometry;
SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC140;
SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC140 WHERE uyrorg>0 LIMIT 10;

------- Explore FC060 and FC140
DROP TABLE casfri50_test.ON_OC_TEST_TEMP_FC_60_140;
CREATE TABLE casfri50_test.ON_OC_TEST_TEMP_FC_60_140 AS SELECT * FROM rawfri.on02 WHERE src_filename ILIKE '%FC060%' OR src_filename ILIKE '%FC140%';
SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'casfri50_test.ON_OC_TEST_TEMP_FC_60_140' AND table_schema = 'casfri50_test' ORDER BY COLUMN_NAME;
ALTER TABLE casfri50_test.ON_OC_TEST_TEMP_FC_60_140 DROP COLUMN wkb_geometry;

SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC_60_140;
SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC_60_140 WHERE uyrorg>0 LIMIT 10;

SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC_60_140 WHERE oht IS NULL AND uht IS NOT NULL;
SELECT polyid, oht, uht, oyrorg, uyrorg FROM casfri50_test.ON_OC_TEST_TEMP_FC_60_140 WHERE oht IS NULL AND uht>0;
SELECT COUNT(*) FROM casfri50_test.ON_OC_TEST_TEMP_FC_60_140 WHERE oht IS NULL AND uht>0; --------- 7105

*/


----- Productivitry compare  --------------


------ EVALUATING upper and lower productivity of layer 1 and layer 2 ----------------------------------------
DROP TABLE IF EXISTS casfri50_test.ON2_PC_TEST_TL;
DROP TABLE IF EXISTS casfri50_test.ON2_PC_TEST_LL;
DROP TABLE IF EXISTS casfri50_test.ON2_PC_TEST_CAS;
DROP TABLE IF EXISTS casfri50_test.ON2_PC_TEST_FRI_CAS; 


---------- Top Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_PC_TEST_TL AS
SELECT 
	cas_id, 
	productivity as productive_for_TL --- Value to be declared 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------

CREATE TABLE casfri50_test.ON2_PC_TEST_LL AS
SELECT 
	cas_id as cas_id_L2, 
	productivity as productive_for_LL --- Value to be declared
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'ON02%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
CREATE TABLE casfri50_test.ON2_PC_TEST_CAS AS
SELECT U.cas_id, 
	U.productive_for_TL,
	L.productive_for_LL
FROM casfri50_test.ON2_PC_TEST_TL U 
LEFT JOIN 
(SELECT 
	cas_id_l2, 
	productive_for_LL	
FROM casfri50_test.ON2_PC_TEST_LL) L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------

CREATE TABLE casfri50_test.ON2_PC_TEST_FRI_CAS AS
SELECT 
	c.cas_id, 
	n.src_filename, 
	n.polyid,
	n.ogc_fid,	
	c.productive_for_TL,
	c.productive_for_LL,
	n.formod, --- Value to be declared 
	n.polytype --- Value to be declared
FROM rawfri.on02 n
JOIN 
	(select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename, 
	--ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	CONCAT(ltrim(split_part(cas_id, '-', 3), 'x'),ltrim(split_part(cas_id, '-', 4), 'x')) AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid 
FROM casfri50_test.ON2_PC_TEST_CAS) c

ON n.src_filename=c.src_filename AND n.polyid=c.poly_id AND n.ogc_fid::text=c.ogc_fid;



---------- Counting numbers ----------------
--------------------------------------------------------------

SELECT DIStinct(formod) FROM rawfri.on02; --------- PF, , RP, MR, , (returned 6 rows)

SELECT DIStinct(polytype) FROM casfri50_test.ON2_PC_TEST_FRI_CAS; ----- FOR
SELECT DIStinct(formod) FROM casfri50_test.ON2_PC_TEST_FRI_CAS; ---- PF, RP, MR



SELECT COUNT(DISTINCT(productive_for_TL, productive_for_LL)) FROM casfri50_test.ON2_PC_TEST_FRI_CAS WHERE formod ='PF';--2
SELECT DIStinct(polytype) FROM rawfri.on02; --------- BSH, GRS, FOR, DAL, BFL, OMS, UCL, WAT, RCK, ISL, TMS
                                            ------[RCK, OMS, BSH, TMS, DAL, GRS, ISL, BFL, UCL, FOR, WAT]

SELECT DIStinct(productivity) FROM casfri50.lyr_all;  ---------- NOT_APPLICABLE, PRODUCTIVE_FOREST, POTENTIALLY_PRODUCTIVE
SELECT DIStinct(productivity) FROM casfri50.lyr_all WHERE cas_id ILIKE 'ON02%'; -- PRODUCTIVE_FOREST, NON_PRODUCTIVE_FOREST
SELECT DIStinct(productive_for_tl) FROM casfri50_test.ON2_PC_TEST_TL; -------- PRODUCTIVE_FOREST (it seems all rows for Ontario has defined forest productivity)
                                                                      

SELECT DIStinct(productive_for_ll) FROM casfri50_test.ON2_PC_TEST_LL;

SELECT * FROM casfri50_test.ON2_PC_TEST_TL WHERE productive_for_tl IS NULL;


SELECT src_filename,  polyid, formod, polytype FROM rawfri.on02  WHERE polytype = ' '; ----- 1 row: 1 row Park_Qhelico, 36013
SELECT * FROM casfri50_test.ON2_PC_TEST_FRI_CAS  WHERE polytype = ' '; ----- 1 row: 0 row
SELECT * FROM casfri50_test.ON2_PC_TEST_FRI_CAS  WHERE productive_for_tl ILIKE '%POTENTIALLY_PRODUCTIVEC%'; -- 0 row

SELECT COUNT(*) FROM casfri50_test.ON2_PC_TEST_TL;  ------ 3635675 : 4024412
SELECT COUNT(*) FROM casfri50_test.ON2_PC_TEST_LL;  ------ 284837

SELECT COUNT(*) FROM casfri50_test.ON2_PC_TEST_CAS; ------ 3635675 : 4024412
SELECT COUNT(*) FROM rawfri.on02;  ------ 6760231
SELECT COUNT(*) FROM casfri50_test.ON2_PC_TEST_FRI_CAS;     ------ 3152975: 3762684

SELECT * FROM casfri50_test.ON2_PC_TEST_FRI_CAS LIMIT 10;
SELECT * FROM casfri50_test.ON2_PC_TEST_FRI_CAS WHERE formod=0 LIMIT 10;

SELECT * FROM casfri50_test.ON2_PC_TEST_FRI_CAS WHERE formod IS NOT NULL LIMIT 10;
SELECT * FROM casfri50_test.ON2_PC_TEST_FRI_CAS WHERE formod IS NULL LIMIT 10;

SELECT cas_id, layer, productive_for FROM casfri50.lyr_all WHERE cas_id ILIKE 'ON02-xxxxxxxxxxFC175-xxxxxxxxxx-0000023283-0972877';

SELECT * FROM casfri50_test.ON2_PC_TEST_FRI_CAS WHERE polytype IS NULL LIMIT 10;
SELECT * FROM rawfri.on02 WHERE polytype IS NULL LIMIT 10;

SELECT * FROM casfri50_test.ON2_PC_TEST_FRI_CAS WHERE polytype>0 LIMIT 10;


SELECT COUNT(*) FROM casfri50_test.ON2_PC_TEST_FRI_CAS WHERE formod IS NULL; ---- 9553

SELECT COUNT(*) FROM casfri50_test.ON2_PC_TEST_FRI_CAS WHERE productive_for_TL<productive_for_LL; -------- 99683


/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/


/* DST INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.disturbance_ON2_CAS;
CREATE TABLE casfri50_test.disturbance_ON2_CAS AS
SELECT *
FROM casfri50.dst_all 
WHERE cas_id ILIKE 'ON02%';

DROP TABLE IF EXISTS casfri50_test.disturbance_ON2_FRI_CAS;
CREATE TABLE casfri50_test.disturbance_ON2_FRI_CAS AS
SELECT 
	c.cas_id, 
	c.dist_type_1,
	c.dist_year_1,
	n.devstage, 
	n.yrdep,
	n.deptype,
	n.oyrorg,
	n.src_filename, 
	n.polyid,
	n.ogc_fid	
	
FROM casfri50_test.disturbance_ON2_CAS c, rawfri.on02 n
WHERE concat(ltrim(substr(c.cas_id, 22, 10), 'x'), ltrim(substr(c.cas_id, 33, 10), 'x')) = n.polyid
AND ltrim(substr(c.cas_id,44, 10), 'x')::INTEGER = n.ogc_fid;


/*************************** NOTE *******************************************************************/

/* AS PER  VALIDATION LEVEL 2 THE ECO TABLE DON'T EXIST IN THE ONTATION FRI */

/*   ================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/



/*  ECO INVESTIGATION   */

DROP TABLE IF EXISTS casfri50_test.eco_ON2_CAS;
CREATE TABLE casfri50_test.eco_ON2_CAS AS
SELECT 
	cas_id, 
	wetland_type,
	wet_veg_cover,
	wet_landform_mod,
	wet_local_mod,
	eco_site
		
FROM casfri50.eco_all WHERE cas_id ILIKE 'ON02%';

--------- ECO FRI CAS

DROP TABLE IF EXISTS casfri50_test.eco_ON2_FRI_CAS;
CREATE TABLE casfri50_test.eco_ON2_FRI_CAS AS
SELECT 
	cas_id, 
	wetland_type,
	wet_veg_cover,
	wet_landform_mod,
	wet_local_mod,
	eco_site,
	n.formod, 
	n.polytype,
	n.deptype,
	n.oyrorg,
	n.src_filename, 
	n.polyid,
	n.ogc_fid	
	
FROM casfri50_test.eco_ON2_CAS c, rawfri.on02 n
WHERE concat(ltrim(substr(c.cas_id, 22, 10), 'x'), ltrim(substr(c.cas_id, 33, 10), 'x')) = n.polyid
AND ltrim(substr(c.cas_id,44, 10), 'x')::INTEGER = n.ogc_fid;



/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/


/* NFL INVESTIGATION */


DROP TABLE IF EXISTS casfri50_test.nfl_ON2_CAS;
CREATE TABLE casfri50_test.nfl_ON2_CAS AS
SELECT *
FROM casfri50.nfl_all 
WHERE cas_id ILIKE 'ON02%';


DROP TABLE IF EXISTS casfri50_test.nfl_ON2_FRI_CAS;
CREATE TABLE casfri50_test.nfl_ON2_FRI_CAS AS
SELECT 
	c.cas_id, 
	c.nat_non_veg,
	c.non_for_veg,
	c.non_for_anth,
	--c.dist_year_1,
	n.formod, 
	n.polytype,
	--n.deptype,
	--n.oyrorg,
	n.src_filename, 
	n.polyid,
	n.ogc_fid	
	
FROM casfri50_test.nfl_ON2_CAS c, rawfri.on02 n
WHERE concat(ltrim(substr(c.cas_id, 22, 10), 'x'), ltrim(substr(c.cas_id, 33, 10), 'x')) = n.polyid
AND ltrim(substr(c.cas_id,44, 10), 'x')::INTEGER = n.ogc_fid;

