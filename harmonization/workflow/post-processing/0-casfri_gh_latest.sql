CREATE SCHEMA IF NOT EXISTS casfri_post_processing;

-- Query geohistory for polygons with valid years between 1950 - 2030, union any duplicate polygons that share cas_id, valid_year_being, valid_year_end
-- Latest polys should have year end set to 2030. some inventories have poly not update for a long time so we retrieve 1950+
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_post_processing.casfri_2030 AS (
    SELECT gh.cas_id, gh.valid_year_begin, gh.valid_year_end, ST_Multi(ST_Union(gh.geom)) as geometry
    FROM casfri50_history.geo_history gh
    WHERE gh.valid_year_end = 2030
    GROUP BY gh.cas_id, gh.valid_year_begin, gh.valid_year_end
);

CREATE INDEX ON casfri_post_processing.casfri_2030 USING btree(cas_id);
CREATE INDEX ON casfri_post_processing.casfri_2030 USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_post_processing.casfri_2030 USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_post_processing.casfri_2030 USING gist(geometry);

COMMENT ON MATERIALIZED VIEW casfri_post_processing.casfri_2030 IS
'Materialized View of geohistory object with valid_end_year 2030 (most recent) to ensure complete national coverage for data products';
--ALTER TABLE casfri_post_processing.vi_unique_gh_years ADD CONSTRAINT vi_genus_polygons_pk PRIMARY KEY (cas_id);

CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_post_processing.casfri_latest_unique AS (
  SELECT DISTINCT ON (cas_id) *
  FROM casfri_post_processing.casfri_2030
  WHERE 4*pi()*ST_area(geometry)/power(ST_Perimeter(geometry), 2) > 0.1 --Remove geometries with sliver index less than 0.1 (which is approximately the 10th percentile)
  ORDER BY cas_id, valid_year_end desc, valid_year_begin desc
);

COMMENT ON MATERIALIZED VIEW casfri_post_processing.casfri_latest_unique IS
'Materialized view of only the most recent valid geohistory objects from the 2030 view, used to generate data product tables. Such that only 1 cas_id per geometry is present';

CREATE INDEX casfri_latest_unique_cas_id_idx ON casfri_post_processing.casfri_latest_unique USING btree(cas_id);
CREATE INDEX ON casfri_post_processing.casfri_latest_unique USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_post_processing.casfri_latest_unique USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_post_processing.casfri_latest_unique USING gist(geometry);

--CREATE TABLE casfri_post_processing.gh_2000_2023 as (
--SELECT *
--FROM casfri50_history.geo_history gh
--WHERE gh.valid_year_begin <= 2023 AND 2000 <= gh.valid_year_end
--);
--