--CREATE MATERIALIZED VIEW casfri_metrics.polygon_area_stats AS (
--    SELECT inventory_id, avg(casfri_area), min(casfri_area), max(casfri_area), (select percentile_cont(0.5) within group(order by casfri_area))
--    FROM casfri50.cas_all
--    group by inventory_id
--);
DROP MATERIALIZED VIEW IF EXISTS casfri_metrics.polygon_area_stats80;
CREATE MATERIALIZED VIEW casfri_metrics.polygon_area_stats80 AS (
    WITH percentile as (select inventory_id, percentile_cont(0.8) within group(order by casfri_area) as percentile8 from casfri_web.cas_latest group by inventory_id)

    SELECT l.inventory_id, avg(casfri_area), min(casfri_area), max(casfri_area), stddev_pop(casfri_area), var_pop(casfri_area)
    FROM casfri_web.cas_latest l, percentile
    where casfri_area <= percentile8
    group by l.inventory_id
);

DROP MATERIALIZED VIEW IF EXISTS casfri_metrics.polygon_area_stats20;
CREATE MATERIALIZED VIEW casfri_metrics.polygon_area_stats20 AS (
    WITH percentile as (select inventory_id, percentile_cont(0.2) within group(order by casfri_area) as percentile2 from casfri_web.cas_latest group by inventory_id)

    SELECT l.inventory_id, avg(casfri_area), min(casfri_area), max(casfri_area), stddev_pop(casfri_area), var_pop(casfri_area)
    FROM casfri_web.cas_latest l, percentile
    where casfri_area >= percentile2
    group by l.inventory_id
);

DROP MATERIALIZED VIEW IF EXISTS casfri_metrics.polygon_area_stats30;
CREATE MATERIALIZED VIEW casfri_metrics.polygon_area_stats30 AS (
    WITH percentile as (select inventory_id, percentile_cont(0.3) within group(order by casfri_area) as percentile3 from casfri_web.cas_latest group by inventory_id)

    SELECT l.inventory_id, avg(casfri_area), min(casfri_area), max(casfri_area), stddev_pop(casfri_area), var_pop(casfri_area)
    FROM casfri_web.cas_latest l, percentile
    where casfri_area >= percentile3
    group by l.inventory_id
);

DROP MATERIALIZED VIEW IF EXISTS casfri_metrics.polygon_area_stats40;
CREATE MATERIALIZED VIEW casfri_metrics.polygon_area_stats40 AS (
    WITH percentile as (select inventory_id, percentile_cont(0.4) within group(order by casfri_area) as percentile4 from casfri_web.cas_latest group by inventory_id)

    SELECT l.inventory_id, avg(casfri_area), min(casfri_area), max(casfri_area), stddev_pop(casfri_area), var_pop(casfri_area)
    FROM casfri_web.cas_latest l, percentile
    where casfri_area >= percentile4
    group by l.inventory_id
);

DROP MATERIALIZED VIEW IF EXISTS casfri_metrics.area_histogram_by_inv;
CREATE MATERIALIZED VIEW casfri_metrics.area_histogram_by_inv AS (
    SELECT
		width_bucket(l.casfri_area, p.min, p.max, 20) as bucket,
		int4range(min(casfri_area)::integer, max(casfri_area)::integer,'[]'),
		count(*) as freq,
		l.inventory_id
    from casfri_metrics.polygon_area_stats80 p, casfri_web.cas_latest l
	where p.inventory_id = l.inventory_id
	group by bucket, l.inventory_id
);