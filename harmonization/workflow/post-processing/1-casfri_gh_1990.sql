CREATE SCHEMA IF NOT EXISTS casfri_post_processing;

-- Query geohistory for polygons with valid years between 1950 - 1990, union any duplicate polygons that share cas_id, valid_year_being, valid_year_end
-- Latest polys should have year end set to 1990. some inventories have poly not update for a long time so we retrieve 1950+
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_post_processing.casfri_1990 AS (
    SELECT gh.cas_id, gh.valid_year_begin, gh.valid_year_end, ST_Multi(ST_Union(gh.geom)) as geometry
    FROM casfri50_history.geo_history gh
    WHERE gh.valid_year_begin <= 1990 AND 1990 <= gh.valid_year_end
    GROUP BY gh.cas_id, gh.valid_year_begin, gh.valid_year_end
);


CREATE INDEX ON casfri_post_processing.casfri_1990 USING btree(cas_id);
CREATE INDEX ON casfri_post_processing.casfri_1990 USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_post_processing.casfri_1990 USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_post_processing.casfri_1990 USING gist(geometry);

COMMENT ON MATERIALIZED VIEW casfri_post_processing.casfri_1990 IS
'Materialized View of geohistory object with valid_end_year 1990 to ensure complete national coverage for data products';
--ALTER TABLE casfri_post_processing.vi_unique_gh_years ADD CONSTRAINT vi_genus_polygons_pk PRIMARY KEY (cas_id);

CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_post_processing.casfri_1990_unique AS (
  SELECT DISTINCT ON (cas_id) *
  FROM casfri_post_processing.casfri_1990
  WHERE 4*pi()*ST_area(geometry)/power(ST_Perimeter(geometry), 2) > 0.1 --Remove geometries with sliver index less than 0.1 (which is approximately the 10th percentile)
  ORDER BY cas_id, valid_year_end desc, valid_year_begin desc
);

COMMENT ON MATERIALIZED VIEW casfri_post_processing.casfri_1990_unique IS
'Materialized view of only the 1990 cut of valid geohistory objects from the 1990 view, used to generate data product tables. Such that only 1 cas_id per geometry is present';

CREATE INDEX casfri_1990_unique_cas_id_idx ON casfri_post_processing.casfri_1990_unique USING btree(cas_id);
CREATE INDEX ON casfri_post_processing.casfri_1990_unique USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_post_processing.casfri_1990_unique USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_post_processing.casfri_1990_unique USING gist(geometry);

