CREATE TABLE casfri_web.vi_unique_gh_test AS
SELECT * FROM casfri_web.vi_forest_sp_genus WHERE geometry @ ST_MakeEnvelope(-2261537, 1289848, -1819130, 1795030, 102001);

COMMENT ON TABLE casfri_web.vi_unique_gh_test IS
'Table to test if gh unique query is properly de-duplicating gh records';

CREATE INDEX ON casfri_web.vi_unique_gh_test USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_web.vi_unique_gh_test USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_web.vi_unique_gh_test USING gist(geometry);

create table casfri_web.test_vi_dedupe as (
  select
    cas_id, ST_MULTI(ST_UNION(ST_MakeValid(geom))) as geometry
  from casfri_web.vi_forest_sp_genus
  group by cas_id
 );