CREATE TABLE casfri_post_processing.vi_gh_years as (
    SELECT DISTINCT ON (cas_id)
        f.cas_id, c.valid_year_end
    from casfri_web.vi_forest_sp_genus f, casfri_post_processing.casfri_latest_unique c
    ORDER BY cas_id, valid_year_end DESC
)