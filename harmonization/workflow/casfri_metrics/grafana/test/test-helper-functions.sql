-- Tests all functions in 0-metric-helper-functions.sql to ensure that the correct result is returned.
-- Run as script to see results of each test

DROP TABLE IF EXISTS helper_function_test_results;
CREATE TABLE IF NOT EXISTS helper_function_test_results (test TEXT, result TEXT);

----------------------------------------------------------------------------------

-- Tests isError() functions
WITH row_validity AS (
	SELECT isError('-8888') = true AS value
	UNION ALL
	SELECT isError('NOT_IN_SET') = true AS value
	UNION ALL
	SELECT isError('NULL_VALUE') = true AS value
	UNION ALL
	SELECT isError('WRONG_TYP') = false AS value
	UNION ALL
	SELECT isError('NOT_AN_ERROR') = false AS value
	UNION ALL
	SELECT isError('-1234') = false AS value
	UNION ALL
	SELECT isError(-9999) = true AS value
	UNION ALL
	SELECT isError(-3333) = true AS value
	UNION ALL
	SELECT isError(5678) = false AS value
	UNION ALL
	SELECT isError(-888) = false AS value
)
INSERT INTO helper_function_test_results SELECT 'isError' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

----------------------------------------------------------------------------------

-- Tests getDistinctValues function to ensure that the correct result is returned.
-- Expected behaviour of the function:
-- Produces a table with a column containing each column name in the table, and a column containing a list of each distinct value in the corresponding column
CREATE TABLE IF NOT EXISTS casfri_metrics.get_distinct_test_table (
	col1 int,
	col2 varchar(255),
	col3 int
);

INSERT INTO casfri_metrics.get_distinct_test_table VALUES 
(1, 'test', '15'),
(2, 'test', '17'),
(3, 'test2', '15');

WITH get_distinct_test_result AS (
	SELECT * 
	FROM casfri_metrics.getDistinctValues('casfri_metrics', 'get_distinct_test_table')
	ORDER BY table_column
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (table_column = 'col1' AND distinct_value = array['1', '2', '3'])
		OR   (table_column = 'col2' AND distinct_value = array['test', 'test2'])
		OR   (table_column = 'col3' AND distinct_value = array['15', '17'])
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM get_distinct_test_result
)
INSERT INTO helper_function_test_results  SELECT 'getDistinctValues' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are three distinct values for each column of the get_distinct_test_result
	AND (SELECT count(DISTINCT table_column) FROM get_distinct_test_result) = 3
	AND (SELECT count(DISTINCT distinct_value) FROM get_distinct_test_result) = 3
	-- Check that there are exactly 3 rows in the result
	AND (SELECT count(*) FROM get_distinct_test_result) = 3
	THEN 'Test passed' ELSE 'Test failed'
END) AS get_distinct_test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_metrics.get_distinct_test_table;

----------------------------------------------------------------------------------

-- Tests getListInvs()
CREATE TABLE IF NOT EXISTS casfri_metrics.hdr_all (
	inventory_id VARCHAR(255) 
);

INSERT INTO casfri_metrics.hdr_all VALUES 
('BC08'),
('BC09'),
('BC08'),
('BC10');

WITH get_list_invs_test_result AS (
	SELECT * 
	FROM casfri_metrics.getListInvs()
	ORDER BY inventory
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory IN ('BC08', 'BC10'))
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM get_list_invs_test_result
)
INSERT INTO helper_function_test_results  SELECT 'getListInvs' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are two distinct inventories
	AND (SELECT count(DISTINCT inventory) FROM get_list_invs_test_result) = 2
	-- Check that there are exactly 2 rows in the result
	AND (SELECT count(*) FROM get_list_invs_test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_metrics.hdr_all;

----------------------------------------------------------------------------------

-- Tests getDistExt(dist_ext_upper NUMERIC, dist_ext_lower numeric) function
WITH row_validity AS (
	SELECT getDistExt(100, 0) = 50 AS value
	UNION ALL
	SELECT getDistExt(90, -8888) = 90 AS value
	UNION ALL
	SELECT getDistExt(-3333, 40) = 40 AS value
	UNION ALL
	SELECT getDistExt(-9999, -8887) = 100 AS value
)
INSERT INTO helper_function_test_results SELECT 'getDistExt' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

----------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS casfri_metrics.attribute_counts_table_converter_test_table (
	inventory VARCHAR,
	cas_id_area_ha INT,
	cas_id_count INT,
	inventory_id_area_ha INT
);

INSERT INTO casfri_metrics.attribute_counts_table_converter_test_table VALUES 
('AB29', 12345, 00000, 67890),
('AB30', 45321, 00000, 56743),
('BC08', 11111, 00000, 86300);

SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'attribute_counts_table_converter_test_table', 'attribute_counts_table_converter_test_result_table', 'area_ha');

WITH row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (attribute_name = 'cas_id_area_ha' AND ab29_area_ha = 12345 AND ab30_area_ha = 45321 AND bc08_area_ha = 11111)
		OR   (attribute_name = 'inventory_id_area_ha' AND ab29_area_ha = 67890 AND ab30_area_ha = 56743 AND bc08_area_ha = 86300)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM attribute_counts_table_converter_test_result_table
)
INSERT INTO helper_function_test_results  SELECT 'attribute_counts_table_converter' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are three distinct values for each column of the attribute_counts_table_converter_test_result_table
	AND (SELECT count(DISTINCT attribute_name) FROM attribute_counts_table_converter_test_result_table) = 2
	AND (SELECT count(DISTINCT ab29_area_ha) FROM attribute_counts_table_converter_test_result_table) = 2
	AND (SELECT count(DISTINCT ab30_area_ha) FROM attribute_counts_table_converter_test_result_table) = 2
	AND (SELECT count(DISTINCT bc08_area_ha) FROM attribute_counts_table_converter_test_result_table) = 2
	-- Check that there are exactly 2 rows in the result
	AND (SELECT count(*) FROM attribute_counts_table_converter_test_result_table) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS attribute_counts_table_converter_test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS attribute_counts_table_converter_test_table;
DROP TABLE IF EXISTS attribute_counts_table_converter_test_result_table;

----------------------------------------------------------------------------------

-- Tests prettyDuration() function
WITH row_validity AS (
	SELECT prettyDuration(1234) = '0 hr, 20 min, 34 sec' AS value
	UNION ALL
	SELECT prettyDuration(23) = '0 hr, 0 min, 23 sec' AS value
	UNION ALL
	SELECT prettyDuration(61) = '0 hr, 1 min, 1 sec' AS value
	UNION ALL
	SELECT prettyDuration(456789) = '126 hr, 53 min, 9 sec' AS value
	UNION ALL
	SELECT prettyDuration(23456) = '6 hr, 30 min, 56 sec' AS value
	UNION ALL
	SELECT prettyDuration(98753) = '27 hr, 25 min, 53 sec' AS value
	UNION ALL
	SELECT prettyDuration(0) = '0 hr, 0 min, 0 sec' AS value
	UNION ALL
	SELECT prettyDuration(120) = '0 hr, 2 min, 0 sec' AS value
)
INSERT INTO helper_function_test_results SELECT 'prettyDuration' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;
	
----------------------------------------------------------------------------------

-- Tests getProgressMessage() function
WITH row_validity AS (
SELECT getProgressMessage(1, 1, now() - INTERVAL '1 hour') = '0 hr, 0 min, 0 sec remaining' AS value
	UNION ALL
	SELECT getProgressMessage(5, 64, now() - INTERVAL '1 hour') = '11 hr, 48 min, 0 sec remaining' AS value
	UNION ALL
	SELECT getProgressMessage(35, 1234, now() - INTERVAL '24 hours') = '822 hr, 10 min, 17 sec remaining' AS value
	UNION ALL
	SELECT getProgressMessage(1, 800, now() - INTERVAL '1 min') = '13 hr, 19 min, 1 sec remaining' AS value
	UNION ALL
	SELECT getProgressMessage(89, 90, now() - INTERVAL '20 min') = '0 hr, 0 min, 13 sec remaining' AS value
	UNION ALL
	SELECT getProgressMessage(23, 213, now() - INTERVAL '2 hours') = '16 hr, 31 min, 18 sec remaining' AS value
	UNION ALL
	SELECT getProgressMessage(214, 213, now() - INTERVAL '2 hours') = '0 hr, 0 min, -34 sec remaining' AS value
	UNION ALL
	SELECT getProgressMessage(1, 2) = '' AS value
)
INSERT INTO helper_function_test_results SELECT 'getProgressMessage' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;
	
----------------------------------------------------------------------------------

-- Tests countInstances() function
WITH row_validity AS (
	SELECT countInstances('test', 'other') = 0 AS value
	UNION ALL
	SELECT countInstances('test', 'te') = 1 AS value
	UNION ALL
	SELECT countInstances('testy', 'testy') = 1 AS value
	UNION ALL
	SELECT countInstances('hellohi', 'h') = 2 AS value
	UNION ALL
	SELECT countInstances('aaa', 'a') = 3 AS value
	UNION ALL
	SELECT countInstances('iI', 'I') = 1 AS value
	UNION ALL
	SELECT countInstances(', test row, -8887, ', ', -8887,') = 1 AS value
	UNION ALL
	SELECT countInstances('123412', '12') = 2 AS value
	UNION ALL
	SELECT countInstances('', 'hi') = 0 AS value
)
INSERT INTO helper_function_test_results SELECT 'countInstances' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;
	
----------------------------------------------------------------------------------

-- Display results
SELECT * FROM helper_function_test_results;
DROP TABLE IF EXISTS helper_function_test_results;
