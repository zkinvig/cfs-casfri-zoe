
-- LATEST CUT- Feature count and sum of areas in each table per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS features_per_inv_latest AS
WITH cas_area_sum AS (
	SELECT LEFT(cas_id, 4) AS inventory, count(cas_id) AS cas_all_count, sum(casfri_area) AS cas_all_area_ha
	FROM cas_latest
	GROUP BY inventory
), dst_area_sum AS (
	SELECT LEFT(d.cas_id, 4) AS inventory, count(d.cas_id) AS dst_all_count, sum(c.casfri_area) AS dst_all_area_ha
	FROM dst_latest d
	INNER JOIN cas_latest c ON d.cas_id = c.cas_id
	GROUP BY inventory
), eco_area_sum AS (
	SELECT LEFT(d.cas_id, 4) AS inventory, count(d.cas_id) AS eco_all_count, sum(c.casfri_area) AS eco_all_area_ha
	FROM eco_latest d
	INNER JOIN cas_latest c ON d.cas_id = c.cas_id
	GROUP BY inventory
), geo_area_sum AS (
	SELECT LEFT(d.cas_id, 4) AS inventory, count(d.cas_id) AS geo_all_count, sum(c.casfri_area) AS geo_all_area_ha
	FROM geo_latest d
	INNER JOIN cas_latest c ON d.cas_id = c.cas_id
	GROUP BY inventory
), lyr_area_sum AS (
	SELECT LEFT(d.cas_id, 4) AS inventory, count(d.cas_id) AS lyr_all_count, sum(c.casfri_area) AS lyr_all_area_ha
	FROM lyr_latest d
	INNER JOIN cas_latest c ON d.cas_id = c.cas_id
	GROUP BY inventory
), nfl_area_sum AS (
	SELECT LEFT(d.cas_id, 4) AS inventory, count(d.cas_id) AS nfl_all_count, sum(c.casfri_area) AS nfl_all_area_ha
	FROM nfl_latest d
	INNER JOIN cas_latest c ON d.cas_id = c.cas_id
	GROUP BY inventory
)
SELECT c.inventory, 
c.cas_all_count,
c.cas_all_area_ha,
d.dst_all_count,
d.dst_all_area_ha,
e.eco_all_count,
e.eco_all_area_ha,
g.geo_all_count,
g.geo_all_area_ha,
l.lyr_all_count,
l.lyr_all_area_ha,
n.nfl_all_count,
n.nfl_all_area_ha
FROM cas_area_sum c
FULL JOIN dst_area_sum d ON c.inventory = d.inventory
FULL JOIN eco_area_sum e ON c.inventory = e.inventory
FULL JOIN geo_area_sum g ON c.inventory = g.inventory
FULL JOIN lyr_area_sum l ON c.inventory = l.inventory
FULL JOIN nfl_area_sum n ON c.inventory = n.inventory;

COMMENT ON MATERIALIZED VIEW features_per_inv_latest IS
'features_per_inv_latest provides counts for features and area sums in each table per inventory for the latest cut';


-- LATEST CUT- Range of stand heights by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_heights_by_inventory_latest AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(height_lower::text) = false THEN height_lower ELSE NULL end) AS min_stand_height, 
max(CASE WHEN isError(height_upper::text) = false THEN height_upper ELSE NULL end) AS max_stand_height
FROM lyr_latest
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW stand_heights_by_inventory_latest IS
'stand_heights_by_inventory_latest provides the range of recorded stand heights for each inventory, for the latest cut';


-- LATEST CUT- Range of stand heights by jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_heights_by_jurisdiction_latest AS
SELECT left(inventory, 2) AS jurisdiction, 
min(min_stand_height) AS min_stand_height, 
max(max_stand_height) AS max_stand_height
FROM stand_heights_by_inventory_latest
GROUP BY jurisdiction;

COMMENT ON MATERIALIZED VIEW stand_heights_by_jurisdiction_latest IS
'stand_heights_by_jurisdiction_latest provides the range of recorded stand heights for each jurisdiction, for the latest cut';


-- LATEST CUT- Range of stand origins by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_origin_by_inventory_latest AS
SELECT LEFT(cas_id, 4) AS inventory, 
min(origin_lower) AS min_stand_origin, 
max(origin_upper) AS max_stand_origin
FROM lyr_latest
WHERE isError(origin_lower::text) = false AND isError(origin_upper::text) = false
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW stand_origin_by_inventory_latest IS
'stand_origin_by_inventory_latest provides the range of recorded stand origin years for each inventory, for the latest cut';


-- LATEST CUT- Range of stand origins by jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_origin_by_jurisdiction_latest AS
SELECT LEFT(inventory, 2) AS jurisdiction, 
min(min_stand_origin) AS min_stand_origin, 
max(max_stand_origin) AS max_stand_origin
FROM stand_origin_by_inventory_latest
GROUP BY jurisdiction;

COMMENT ON MATERIALIZED VIEW stand_origin_by_jurisdiction_latest IS
'stand_origin_by_jurisdiction provides the range of recorded stand origin years for each jurisdiction, for the latest cut';


-- LATEST CUT- Range of stand photo years by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_years_per_inv_latest AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(stand_photo_year::text) = false THEN stand_photo_year ELSE NULL end) AS min_stand_photo_year, 
max(CASE WHEN isError(stand_photo_year::text) = false THEN stand_photo_year ELSE NULL end) AS max_stand_photo_year
FROM cas_latest
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_years_per_inv_latest IS
'range_of_years_per_inv_latest provides the range of recorded stand photo years for each inventory, for the latest cut';


-- LATEST CUT- Range of stand photo years by jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_years_per_jurisdiction_latest AS
SELECT left(inventory, 2) AS jurisdiction, 
min(min_stand_photo_year) AS min_stand_photo_year, 
max(max_stand_photo_year) AS max_stand_photo_year
FROM range_of_years_per_inv_latest
GROUP BY jurisdiction;

COMMENT ON MATERIALIZED VIEW range_of_years_per_jurisdiction_latest IS
'range_of_years_per_jurisdiction_latest provides the range of recorded stand photo years for each jurisdiction, for the latest cut';


-- LATEST CUT- Species area and count per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_area_counts_per_inv_latest AS
WITH species_table AS (
	-- Filter table beforehand to optimize
	SELECT LEFT(l.cas_id, 4) AS inventory, species_1, species_per_1, species_2, species_per_2, species_3, species_per_3, species_4, species_per_4, species_5, species_per_5,
		   species_6, species_per_6, species_7, species_per_7, species_8, species_per_8, species_9, species_per_9, species_10, species_per_10, c.casfri_area AS area
	FROM lyr_latest l
	INNER JOIN cas_latest c ON l.cas_id = c.cas_id
), all_species AS (
	SELECT inventory, species_1 AS species, count(*) AS feature_count, sum(area*species_per_1/100) AS area_ha
	FROM species_table
	WHERE isError(species_1) = FALSE AND isError(species_per_1::text) = FALSE
	GROUP BY inventory, species_1
	UNION ALL
	SELECT inventory, species_2 AS species, count(*) AS feature_count, sum(area*species_per_2/100) AS area_ha
	FROM species_table
	WHERE isError(species_2) = FALSE AND isError(species_per_2::text) = FALSE
	GROUP BY inventory, species_2
	UNION ALL
	SELECT inventory, species_3 AS species, count(*) AS feature_count, sum(area*species_per_3/100) AS area_ha
	FROM species_table
	WHERE isError(species_3) = FALSE AND isError(species_per_3::text) = FALSE
	GROUP BY inventory, species_3
	UNION ALL
	SELECT inventory, species_4 AS species, count(*) AS feature_count, sum(area*species_per_4/100) AS area_ha
	FROM species_table
	WHERE isError(species_4) = FALSE AND isError(species_per_4::text) = FALSE
	GROUP BY inventory, species_4
	UNION ALL
	SELECT inventory, species_5 AS species, count(*) AS feature_count, sum(area*species_per_5/100) AS area_ha
	FROM species_table
	WHERE isError(species_5) = FALSE AND isError(species_per_5::text) = FALSE
	GROUP BY inventory, species_5
	UNION ALL
	SELECT inventory, species_6 AS species, count(*) AS feature_count, sum(area*species_per_6/100) AS area_ha
	FROM species_table
	WHERE isError(species_6) = FALSE AND isError(species_per_6::text) = FALSE
	GROUP BY inventory, species_6
	UNION ALL
	SELECT inventory, species_7 AS species, count(*) AS feature_count, sum(area*species_per_7/100) AS area_ha
	FROM species_table
	WHERE isError(species_7) = FALSE AND isError(species_per_7::text) = FALSE
	GROUP BY inventory, species_7
	UNION ALL
	SELECT inventory, species_8 AS species, count(*) AS feature_count, sum(area*species_per_8/100) AS area_ha
	FROM species_table
	WHERE isError(species_8) = FALSE AND isError(species_per_8::text) = FALSE
	GROUP BY inventory, species_8
	UNION ALL
	SELECT inventory, species_9 AS species, count(*) AS feature_count, sum(area*species_per_9/100) AS area_ha
	FROM species_table
	WHERE isError(species_9) = FALSE AND isError(species_per_9::text) = FALSE
	GROUP BY inventory, species_9
	UNION ALL
	SELECT inventory, species_10 AS species, count(*) AS feature_count, sum(area*species_per_10/100) AS area_ha
	FROM species_table
	WHERE isError(species_10) = FALSE AND isError(species_per_10::text) = FALSE
	GROUP BY inventory, species_10
)
SELECT inventory, species, sum(feature_count) AS feature_count, sum(area_ha) AS area_ha
FROM all_species
GROUP BY inventory, species
ORDER BY inventory, sum(area_ha) desc;

COMMENT ON MATERIALIZED VIEW all_species_area_counts_per_inv_latest IS
'all_species_area_counts_per_inv_latest provides the total area and feature count of each species type in the lyr_latest table, per inventory, for the latest cut';


-- LATEST CUT- Total species area and count per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_area_counts_per_jurisdiction_latest AS
SELECT LEFT(inventory, 2) AS jurisdiction, species, sum(feature_count) AS feature_count, sum(area_ha) AS area_ha, 
rank() over (PARTITION BY LEFT(inventory, 2) ORDER BY sum(area_ha) desc)
FROM all_species_area_counts_per_inv_latest
GROUP BY jurisdiction, species
ORDER BY jurisdiction, area_ha desc;

COMMENT ON MATERIALIZED VIEW all_species_area_counts_per_jurisdiction_latest IS
'all_species_area_counts_per_jurisdiction_latest provides the total area and feature count of each species type in the lyr_latest table, per jurisdiction, for the latest cut';


-- LATEST CUT- Total species area and count
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_area_counts_latest AS
SELECT species, sum(feature_count) AS feature_count, sum(area_ha) AS area_ha, 
rank() over (order by sum(area_ha) desc)
FROM all_species_area_counts_per_inv_latest
GROUP BY species
ORDER BY area_ha desc;

COMMENT ON MATERIALIZED VIEW all_species_area_counts_latest IS
'all_species_area_counts_latest provides the total area and feature count of each species type in the lyr_latest table, for the latest cut';


-- LATEST CUT- Total species area and count, per species rank, per jurisdiciton
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_1_to_10_area_count_per_jurisdiction_latest AS
WITH species_table AS (
	-- Filter table beforehand to optimize
	SELECT LEFT(l.cas_id, 2) AS jurisdiction, species_1, species_per_1, species_2, species_per_2, species_3, species_per_3, species_4, species_per_4, species_5, species_per_5,
	       species_6, species_per_6, species_7, species_per_7, species_8, species_per_8, species_9, species_per_9, species_10, species_per_10, c.casfri_area AS area
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
), species_1 AS (
	SELECT jurisdiction, species_1, count(*) AS species_1_feature_count, sum(area*species_per_1/100) AS species_1_area_ha
	FROM species_table
	WHERE isError(species_1) = FALSE AND isError(species_per_1::text) = FALSE
	GROUP BY jurisdiction, species_1
), species_2 AS (
	SELECT jurisdiction, species_2, count(*) AS species_2_feature_count, sum(area*species_per_2/100) AS species_2_area_ha
	FROM species_table
	WHERE isError(species_2) = FALSE AND isError(species_per_2::text) = FALSE
	GROUP BY jurisdiction, species_2
), species_3 AS (
	SELECT jurisdiction, species_3, count(*) AS species_3_feature_count, sum(area*species_per_3/100) AS species_3_area_ha
	FROM species_table
	WHERE isError(species_3) = FALSE AND isError(species_per_3::text) = FALSE
	GROUP BY jurisdiction, species_3
), species_4 AS (
	SELECT jurisdiction, species_4, count(*) AS species_4_feature_count, sum(area*species_per_4/100) AS species_4_area_ha
	FROM species_table
	WHERE isError(species_4) = FALSE AND isError(species_per_4::text) = FALSE
	GROUP BY jurisdiction, species_4
), species_5 AS (
	SELECT jurisdiction, species_5, count(*) AS species_5_feature_count, sum(area*species_per_5/100) AS species_5_area_ha
	FROM species_table
	WHERE isError(species_5) = FALSE AND isError(species_per_5::text) = FALSE
	GROUP BY jurisdiction, species_5
), species_6 AS (
	SELECT jurisdiction, species_6, count(*) AS species_6_feature_count, sum(area*species_per_6/100) AS species_6_area_ha
	FROM species_table
	WHERE isError(species_6) = FALSE AND isError(species_per_6::text) = FALSE
	GROUP BY jurisdiction, species_6
), species_7 AS (
	SELECT jurisdiction, species_7, count(*) AS species_7_feature_count, sum(area*species_per_7/100) AS species_7_area_ha
	FROM species_table
	WHERE isError(species_7) = FALSE AND isError(species_per_7::text) = FALSE
	GROUP BY jurisdiction, species_7
), species_8 AS (
	SELECT jurisdiction, species_8, count(*) AS species_8_feature_count, sum(area*species_per_8/100) AS species_8_area_ha
	FROM species_table
	WHERE isError(species_8) = FALSE AND isError(species_per_8::text) = FALSE
	GROUP BY jurisdiction, species_8
), species_9 AS (
	SELECT jurisdiction, species_9, count(*) AS species_9_feature_count, sum(area*species_per_9/100) AS species_9_area_ha
	FROM species_table
	WHERE isError(species_9) = FALSE AND isError(species_per_9::text) = FALSE
	GROUP BY jurisdiction, species_9
), species_10 AS (
	SELECT jurisdiction, species_10, count(*) AS species_10_feature_count, sum(area*species_per_10/100) AS species_10_area_ha
	FROM species_table
	WHERE isError(species_10) = FALSE AND isError(species_per_10::text) = FALSE
	GROUP BY jurisdiction, species_10
)
SELECT s1.jurisdiction, 
-- Case statement is needed here to not display a null species if a species only is ever a species_5 for example
CASE WHEN (s1.species_1 IS NOT null) THEN s1.species_1 
WHEN (s2.species_2 IS NOT null) THEN s2.species_2
WHEN (s3.species_3 IS NOT null) THEN s3.species_3
WHEN (s4.species_4 IS NOT null) THEN s4.species_4
WHEN (s5.species_5 IS NOT null) THEN s5.species_5
WHEN (s6.species_6 IS NOT null) THEN s6.species_6
WHEN (s7.species_7 IS NOT null) THEN s7.species_7
WHEN (s8.species_8 IS NOT null) THEN s8.species_8
WHEN (s9.species_9 IS NOT null) THEN s9.species_9
ELSE s10.species_10 END AS species,
s1.species_1_feature_count, s1.species_1_area_ha, 
s2.species_2_feature_count, s2.species_2_area_ha,
s3.species_3_feature_count, s3.species_3_area_ha,
s4.species_4_feature_count, s4.species_4_area_ha,
s5.species_5_feature_count, s5.species_5_area_ha,
s6.species_6_feature_count, s6.species_6_area_ha,
s7.species_7_feature_count, s7.species_7_area_ha,
s8.species_8_feature_count, s8.species_8_area_ha,
s9.species_9_feature_count, s9.species_9_area_ha,
s10.species_10_feature_count, s10.species_10_area_ha
FROM species_1 s1
FULL JOIN species_2 s2 ON s1.species_1 = s2.species_2 AND s1.jurisdiction = s2.jurisdiction
FULL JOIN species_3 s3 ON s1.species_1 = s3.species_3 AND s1.jurisdiction = s3.jurisdiction
FULL JOIN species_4 s4 ON s1.species_1 = s4.species_4 AND s1.jurisdiction = s4.jurisdiction
FULL JOIN species_5 s5 ON s1.species_1 = s5.species_5 AND s1.jurisdiction = s5.jurisdiction
FULL JOIN species_6 s6 ON s1.species_1 = s6.species_6 AND s1.jurisdiction = s6.jurisdiction
FULL JOIN species_7 s7 ON s1.species_1 = s7.species_7 AND s1.jurisdiction = s7.jurisdiction
FULL JOIN species_8 s8 ON s1.species_1 = s8.species_8 AND s1.jurisdiction = s8.jurisdiction
FULL JOIN species_9 s9 ON s1.species_1 = s9.species_9 AND s1.jurisdiction = s9.jurisdiction
FULL JOIN species_10 s10 ON s1.species_1 = s10.species_10 AND s1.jurisdiction = s10.jurisdiction;

COMMENT ON MATERIALIZED VIEW all_species_1_to_10_area_count_per_jurisdiction_latest IS
'all_species_1_to_10_area_count_per_jurisdiction_latest provides the area and feature count of each of species_1 to species_10 in the lyr_latest table, per jurisdiction, for the latest cut';


-- LATEST CUT- Total species area and count, per species rank
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_1_to_10_area_count_latest AS
SELECT species,
sum(species_1_feature_count) AS species_1_feature_count, sum(species_1_area_ha) AS species_1_area_ha, 
sum(species_2_feature_count) AS species_2_feature_count, sum(species_2_area_ha) AS species_2_area_ha,
sum(species_3_feature_count) AS species_3_feature_count, sum(species_3_area_ha) AS species_3_area_ha,
sum(species_4_feature_count) AS species_4_feature_count, sum(species_4_area_ha) AS species_4_area_ha,
sum(species_5_feature_count) AS species_5_feature_count, sum(species_5_area_ha) AS species_5_area_ha,
sum(species_6_feature_count) AS species_6_feature_count, sum(species_6_area_ha) AS species_6_area_ha,
sum(species_7_feature_count) AS species_7_feature_count, sum(species_7_area_ha) AS species_7_area_ha,
sum(species_8_feature_count) AS species_8_feature_count, sum(species_8_area_ha) AS species_8_area_ha,
sum(species_9_feature_count) AS species_9_feature_count, sum(species_9_area_ha) AS species_9_area_ha,
sum(species_10_feature_count) AS species_10_feature_count, sum(species_10_area_ha) AS species_10_area_ha
FROM all_species_1_to_10_area_count_per_jurisdiction_latest
GROUP BY species
ORDER BY species;

COMMENT ON MATERIALIZED VIEW all_species_1_to_10_area_count_latest IS
'all_species_1_to_10_area_count_latest provides the area and feature count of each of species_1 to species_10 in the lyr_latest table, for the latest cut';


-- LATEST CUT- Area of species originating in each year, per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS species_area_by_stand_origin_year_per_jurisdiction_latest AS
WITH species_filter AS (
	SELECT l.cas_id, (origin_lower+origin_upper)/2 AS est_origin_year, c.casfri_area AS area_ha, 
	species_1, species_2, species_3, species_4, species_5, species_6, species_7, species_8, species_9, species_10,
	species_per_1, species_per_2, species_per_3, species_per_4, species_per_5, species_per_6, species_per_7, species_per_8, species_per_9, species_per_10
	FROM lyr_latest l
	INNER JOIN cas_latest c ON l.cas_id = c.cas_id
	WHERE isError(origin_upper::text) =  FALSE AND isError(origin_lower::text) = FALSE
), all_species AS (
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_1 AS species, sum(species_per_1*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_1) = FALSE and isError(species_per_1::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_2 AS species, sum(species_per_2*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_2) = FALSE and isError(species_per_2::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_3 AS species, sum(species_per_3*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_3) = FALSE and isError(species_per_3::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_4 AS species, sum(species_per_4*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_4) = FALSE and isError(species_per_4::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_5 AS species, sum(species_per_5*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_5) = FALSE and isError(species_per_5::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_6 AS species, sum(species_per_6*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_6) = FALSE and isError(species_per_6::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_7 AS species, sum(species_per_7*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_7) = FALSE and isError(species_per_7::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_8 AS species, sum(species_per_8*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_8) = FALSE and isError(species_per_8::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_9 AS species, sum(species_per_9*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_9) = FALSE and isError(species_per_9::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_10 AS species, sum(species_per_10*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_10) = FALSE and isError(species_per_10::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
)
SELECT est_origin_year, jurisdiction, species, sum(area_ha) AS area_ha
FROM all_species 
GROUP BY est_origin_year, jurisdiction, species
ORDER BY est_origin_year, jurisdiction, species;

COMMENT ON MATERIALIZED VIEW casfri_metrics.species_area_by_stand_origin_year_per_jurisdiction_latest IS
'species_area_by_stand_origin_year_per_jurisdiction_latest provides the total area of each species per origin year, per jurisdiction, for the latest cut. Origin year is average of origin_upper and origin_lower';


-- LATEST CUT- Area of species originating in each year
CREATE MATERIALIZED VIEW IF NOT EXISTS species_area_by_stand_origin_year_latest AS
SELECT est_origin_year, species, sum(area_ha) AS area_ha
FROM species_area_by_stand_origin_year_per_jurisdiction_latest
GROUP BY est_origin_year, species
ORDER BY est_origin_year, species;

COMMENT ON MATERIALIZED VIEW casfri_metrics.species_area_by_stand_origin_year_latest IS
'species_area_by_stand_origin_year_latest provides the total area of each species per origin year. Origin year is average of origin_upper and origin_lower, for the latest cut';


-- LATEST CUT- Forest layer area by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_layer_area_by_inv_latest AS
SELECT LEFT(cas_latest.cas_id, 4) AS inventory, 
sum(CASE WHEN layer = 1 THEN cas_latest.casfri_area ELSE 0 end) AS layer_1_area_ha,
sum(CASE WHEN layer = 2 THEN cas_latest.casfri_area ELSE 0 end) AS layer_2_area_ha,
sum(CASE WHEN layer = 3 THEN cas_latest.casfri_area ELSE 0 end) AS layer_3_area_ha,
sum(CASE WHEN layer = 4 THEN cas_latest.casfri_area ELSE 0 end) AS layer_4_area_ha
FROM lyr_latest
INNER JOIN cas_latest ON cas_latest.cas_id = lyr_latest.cas_id 
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW forest_layer_area_by_inv_latest IS
'forest_layer_area_by_inv_latest provides the sum of all forested area in each inventory, for each layer, for the latest cut';


-- LATEST CUT- Disturbance type area by year per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS dst_type_area_by_jurisdiction_year_latest AS
SELECT dist_type_filter.year, dist_type_filter.dist_type, dist_type_filter.jurisdiction, sum(dist_type_filter.area_ha) AS area_ha 
FROM(
	SELECT dist_year_1 AS year, dist_type_1 AS dist_type, LEFT(dst_latest.cas_id, 2) AS jurisdiction, 
	sum(cas_latest.casfri_area*getDistExt(dist_ext_upper_1, dist_ext_lower_1)/100) AS area_ha, 
		rank() over (
		partition by dist_type_1
		ORDER BY dist_type_1
		)
	FROM dst_latest
	INNER JOIN cas_latest ON cas_latest.cas_id = dst_latest.cas_id
	WHERE isError(dist_type_1) = false
	AND isError(dist_year_1::text) = false
	GROUP BY year, dist_type_1, LEFT(dst_latest.cas_id, 2)
	-- Stack each of dist_1, dist_2, dist_3 then sum the areas and regroup
	UNION ALL 
	SELECT dist_year_2 AS year, dist_type_2 AS dist_type, LEFT(dst_latest.cas_id, 2) AS jurisdiction, 
	sum(cas_latest.casfri_area*getDistExt(dist_ext_upper_2, dist_ext_lower_2)/100) AS area_ha, 
		rank() over (
		partition by dist_type_2
		ORDER BY dist_type_2
		)
	FROM dst_latest
	INNER JOIN cas_latest ON cas_latest.cas_id = dst_latest.cas_id
	where isError(dist_type_2) = false
	AND isError(dist_year_2::text) = false
	GROUP BY year, dist_type_2, LEFT(dst_latest.cas_id, 2)
	UNION ALL 
	SELECT dist_year_3 AS year, dist_type_3 AS dist_type, LEFT(dst_latest.cas_id, 2) AS jurisdiction, 
	sum(cas_latest.casfri_area*getDistExt(dist_ext_upper_3, dist_ext_lower_3)/100) AS area_ha, 
		rank() over (
		partition by dist_type_3
		ORDER BY dist_type_3
		)
	FROM dst_latest
	INNER JOIN cas_latest ON cas_latest.cas_id = dst_latest.cas_id
	WHERE isError(dist_type_3) = false
	AND isError(dist_year_3::text) = false
	GROUP BY year, dist_type_3, LEFT(dst_latest.cas_id, 2)
) dist_type_filter
GROUP BY year, dist_type, jurisdiction
ORDER BY year ASC, jurisdiction;		


COMMENT ON MATERIALIZED VIEW dst_type_area_by_jurisdiction_year_latest IS
'dst_type_area_by_jurisdiction_year_latest provides the sum of areas affected by each disturbance type for each jurisdiction, in each year, for the latest cut';


-- LATEST CUT- Disturbance type area by year
CREATE MATERIALIZED VIEW IF NOT EXISTS dst_type_area_by_year_latest AS
SELECT YEAR, dist_type, sum(area_ha) AS area_ha
FROM dst_type_area_by_jurisdiction_year_latest
GROUP BY year, dist_type
ORDER BY year ASC, sum(area_ha) DESC;		

COMMENT ON MATERIALIZED VIEW dst_type_area_by_year_latest IS
'dst_type_area_by_year_latest provides the sum of areas affected by each disturbance type for each year, for the latest cut';

-- LATEST CUT- Area of each disturbance type per species, per jurisdicition
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_disturbed_area_per_jurisdiction_latest AS
WITH all_species AS (
	SELECT cas_id, species_1 AS species, species_per_1 AS species_per
	FROM lyr_latest
	WHERE isError(species_1) = FALSE AND isError(species_per_1::text) = FALSE
	UNION ALL
	SELECT cas_id, species_2 AS species, species_per_2 AS species_per
	FROM lyr_latest
	WHERE isError(species_2) = FALSE AND isError(species_per_2::text) = FALSE
	UNION ALL
	SELECT cas_id, species_3 AS species, species_per_3 AS species_per
	FROM lyr_latest
	WHERE isError(species_3) = FALSE AND isError(species_per_3::text) = FALSE
	UNION ALL
	SELECT cas_id, species_4 AS species, species_per_4 AS species_per
	FROM lyr_latest
	WHERE isError(species_4) = FALSE AND isError(species_per_4::text) = FALSE
	UNION ALL
	SELECT cas_id, species_5 AS species, species_per_5 AS species_per
	FROM lyr_latest
	WHERE isError(species_5) = FALSE AND isError(species_per_5::text) = FALSE
	UNION ALL
	SELECT cas_id, species_6 AS species, species_per_6 AS species_per
	FROM lyr_latest
	WHERE isError(species_6) = FALSE AND isError(species_per_6::text) = FALSE
	UNION ALL
	SELECT cas_id, species_7 AS species, species_per_7 AS species_per
	FROM lyr_latest
	WHERE isError(species_7) = FALSE AND isError(species_per_7::text) = FALSE
	UNION ALL
	SELECT cas_id, species_8 AS species, species_per_8 AS species_per
	FROM lyr_latest
	WHERE isError(species_8) = FALSE AND isError(species_per_8::text) = FALSE
	UNION ALL
	SELECT cas_id, species_9 AS species, species_per_9 AS species_per
	FROM lyr_latest
	WHERE isError(species_9) = FALSE AND isError(species_per_9::text) = FALSE
	UNION ALL
	SELECT cas_id, species_10 AS species, species_per_10 AS species_per
	FROM lyr_latest
	WHERE isError(species_10) = FALSE AND isError(species_per_10::text) = FALSE
), all_dist_types AS (
	SELECT d.cas_id, d.dist_type_1 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_1, dist_ext_lower_1)/100 AS casfri_area
	FROM dst_latest d
	INNER JOIN cas_latest c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_1) = FALSE AND dist_type_1 NOT LIKE '1%' AND dist_type_1 NOT LIKE '2%'
	UNION ALL
	SELECT d.cas_id, d.dist_type_2 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_2, dist_ext_lower_2)/100 AS casfri_area
	FROM dst_latest d
	INNER JOIN cas_latest c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_2) = FALSE AND dist_type_2 NOT LIKE '1%' AND dist_type_2 NOT LIKE '2%'
	UNION ALL
	SELECT d.cas_id, d.dist_type_3 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_3, dist_ext_lower_3)/100 AS casfri_area
	FROM dst_latest d
	INNER JOIN cas_latest c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_3) = FALSE AND dist_type_3 NOT LIKE '1%' AND dist_type_3 NOT LIKE '2%'
)
SELECT LEFT(d.cas_id, 2) AS jurisdiction, d.dist_type, 
s.species,
sum(d.casfri_area*s.species_per/100) AS area_ha,
count(d.dist_type) AS feature_count,
RANK () OVER (
	PARTITION BY LEFT(d.cas_id, 2), d.dist_type
	ORDER BY count(d.dist_type) desc
)
FROM all_dist_types d
INNER JOIN all_species s ON d.cas_id = s.cas_id
GROUP BY jurisdiction, d.dist_type, s.species
ORDER BY jurisdiction, feature_count desc;

COMMENT ON MATERIALIZED VIEW all_species_disturbed_area_per_jurisdiction_latest IS
'all_species_disturbed_area_per_jurisdiction_latest provides the feature and area count of each species disturbed, per jurisdiction, for the latest cut';


-- LATEST CUT- Area of each disturbance type per species
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_disturbed_area_latest AS
WITH all_species AS (
	SELECT dist_type, species, sum(area_ha) AS area_ha, sum(feature_count) AS feature_count
	FROM all_species_disturbed_area_per_jurisdiction_latest 
	GROUP BY dist_type, species
)
SELECT dist_type, species, area_ha, feature_count,
RANK () OVER (
	PARTITION BY dist_type
	ORDER BY feature_count desc
)
FROM all_species 
ORDER BY feature_count desc;

COMMENT ON MATERIALIZED VIEW all_species_disturbed_area_latest IS
'all_species_disturbed_area_latest provides the feature and area count of each species disturbed, across all inventories, for the latest cut';


-- LATEST CUT- Species most often disturbed per disturbance type
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.species_most_often_disturbed_latest AS
SELECT dist_type, species, area_ha, feature_count
FROM all_species_disturbed_area_latest
WHERE RANK = 1
ORDER BY feature_count desc;

COMMENT ON MATERIALIZED VIEW casfri_metrics.species_most_often_disturbed_latest IS
'species_most_often_disturbed_latest provides the top species disturbed per disturbance type, with the feature and area count, across all inventories, for the latest cut';


-- LATEST CUT- Disturbance type area and feature count ranked per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS dst_area_ranking_by_inv_latest AS
WITH dist_types AS (
	SELECT LEFT(dst_latest.cas_id, 4) AS inventory, dist_type_1 AS dist_type, sum(cas_latest.casfri_area*getDistExt(dist_ext_upper_1, dist_ext_lower_1)/100) AS area_ha, 
	count(dist_type_1) AS feature_count
	FROM dst_latest
	INNER JOIN cas_latest ON cas_latest.cas_id = dst_latest.cas_id
	-- Need to check that dist_type isnt a number because of translation errors in this geohistory table
	WHERE isError(dist_type_1) = FALSE AND dist_type_1 NOT LIKE '1%' AND dist_type_1 NOT LIKE '2%'
	GROUP BY inventory, dist_type_1
	-- Union each disturbance type and its areas and feature counts
	UNION ALL 
	SELECT LEFT(dst_latest.cas_id, 4) AS inventory, dist_type_2 AS dist_type, sum(cas_latest.casfri_area*getDistExt(dist_ext_upper_2, dist_ext_lower_2)/100) AS area_ha, 
	count(dist_type_2) AS feature_count
	FROM dst_latest
	INNER JOIN cas_latest ON cas_latest.cas_id = dst_latest.cas_id
	WHERE isError(dist_type_2) = FALSE AND dist_type_2 NOT LIKE '1%' AND dist_type_2 NOT LIKE '2%'
	GROUP BY inventory, dist_type_2
	UNION ALL 
	SELECT LEFT(dst_latest.cas_id, 4) AS inventory, dist_type_3 AS dist_type, sum(cas_latest.casfri_area*getDistExt(dist_ext_upper_3, dist_ext_lower_3)/100) AS area_ha, 
	count(dist_type_3) AS feature_count
	FROM dst_latest
	INNER JOIN cas_latest ON cas_latest.cas_id = dst_latest.cas_id
	WHERE isError(dist_type_3) = FALSE AND dist_type_3 NOT LIKE '1%' AND dist_type_3 NOT LIKE '2%'
	GROUP BY inventory, dist_type_3
)
--Sum up individual disturbance type sums and assign ranking based on feature count
SELECT inventory, dist_type, sum(area_ha) AS area_ha, sum(feature_count) AS feature_count,
rank() over (
	partition BY inventory
	ORDER BY sum(feature_count) DESC
	)
FROM dist_types
GROUP BY inventory, dist_type
ORDER BY inventory, RANK;

COMMENT ON MATERIALIZED VIEW dst_area_ranking_by_inv_latest IS
'dst_area_ranking_by_inv_latest provides the total area and feature count of all disturbances, per inventory, for the latest cut';


-- LATEST CUT- Centroid coordinates of the 100 largest disturbances of each type per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS disturbance_coords_per_jurisdiction_latest AS
WITH dists AS (
	SELECT d.cas_id, LEFT(d.cas_id, 2) AS jurisdiction, dist_type_1 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_1, dist_ext_lower_1)/100 AS area_ha
	FROM dst_latest d
	INNER JOIN cas_latest c ON c.cas_id = d.cas_id
	-- Need to check that dist_type isnt a number because of translation errors in this geohistory table
	WHERE isError(dist_type_1) = FALSE AND dist_type_1 NOT LIKE '1%' AND dist_type_1 NOT LIKE '2%'
	UNION ALL
	SELECT d.cas_id, LEFT(d.cas_id, 2) AS jurisdiction, dist_type_2 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_2, dist_ext_lower_2)/100 AS area_ha
	FROM dst_latest d
	INNER JOIN cas_latest c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_2) = FALSE AND dist_type_2 NOT LIKE '1%' AND dist_type_2 NOT LIKE '2%'
	UNION ALL
	SELECT d.cas_id, LEFT(d.cas_id, 2) AS jurisdiction, dist_type_3 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_3, dist_ext_lower_3)/100 AS area_ha
	FROM dst_latest d
	INNER JOIN cas_latest c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_3) = FALSE AND dist_type_3 NOT LIKE '1%' AND dist_type_3 NOT LIKE '2%'
), ranks AS (
	SELECT cas_id, jurisdiction, dist_type, area_ha, rank() OVER (PARTITION BY jurisdiction, dist_type ORDER BY area_ha DESC)
	FROM dists
)
SELECT jurisdiction, r.dist_type, r.area_ha, st_x(st_transform(st_centroid(g.geometry),4326)) as lon, st_y(st_transform(st_centroid(g.geometry),4326)) as lat
FROM ranks r
INNER JOIN geo_latest g ON r.cas_id = g.cas_id
WHERE r.RANK <= 100;

COMMENT ON MATERIALIZED VIEW disturbance_coords_per_jurisdiction_latest IS
'disturbance_coords_per_jurisdiction_latest records the area and centroid coordinates of the 100 largest disturbances per jurisdiction for each disturbance type, for the latest cut';


-- LATEST CUT- Centroid coordinates of the 100 largest disturbances of each type 
CREATE MATERIALIZED VIEW IF NOT EXISTS disturbance_coords_latest AS
WITH ranks AS (
	SELECT dist_type, area_ha, lon, lat, rank() OVER (PARTITION BY dist_type ORDER BY area_ha desc)
	FROM disturbance_coords_per_jurisdiction_latest
)
SELECT dist_type, area_ha, lon, lat
FROM ranks r
WHERE r.RANK <= 100;

COMMENT ON MATERIALIZED VIEW disturbance_coords_latest IS
'disturbance_coords_latest records the area and centroid coordinates of the 100 largest disturbances for each disturbance type, for the latest cut';


-- LATEST CUT- Soil moisture regime areas per species, per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS soil_moist_reg_per_species_per_inventory_latest AS
WITH all_species AS (
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_1 AS species, l.soil_moist_reg, c.casfri_area*species_per_1/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_1) = FALSE AND isError(species_per_1::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_2 AS species, l.soil_moist_reg, c.casfri_area*species_per_2/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_2) = FALSE AND isError(species_per_2::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_3 AS species, l.soil_moist_reg, c.casfri_area*species_per_3/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_3) = FALSE AND isError(species_per_3::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_4 AS species, l.soil_moist_reg, c.casfri_area*species_per_4/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_4) = FALSE AND isError(species_per_4::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_5 AS species, l.soil_moist_reg, c.casfri_area*species_per_5/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_5) = FALSE AND isError(species_per_5::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_6 AS species, l.soil_moist_reg, c.casfri_area*species_per_6/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_6) = FALSE AND isError(species_per_6::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_7 AS species, l.soil_moist_reg, c.casfri_area*species_per_7/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_7) = FALSE AND isError(species_per_7::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_8 AS species, l.soil_moist_reg, c.casfri_area*species_per_8/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_8) = FALSE AND isError(species_per_8::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_9 AS species, l.soil_moist_reg, c.casfri_area*species_per_9/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_9) = FALSE AND isError(species_per_9::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_10 AS species, l.soil_moist_reg, c.casfri_area*species_per_10/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_10) = FALSE AND isError(species_per_10::text) = FALSE AND layer = 1
)
SELECT inventory, species, soil_moist_reg, sum(area_ha) AS area_ha
FROM all_species
GROUP BY inventory, species, soil_moist_reg
ORDER BY inventory, species, soil_moist_reg;

COMMENT ON MATERIALIZED VIEW soil_moist_reg_per_species_per_inventory_latest IS
'soil_moist_reg_per_species_per_inventory_latest records the area of each soil moisture regime type per species, per inventory, for the latest cut';


-- LATEST CUT- Forest productivity areas per leading species, per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_productivity_per_species_per_inventory_latest AS
WITH all_species AS (
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_1 AS species, l.productivity, c.casfri_area*species_per_1/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_1) = FALSE AND isError(species_per_1::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_2 AS species, l.productivity, c.casfri_area*species_per_2/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_2) = FALSE AND isError(species_per_2::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_3 AS species, l.productivity, c.casfri_area*species_per_3/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_3) = FALSE AND isError(species_per_3::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_4 AS species, l.productivity, c.casfri_area*species_per_4/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_4) = FALSE AND isError(species_per_4::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_5 AS species, l.productivity, c.casfri_area*species_per_5/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_5) = FALSE AND isError(species_per_5::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_6 AS species, l.productivity, c.casfri_area*species_per_6/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_6) = FALSE AND isError(species_per_6::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_7 AS species, l.productivity, c.casfri_area*species_per_7/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_7) = FALSE AND isError(species_per_7::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_8 AS species, l.productivity, c.casfri_area*species_per_8/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_8) = FALSE AND isError(species_per_8::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_9 AS species, l.productivity, c.casfri_area*species_per_9/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_9) = FALSE AND isError(species_per_9::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_10 AS species, l.productivity, c.casfri_area*species_per_10/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_10) = FALSE AND isError(species_per_10::text) = FALSE AND layer = 1
)
SELECT inventory, species, productivity, sum(area_ha) AS area_ha
FROM all_species
GROUP BY inventory, species, productivity
ORDER BY inventory, species, productivity;

COMMENT ON MATERIALIZED VIEW forest_productivity_per_species_per_inventory_latest IS
'forest_productivity_per_species_per_inventory_latest records the area of each productivity value per leading species, per inventory, for the latest cut';


-- LATEST CUT- Forest productivity type areas per leading species, per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_productivity_type_per_species_per_inventory_latest AS
WITH all_species AS (
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_1 AS species, l.productivity_type, c.casfri_area*species_per_1/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_1) = FALSE AND isError(species_per_1::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_2 AS species, l.productivity_type, c.casfri_area*species_per_2/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_2) = FALSE AND isError(species_per_2::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_3 AS species, l.productivity_type, c.casfri_area*species_per_3/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_3) = FALSE AND isError(species_per_3::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_4 AS species, l.productivity_type, c.casfri_area*species_per_4/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_4) = FALSE AND isError(species_per_4::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_5 AS species, l.productivity_type, c.casfri_area*species_per_5/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_5) = FALSE AND isError(species_per_5::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_6 AS species, l.productivity_type, c.casfri_area*species_per_6/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_6) = FALSE AND isError(species_per_6::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_7 AS species, l.productivity_type, c.casfri_area*species_per_7/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_7) = FALSE AND isError(species_per_7::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_8 AS species, l.productivity_type, c.casfri_area*species_per_8/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_8) = FALSE AND isError(species_per_8::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_9 AS species, l.productivity_type, c.casfri_area*species_per_9/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_9) = FALSE AND isError(species_per_9::text) = FALSE AND layer = 1
	UNION ALL
	SELECT LEFT(l.cas_id, 4) AS inventory, l.species_10 AS species, l.productivity_type, c.casfri_area*species_per_10/100 AS area_ha
	FROM lyr_latest l
	INNER JOIN cas_latest c ON c.cas_id = l.cas_id
	WHERE isError(l.species_10) = FALSE AND isError(species_per_10::text) = FALSE AND layer = 1
)
SELECT inventory, species, productivity_type, sum(area_ha) AS area_ha
FROM all_species
GROUP BY inventory, species, productivity_type
ORDER BY inventory, species, productivity_type;

COMMENT ON MATERIALIZED VIEW forest_productivity_type_per_species_per_inventory_latest IS
'forest_productivity_type_per_species_per_inventory_latest records the area of each productivity type value per leading species, per inventory, for the latest cut';


-- LATEST CUT- Eco site wetland type areas by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS eco_wetland_type_per_inventory_latest AS
SELECT LEFT(e.cas_id, 4) AS inventory, wetland_type, sum(c.casfri_area) AS area_ha
FROM eco_latest e
INNER JOIN cas_latest c ON e.cas_id = c.cas_id
GROUP BY inventory, wetland_type
ORDER BY inventory, wetland_type;

COMMENT ON MATERIALIZED VIEW eco_wetland_type_per_inventory_latest IS
'eco_wetland_type_per_inventory_latest records the area of each eco_latest wetland type per inventory, for the latest cut';


-- LATEST CUT- Eco site wet_veg_cover areas by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS eco_wet_veg_cover_per_inventory_latest AS
SELECT LEFT(e.cas_id, 4) AS inventory, wet_veg_cover, sum(c.casfri_area) AS area_ha
FROM eco_latest e
INNER JOIN cas_latest c ON e.cas_id = c.cas_id
GROUP BY inventory, wet_veg_cover
ORDER BY inventory, wet_veg_cover;

COMMENT ON MATERIALIZED VIEW eco_wet_veg_cover_per_inventory_latest IS
'eco_wet_veg_cover_per_inventory_latest records the area of each eco_latest wet_veg_cover type per inventory, for the latest cut';


-- LATEST CUT- Eco site wet_landform_mod areas by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS eco_wet_landform_mod_per_inventory_latest AS
SELECT LEFT(e.cas_id, 4) AS inventory, wet_landform_mod, sum(c.casfri_area) AS area_ha
FROM eco_latest e
INNER JOIN cas_latest c ON e.cas_id = c.cas_id
GROUP BY inventory, wet_landform_mod
ORDER BY inventory, wet_landform_mod;

COMMENT ON MATERIALIZED VIEW eco_wet_landform_mod_per_inventory_latest IS
'eco_wet_landform_mod_per_inventory_latest records the area of each eco_latest wet_landform_mod type per inventory, for the latest cut';


-- LATEST CUT- Eco site wet_local_mod areas by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS eco_wet_local_mod_per_inventory_latest AS
SELECT LEFT(e.cas_id, 4) AS inventory, wet_local_mod, sum(c.casfri_area) AS area_ha
FROM eco_latest e
INNER JOIN cas_latest c ON e.cas_id = c.cas_id
GROUP BY inventory, wet_local_mod
ORDER BY inventory, wet_local_mod;

COMMENT ON MATERIALIZED VIEW eco_wet_local_mod_per_inventory_latest IS
'eco_wet_local_mod_per_inventory_latest records the area of each eco_latest wet_local_mod type per inventory, for the latest cut';


-- LATEST CUT- Treed polygon areas
CREATE MATERIALIZED VIEW IF NOT EXISTS treed_polygon_sizes_overall_latest AS
SELECT avg(c.casfri_area) AS mean_area_ha,
stddev(c.casfri_area) AS std_dev
FROM lyr_latest l
INNER JOIN cas_latest c ON c.cas_id = l.cas_id
WHERE layer = 1;

COMMENT ON MATERIALIZED VIEW treed_polygon_sizes_overall_latest IS
'treed_polygon_sizes_overall_latest records the area and standard deviation of the total forested area in CASFRI layer 1, for the latest cut';


-- LATEST CUT- Treed polygon areas per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS treed_polygon_sizes_per_jurisdiction_latest AS
SELECT LEFT(c.cas_id, 2) AS jurisdiction, avg(c.casfri_area) AS mean_area_ha,
stddev(c.casfri_area) AS std_dev
FROM lyr_latest l
INNER JOIN cas_latest c ON c.cas_id = l.cas_id
WHERE layer = 1
GROUP BY jurisdiction;

COMMENT ON MATERIALIZED VIEW treed_polygon_sizes_per_jurisdiction_latest IS
'treed_polygon_sizes_per_jurisdiction_latest records the area and standard deviation of the total forested area in CASFRI layer 1, per jurisdiction, for the latest cut';


-- LATEST CUT- Stand ages for each forest stand
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_layer_stand_ages_latest AS
SELECT cas_id, layer, casfri_post_processing.getStandAge(cas_id, layer, origin_upper, 'casfri50', 'casfri_validation') AS stand_age
FROM lyr_latest
WHERE layer_rank IN (1, -8887);

COMMENT ON MATERIALIZED VIEW forest_layer_stand_ages_latest IS
'forest_layer_stand_ages_latest provides the age of each layer_rank 1 forest stand in lyr_all';


-- LATEST CUT- Range of stand_age by inventory 
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_stand_age_per_inv_latest AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN casfri_metrics.isError(stand_age::text) = FALSE THEN stand_age ELSE NULL END) AS min_stand_age, 
max(CASE WHEN casfri_metrics.isError(stand_age::text) = FALSE THEN stand_age ELSE NULL END) AS max_stand_age
FROM forest_layer_stand_ages_latest
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_stand_age_per_inv IS
'range_of_stand_age_per_inv_latest provides the range of recorded stand_age for each inventory';


-- LATEST CUT- stand_age aggregation to be displayed in grafana
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_age_histogram_latest AS
WITH ranges AS (
	SELECT foo.inventory, r.min_stand_age, r.max_stand_age
	FROM casfri_metrics.getListInvs() AS foo(inventory)
	INNER JOIN casfri_metrics.range_of_stand_age_per_inv_latest r ON r.inventory = foo.inventory
), series AS (
	SELECT inventory, generate_series(COALESCE(min_stand_age,0), COALESCE(max_stand_age,900)) stand_age
	FROM ranges
), agg AS (
	SELECT left(cas_id,4) AS inventory, stand_age, count(*)
	FROM forest_layer_stand_ages_latest
	WHERE casfri_metrics.isError(stand_age) = FALSE
	GROUP BY inventory, stand_age
)
SELECT s.inventory, s.stand_age, COALESCE(count, 0) AS count
FROM agg y
FULL JOIN series s ON s.inventory = y.inventory AND s.stand_age = y.stand_age
ORDER BY inventory, stand_age;

COMMENT ON MATERIALIZED VIEW stand_age_histogram_latest IS
'stand_age_histogram_latest aggregates the stand_age per inventory';
