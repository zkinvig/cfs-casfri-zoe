-- This file contains functions to generate statistics on each raw inventory
-- This is for raw data, NOT the harmonized CASFRI data
-- File is intended to be ran as a script to regenerate all tables
-- This is all done in dynamic functions since the raw tables all have very different column names and data types
-- Ensure your search path is set to create tables in an appropriate location


/*
-- Uncomment to add new column names to be ignored
-- Note: This list is only used for text columns, these columns are not ignored for numeric, geo, or date stats

DROP TABLE IF EXISTS raw_ignored_text_columns;
CREATE TABLE IF NOT EXISTS raw_ignored_text_columns (column_name TEXT);
INSERT INTO raw_ignored_text_columns VALUES 
('aris'),
('field_id'),
('full_label'),
('geoc_maj'),
('geoc_maj_11_20'),
('geoc_maj_1_10'),
('geocode'),
('harv_id'),
('id_eforest'),
('inf_eta_ess_pc'),
('info'),
('initelim'),
('key_'),
('lic_key'),
('line_2_polygon_id'),
('line_3_tree_species'),
('line_4_classes_indexes'),
('mapstand'),
('mapstand_'),
('nid'),
('no_sec_int'),
('ospcomp'),
('plant_id'),
('poly_id'),
('polyid'),
('polyid_11_20'),
('polyid_1_10'),
('small_label'),
('species'),
('spp_sum'),
('standid'),
('strate'),
('sup_eta_ess_pc'),
('thin_id'),
('toponyme'),
('unique_id'),
('uspcomp'),
('checkelim');

*/


-- columnContainsNonNumerics(table_name, column_name, schema_name)
-- Function to determine if a column in a specified table contains any non numeric values
-- Input:
	-- table_name: the name of the table to check, as text. ex: 'AB29'
	-- column_name: the name of the column to check, as text. ex: 'species_1'
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
-- Output: A boolean value
	-- If the column only contains digits and NULLs, will return false
	-- If the column contains anything other than digits and NULLs, returns true
	-- If the column contains only NULLs, will return true so that it is treated as a text column
CREATE OR REPLACE FUNCTION columnContainsNonNumerics(table_name TEXT, column_name TEXT, schema_name TEXT)
RETURNS BOOLEAN
LANGUAGE plpgsql AS
$func$
DECLARE
	column_record RECORD;
	not_null BOOLEAN;
BEGIN
	not_null = FALSE;
	-- Iterate through each record in the column in specified table
	FOR column_record IN
        EXECUTE format('SELECT %s::text AS value FROM %s.%s', column_name, schema_name, table_name)
    LOOP
	   	IF column_record.value::TEXT IS NOT NULL
	   	THEN
	   		not_null = TRUE;
	   	END IF;
	   	-- If found a non numeric, return true and end function
        IF column_record.value::text ~ '^[0-9]+$' =  FALSE AND column_record.value IS NOT NULL
        THEN
			RETURN TRUE;   
		END IF;
    END LOOP;
   -- If there is only nulls, then consider it to be a text column
   IF not_null = FALSE
   THEN 
   RETURN TRUE;
   END IF; 
   RETURN FALSE;
END;
$func$;

-- getNumericTextColumnNames(inventory, schema_name)
-- Given a table and schema name, returns a table with all the columns that have a text datatype, but only contain numeric values
-- Input:
	-- inventory: the name of the table to check, as text. ex: 'AB29'
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
-- Output: TABLE (column_name TEXT, data_type TEXT, ordinal_position NUMERIC)
	-- column_name: name of the column
	-- data_type: the original data type of the column
	-- ordinal_position: the column index of the specified column
CREATE OR REPLACE FUNCTION getNumericTextColumnNames(inventory TEXT, schema_name TEXT)
RETURNS TABLE (column_name TEXT, data_type TEXT, ordinal_position NUMERIC)
LANGUAGE plpgsql AS
$func$
DECLARE
	column_record RECORD;
	dynamic_sql TEXT;
	bool_val BOOLEAN;
BEGIN
	RAISE NOTICE 'Numeric Text Columns- (%) Function started ----------', LEFT(clock_timestamp()::TEXT, 19);
	dynamic_sql := '';
	-- Iterate through each text column in the table
	FOR column_record IN 
		EXECUTE format('SELECT * FROM getColumnNames(''%s'', ''text'', ''%s'')', inventory, schema_name)
	LOOP
        -- Find truth value of if the column contains non numerics
		EXECUTE format('SELECT * FROM columnContainsNonNumerics(''%s'', ''%s'', ''%s'')', inventory, column_record.column_name, schema_name) INTO bool_val;
		IF bool_val = FALSE
		THEN
			-- If the text column contains only numerics, add entry to result table
			dynamic_sql := dynamic_sql || format('(SELECT ''%s'' AS column_name, ''%s'' AS data_type, %s::numeric AS ordinal_position FROM %s.%s LIMIT 1) UNION ALL ' 
			, column_record.column_name, column_record.data_type, column_record.ordinal_position::numeric, schema_name, inventory);
		END IF; 
	END LOOP;
	RAISE NOTICE 'Numeric Text Columns- (%) Function completed', LEFT(clock_timestamp()::TEXT, 19);
	IF dynamic_sql != ''
	THEN
		RETURN QUERY EXECUTE trim(TRAILING ' UNION ALL ' FROM dynamic_sql);
	END IF;
	RETURN;
END;
$func$;


-- getColumnNames(inventory, column_type, schema_name)
-- Given an inventory, schema, and a type of column to look for, returns a table containing the names of all columns matching the type
-- Input:
	-- inventory: the name of the table to check, as text. ex: 'AB29'
	-- column_type: must be either 'numeric', 'text', or 'date'. Flags which type of column to search for
		-- numeric columns include all integer, float, and double numeric data types
		-- text columns include varchar and text data types
		-- date columns include timestamps with timezone data type
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
-- Output: TABLE (column_name TEXT, data_type TEXT, ordinal_position NUMERIC)
	-- column_name: name of the column
	-- data_type: the original data type of the column
	-- ordinal_position: the column index of the specified column
CREATE OR REPLACE FUNCTION getColumnNames(inventory TEXT, column_type TEXT, schema_name TEXT)
RETURNS TABLE (column_name TEXT, data_type TEXT, ordinal_position NUMERIC)
LANGUAGE plpgsql AS
$func$
BEGIN
	IF column_type = 'numeric'
	THEN
		RETURN query EXECUTE 
			format('(SELECT column_name::text, data_type::text, ordinal_position::numeric 
			FROM information_schema.COLUMNS
			WHERE table_schema = ''%s''
   			AND lower(table_name) = lower(''%s'') 
			AND lower(data_type) IN (''integer'', ''bigint'', ''double precision'', ''numeric'', ''real'', ''smallint''))
			UNION ALL
			SELECT * FROM getNumericTextColumnNames(''%s'', ''%s'')', schema_name, inventory, inventory, schema_name);
	ELSIF column_type = 'text'
	THEN 
		RETURN query EXECUTE 
			format('SELECT column_name::text, data_type::text, ordinal_position::numeric 
			FROM information_schema.COLUMNS
			WHERE table_schema = ''%s''
   			AND lower(table_name) = lower(''%s'') 
			AND lower(data_type) IN (''text'', ''character varying'') 
			ORDER BY ordinal_position', schema_name, inventory, inventory);
	ELSIF column_type = 'date'
	THEN 
		RETURN query EXECUTE 
			format('SELECT column_name::text, data_type::text, ordinal_position::numeric 
			FROM information_schema.COLUMNS
			WHERE table_schema = ''%s''
   			AND lower(table_name) = lower(''%s'') 
			AND lower(data_type) = ''timestamp with time zone''
			ORDER BY ordinal_position', schema_name, inventory, inventory);
	ELSIF column_type = 'all'
	THEN 
		RETURN query EXECUTE 
			format('SELECT column_name::text, data_type::text, ordinal_position::numeric 
			FROM information_schema.COLUMNS
			WHERE table_schema = ''%s''
   			AND lower(table_name) = lower(''%s'') 
			ORDER BY ordinal_position', schema_name, inventory, inventory);
	END IF;
END;
$func$;


-- createColumnNamesList(target_table_name, schema_name, inventory_source)
-- Generates a table of column names and types for text and numeric columns aggregrated by each specified inventory
-- Does not find geometry or date columns
-- Input:
	-- target_table_name: the name of the table to be created. ex: 'result_table'. 
		-- If the target table already exists, it will be dropped
		-- Note that the table will be created in your search path's default schema, NOT the specified schema
	-- schema_name: the name of the schema to search for inventories in, as text. ex: 'rawfri'
	-- inventory_source: the source of the list of inventories. Must be a table in the form (inventory TEXT)
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, or a value list are all appropriate
		-- ex: 'getListInvs()'
-- Output: VOID
	-- Produces a table with target_table_name in form of (inventory TEXT, column_name TEXT, data_type TEXT, ordinal_position NUMERIC)
		-- inventory: the inventory (or table) that the column belongs to
		-- column_name: name of the column
		-- data_type: the original data type of the column, generalized to either 'text' or 'numeric'
			-- If the column was a text column containing only numeric values, will be switched to 'numeric'
		-- ordinal_position: the column index of the specified column
CREATE OR REPLACE FUNCTION createColumnNamesList(target_table_name TEXT, schema_name TEXT, inventory_source TEXT)
RETURNS VOID
LANGUAGE plpgsql AS
$func$
DECLARE 
	inventory_record RECORD;
	column_record RECORD;
	bool_val BOOLEAN;
	total_operations INTEGER;
    start_time TIMESTAMPTZ;
   	operation_count INTEGER;
    msg TEXT;
BEGIN
	RAISE NOTICE 'Create Column Names- (%) Function started ----------', LEFT(clock_timestamp()::TEXT, 19);
	start_time := now();
	operation_count := 0;
	total_operations := 0;
	EXECUTE format('SELECT count(*)::integer FROM %s', inventory_source) INTO total_operations;
	EXECUTE format('DROP TABLE IF EXISTS %s', target_table_name);
	EXECUTE format('CREATE TABLE IF NOT EXISTS %s (inventory TEXT, column_name TEXT, data_type TEXT, ordinal_position NUMERIC)', target_table_name);
	-- Iterate through each inventory
	FOR inventory_record IN
		EXECUTE format('SELECT * FROM %s', inventory_source)
	LOOP
		RAISE NOTICE 'Create Column Names- (%) Processing inventory: %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory;
		-- Iterate through each numeric type column, as well as numeric text columns
		FOR column_record IN 
			EXECUTE format('SELECT * FROM getColumnNames(''%s'', ''numeric'', ''%s'')', inventory_record.inventory, schema_name)
		LOOP
			-- Add to table
			RAISE NOTICE 'Create Column Names- (%) 	Added numeric column: %', LEFT(clock_timestamp()::TEXT, 19), column_record.column_name;
			EXECUTE format('INSERT INTO %s SELECT ''%s'' AS inventory, ''%s'' AS column_name, ''numeric'' AS data_type, %s::numeric AS ordinal_position'
			, target_table_name, inventory_record.inventory, column_record.column_name, column_record.ordinal_position);
		END LOOP;
		
		-- Iterate through each text type column
		FOR column_record IN 
			EXECUTE format('SELECT * FROM getColumnNames(''%s'', ''text'', ''%s'')', inventory_record.inventory, schema_name)
		LOOP	
			-- Determine if the column has already been added (ie: a numeric text column)
			EXECUTE format('SELECT ''%s'' IN (SELECT column_name FROM %s WHERE inventory = ''%s'' AND data_type = ''numeric'') FROM %s LIMIT 1'
			, column_record.column_name, target_table_name, inventory_record.inventory, target_table_name) INTO bool_val;
			IF bool_val = FALSE OR bool_val IS NULL
			THEN
				-- Add to table
				RAISE NOTICE 'Create Column Names- (%) 	Added text column: %', LEFT(clock_timestamp()::TEXT, 19), column_record.column_name;
				EXECUTE format('INSERT INTO %s SELECT ''%s'' AS inventory, ''%s'' AS column_name, ''text'' AS data_type, %s::numeric AS ordinal_position'
			, target_table_name, inventory_record.inventory, column_record.column_name, column_record.ordinal_position);
			END IF; 
		END LOOP;
		operation_count := operation_count + 1;
		EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
		RAISE NOTICE 'Numeric Stats- (%) 	Processed inventory ''%'' - %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, msg;
	END LOOP;
	RAISE NOTICE 'Create Column Names- (%) Function completed', LEFT(clock_timestamp()::TEXT, 19);
END;
$func$;


-- getNumericValsForColumn(col, table_name, schema_name, ordinal_pos)
-- Given a column name and table, returns a table containing the min, max, avg, median, mode, and stddev of all numeric values in the column
-- Input:
	-- col: column name to generate stats for ex: 'species_1'
	-- table_name: the table to search in. ex: 'AB29'
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
	-- ordinal_pos: the column index of the specified column
-- Output: TABLE (inventory TEXT, column_name TEXT, min NUMERIC, max NUMERIC, avg NUMERIC, median NUMERIC, mode NUMERIC, stddev NUMERIC, ordinal_position NUMERIC, null_count NUMERIC, total_vals NUMERIC)
	-- inventory: the table/inventory that the column is in
	-- column_name: the name of the column
	-- min, max, avg, median, mode, stddev: self explanatory stats on each value in the column
	-- ordinal_position: the column index (to be used when sorting table)
	-- null_count: count of null values in the column
	-- total_vals: total count of items in the table (to generate a null percentage if desired)
CREATE OR REPLACE FUNCTION getNumericValsForColumn(col TEXT, table_name TEXT, schema_name TEXT, ordinal_pos NUMERIC)
RETURNS TABLE (inventory TEXT, column_name TEXT, min NUMERIC, max NUMERIC, avg NUMERIC, median NUMERIC, mode NUMERIC, stddev NUMERIC, ordinal_position NUMERIC, null_count NUMERIC, total_vals NUMERIC)
LANGUAGE plpgsql AS
$func$
BEGIN
	RETURN QUERY 
    EXECUTE format(
        'SELECT ''%s'' AS inventory,
				''%s'' AS column_name, 
                min(%s::numeric)::numeric AS min, 
                max(%s::numeric)::numeric AS max, 
                avg(%s::numeric)::numeric AS avg, 
                (percentile_cont(0.5) WITHIN GROUP (ORDER BY %s::numeric))::numeric AS median, 
				(mode() WITHIN GROUP (ORDER BY %s::numeric))::numeric as mode,
                stddev(%s::numeric)::numeric AS stddev,
				%s::numeric as ordinal_position,
				sum(CASE WHEN %s IS NULL THEN 1 ELSE 0 END)::numeric AS null_count,
				count(*)::numeric AS total_vals
         FROM %s.%s',
        table_name, col, col, col, col, col, col, col, ordinal_pos, col, schema_name, table_name
    );
END;
$func$;


-- getTextValsForColumn(col, table_name, schema_name, ordinal_pos)
-- Given a column name and table, returns a table containing the count of each distinct value in the given column
-- Input:
	-- col: column name to generate stats for ex: 'species_1'
	-- table_name: the table to search in. ex: 'AB29'
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
	-- ordinal_pos: the column index of the specified column
-- Output: TABLE (inventory TEXT, column_name TEXT, value TEXT, count NUMERIC, ordinal_position NUMERIC)
	-- inventory: the table/inventory that the column is in
	-- column_name: the name of the column
	-- value: a distinct value in the specified column and table
	-- count: the count of that distinct value's occurences in the column
	-- ordinal_position: the column index (to be used when sorting table)
CREATE OR REPLACE FUNCTION getTextValsForColumn(col TEXT, table_name TEXT, schema_name TEXT, ordinal_pos NUMERIC)
RETURNS TABLE (inventory TEXT, column_name TEXT, value TEXT, count NUMERIC, ordinal_position NUMERIC)
LANGUAGE plpgsql AS
$func$
BEGIN
	RETURN QUERY 
    EXECUTE format(
        '(SELECT ''%s'' AS inventory,
		 ''%s'' AS column_name, 
		 %s::text AS value, 
		 count(*)::numeric AS count,
		 %s::numeric as ordinal_position
         FROM %s.%s
		 GROUP BY %s)',
        table_name, col, col, ordinal_pos, schema_name, table_name, col
    );
END;
$func$;


-- getDateValsForColumn(col, table_name, schema_name, ordinal_pos)
-- Given a column name and table, returns a table containing the min, max, avg, median_year, and mode_year of all timestamp values in the column
-- Input:
	-- col: column name to generate stats for ex: 'species_1'
	-- table_name: the table to search in. ex: 'AB29'
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
	-- ordinal_pos: the column index of the specified column
-- Output: TABLE (inventory TEXT, column_name TEXT, value TEXT, count NUMERIC, ordinal_position NUMERIC)
	-- inventory: the table/inventory that the column is in
	-- column_name: the name of the column
	-- min, max, avg: self explanatory date stats in the column
	-- median_year, mode_year- self explanatory numeric year stats in the column
	-- ordinal_position: the column index (to be used when sorting table)
CREATE OR REPLACE FUNCTION getDateValsForColumn(col TEXT, table_name TEXT, schema_name TEXT, ordinal_pos NUMERIC)
RETURNS TABLE (inventory TEXT, column_name TEXT, min DATE, max DATE, avg DATE, median_year NUMERIC, mode_year NUMERIC, ordinal_position NUMERIC)
LANGUAGE plpgsql AS
$func$
BEGIN
	RETURN QUERY 
    EXECUTE format(
        '(SELECT ''%s'' AS inventory, ''%s'' AS column_name, 
		min(%s)::date, 
		max(%s)::date, 
		to_timestamp(avg(EXTRACT(epoch FROM %s)))::date AS avg,
		(percentile_cont(0.5) WITHIN GROUP (ORDER BY date_part(''year'', %s)))::numeric AS median_year, 
		(mode() WITHIN GROUP (ORDER BY date_part(''year'', %s)))::numeric AS mode_year,
		%s::numeric as ordinal_position
		FROM %s.%s)',
        table_name, col, col, col, col, col, col, ordinal_pos, schema_name, table_name
    );
END;
$func$;


-- generateRawNumericStats(inventory_source, result_table_name, schema_name, column_names_source)
-- Overall function to automatically generate a table of raw numeric stats
-- Input:
	-- inventory_source: the source of the list of inventories. Must be a table in the form (inventory TEXT)
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, or a value list are all appropriate
		-- ex: 'getListInvs()'
	-- target_table_name: the name of the table to be created. ex: 'result_table'. 
		-- If the target table already exists, it will be dropped
		-- Note that the table will be created in your search path's default schema, NOT the specified schema
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
	-- column_names_source: the source of the list of columns per each inventory. Must be in the form (inventory TEXT, column_name TEXT, data_type TEXT, ordinal_position NUMERIC)
		-- Intended to be a result from createColumnNamesList()
-- Output: VOID
	-- Generates table with result_table_name in form of TABLE (inventory TEXT, column_name TEXT, min NUMERIC, max NUMERIC, avg NUMERIC, median NUMERIC, mode NUMERIC, stddev NUMERIC, ordinal_position NUMERIC, null_count NUMERIC, total_vals NUMERIC)
		-- inventory: the table/inventory that the column is in
		-- column_name: the name of the column
		-- min, max, avg, median, mode, stddev: self explanatory stats on each value in the column
		-- ordinal_position: the column index (to be used when sorting table)
		-- null_count: count of null values in the column
		-- total_vals: total count of items in the table (to generate a null percentage if desired)
CREATE OR REPLACE FUNCTION generateRawNumericStats(inventory_source TEXT, result_table_name TEXT, schema_name TEXT, column_names_source TEXT)
RETURNS VOID 
LANGUAGE plpgsql AS
$func$
DECLARE
    inventory_record RECORD;
    column_record RECORD;
    total_operations INTEGER;
    start_time TIMESTAMPTZ;
   	operation_count INTEGER;
   	temp_count INTEGER;
    msg TEXT;
BEGIN
	RAISE NOTICE 'Numeric Stats- (%) Function started ----------', LEFT(clock_timestamp()::TEXT, 19);
	start_time := now();
	operation_count := 0;
	total_operations := 0;
	RAISE NOTICE 'Numeric Stats- (%) Estimating function cost', LEFT(clock_timestamp()::TEXT, 19);
	FOR inventory_record IN
        EXECUTE format('SELECT * FROM %s', inventory_source)
    LOOP
    	EXECUTE format('SELECT count(*)::integer FROM %s WHERE inventory = ''%s'' AND data_type = ''numeric''', column_names_source, inventory_record.inventory) INTO temp_count;
    	total_operations := total_operations + temp_count;
    END LOOP;
   
	EXECUTE format('DROP TABLE IF EXISTS %s CASCADE', result_table_name);
	EXECUTE format('CREATE TABLE IF NOT EXISTS %s (inventory TEXT, column_name TEXT, min NUMERIC, max NUMERIC, avg NUMERIC, median NUMERIC, mode NUMERIC, stddev NUMERIC, ordinal_position NUMERIC, null_count NUMERIC, total_vals NUMERIC)', result_table_name);
    -- Iterate through each inventory
    FOR inventory_record IN
        EXECUTE format('SELECT * FROM %s', inventory_source)
    LOOP
        
        RAISE NOTICE 'Numeric Stats- (%) Processing inventory: %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory;
        
        FOR column_record IN
			EXECUTE format('SELECT * FROM %s WHERE inventory = ''%s'' AND data_type = ''numeric'' ORDER BY ordinal_position', column_names_source, inventory_record.inventory)
		LOOP 
			operation_count := operation_count + 1;
			-- Insert into table with result from getNumericValsForColumn() function with current column
			EXECUTE format('INSERT INTO %s SELECT * FROM getNumericValsForColumn(''%s'', ''%s'', ''%s'', %s)', result_table_name, column_record.column_name, inventory_record.inventory, schema_name, column_record.ordinal_position);
			EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
			RAISE NOTICE 'Numeric Stats- (%) 	Processed column ''%'' - %', LEFT(clock_timestamp()::TEXT, 19), column_record.column_name, msg;
		END LOOP;
		RAISE NOTICE 'Numeric Stats- (%) Finished processing inventory: %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory;
    END LOOP;
   RAISE NOTICE 'Numeric Stats- (%) Function completed', LEFT(clock_timestamp()::TEXT, 19);
END;
$func$;


-- generateRawTextStats(inventory_source, result_table_name, schema_name, column_names_source)
-- Overall function to automatically generate a table of raw text stats
-- Input:
	-- inventory_source: the source of the list of inventories. Must be a table in the form (inventory TEXT)
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, or a value list are all appropriate
		-- ex: 'getListInvs()'
	-- target_table_name: the name of the table to be created. ex: 'result_table'. 
		-- If the target table already exists, it will be dropped
		-- Note that the table will be created in your search path's default schema, NOT the specified schema
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
	-- column_names_source: the source of the list of columns per each inventory. Must be in the form (inventory TEXT, column_name TEXT, data_type TEXT, ordinal_position NUMERIC)
		-- Intended to be a result from createColumnNamesList()
-- Output: VOID
	-- Generates table with result_table_name in form of TABLE (inventory TEXT, column_name TEXT, value TEXT, count NUMERIC, ordinal_position NUMERIC)
		-- inventory: the table/inventory that the column is in
		-- column_name: the name of the column
		-- value: a distinct value in the specified column and table
		-- count: the count of that distinct value's occurences in the column
		-- ordinal_position: the column index (to be used when sorting table)
CREATE OR REPLACE FUNCTION generateRawTextStats(inventory_source TEXT, result_table_name TEXT, schema_name TEXT, column_names_source TEXT)
RETURNS VOID 
LANGUAGE plpgsql AS
$func$
DECLARE
    inventory_record RECORD;
    column_record RECORD;
    total_operations INTEGER;
    start_time TIMESTAMPTZ;
   	operation_count INTEGER;
   	temp_count INTEGER;
    msg TEXT;
BEGIN
	RAISE NOTICE 'Text Stats- (%) Function started ----------', LEFT(clock_timestamp()::TEXT, 19);
	start_time := now();
	operation_count := 0;
	total_operations := 0;
	RAISE NOTICE 'Text Stats- (%) Estimating function cost', LEFT(clock_timestamp()::TEXT, 19);
	FOR inventory_record IN
        EXECUTE format('SELECT * FROM %s', inventory_source)
    LOOP
    	EXECUTE format('SELECT count(*)::integer FROM %s WHERE inventory = ''%s'' AND data_type = ''text''', column_names_source, inventory_record.inventory) INTO temp_count;
    	total_operations := total_operations + temp_count;
    END LOOP;
   
    EXECUTE format('DROP TABLE IF EXISTS %s CASCADE', result_table_name);
	EXECUTE format('CREATE TABLE IF NOT EXISTS %s (inventory TEXT, column_name TEXT, value TEXT, count NUMERIC, ordinal_position NUMERIC)', result_table_name);
   
    -- Iterate through each inventory
    FOR inventory_record IN
        EXECUTE format('SELECT * FROM %s', inventory_source)
    LOOP
        
        RAISE NOTICE 'Text Stats- (%) Processing inventory: %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory;
        
		FOR column_record IN
			EXECUTE format('SELECT * FROM %s WHERE inventory = ''%s'' AND data_type = ''text'' AND column_name NOT IN (SELECT column_name FROM raw_ignored_text_columns) ORDER BY ordinal_position'
			, column_names_source, inventory_record.inventory)
		LOOP 
			operation_count := operation_count + 1;
			-- Insert into table with result from getTextValsForColumn() function with current column
			EXECUTE format('INSERT INTO %s SELECT * FROM getTextValsForColumn(''%s'', ''%s'', ''%s'', %s)', result_table_name, column_record.column_name, inventory_record.inventory, schema_name, column_record.ordinal_position);
			EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
			RAISE NOTICE 'Text Stats- (%) 	Processed column ''%'' - %', LEFT(clock_timestamp()::TEXT, 19), column_record.column_name, msg;
		END LOOP;
		RAISE NOTICE 'Text Stats- (%) Processed inventory: %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory;
    END LOOP;
   RAISE NOTICE 'Text Stats- (%) Function completed', LEFT(clock_timestamp()::TEXT, 19);
END;
$func$;


-- generateRawDateStats(inventory_source, result_table_name, schema_name)
-- Overall function to automatically generate a table of raw date stats
-- Input:
	-- inventory_source: the source of the list of inventories. Must be a table in the form (inventory TEXT)
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, or a value list are all appropriate
		-- ex: 'getListInvs()'
	-- target_table_name: the name of the table to be created. ex: 'result_table'. 
		-- If the target table already exists, it will be dropped
		-- Note that the table will be created in your search path's default schema, NOT the specified schema
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
-- Output: VOID
	-- Generates table with result_table_name in form of TABLE (inventory TEXT, column_name TEXT, value TEXT, count NUMERIC, ordinal_position NUMERIC)
		-- inventory: the table/inventory that the column is in
		-- column_name: the name of the column
		-- min, max, avg: self explanatory date stats in the column
		-- median_year, mode_year- self explanatory numeric year stats in the column
		-- ordinal_position: the column index (to be used when sorting table)
CREATE OR REPLACE FUNCTION generateRawDateStats(inventory_source TEXT, result_table_name TEXT, schema_name TEXT)
RETURNS VOID 
LANGUAGE plpgsql AS
$func$
DECLARE
    inventory_record RECORD;
    column_record RECORD;
    total_operations INTEGER;
    start_time TIMESTAMPTZ;
   	operation_count INTEGER;
   	temp_count INTEGER;
    msg TEXT;
BEGIN
	RAISE NOTICE 'Date Stats- (%) Function started ----------', LEFT(clock_timestamp()::TEXT, 19);
	start_time := now();
	operation_count := 0;
	total_operations := 0;
	RAISE NOTICE 'Date Stats- (%) Estimating function cost', LEFT(clock_timestamp()::TEXT, 19);
	FOR inventory_record IN
        EXECUTE format('SELECT * FROM %s', inventory_source)
    LOOP
    	EXECUTE format('SELECT count(*)::integer FROM getColumnNames(''%s'', ''date'', ''%s'')', inventory_record.inventory, schema_name) INTO temp_count;
    	total_operations := total_operations + temp_count;
    END LOOP;
    
    EXECUTE format('DROP TABLE IF EXISTS %s CASCADE', result_table_name);
	EXECUTE format('CREATE TABLE IF NOT EXISTS %s (inventory TEXT, column_name TEXT, min DATE, max DATE, avg DATE, median_year NUMERIC, mode_year NUMERIC, ordinal_position NUMERIC)', result_table_name);
    
	-- Iterate through each inventory containing dates
    FOR inventory_record IN
        EXECUTE format('SELECT * FROM %s', inventory_source)
    LOOP
        
        RAISE NOTICE 'Date Stats- (%) Processing inventory: %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory;
        
		FOR column_record IN
			EXECUTE format('SELECT * FROM getColumnNames(''%s'', ''date'', ''%s'') ORDER BY ordinal_position', inventory_record.inventory, schema_name)
		LOOP 
			operation_count := operation_count + 1;
			-- Insert into table with result from getDateValsForColumn() function with current column
			EXECUTE format('INSERT INTO %s SELECT * FROM getDateValsForColumn(''%s'', ''%s'', ''%s'', %s)', result_table_name, column_record.column_name, inventory_record.inventory, schema_name, column_record.ordinal_position);
			EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', operation_count, total_operations, start_time::text) INTO msg;
			RAISE NOTICE 'Date Stats- (%) 	Processed column ''%'' - %', LEFT(clock_timestamp()::TEXT, 19), column_record.column_name, msg;
		END LOOP;
	
		RAISE NOTICE 'Date Stats- (%) Processed inventory: %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory;
        
    END LOOP;
   RAISE NOTICE 'Date Stats- (%) Function completed', LEFT(clock_timestamp()::TEXT, 19);
END;
$func$;


-- generateRawGeoStats(inventory_source, result_table_name, schema_name)
-- Overall function to automatically generate a table of raw geometry stats
-- Input:
	-- inventory_source: the source of the list of inventories. Must be a table in the form (inventory TEXT)
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, or a value list are all appropriate
		-- ex: 'getListInvs()'
	-- target_table_name: the name of the table to be created. ex: 'result_table'. 
		-- If the target table already exists, it will be dropped
		-- Note that the table will be created in your search path's default schema, NOT the specified schema
	-- schema_name: the name of the schema to search in, as text. ex: 'rawfri'
-- Output: VOID
	-- Generates table with result_table_name in form of 
	-- TABLE (inventory TEXT, mean_area_m2 NUMERIC, median_area_m2 NUMERIC, mode_area_m2 NUMERIC, 
	--		mean_vertex_count NUMERIC, median_vertex_count NUMERIC, mode_vertex_count NUMERIC, mean_perimeter_m2 NUMERIC, median_perimeter_m2 NUMERIC, mode_perimeter_m2 NUMERIC, 
	--		mean_thinness_ratio NUMERIC, median_thinness_ratio NUMERIC, mode_thinness_ratio NUMERIC)
		-- inventory: the table/inventory that the wkg_geometry column is in
		-- area: area of the geometry
		-- vertex_count: count of verticies
		-- perimeter: distance around perimeter of geometry
		-- thinness ratio: formula is 4 * pi * area/(length*^2). A ratio of 0 would be a line, and a 1 would be a perfect circle.
CREATE OR REPLACE FUNCTION generateRawGeoStats(inventory_source TEXT, result_table_name TEXT, schema_name TEXT)
RETURNS VOID 
LANGUAGE plpgsql AS
$func$
DECLARE
    inventory_record RECORD;
   	start_time TIMESTAMPTZ;
    total_invs INTEGER;
   	inv_count INTEGER;
    msg TEXT;
BEGIN
	RAISE NOTICE 'Geo Stats- (%) Function started ----------', LEFT(clock_timestamp()::TEXT, 19);
	start_time := now();
	inv_count := 0;
	EXECUTE format('SELECT count(*)::integer FROM %s', inventory_source) INTO total_invs;
    -- Geometries are handled a little differently since there is only a wkg_geometry column for each inventory, so no external functions are needed to iterate through columns
    EXECUTE format('DROP TABLE IF EXISTS %s CASCADE', result_table_name);
	EXECUTE format('CREATE TABLE IF NOT EXISTS %s (inventory TEXT, mean_area_m2 NUMERIC, median_area_m2 NUMERIC, mode_area_m2 NUMERIC, 
	mean_vertex_count NUMERIC, median_vertex_count NUMERIC, mode_vertex_count NUMERIC, mean_perimeter_m2 NUMERIC, median_perimeter_m2 NUMERIC, mode_perimeter_m2 NUMERIC, 
	mean_thinness_ratio NUMERIC, median_thinness_ratio NUMERIC, mode_thinness_ratio NUMERIC)', result_table_name);
	-- Iterate through each inventory
    FOR inventory_record IN
        EXECUTE format ('SELECT * FROM %s', inventory_source)
    LOOP
        
       RAISE NOTICE 'Geo Stats- (%) Processing inventory: %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory;
       -- Insert geo stats for the inventory into the table
        EXECUTE format('INSERT INTO %s SELECT ''%s'' AS inventory,
		avg(ST_area(wkb_geometry)) AS mean_area_m2,
		PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY ST_area(wkb_geometry)) AS median_area_m2,
		mode() WITHIN GROUP (ORDER BY ST_area(wkb_geometry)) AS mode_area_m2,
		avg(ST_NPoints(wkb_geometry)) AS mean_vertex_count,
		PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY ST_NPoints(wkb_geometry)) AS median_vertex_count,
		mode() WITHIN GROUP (ORDER BY ST_NPoints(wkb_geometry)) AS mode_vertex_count,
		avg(ST_Perimeter(wkb_geometry)) AS mean_perimeter_m2,
		PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY ST_Perimeter(wkb_geometry)) AS median_perimeter_m2,
		mode() WITHIN GROUP (ORDER BY ST_Perimeter(wkb_geometry)) AS mode_perimeter_m2,
		avg(4*pi()*ST_area(wkb_geometry)/power(ST_Perimeter(wkb_geometry), 2)) AS mean_thinness_ratio,
		PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY (4*pi()*ST_area(wkb_geometry)/power(ST_Perimeter(wkb_geometry), 2))) AS median_thinness_ratio,
		mode() WITHIN GROUP (ORDER BY (4*pi()*ST_area(wkb_geometry)/power(ST_Perimeter(wkb_geometry), 2))) AS mode_thinness_ratio
		FROM %s.%s', result_table_name, inventory_record.inventory, schema_name, inventory_record.inventory);
		inv_count := inv_count + 1;
		EXECUTE format('SELECT * FROM getProgressMessage(%s, %s, TO_TIMESTAMP(''%s'', ''YYYY-MM-DD HH24:MI:SS''))', inv_count, total_invs, start_time::text) INTO msg;
		RAISE NOTICE 'Geo Stats- (%) Processed inventory: % - %', LEFT(clock_timestamp()::TEXT, 19), inventory_record.inventory, msg;
    END LOOP;
   RAISE NOTICE 'Geo Stats- (%) Function completed', LEFT(clock_timestamp()::TEXT, 19);
END;
$func$;


-- Calls to drop existing tables
DROP TABLE IF EXISTS raw_numeric_stats;
DROP TABLE IF EXISTS raw_text_stats CASCADE;
DROP TABLE IF EXISTS raw_geo_stats;
DROP TABLE IF EXISTS raw_date_stats;


-- Generate the helper column table tables
SELECT * FROM createColumnNamesList('inventory_column_names', 'rawfri', 'getListInvs()');

SELECT * FROM generateRawNumericStats('getListInvs()', 'raw_numeric_stats', 'rawfri', 'inventory_column_names');
SELECT * FROM generateRawTextStats('getListInvs()', 'raw_text_stats', 'rawfri', 'inventory_column_names');
SELECT * FROM generateRawDateStats('(WITH data_types AS (
			SELECT DISTINCT table_name AS inventory, data_type
			FROM information_schema.COLUMNS
			WHERE table_schema = ''rawfri''
		)
		SELECT DISTINCT i.inventory
		FROM (SELECT * FROM getlistinvs()) AS i
		INNER JOIN data_types d ON lower(d.inventory) = lower(i.inventory)
		WHERE d.data_type = ''timestamp with time zone''
		ORDER BY i.inventory) as foo', 'raw_date_stats', 'rawfri');
SELECT * FROM generateRawGeoStats('getListInvs()', 'raw_geo_stats', 'rawfri');

-- Uncomment if you would like to wipe the table after running scripts
--DROP TABLE IF EXISTS inventory_column_names;

-- raw_text_stats_reduced is reduced version of raw_text_stats that excludes any columns with 100 or more distinct values, and columns with only 1 distinct value
-- This view is intended to be used in grafana to speed up processing and ensure that graphs are readable
CREATE MATERIALIZED VIEW IF NOT EXISTS raw_text_stats_reduced AS 
WITH val_counts AS (
	SELECT inventory, column_name, count(*) AS count
	FROM raw_text_stats
	GROUP BY inventory, column_name
)
SELECT r.*
FROM raw_text_stats r
INNER JOIN val_counts v ON v.column_name = r.column_name AND v.inventory = r.inventory
WHERE v.count < 100 AND v.count != 1;


-- raw_text_stats_solo contains only the columns with a singular distinct value
-- This view is intended to be used in grafana to display singular values differently than multi-valued columns
CREATE MATERIALIZED VIEW IF NOT EXISTS raw_text_stats_solo AS 
WITH val_counts AS (
	SELECT inventory, column_name, count(*) AS count
	FROM raw_text_stats
	GROUP BY inventory, column_name
)
SELECT r.*
FROM raw_text_stats r
INNER JOIN val_counts v ON v.column_name = r.column_name AND v.inventory = r.inventory
WHERE v.count = 1;

