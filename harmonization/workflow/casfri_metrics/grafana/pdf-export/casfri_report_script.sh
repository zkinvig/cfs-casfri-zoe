# To run, navigate to the /harmonization/workflow/casfri_metrics/grafana/pdf-export directory
# Then run 'bash casfri_report_script.sh'
# This script creates ALL casfri reports. If you would like to create an individual report, either use the 'Export' button in the CASFRI UI,
# or run a curl command with the correct dashboard ID, file names, and variable values where applicable
# Script takes approximately 9 hours to run

GRAF_KEY=glsa_IrgkbKghoY5SR638LEZAFT9cjiRp8ckE_8979b1e3
GRAF_URL=http://localhost:8000
DATE="$(date +'%m-%d-%Y')"

# Make general casfri reports that don't have variables to loop through
mkdir casfri_reports_${DATE}

echo "Creating casfri report"
DASH_ID=cdy70c3lc2ghsa
curl -o casfri_reports_${DATE}/casfri_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}"  2>&1 | tee logs/casfri_${DATE}.txt

echo "Creating casfri disturbance report"
DASH_ID=fdy70dkl6ik1se
curl -o casfri_reports_${DATE}/casfri_dists_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}&from=now-110y&to=now"  2>&1 | tee logs/casfri_dists_${DATE}.txt

echo "Creating casfri species report"
DASH_ID=ddy70hzn09i4gb
curl -o casfri_reports_${DATE}/casfri_species_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}"  2>&1 | tee logs/casfri_species_${DATE}.txt

echo "Creating example raw inventory report"
# Curently we just create a small example of a raw inventory report
DASH_ID=cdyab97v0ptz4f
curl -o casfri_reports_${DATE}/raw_inventory_example_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}"  2>&1 | tee logs/raw_inventory_example_${DATE}.txt

echo "Creating raw validation report"
DASH_ID=be0kti07mb1tsb
curl -o casfri_reports_${DATE}/casfri_validation_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}"  2>&1 | tee logs/casfri_validation_${DATE}.txt


# Make reports that must loop through each jurisdiction

mkdir jurisdiction_reports_${DATE}
mkdir jurisdiction_species_reports_${DATE}
mkdir jurisdiction_dist_reports_${DATE}

declare -A juris
juris[AB]="AB"
juris[BC]="BC"
juris[MB]="MB"
juris[NB]="NB"
juris[NS]="NS"
juris[NT]="NT"
juris[ON]="ON"
juris[PE]="PE"
juris[QC]="QC"
juris[YT]="YT"


echo "Starting jurisdiction loop"
for key in "${!juris[@]}"; do
	
	echo "Creating ${key} report"
 	DASH_ID=ady70g2olo7b4d
 	echo jurisdiction_reports_${DATE}/${key}_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
 	"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}&var-jurisdiction=${key}"

 	curl -o jurisdiction_reports_${DATE}/${key}_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
 	"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}&var-jurisdiction=${key}"  2>&1 | tee logs/${key}_${DATE}.txt
 	
	echo "Creating ${key} species report"
	DASH_ID=cdy70haa2vgn4e
 	curl -o jurisdiction_species_reports_${DATE}/${key}_species_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
 	"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}&var-jurisdiction=${key}"  2>&1 | tee logs/${key}_species_${DATE}.txt

	echo "Creating ${key} disturbance report"
	DASH_ID=edy70gpisr0n4f
 	curl -o jurisdiction_dist_reports_${DATE}/${key}_dists_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
 	"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}&var-jurisdiction=${key}&from=now-110y&to=now"  2>&1 | tee logs/${key}_dists_${DATE}.txt
done

# Make reports that must loop through each inventory
mkdir inventory_reports_${DATE}

declare -A inv
inv[AB29]=”AB29“
inv[AB30]=”AB30“
inv[BC08]=”BC08“
inv[BC10]=”BC10“
inv[BC11]=”BC11“
inv[BC12]=”BC12“
inv[BC13]=”BC13“
inv[BC14]=”BC14“
inv[BC15]=”BC15“
inv[BC16]=”BC16“
inv[MB03]=”MB03“
inv[MB06]=”MB06“
inv[MB07]=”MB07“
inv[MB10]=”MB10“
inv[MB11]=”MB11“
inv[MB12]=”MB12“
inv[MB13]=”MB13“
inv[NB02]=”NB02“
inv[NB03]=”NB03“
inv[NS03]=”NS03“
inv[NS04]=”NS04“
inv[NT03]=”NT03“
inv[NT04]=”NT04“
inv[ON02]=”ON02“
inv[PE01]=”PE01“
inv[PE02]=”PE02“
inv[PE03]=”PE03“
inv[PE04]=”PE04“
inv[QC03]=”QC03“
inv[QC04]=”QC04“
inv[QC05]=”QC05“
inv[QC08]=”QC08“
inv[QC09]=”QC09“
inv[QC10]=”QC10“
inv[YT03]=”YT03“
inv[YT04]=”YT04“

echo "Starting inventory loop"
for key in "${!inv[@]}"; do
	
	echo "Creating ${key} report"
 	DASH_ID=cdy70erpxwa2of
 	curl -o inventory_reports_${DATE}/${key}_${DATE}.pdf -H "Authorization: Bearer ${GRAF_KEY}" \
 	"${GRAF_URL}/api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=${DASH_ID}&var-inventory=${key}"  2>&1 | tee logs/${key}_${DATE}.txt
done





