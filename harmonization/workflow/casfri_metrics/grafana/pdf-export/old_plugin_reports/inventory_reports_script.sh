# To run, navigate to the /harmonization/workflow/casfri_metrics/grafana/pdf-export directory
# Then run 'bash inventory_reports_script.sh'

GRAF_TUNNEL_PORT=8000
GRAF_RUN_HEADLESS=1
GRAF_KEY=glsa_7JVGe8SqjJXJoOPxl91dCk8SLJTns7z3_394e3c94
GRAF_IP=localhost:$GRAF_TUNNEL_PORT
GRAF_TEMPLATES=latex_templates
GRAF_TEMPLATE=inventory_template
DATE="$(date +'%m-%d-%Y')"
GRAF_OUTPUT_DIR=inventory_reports_$DATE
OUTPUT_SUFFIX=$DATE.pdf
# You can specify variable values in the time span command argument since this is just passed into the URL
TIME_SPAN="from=now-3h&to=now&var-inventory="
DASH="cdsnjtlx3jsw0a"

declare -A inv

inv[AB29]=”AB29“
inv[AB30]=”AB30“
inv[BC08]=”BC08“
inv[BC10]=”BC10“
inv[BC11]=”BC11“
inv[BC12]=”BC12“
inv[BC13]=”BC13“
inv[BC14]=”BC14“
inv[BC15]=”BC15“
inv[BC16]=”BC16“
inv[MB03]=”MB03“
inv[MB06]=”MB06“
inv[MB07]=”MB07“
inv[MB10]=”MB10“
inv[MB11]=”MB11“
inv[MB12]=”MB12“
inv[MB13]=”MB13“
inv[NB02]=”NB02“
inv[NB03]=”NB03“
inv[NS03]=”NS03“
inv[NS04]=”NS04“
inv[NT03]=”NT03“
inv[NT04]=”NT04“
inv[ON02]=”ON02“
inv[PE01]=”PE01“
inv[PE02]=”PE02“
inv[PE03]=”PE03“
inv[PE04]=”PE04“
inv[QC03]=”QC03“
inv[QC04]=”QC04“
inv[QC05]=”QC05“
inv[QC08]=”QC08“
inv[QC09]=”QC09“
inv[QC10]=”QC10“
inv[YT03]=”YT03“
inv[YT04]=”YT04“


mkdir $GRAF_OUTPUT_DIR

for key in "${!inv[@]}"; do

  output_file=${key}_${OUTPUT_SUFFIX}

  grafana-reporter \
    -cmd_enable=$GRAF_RUN_HEADLESS \
    -cmd_apiKey $GRAF_KEY \
    -cmd_ts ${TIME_SPAN}${key} \
    -ip  $GRAF_IP\
    -cmd_dashboard $DASH \
    -cmd_o $GRAF_OUTPUT_DIR/$output_file \
    -grid-layout=1 \
    -cmd_template $GRAF_TEMPLATE \
    -templates $GRAF_TEMPLATES \
     2>&1 | tee logs/${key}_${DATE}.txt
done
