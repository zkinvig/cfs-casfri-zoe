# To run, navigate to the /harmonization/workflow/casfri_metrics/grafana/pdf-export directory
# Then run 'bash disturbance_report_script.sh'

GRAF_TUNNEL_PORT=8000
GRAF_RUN_HEADLESS=1
GRAF_KEY=glsa_7JVGe8SqjJXJoOPxl91dCk8SLJTns7z3_394e3c94
GRAF_IP=localhost:$GRAF_TUNNEL_PORT
GRAF_TEMPLATES=latex_templates
GRAF_TEMPLATE=disturbance_template
DATE="$(date +'%m-%d-%Y')"
GRAF_OUTPUT_DIR=casfri_reports_$DATE
OUTPUT_SUFFIX=$DATE.pdf
TIME_SPAN="from=now-135y&to=now"
DASH="cdsnyjz1xk5xca"
FILE_NAME="casfri_dists"

mkdir $GRAF_OUTPUT_DIR

output_file=${FILE_NAME}_${OUTPUT_SUFFIX}

grafana-reporter \
  -cmd_enable=$GRAF_RUN_HEADLESS \
  -cmd_apiKey $GRAF_KEY \
  -ip  $GRAF_IP\
  -cmd_ts ${TIME_SPAN} \
  -cmd_dashboard $DASH \
  -cmd_o $GRAF_OUTPUT_DIR/$output_file \
  -grid-layout=1 \
  -cmd_template $GRAF_TEMPLATE \
  -templates $GRAF_TEMPLATES \
  2>&1 | tee logs/${FILE_NAME}_${DATE}.txt

