# To run, navigate to the /harmonization/workflow/casfri_metrics/grafana/pdf-export directory
# Then run 'bash generate_all.sh'


bash casfri_report_script.sh
bash disturbance_report_script.sh
bash species_report_script.sh
bash inventory_reports_script.sh
bash jurisdiction_reports_script.sh
bash jurisdiction_disturbance_reports_script.sh
bash jurisdiction_species_reports_script.sh
