-- Regenerating all attribute_count helper tables

DROP TABLE IF EXISTS casfri_metrics.non_error_cas_all_attribute_counts_grafana_area;
DROP TABLE IF EXISTS casfri_metrics.non_error_cas_all_attribute_counts_grafana_count;
DROP TABLE IF EXISTS casfri_metrics.non_error_dst_all_attribute_counts_grafana_area;
DROP TABLE IF EXISTS casfri_metrics.non_error_dst_all_attribute_counts_grafana_count;
DROP TABLE IF EXISTS casfri_metrics.non_error_eco_all_attribute_counts_grafana_area;
DROP TABLE IF EXISTS casfri_metrics.non_error_eco_all_attribute_counts_grafana_count;
DROP TABLE IF EXISTS casfri_metrics.non_error_lyr_all_attribute_counts_grafana_area;
DROP TABLE IF EXISTS casfri_metrics.non_error_lyr_all_attribute_counts_grafana_count;
DROP TABLE IF EXISTS casfri_metrics.non_error_nfl_all_attribute_counts_grafana_area;
DROP TABLE IF EXISTS casfri_metrics.non_error_nfl_all_attribute_counts_grafana_count;
DROP TABLE IF EXISTS casfri_metrics.non_error_geo_all_attribute_counts_grafana_area_m2;
DROP TABLE IF EXISTS casfri_metrics.non_error_geo_all_attribute_counts_grafana_vertex_count;
DROP TABLE IF EXISTS casfri_metrics.non_error_geo_all_attribute_counts_grafana_thinness_ratio;
DROP TABLE IF EXISTS casfri_metrics.non_error_geo_all_attribute_counts_grafana_perimeter_m2;

-- cas_all
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_cas_all_attribute_counts', 'non_error_cas_all_attribute_counts_grafana_area', 'area_ha');
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_cas_all_attribute_counts', 'non_error_cas_all_attribute_counts_grafana_count', 'count');

-- dst_all
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_dst_all_attribute_counts', 'non_error_dst_all_attribute_counts_grafana_area', 'area_ha');
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_dst_all_attribute_counts', 'non_error_dst_all_attribute_counts_grafana_count', 'count');

-- eco_all
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_eco_all_attribute_counts', 'non_error_eco_all_attribute_counts_grafana_area', 'area_ha');
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_eco_all_attribute_counts', 'non_error_eco_all_attribute_counts_grafana_count', 'count');

-- lyr_all
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_lyr_all_attribute_counts', 'non_error_lyr_all_attribute_counts_grafana_area', 'area_ha');
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_lyr_all_attribute_counts', 'non_error_lyr_all_attribute_counts_grafana_count', 'count');

-- nfl_all
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_nfl_all_attribute_counts', 'non_error_nfl_all_attribute_counts_grafana_area', 'area_ha');
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_nfl_all_attribute_counts', 'non_error_nfl_all_attribute_counts_grafana_count', 'count');

-- geo_all
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_geo_all_attribute_counts', 'non_error_geo_all_attribute_counts_grafana_area_m2', 'area_m2');
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_geo_all_attribute_counts', 'non_error_geo_all_attribute_counts_grafana_vertex_count', 'vertex_count');
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_geo_all_attribute_counts', 'non_error_geo_all_attribute_counts_grafana_thinness_ratio', 'thinness_ratio');
SELECT * FROM attribute_counts_table_converter('casfri_metrics', 'non_error_geo_all_attribute_counts', 'non_error_geo_all_attribute_counts_grafana_perimeter_m2', 'perimeter_m2');



