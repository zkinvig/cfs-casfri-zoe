CREATE SCHEMA IF NOT EXISTS casfri_metrics;

---- Country Summary -----
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.inventory_summary_latest AS
	WITH cas_count as (SELECT inventory_id, count(*) as num from casfri_web.cas_latest group by inventory_id),
	lyr_count as (SELECT left(cas_id, 4) as inventory_id, count(*) as num from casfri_web.lyr_latest group by inventory_id),
	nfl_count as (SELECT left(cas_id, 4) as inventory_id, count(*) as num from casfri_web.nfl_latest group by inventory_id),
	dst_count as (SELECT left(cas_id, 4) as inventory_id, count(*) as num from casfri_web.dst_latest group by inventory_id),
	eco_count as (SELECT left(cas_id, 4) as inventory_id, count(*) as num from casfri_web.eco_latest group by inventory_id),
	geo_count as (SELECT left(cas_id, 4) as inventory_id, count(*) as num from casfri_web.geo_latest group by inventory_id)

	SELECT c.inventory_id, c.num, l.num, n.num, d.num, e.num, g.num
	from cas_count c, lyr_count l, nfl_count n, dst_count d, eco_count e, geo_count g
	where c.inventory_id = l.inventory_id OR c.inventory_id = n.inventory_id OR c.inventory_id = d.inventory_id OR c.inventory_id = e.inventory_id OR c.inventory_id = g.inventory_id;

COMMENT ON MATERIALIZED VIEW casfri_metrics.inventory_summary_latest IS
'inventory_summary_latest provides raw feature counts of all 6 CASFRI counts grouped by inventory_id for the most recent cut';

-- counts ------
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.leading_species_by_inv_latest AS
SELECT left(cas_id, 4) as inventory, species_1, count(*) as count
from casfri_web.lyr_latest
where layer = 1
group by inventory, species_1;

COMMENT ON MATERIALIZED VIEW casfri_metrics.leading_species_by_inv_latest IS
'leading_species_by_inv_latest provides feature counts of all layer 1 species grouped by inventory_id for the most recent cut';

CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.leading_species_counts_latest AS
SELECT species_1, count(*)
from casfri_web.lyr_latest
where layer = 1
group by species_1;

COMMENT ON MATERIALIZED VIEW casfri_metrics.leading_species_counts_latest IS
'leading_species_counts_latest provides feature counts of all layer 1 species for the most recent cut';

CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.leading_species_area_latest AS
SELECT species_1, SUM(ST_Area(g.geometry)) as area_m2
FROM casfri_web.lyr_latest l, casfri_web.geo_latest g
where l.layer = 1 and l.cas_id = g.cas_id
group by species_1;

COMMENT ON MATERIALIZED VIEW casfri_metrics.leading_species_area_latest IS
'leading_species_area_latest provides a summation of area(m2) of all layer 1 species for the most recent cut';

----------------- BC Forested area hA & m2 -----
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.bc_inv_forested_area_latest AS
SELECT left(g.cas_id, 4) as inventory, SUM(ST_Area(g.geometry)) as area_m2
FROM casfri_web.lyr_latest l, casfri50.geo_all g
WHERE l.layer = 1 and left(l.cas_id, 2) = 'BC' and l.cas_id = g.cas_id
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW casfri_metrics.bc_inv_forested_area_latest IS
'bc_inv_forested_area_latest provides a summation of area(m2) of all layer 1 species in BC for the most recent cut';

CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.bc_inv_forested_ha_latest AS
    WITH cas_area as (
        SELECT left(c.cas_id, 4) as inv1, SUM(c.casfri_area) as area_hA
        FROM casfri_web.lyr_latest l, casfri_web.cas_latest c
        WHERE l.layer = 1 and left(l.cas_id, 2) = 'BC' and l.cas_id = c.cas_id
        GROUP BY inv1
    ),

    src_area as (
        SELECT left(c.cas_id, 4) as inv2, SUM(c.src_inv_area) as src_area_hA
        FROM casfri_web.lyr_latest l, casfri_web.cas_latest c
        WHERE l.layer = 1 and left(l.cas_id, 2) = 'BC' and c.src_inv_area >= 0.01 and l.cas_id = c.cas_id
        GROUP BY inv2
    )

    SELECT c.inv1, c.area_hA, s.src_area_hA
    FROM cas_area c
    FULL OUTER JOIN src_area s ON c.inv1 = s.inv2;

COMMENT ON MATERIALIZED VIEW casfri_metrics.bc_inv_forested_area_ha_latest IS
'bc_inv_forested_area_ha_latest provides a summation of area(hA) of all layer 1 species in BC for the most recent cut';

--------- All Inventory forested area
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.inv_forested_area_latest AS
SELECT left(g.cas_id, 4) as inventory, SUM(ST_Area(g.geometry)) as area_m2
FROM casfri_web.lyr_latest l, casfri_web.geo_latest g
WHERE l.layer = 1 and l.cas_id = g.cas_id
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW casfri_metrics.inv_forested_area_latest IS
'inv_forested_area_latest provides a summation of area(m2) of all layer 1 species for the most recent cut';

CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.inv_forested_ha_latest AS
    WITH cas_area as (
        SELECT left(c.cas_id, 4) as inv1, SUM(c.casfri_area) as area_hA
        FROM casfri_web.lyr_latest l, casfri_web.cas_latest c
        WHERE l.layer = 1 and l.cas_id = c.cas_id
        GROUP BY inv1
    ),

    src_area as (
        SELECT left(c.cas_id, 4) as inv2, SUM(c.src_inv_area) as src_area_hA
        FROM casfri_web.lyr_latest l, casfri_web.cas_latest c
        WHERE l.layer = 1 and c.src_inv_area >= 0.01 and l.cas_id = c.cas_id
        GROUP BY inv2
    )

    SELECT c.inv1, c.area_hA, s.src_area_hA
    FROM cas_area c
    FULL OUTER JOIN src_area s ON c.inv1 = s.inv2;

COMMENT ON MATERIALIZED VIEW casfri_metrics.inv_forested_ha_latest IS
'inv_forested_ha_latest provides a summation of area(hA) of all layer 1 species for the most recent cut';

---- AREA -------
-- total area (hA) of inventory (can probably use the view below instead)
--select inventory_id, SUM(casfri_area) as area from casfri50.cas_all group by inventory_id

-- inventory coverage of managed forests by prov this will include all layers and nfl
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.inv_coverage_by_prov_latest AS
  WITH managed_coverage as (
    SELECT g.cas_id, g.geometry
    from casfri_web.geo_latest g, casfri_metrics.managed_forests_simple f
    where f.managedfor = 1 and ST_Within( g.geometry, f.geom)
  ),

  cas_area as (
    select c.inventory_id, sum(c.casfri_area) as area
    from casfri_web.cas_latest c, managed_coverage
    where managed_coverage.cas_id = c.cas_id
    group by c.inventory_id
  ),

  total_prov_managed_area as (
    SELECT prov, SUM(area) as area from casfri_metrics.managed_forests_simple
    group by prov
  )

  SELECT
    m.inventory_id,
    m.area as managed_area,
    t.area as province_area
  FROM cas_area m, total_prov_managed_area t
  group by m.inventory_id, t.area, m.area;

COMMENT ON MATERIALIZED VIEW casfri_metrics.inv_coverage_by_prov_latest IS
'inv_coverage_by_prov_latest provides a summation of area(m2) within managed forests by jurisdiction for the most recent cut';

CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.dst_area_by_inv_latest AS
  SELECT c.inventory_id, SUM(c.casfri_area) as area
  from casfri_web.cas_latest c, casfri_web.dst_latest d
  where d.cas_id = c.cas_id
  group by c.inventory_id;

COMMENT ON MATERIALIZED VIEW casfri_metrics.dst_area_by_inv_latest IS
'dst_area_by_inv_latest provides a summation of area(m2) of all disturbance features by inventory for the most recent cut';

--- nfl area by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.nfl_type_area_by_inv_latest AS
  WITH non_for_anth as (
    SELECT c.inventory_id, SUM(c.casfri_area) as area
    from casfri_web.cas_latest c, casfri_web.nfl_latest n
    where n.non_for_anth is not null and n.cas_id = c.cas_id
    group by c.inventory_id
  ),

  nat_non_veg as (
    SELECT c.inventory_id, SUM(c.casfri_area) as area
    from casfri_web.cas_latest c, casfri_web.nfl_latest n
    where n.nat_non_veg is not null and  n.cas_id = c.cas_id
    group by c.inventory_id

  ),

  non_for_veg as (
    SELECT c.inventory_id, SUM(c.casfri_area) as area
    from casfri_web.cas_latest c, casfri_web.nfl_latest n
    where n.non_for_veg is not null and  n.cas_id = c.cas_id
    group by c.inventory_id
  ),

  total_prov_area as (
    SELECT prov, SUM(area) as area from casfri_metrics.managed_forests_simple
    group by prov
  )

  SELECT
    a.inventory_id,
    a.area as anth_area,
    n.area as non_for_veg_area,
    v.area as nat_non_veg_area,
    t.area as province_area
  FROM non_for_anth a
  JOIN nat_non_veg n ON a.inventory_id = n.inventory_id
  JOIN non_for_veg v ON a.inventory_id = v.inventory_id
  JOIN total_prov_area t ON lower(left(a.inventory_id,2)) = t.prov;

COMMENT ON MATERIALIZED VIEW casfri_metrics.nfl_type_area_by_inv_latest IS
'nfl_type_area_by_inv_latest provides a summation of area(m2) of non-forest layer features grouped by nfl type and inventory for the most recent cut';
-------------------------
