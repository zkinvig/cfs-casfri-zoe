DROP TABLE IF EXISTS casfri_web.cas_latest CASCADE;
CREATE TABLE casfri_web.cas_latest AS
    SELECT c.* FROM casfri50.cas_all c, casfri_post_processing.casfri_latest_unique l
    where c.cas_id = l.cas_id;

CREATE INDEX cas_latest_casid_idx ON casfri_web.cas_latest USING btree(cas_id);
CREATE INDEX cas_latest_inventory_idx ON casfri_web.cas_latest USING btree(left(cas_id, 4));
CREATE INDEX cas_all_inventoryid_idx ON casfri_web.cas_latest USING btree(inventory_id);

DROP TABLE IF EXISTS casfri_web.dst_latest;
CREATE TABLE casfri_web.dst_latest AS
    SELECT d.* FROM casfri50.dst_all d, casfri_post_processing.casfri_latest_unique l
    WHERE d.cas_id = l.cas_id;

CREATE INDEX dst_latest_casid_idx ON casfri_web.dst_latest USING btree(cas_id);
CREATE INDEX dst_latest_inventory_idx ON casfri_web.dst_latest USING btree(left(cas_id, 4));

DROP TABLE IF EXISTS casfri_web.eco_latest;
CREATE TABLE casfri_web.eco_latest AS
    SELECT e.* FROM casfri50.eco_all e, casfri_post_processing.casfri_latest_unique l
    WHERE e.cas_id = l.cas_id;

CREATE INDEX eco_latest_casid_idx ON casfri_web.eco_latest USING btree(cas_id);
CREATE INDEX eco_latest_inventory_idx ON casfri_web.eco_latest USING btree(left(cas_id, 4));

DROP TABLE IF EXISTS casfri_web.nfl_latest;
CREATE TABLE casfri_web.nfl_latest AS
    SELECT n.* FROM casfri50.nfl_all n, casfri_post_processing.casfri_latest_unique l
    WHERE n.cas_id = l.cas_id;

CREATE INDEX nfl_latest_casid_idx ON casfri_web.nfl_latest USING btree(cas_id);
CREATE INDEX nfl_latest_inventory_idx ON casfri_web.nfl_latest USING btree(left(cas_id, 4));

DROP TABLE IF EXISTS casfri_web.lyr_latest;
CREATE TABLE casfri_web.lyr_latest AS
    SELECT ly.* FROM casfri50.lyr_all ly, casfri_post_processing.casfri_latest_unique l
    WHERE ly.cas_id = l.cas_id;

CREATE INDEX lyr_latest_casid_idx ON casfri_web.lyr_latest USING btree(cas_id);
CREATE INDEX lyr_latest_inventory_idx ON casfri_web.lyr_latest USING btree(left(cas_id, 4));

DROP TABLE IF EXISTS casfri_web.geo_latest;
CREATE TABLE casfri_web.geo_latest AS
    SELECT g.* FROM casfri50.geo_all g, casfri_post_processing.casfri_latest_unique l
    WHERE g.cas_id = l.cas_id;

CREATE INDEX geo_latest_casid_idx ON casfri_web.geo_latest USING btree(cas_id);
CREATE INDEX geo_latest_inventory_idx ON casfri_web.geo_latest USING btree(left(cas_id, 4));
CREATE INDEX geo_latest_geom_idx ON casfri_web.geo_latest USING gist(geometry);