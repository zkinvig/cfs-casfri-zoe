<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>tree_type_plain</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="values">
              <sld:ColorMapEntry color="#d7d89e" quantity="1" label="1 (1 - 20)"/>
              <sld:ColorMapEntry color="#ebffbe" quantity="2" label="2 (21 - 40)"/>
              <sld:ColorMapEntry color="#eef8e9" quantity="3" label="3 (41 - 60)"/>
              <sld:ColorMapEntry color="#caeac0" quantity="4" label="4 (61 - 80)"/>
              <sld:ColorMapEntry color="#a6da9b" quantity="5" label="5 (81 - 100)"/>
              <sld:ColorMapEntry color="#7cc575" quantity="6" label="6 (101 - 120)"/>
              <sld:ColorMapEntry color="#50ac5c" quantity="7" label="7 (121 - 140)"/>
              <sld:ColorMapEntry color="#358c44" quantity="8" label="8 (141 - 250)"/>
              <sld:ColorMapEntry color="#165b31" quantity="9" label="9 (>= 251)"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
