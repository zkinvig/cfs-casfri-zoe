# Run script to generate a GeoTif of the dominant genus data product

#Initialize variables
source common.sh

DATE=$(date '+%Y-%m-%d')
SCHEMA="casfri_web"
TABLE="forest_species_genus"
ATTRIBUTE="genus"
ATT_SHORT="genus"
OUTPUT="dominant_genus"
OUTPUT_PATH="${RASTER_FILES_LOCATION}/${OUTPUT}_${DATE}"

mkdir -p $OUTPUT_PATH

echo "Creating raster from table in database"

gdal_rasterize \
-a $ATT_SHORT \
-sql "SELECT (CASE COALESCE(trim(lower($ATTRIBUTE)), 'null')
WHEN 'picea' THEN 1 
WHEN 'abies' THEN 2 
WHEN 'thuja' THEN 3 
WHEN 'pinus' THEN 4 
WHEN 'tsuga' THEN 5 
WHEN 'pseudotsuga' THEN 6 
WHEN 'populus' THEN 7 
WHEN 'acer' THEN 8 
WHEN 'betula' THEN 9 
WHEN 'larix' THEN 10 
WHEN 'null' THEN 11 
ELSE 12 END) AS $ATT_SHORT, geometry FROM $SCHEMA.$TABLE" \
-tr 30 30 \
-a_nodata 0 \
-ot Byte \
-of GTiff \
-co TILED=YES \
-co BLOCKXSIZE=128 \
-co BLOCKYSIZE=128 \
-co COMPRESS=LZW \
-co INTERLEAVE=BAND \
-co NBITS=4 \
-co TFW=YES \
"PG:dbname='$DB_NAME' host='$DB_HOST' user='$DB_USER' port='$DB_PORT' password='$DB_PASSWORD'" \
$OUTPUT_PATH/${OUTPUT}_unprojected.tif

echo "Reprojecting"

gdalwarp -t_srs ESRI:102001 $OUTPUT_PATH/${OUTPUT}_unprojected.tif $OUTPUT_PATH/${OUTPUT}.tif

echo "Adding pyramids"

gdaladdo \
$OUTPUT_PATH/${OUTPUT}.tif \
-ro 2 4 8 16 32 64 128 256

echo "Generating auxillary files"

gdalinfo ${OUTPUT_PATH}/${OUTPUT}.tif -mm -stats >/dev/null
gdalinfo ${OUTPUT_PATH}/${OUTPUT}.tif.ovr -mm -stats >/dev/null

echo "Copying style files"

cp styling_files/${OUTPUT}.qml ${OUTPUT_PATH}
cp styling_files/${OUTPUT}.sld ${OUTPUT_PATH}

echo "Zipping"

tar -czvf ${RASTER_FILES_LOCATION}/${OUTPUT}_${DATE}.tar.gz -C ${RASTER_FILES_LOCATION} ${OUTPUT}_${DATE}

