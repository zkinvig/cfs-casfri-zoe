--todo switch from casfri_web to post-processing
CREATE TABLE IF NOT EXISTS casfri_web.casfri_flat_all_latest AS (
  SELECT f.* from casfri50_flat.cas_flat_all_layers_same_row f, casfri_web.casfri_latest_unique u
  where f.cas_id = u.cas_id
  order by random()
);

COMMENT ON TABLE casfri_web.casfri_flat_all_latest IS
'The flat all latest table is a copy for casfri flat all containing only features from 2000-2022';

CREATE INDEX ON casfri_web.casfri_flat_all_latest USING gist(geom);
CREATE INDEX ON casfri_web.casfri_flat_all_latest USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_web.casfri_flat_all_latest USING btree(left(cas_id, 4));
