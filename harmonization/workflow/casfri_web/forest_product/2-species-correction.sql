update casfri_post_processing.casfri_forest_layer f set species_1 = lyr1_species_1b
from casfri50_test.qc04_product_species_translation_corrected_l1_fri t
where f.cas_id = t.cas_id and f.species_1 = 'TRANSLATION_ERROR';

update casfri_post_processing.casfri_forest_layer f set species_1 = lyr1_species_1b
from casfri50_test.qc04_product_species_translation_corrected_l1_fri t
where f.cas_id = t.cas_id and f.species_1 = 'TRANSLATION_ERROR';

update casfri_post_processing.casfri_forest_layer f set species_1 = lyr1_species_1b
from casfri50_test.nb02_product_species_translation_corrected_l1_fri t
where f.cas_id = t.cas_id and f.species_1 = 'TRANSLATION_ERROR';

update casfri_post_processing.casfri_forest_layer f set species_1 = lyr1_species_1b
from casfri50_test.nt04_product_species_translation_corrected_l1_fri t
where f.cas_id = t.cas_id and f.species_1 = 'NOT_APPLICABLE';

CREATE TABLE  casfri_web.forest_species_genus AS
SELECT
       f.cas_id,
       left(cas_id, 4) as inventory_id,
  	   f.species_1,
  	   f.species_per_1,
  	   f.species_2,
  	   f.species_per_2,
  	   f.species_3,
  	   f.species_per_3,
  	   f.species_4,
  	   f.species_per_4,
  	   f.species_5,
  	   f.species_per_5,
  	   f.crown_closure_upper,
  	   f.crown_closure_lower,
  	   f.height_upper,
  	   f.height_lower,
  	   f.productivity,
  	   f.productivity_type,
  	   f.soil_moist_reg,
  	   f.site_class,
  	   f.origin_upper,
  	   f.origin_lower,
       n.id,
       n.genus,
       n.species,
       n.ec_genus,
       n.species_type,
       n.gen_sp_susp,
       f.geometry
FROM casfri_post_processing.casfri_forest_layer f
LEFT JOIN casfri_post_processing.nfi_eco_species_list_enriched_unique_1 n
ON f.species_1 = n.gen_sp_susp
WHERE f.species_1 NOT IN  ('NULL_VALUE', 'NOT_APPLICABLE', 'EMPTY STRING', 'BLANK' , 'NOT_IN_SET', 'TRANSLATION_ERROR' , 'UNKNOWN_VALUE', 'UNKN_SPP');

COMMENT ON TABLE casfri_web.forest_species_genus IS
'The forest species/genus table is the CASFRI lyr table joined with a cut of geohistory 2000-2022 to display leading species and genus web views';
ALTER TABLE casfri_web.forest_species_genus
ADD CONSTRAINT forest_sp_genus_pk PRIMARY KEY (cas_id);

CREATE INDEX ON casfri_web.forest_species_genus USING btree(cas_id);
CREATE INDEX ON casfri_web.forest_species_genus USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_web.forest_species_genus USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_web.forest_species_genus USING gist(geometry);
CREATE INDEX fores_genus_idx ON casfri_web.forest_species_genus  USING btree(genus);
CREATE INDEX forest_species1_idx ON casfri_web.forest_species_genus  USING btree(species_1);
