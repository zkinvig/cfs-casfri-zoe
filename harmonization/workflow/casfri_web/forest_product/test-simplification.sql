DROP TABLE IF EXISTS casfri_web.vi_forest_species_genus_simplified1;
CREATE UNLOGGED TABLE casfri_web.vi_forest_species_genus_simplified1 AS
  SELECT cas_id,
  species_1,
  genus,
  ST_Simplify(geometry, 400) AS geometry
  FROM casfri_web.vi_forest_sp_genus;

CREATE INDEX ON casfri_web.vi_forest_species_genus_simplified1 USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_web.vi_forest_species_genus_simplified1 USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_web.vi_forest_species_genus_simplified1 USING gist(geom);
CREATE INDEX vi_forest_genus_simple_1_idx ON casfri_web.vi_forest_species_genus_simplified1  USING btree(genus);
CREATE INDEX vi_forest_species1_simple_1_idx ON casfri_web.vi_forest_species_genus_simplified1  USING btree(species_1);

DROP TABLE IF EXISTS casfri_web.vi_forest_species_genus_simplified2;
CREATE UNLOGGED TABLE casfri_web.vi_forest_species_genus_simplified2 AS
  SELECT cas_id,
  species_1
  genus,
  ST_Simplify(geometry, 1000) AS geometry
  FROM casfri_web.vi_forest_species_genus;

CREATE INDEX ON casfri_web.vi_forest_species_genus_simplified2 USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_web.vi_forest_species_genus_simplified2 USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_web.vi_forest_species_genus_simplified2 USING gist(geom);
CREATE INDEX vi_forest_genus_simple_2_idx ON casfri_web.vi_forest_species_genus_simplified2  USING btree(genus);
CREATE INDEX vi_forest_species1_simple_2_idx ON casfri_web.vi_forest_species_genus_simplified2  USING btree(species_1);

DROP TABLE IF EXISTS casfri_web.vi_forest_species_genus_simplified3;
CREATE UNLOGGED TABLE casfri_web.vi_forest_species_genus_simplified3 AS
  SELECT cas_id,
  species_1
  genus,
  ST_Simplify(geometry, 2000) AS geometry
  FROM casfri_web.vi_forest_species_genus;

CREATE INDEX ON casfri_web.vi_forest_species_genus_simplified3 USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_web.vi_forest_species_genus_simplified3 USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_web.vi_forest_species_genus_simplified3 USING gist(geom);
CREATE INDEX vi_fores_genus_simple_3_idx ON casfri_web.vi_forest_species_genus_simplified3  USING btree(genus);
CREATE INDEX vi_forest_species1_simple_3_idx ON casfri_web.vi_forest_species_genus_simplified3  USING btree(species_1);