CREATE TABLE casfri_post_processing.casfri_forest_layer AS
SELECT l.*, ST_Transform(w.geometry, 3978) AS geometry, casfri_web.getStandAge(l.cas_id, l.layer, l.origin_upper, 'casfri50', 'casfri_validation') AS stand_age
FROM  casfri50.lyr_all l
JOIN casfri_post_processing.casfri_latest_unique w
ON l.cas_id = w.cas_id
WHERE l.layer_rank IN (1, -8887)
ORDER BY random();

COMMENT ON TABLE casfri_post_processing.casfri_forest_layer IS
'The forest_layer table contains only features from the latest (2000-2030) geohistory table, with polygons merged back together since gh processing splits them.';

-- random order before geom indexing should speed up index
CREATE INDEX forest_layer_geom_idx ON casfri_post_processing.casfri_forest_layer USING gist(geometry);
CREATE INDEX forest_layer_casid_idx ON casfri_post_processing.casfri_forest_layer USING btree(cas_id);
CREATE INDEX forest_layer_prov_idx ON casfri_post_processing.casfri_forest_layer USING btree(left(cas_id, 2));
CREATE INDEX forest_layer_inv_idx ON casfri_post_processing.casfri_forest_layer USING btree(left(cas_id, 4));
