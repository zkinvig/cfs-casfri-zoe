CREATE TABLE casfri_web.species_genus_zoom3 as (
  WITH cas_id_30percentile as (
    select c.cas_id from casfri50.cas_all c, casfri_metrics.polygon_area_stats30 p
    where c.inventory_id = p.inventory_id and c.casfri_area >= p.avg
  )
  SELECT f.* from casfri_web.forest_species_genus f, cas_id_30percentile c
  WHERE f.cas_id = c.cas_id
);


COMMENT ON TABLE casfri_web.species_genus_zoom3 IS
'The forest species/genus table is the CASFRI lyr table joined with a cut of geohistory 2000-2022 to display leading species and genus web views zoom0-5';
ALTER TABLE casfri_web.species_genus_zoom5
ADD CONSTRAINT forest_sp_genus_zoom3_pk PRIMARY KEY (cas_id);

CREATE INDEX species_genus_zoom3_jur_idx ON casfri_web.species_genus_zoom3 USING btree(left(cas_id, 2));
CREATE INDEX species_genus_zoom3_inv_id_idx ON casfri_web.species_genus_zoom3 USING btree(left(cas_id, 4));
CREATE INDEX species_genus_zoom3_geom_idx ON casfri_web.species_genus_zoom3 USING gist(geometry);
CREATE INDEX forest_genus_zoom3_idx ON casfri_web.species_genus_zoom3  USING btree(genus);
CREATE INDEX forest_species1_zoom3_idx ON casfri_web.species_genus_zoom3  USING btree(species_1);
