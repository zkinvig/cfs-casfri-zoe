CREATE TABLE casfri_web.species_genus_zoom5 as (
  WITH cas_id_20percentile as (
    select c.cas_id from casfri50.cas_all c, casfri_metrics.polygon_area_stats20 p
    where c.inventory_id = p.inventory_id and c.casfri_area >= p.avg
  )
  SELECT f.* from casfri_web.forest_species_genus f, cas_id_20percentile c
  WHERE f.cas_id = c.cas_id
);


COMMENT ON TABLE casfri_web.species_genus_zoom5 IS
'The forest species/genus table is the CASFRI lyr table joined with a cut of geohistory 2000-2022 to display leading species and genus web views zoom0-5';
ALTER TABLE casfri_web.species_genus_zoom5
ADD CONSTRAINT forest_sp_genus_pk PRIMARY KEY (cas_id);

CREATE INDEX forest_species_genus_zoom5_jur_idx ON casfri_web.species_genus_zoom5 USING btree(left(cas_id, 2));
CREATE INDEX forest_species_genus_zoom5_inv_id_idx ON casfri_web.species_genus_zoom5 USING btree(left(cas_id, 4));
CREATE INDEX forest_species_genus_zoom5_geom_idx ON casfri_web.species_genus_zoom5 USING gist(geometry);
CREATE INDEX forest_genus_zoom5_idx ON casfri_web.species_genus_zoom5  USING btree(genus);
CREATE INDEX forest_species1_zoom5_idx ON casfri_web.species_genus_zoom5  USING btree(species_1);
