<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <!-- a named layer is the basic building block of an sld document -->


  <NamedLayer>
    <Name>casfri_flat_all</Name>
    <UserStyle>

      <FeatureTypeStyle>
        <Rule>
          <Name>Non-forested</Name>
          <ogc:Filter>
            <ogc:Or>
              <ogc:PropertyIsNull>
                <ogc:PropertyName>lyr1_species_1</ogc:PropertyName>
              </ogc:PropertyIsNull>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>lyr1_species_1</ogc:PropertyName>
                <ogc:Literal>NOT_APPLICABLE</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:Or>
          </ogc:Filter>

          <MaxScaleDenominator>99999</MaxScaleDenominator>

          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#EEEEEE</CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#FFFFFF</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>


        <Rule>
          <Name>Forested_CC_25</Name>

          <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsNotEqualTo>
                <ogc:PropertyName>lyr1_species_1</ogc:PropertyName>
                <ogc:Literal>NOT_APPLICABLE</ogc:Literal>
              </ogc:PropertyIsNotEqualTo>
              <ogc:PropertyIsLessThan>
                <ogc:PropertyName>lyr1_crown_closure_upper</ogc:PropertyName>
                <ogc:Literal>25</ogc:Literal>
              </ogc:PropertyIsLessThan>
            </ogc:And>
          </ogc:Filter>
       
          <MaxScaleDenominator>99999</MaxScaleDenominator>

          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#e5ebaf</CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#e5ebaf</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
	    </Stroke>
          </PolygonSymbolizer>
        </Rule>

        <Rule>
          <Name>Forested_CC_50</Name>

          <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsNotEqualTo>
                <ogc:PropertyName>lyr1_species_1</ogc:PropertyName>
                <ogc:Literal>NOT_APPLICABLE</ogc:Literal>
              </ogc:PropertyIsNotEqualTo>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>lyr1_crown_closure_upper</ogc:PropertyName>
                <ogc:Literal>25</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsLessThan>
                <ogc:PropertyName>lyr1_crown_closure_upper</ogc:PropertyName>
                <ogc:Literal>50</ogc:Literal>
              </ogc:PropertyIsLessThan>
            </ogc:And>
          </ogc:Filter>
       
          <MaxScaleDenominator>99999</MaxScaleDenominator>

          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#c2fdae</CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#c2fdae</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
	    </Stroke>
          </PolygonSymbolizer>
        </Rule>

        <Rule>
          <Name>Forested_CC_75</Name>

          <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsNotEqualTo>
                <ogc:PropertyName>lyr1_species_1</ogc:PropertyName>
                <ogc:Literal>NOT_APPLICABLE</ogc:Literal>
              </ogc:PropertyIsNotEqualTo>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>lyr1_crown_closure_upper</ogc:PropertyName>
                <ogc:Literal>50</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsLessThan>
                <ogc:PropertyName>lyr1_crown_closure_upper</ogc:PropertyName>
                <ogc:Literal>75</ogc:Literal>
              </ogc:PropertyIsLessThan>
            </ogc:And>
          </ogc:Filter>
       
          <MaxScaleDenominator>99999</MaxScaleDenominator>

          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#7ace19</CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#7ace19</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
	    </Stroke>
          </PolygonSymbolizer>
        </Rule>

        <Rule>
          <Name>Forested_CC_100</Name>

          <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsNotEqualTo>
                <ogc:PropertyName>lyr1_species_1</ogc:PropertyName>
                <ogc:Literal>NOT_APPLICABLE</ogc:Literal>
              </ogc:PropertyIsNotEqualTo>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>lyr1_crown_closure_upper</ogc:PropertyName>
                <ogc:Literal>75</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>lyr1_crown_closure_upper</ogc:PropertyName>
                <ogc:Literal>100</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
       
          <MaxScaleDenominator>99999</MaxScaleDenominator>

          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#008302</CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#008302</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
	    </Stroke>
          </PolygonSymbolizer>
        </Rule>

       <Rule>
          <Name>Forested_labels</Name>

          <ogc:Filter>
              <ogc:PropertyIsNotEqualTo>
                <ogc:PropertyName>lyr1_species_1</ogc:PropertyName>
                <ogc:Literal>NOT_APPLICABLE</ogc:Literal>
              </ogc:PropertyIsNotEqualTo>
          </ogc:Filter>
       
          <MaxScaleDenominator>19999</MaxScaleDenominator>

          <TextSymbolizer>
            <Label>
              <ogc:PropertyName>lyr1_species_1</ogc:PropertyName>
            </Label>
            <Font>
              <CssParameter name="font-family">Arial</CssParameter>
              <CssParameter name="font-size">11</CssParameter>
              <CssParameter name="font-style">normal</CssParameter>
              <CssParameter name="font-weight">bold</CssParameter>
            </Font>
            <LabelPlacement>
              <PointPlacement>
                <AnchorPoint>
                  <AnchorPointX>0.5</AnchorPointX>
                  <AnchorPointY>0.5</AnchorPointY>
                </AnchorPoint>
              </PointPlacement>
            </LabelPlacement>
            <Fill>
              <CssParameter name="fill">#000000</CssParameter>
            </Fill>
            <VendorOption name="autoWrap">60</VendorOption>
            <VendorOption name="maxDisplacement">150</VendorOption>
          </TextSymbolizer>
  
        </Rule>


      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>

